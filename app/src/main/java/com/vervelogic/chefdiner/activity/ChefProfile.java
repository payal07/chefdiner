package com.vervelogic.chefdiner.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.squareup.picasso.Picasso;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;
import com.stacktips.view.DayDecorator;
import com.stacktips.view.DayView;
import com.stacktips.view.utils.CalendarUtils;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.adapter.InstagramAdapter;
import com.vervelogic.chefdiner.adapter.ProfileMenuAdapter;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.ChooseSubMenuModel;
import com.vervelogic.chefdiner.model.GetDateModel;
import com.vervelogic.chefdiner.model.InstaModel;
import com.vervelogic.chefdiner.model.ProfileMenuModel;
import com.vervelogic.chefdiner.utils.SessionManager;
import com.vervelogic.chefdiner.utils.SharedPreference;
import com.vervelogic.chefdiner.utils.WorkaroundMapFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChefProfile extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    private static final String TAG = ChefProfile.class.getSimpleName();
    /*-------------------------------Declarations---------------------------------------------------*/
    Toolbar toolbar;
    RecyclerView menuRecyclerView, instagramRecyclerView;
    ProfileMenuAdapter profileMenuAdapter;

    List<ProfileMenuModel> profileMenuModels = new ArrayList<>();
    List<InstaModel> instaModels = new ArrayList<>();
    InstagramAdapter instagramAdapter;
    ImageView wishlist_image;
    LinearLayout wishlist_ly;
    int favAdd = 0;
    String favorite;
    Activity context = this;
    String user_id = "";
    NestedScrollView nestedScrollView;
    String language;
    CustomCalendarView calendarView;
    SimpleRatingBar avgRatingChefProfile;

    DbHelper dbHelper;
    SessionManager session;
    Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());
    Button enquiry, message, bookNow;
    CircleImageView profileImage, chef_image;
    TextView avg_price, bookmarked_count, imageCount, chef_kitchen_title, chef_name, city, kitchen_descText, kitchen_desc_viewMore,
            chef_address_profile, minimum_person, maximum_person, house_rules, language_known, kitchen_titleText,
            address, about_chef, viewMore_aboutChef, lunch, brunch, dinner, cuisine_type_profile, rate_count_profile, review_text,
            tongue_out_count, happy_count, sad_count, insta_text, seat_available;
    ImageView image1, image2, image3;
    ImageView food_delivery_image, home_private_meal_profile_image, daily_meal_profile_image,
            cooking_workshop_profile_image, restaurant_private_meal_profile_image, chef_onsite_cooking_profile_image, share_button;
    /*--------------------------------------------chef details----------------------------------------------------------*/
    String chef_id, chef_fname, chef_lname, chef_address, profile_pic = "", imageUrl1 = "", imageUrl2 = "", imageUrl3 = "", kitchen_title, kitchen_desc,
            fav, service_type = "", from_guest, upto_guest, languageSpeak, priceStr, chefLatitude = "", whatsHotKey = "",
            chefLongitude = "", allImages, lunchStr, brunchStr, dinnerStr, currencyStr, cuisine = "", access_token = "",
            countryChef = "", maxprice = "", keyChef = "";
    int imageLength;
    ChooseSubMenuModel googleLoginModel;
    LinearLayout rating_ly;
    /*--------------------Calander-----------------------------------------*/
    List<String> GetchefCalendarBean = new ArrayList<>();
    List<String> dateAvail = new ArrayList<>();
    List<GetDateModel.GetchefCalendarBean> getchefCalendarBeans = new ArrayList<>();
    String de = "";
    private SharedPreference sharedPreference;
    /*------------------------------------Google Map--------------------------*/
    private GoogleMap mMap;

    @SuppressLint({"SetTextI18n", "LongLogTag"})
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chef_profile);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        language = intent.getStringExtra("language");
        whatsHotKey = intent.getStringExtra("whatsHotKey");
        keyChef = intent.getStringExtra("keyChef");

        dbHelper = new DbHelper(getApplicationContext());

        //--------------------------------------------- Session Manager--------------------------------
        session = new SessionManager(getApplicationContext());

        /*------------------------------------------------IDs------------------------------*/
        menuRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_menu_profile);
        instagramRecyclerView = (RecyclerView) findViewById(R.id.insta_photos_recycler);
        wishlist_image = (ImageView) findViewById(R.id.ic_wishlist_image);
        wishlist_ly = (LinearLayout) findViewById(R.id.wishlist_ly);
        nestedScrollView = (NestedScrollView) findViewById(R.id.sv_container);
        enquiry = (Button) findViewById(R.id.enquiry);
        message = (Button) findViewById(R.id.message);
        bookNow = (Button) findViewById(R.id.book_now);
        calendarView = (CustomCalendarView) findViewById(R.id.calendarView);
        rating_ly = (LinearLayout) findViewById(R.id.rating_ly);

        profileImage = (CircleImageView) findViewById(R.id.profile_image);
        chef_image = (CircleImageView) findViewById(R.id.chef_image);
        image1 = (ImageView) findViewById(R.id.image1_profile);
        image2 = (ImageView) findViewById(R.id.image2_profile);
        image3 = (ImageView) findViewById(R.id.image3_profile);
        avgRatingChefProfile = (SimpleRatingBar) findViewById(R.id.avgRatingChefProfile);

        avg_price = (TextView) findViewById(R.id.avg_price);
        seat_available = (TextView) findViewById(R.id.seat_available);
        rate_count_profile = (TextView) findViewById(R.id.rate_count_profile);
        review_text = (TextView) findViewById(R.id.review_text);
        tongue_out_count = (TextView) findViewById(R.id.tongue_out_count);
        happy_count = (TextView) findViewById(R.id.happy_count);
        bookmarked_count = (TextView) findViewById(R.id.bookmarked_count);
        imageCount = (TextView) findViewById(R.id.food_style1);
        sad_count = (TextView) findViewById(R.id.sad_count);
        chef_kitchen_title = (TextView) findViewById(R.id.chef_kitchen_title);
        chef_name = (TextView) findViewById(R.id.chef_name);
        city = (TextView) findViewById(R.id.city);
        kitchen_descText = (TextView) findViewById(R.id.kitchen_desc);
        kitchen_desc_viewMore = (TextView) findViewById(R.id.kitchen_desc_viewMore);
        chef_address_profile = (TextView) findViewById(R.id.chef_address_profile);
        minimum_person = (TextView) findViewById(R.id.minimum_person);
        maximum_person = (TextView) findViewById(R.id.maximum_person);
        house_rules = (TextView) findViewById(R.id.house_rules);
        language_known = (TextView) findViewById(R.id.language_known);
        kitchen_titleText = (TextView) findViewById(R.id.kitchen_title);
        address = (TextView) findViewById(R.id.address);
        about_chef = (TextView) findViewById(R.id.about_chef);
        viewMore_aboutChef = (TextView) findViewById(R.id.viewMore_aboutChef);
        share_button = (ImageView) findViewById(R.id.share_button);
        lunch = (TextView) findViewById(R.id.lunch);
        brunch = (TextView) findViewById(R.id.brunch);
        dinner = (TextView) findViewById(R.id.dinner);
        cuisine_type_profile = (TextView) findViewById(R.id.cuisine_type_profile);
        insta_text = (TextView) findViewById(R.id.insta_text);

        food_delivery_image = (ImageView) findViewById(R.id.food_delivery_image);
        home_private_meal_profile_image = (ImageView) findViewById(R.id.home_private_meal_profile_image);
        daily_meal_profile_image = (ImageView) findViewById(R.id.daily_meal_profile_image);
        cooking_workshop_profile_image = (ImageView) findViewById(R.id.cooking_workshop_profile_image);
        restaurant_private_meal_profile_image = (ImageView) findViewById(R.id.restaurant_private_meal_profile_image);
        chef_onsite_cooking_profile_image = (ImageView) findViewById(R.id.chef_onsite_cooking_profile_image);

        /*------------ Obtain the SupportMapFragment and get notified when the map is ready to be used.-----------*/
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                nestedScrollView.requestDisallowInterceptTouchEvent(true);
            }
        });

        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            user_id = sharedPreference.getUserId(context);
        } else {
            user_id = "";
        }

        /*calendarView.setSelectionColor(getResources().getColor(R.color.button_color));
        calendarView.state().edit()
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setMinimumDate(Calendar.getInstance())
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();
        List<DayDecorator> decorators = new ArrayList<>();
       // decorators.add(new ColorDecorator());
        *//*calendarView.addDecorator(new ColorDecorator());*//*

        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                   *//* int color = getResources().getColor(R.color.blue);
                   calendarView.setba(color);*//*
                    Toast.makeText(ChefProfile.this, df.format(materialCalendarView.getSelectedDate().getDate()), Toast.LENGTH_SHORT).show();


            }
        });*/

        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);



        /*//Setting custom font
        final Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Arch_Rival_Bold.ttf");
        if (null != typeface) {
            calendarView.setCustomTypeface(typeface);
            calendarView.refreshCalendar(currentCalendar);
        }*/

        //adding calendar day decorators
        List<DayDecorator> decorators = new ArrayList<>();
        decorators.add(new ColorDecorator());
        calendarView.setDecorators(decorators);
        calendarView.refreshCalendar(currentCalendar);

        /*------------------------------------------------chef data-----------------------------------------------------------*/

        chef_id = intent.getStringExtra("Chef_id");
        //maxprice = intent.getStringExtra("maxprice");

        Log.e("whatsHotKeyChefProfileIntent", "" + whatsHotKey);

        Cursor cursor = dbHelper.getChefDetails(Integer.valueOf(chef_id));
        if (cursor.moveToFirst()) {
            chef_fname = cursor.getString(1);
            chef_lname = cursor.getString(2);
            chef_address = cursor.getString(12);
            profile_pic = cursor.getString(7);
            kitchen_title = cursor.getString(19);
            kitchen_desc = cursor.getString(20);
            service_type = cursor.getString(15);
            from_guest = cursor.getString(13);
            upto_guest = cursor.getString(14);
            languageSpeak = cursor.getString(9);
            lunchStr = cursor.getString(23);
            brunchStr = cursor.getString(24);
            dinnerStr = cursor.getString(25);
            currencyStr = cursor.getString(26);
            allImages = cursor.getString(36);
            priceStr = cursor.getString(35);
            chefLatitude = cursor.getString(21);
            chefLongitude = cursor.getString(22);
            cuisine = cursor.getString(27);
            access_token = cursor.getString(32);
            fav = cursor.getString(34);
            countryChef = cursor.getString(11);
            rate_count_profile.setText(cursor.getString(40));
            review_text.setText(cursor.getString(41));
            tongue_out_count.setText(cursor.getString(38));
            happy_count.setText(cursor.getString(37));
            sad_count.setText(cursor.getString(39));
            avgRatingChefProfile.setRating(Float.parseFloat(cursor.getString(40)));
            maxprice = cursor.getString(42);
        }
        if (!TextUtils.isEmpty(access_token)) {
            AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
            asyncTaskRunner.execute();
        } else {
            insta_text.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(allImages)) {
            List<String> imageList = Arrays.asList(allImages.split("\\|"));
            ArrayList<String> listView = new ArrayList<>();

            for (int j = 0; j < imageList.size(); j++) {
                if (!imageList.get(j).equals("")) {
                    listView.add(imageList.get(j));
                }
            }
            if (!listView.isEmpty()) {
                imageUrl1 = listView.get(0);
                imageUrl2 = listView.get(1);
                imageUrl3 = listView.get(2);
            }
            imageLength = listView.size();
        }
        Log.e("chefLatitude", "" + chefLatitude);
        Log.e("chefLongitude", "" + chefLongitude);
        getSupportActionBar().setTitle(kitchen_title);
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new android.support.v7.app.AlertDialog.Builder(ChefProfile.this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            wishListAPI();
            getCalDateAPI();
        }

        if (!TextUtils.isEmpty(keyChef)) {
            if (keyChef.equalsIgnoreCase("Home")) {

                message.setVisibility(View.VISIBLE);
                enquiry.setVisibility(View.VISIBLE);
                bookNow.setVisibility(View.VISIBLE);
                 /*------------------------------------Listeners-----------------------------*/
                wishlist_ly.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (!TextUtils.isEmpty(user_id)) {
                            if (favAdd == 0) {
                                Log.e("Fav", "" + favAdd);
                                wishlist_image.setImageResource(R.drawable.ic_heart);
                                favorite = "0";
                                favAdd = 1;
                                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    new android.support.v7.app.AlertDialog.Builder(ChefProfile.this)
                                            .setTitle(getResources().getString(R.string.app_name))
                                            .setMessage(getResources().getString(R.string.internet_error))
                                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                } else {
                                    favoriteAPI(favorite);
                                }

                            } else {
                                Log.e("Fav", "" + favAdd);
                                wishlist_image.setImageResource(R.drawable.ic_heart_red);
                                favorite = "1";
                                favAdd = 0;
                                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    new android.support.v7.app.AlertDialog.Builder(ChefProfile.this)
                                            .setTitle(getResources().getString(R.string.app_name))
                                            .setMessage(getResources().getString(R.string.internet_error))
                                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                } else {
                                    favoriteAPI(favorite);
                                }
                            }
                        } else {
                            Intent intent = new Intent(getApplicationContext(), RegisterSignUpScreen.class);
                            intent.putExtra("language", language);
                            intent.putExtra("type", "ChefProfile");
                            intent.putExtra("Chef_id", chef_id);
                            startActivity(intent);
                        }
                    }
                });
            }
            if (keyChef.equalsIgnoreCase("preview")) {
                wishlist_ly.setClickable(false);
                wishlist_image.setClickable(false);
                message.setVisibility(View.GONE);
                enquiry.setVisibility(View.GONE);
                bookNow.setVisibility(View.GONE);
            }
        }



        /*----------------------------------------------------Populating data--------------------------------------------------------*/
        avg_price.setText(currencyStr + " " + priceStr + "-" + maxprice);
        bookmarked_count.setText(fav);
        chef_kitchen_title.setText(kitchen_title);
        chef_name.setText(chef_fname + " " + chef_lname);
        city.setText(chef_address);
        kitchen_descText.setText(kitchen_desc);
        chef_address_profile.setText(chef_address + "," + countryChef);
        minimum_person.setText(getString(R.string.minimum) + " " + from_guest);
        maximum_person.setText(getString(R.string.maximum) + " " + upto_guest);
        house_rules.setText(kitchen_desc);
        language_known.setText(languageSpeak);
        kitchen_titleText.setText(kitchen_title);
        address.setText(chef_address + "," + countryChef);
        about_chef.setText(kitchen_desc);
        imageCount.setText(imageLength + getString(R.string.more));
        lunch.setText(getString(R.string.lunch) + "   - " + lunchStr);
        brunch.setText(getString(R.string.brunch) + "  - " + brunchStr);
        dinner.setText(getString(R.string.dinner) + "  - " + dinnerStr);
        if (!TextUtils.isEmpty(cuisine)) {
            cuisine_type_profile.setText(cuisine);
        }
        if (!TextUtils.isEmpty(profile_pic)) {
            Picasso.with(getApplicationContext())
                    .load(profile_pic).fit()
                /*    .error(R.drawable.profile)*/
                    .into(profileImage);
            Picasso.with(getApplicationContext())
                    .load(profile_pic).fit()
                   /* .error(R.drawable.profile)*/
                    .into(chef_image);
        }
        if (!TextUtils.isEmpty(imageUrl1)) {
            Picasso.with(getApplicationContext())
                    .load(imageUrl1).fit()
//                    .error(R.drawable.people)
                    .into(image1);
        }
        if (!TextUtils.isEmpty(imageUrl2)) {
            Picasso.with(getApplicationContext())
                    .load(imageUrl2).resize(400, 0)
//                    .error(R.drawable.food)
                    .into(image2);
        }
        if (!TextUtils.isEmpty(imageUrl3)) {
            Picasso.with(getApplicationContext())
                    .load(imageUrl3).resize(400, 0)
                  /*  .error(R.drawable.dessert)*/
                    .into(image3);
        }

        if (!TextUtils.isEmpty(service_type)) {
            List<String> items = Arrays.asList(service_type.split("\\s*,\\s*"));
            for (int i = 0; i < items.size(); i++) {
                String stringList = items.get(i);
                if (stringList.equalsIgnoreCase("Food delivery")) {
                    food_delivery_image.setImageResource(R.drawable.ic_check_square);
                } else if (stringList.equalsIgnoreCase("Home private meal")) {
                    home_private_meal_profile_image.setImageResource(R.drawable.ic_check_square);
                } else if (stringList.equalsIgnoreCase("Restaurant private meal")) {
                    restaurant_private_meal_profile_image.setImageResource(R.drawable.ic_check_square);
                } else if (stringList.equalsIgnoreCase("Chef onsite cooking")) {
                    chef_onsite_cooking_profile_image.setImageResource(R.drawable.ic_check_square);
                } else if (stringList.equalsIgnoreCase("Cooking workshop")) {
                    cooking_workshop_profile_image.setImageResource(R.drawable.ic_check_square);
                } else if (stringList.equalsIgnoreCase("Daily meal")) {
                    daily_meal_profile_image.setImageResource(R.drawable.ic_check_square);
                }

            }
        }


       /*------------------------------------ Menu Recycler View----------------------------------------------------*/

        setMenu();

        enquiry.setOnClickListener(this);
        message.setOnClickListener(this);
        bookNow.setOnClickListener(this);

        imageCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getApplicationContext(), GridImagesChef.class);
                intent1.putExtra("allImages", allImages);
                intent1.putExtra("imageCount", imageLength);
                startActivity(intent1);
            }
        });

        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                    String sAux = "\nLet me recommend you this application\n\n";
                    sAux = sAux + "https://play.google.com/store/apps/details?id=chefDiner/" + kitchen_title + "\n\n";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch (Exception e) {
                    //e.toString();
                }
            }
        });

        rating_ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cal = new Intent(getApplicationContext(), DetailedReviewScreen.class);
                cal.putExtra("Chef_id", chef_id);
                startActivity(cal);
            }
        });

    }

    public void fetchImage(final String mAccessToken) {
        new Thread() {
            @Override
            public void run() {
                super.run();
                URL example = null;
                try {
                    example = new URL("https://api.instagram.com/v1/users/self/media/recent?access_token="
                            + mAccessToken);

                    URLConnection tc = example.openConnection();
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            tc.getInputStream()));

                    String line;
                    while ((line = in.readLine()) != null) {
                        JSONObject ob = new JSONObject(line);
                        JSONArray object = ob.getJSONArray("data");
                        Log.e("Obj length", "" + object);
                        for (int i = 0; i < object.length(); i++) {
                            JSONObject jo = (JSONObject) object.get(i);
                            JSONObject nja = jo.getJSONObject("images");
                            JSONObject purl3 = nja
                                    .getJSONObject("thumbnail");
                            Log.e("Chef", "" + purl3.getString("url"));
                            InstaModel instaModel = new InstaModel();
                            instaModel.setUri(purl3.getString("url"));
                            instaModels.add(instaModel);
                              /*-------------------------------------Instagram Recycler View---------------------------------------------------*/
                            LinearLayoutManager horizontalLayoutManagaer1
                                    = new LinearLayoutManager(ChefProfile.this, LinearLayoutManager.HORIZONTAL, false);
                            instagramRecyclerView.setLayoutManager(horizontalLayoutManagaer1);
                            instagramAdapter = new InstagramAdapter(getApplicationContext(), instaModels);
                            instagramRecyclerView.setAdapter(instagramAdapter);
                        }
                        instagramAdapter.notifyDataSetChanged();
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (JSONException e1) {
                    e1.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }.start();

    }

    private void setMenu() {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new android.support.v7.app.AlertDialog.Builder(ChefProfile.this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            getMenuChefProfile();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
       /* Intent intent = new Intent(getApplicationContext(), HomePage.class);
        startActivity(intent);*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.enquiry:
                if (session.isLoggedIn()) {
                    dbHelper.deleteBooking();
                    /*Log.e("De", "" + de);
                    if (!TextUtils.isEmpty(de)) {*/
                    Intent intent = new Intent(getApplicationContext(), RequestABook.class);
                    intent.putExtra("language", language);
                    intent.putExtra("reservationType", "0");
                    intent.putExtra("chef_id", chef_id);
                    intent.putExtra("date", de);
                    intent.putExtra("minimum_person", from_guest);
                    intent.putExtra("maximum_person", upto_guest);
                    startActivity(intent);
                   /* } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.select_another_date), Toast.LENGTH_SHORT).show();
                    }*/
                } else {
                    Intent intent = new Intent(getApplicationContext(), RegisterSignUpScreen.class);
                    intent.putExtra("language", language);
                    intent.putExtra("type", "ChefProfile");
                    intent.putExtra("Chef_id", chef_id);
                    startActivity(intent);
                }
                break;
            case R.id.message:
                if (session.isLoggedIn()) {
                    dbHelper.deleteBooking();
                    /*Log.e("De", "" + de);
                    if (!TextUtils.isEmpty(de)) {*/
                    Intent intent = new Intent(getApplicationContext(), RequestABook.class);
                    intent.putExtra("language", language);
                    intent.putExtra("reservationType", "0");
                    intent.putExtra("chef_id", chef_id);
                    intent.putExtra("date", de);
                    intent.putExtra("minimum_person", from_guest);
                    intent.putExtra("maximum_person", upto_guest);
                    startActivity(intent);
                   /* } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.select_another_date), Toast.LENGTH_SHORT).show();
                    }*/
                } else {
                    Intent intent = new Intent(getApplicationContext(), RegisterSignUpScreen.class);
                    intent.putExtra("language", language);
                    intent.putExtra("type", "ChefProfile");
                    intent.putExtra("Chef_id", chef_id);
                    startActivity(intent);
                }
                break;
            case R.id.book_now:
                if (session.isLoggedIn()) {
                    dbHelper.deleteBooking();
                    /*Log.e("De", "" + de);
                    if (!TextUtils.isEmpty(de)) {*/
                    Intent intent = new Intent(getApplicationContext(), RequestABook.class);
                    intent.putExtra("language", language);
                    intent.putExtra("reservationType", "1");
                    intent.putExtra("chef_id", chef_id);
                    intent.putExtra("date", de);
                    intent.putExtra("minimum_person", from_guest);
                    intent.putExtra("maximum_person", upto_guest);
                    startActivity(intent);
                   /* } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.select_another_date), Toast.LENGTH_SHORT).show();
                    }*/
                } else {
                    Intent intent = new Intent(getApplicationContext(), RegisterSignUpScreen.class);
                    intent.putExtra("language", language);
                    intent.putExtra("type", "ChefProfile");
                    intent.putExtra("Chef_id", chef_id);
                    startActivity(intent);
                }

                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style);
        mMap.setMapStyle(style);
        mMap.getUiSettings().setZoomControlsEnabled(false);

        if (!TextUtils.isEmpty(chefLatitude) && !TextUtils.isEmpty(chefLongitude)) {
            Double lat = Double.valueOf(chefLatitude);
            Double lon = Double.valueOf(chefLongitude);
            LatLng loc = new LatLng(lat, lon);
            // Add a marker in Sydney and move the camera
            // create marker
            MarkerOptions marker = new MarkerOptions().position(loc).title(kitchen_title);

            marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker_chef_profile));
            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(lat, lon)).zoom(10).build();

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.addMarker(marker);

        } else {
            Double lat = 26.911974;
            Double lon = 75.789882;
          /*  LatLng loc = new LatLng(lat, lon);
            // Add a marker in Sydney and move the camera
            MarkerOptions marker = new MarkerOptions().position(loc).title(kitchen_title);

            marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker_chef_profile));
            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(lat, lon)).zoom(10).build();

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.addMarker(marker);*/
        }


    }

    /*-----------------------------------Favorite API--------------------------------------------*/
    public void favoriteAPI(final String favorite) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.FAVORITE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            Log.e(TAG, response);
                           /* wishListAPI();*/
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();

                map.put("user_id", user_id);
                map.put("chef_id", chef_id);
                map.put("favourite", favorite);

                Log.e("Data", "" + map);

                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /*-----------------------------------------Wishlist count------------------------------------*/
    public void wishListAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.WISHLIST_URL + "?user_id=%1$s", user_id + "&chef_id=" + chef_id);
        Log.e("Url", "" + uri);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            favAdd = jsonObject.getInt("status");
                            if (favAdd == 1) {

                                Log.e(TAG, "Status" + favAdd);
                                wishlist_image.setImageResource(R.drawable.ic_heart_red);
                                favAdd = 0;

                            } else if (favAdd == 0) {

                                Log.e("Response When 0", "Status" + favAdd);
                                wishlist_image.setImageResource(R.drawable.ic_heart);
                                favAdd = 1;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getApplicationContext() != null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e(TAG, "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /*----------------------------------Get Chef's Menu------------------------------------------*/
    public void getMenuChefProfile() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.GET_MENU_CHEFPROFILE_URL + "?chef_id=%1$s", chef_id);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.GET_MENU_SUBMENU_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        try {
                            Log.e(TAG, "" + s);
                            googleLoginModel = new Gson().fromJson(s, ChooseSubMenuModel.class);

                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {

                                LinearLayoutManager horizontalLayoutManager
                                        = new LinearLayoutManager(ChefProfile.this, LinearLayoutManager.VERTICAL, false);
                                menuRecyclerView.setLayoutManager(horizontalLayoutManager);

                                /*for (int i=0;i<googleLoginModel.getMenusSubmenus().size();i++)
                                {
                                    googleLoginModel.setMenusSubmenus();
                                }
*/
                                menuRecyclerView.setItemAnimator(new DefaultItemAnimator());
                                profileMenuAdapter = new ProfileMenuAdapter(ChefProfile.this, googleLoginModel.getMenusSubmenus(), whatsHotKey);
                                menuRecyclerView.setAdapter(profileMenuAdapter);

                                profileMenuAdapter.notifyDataSetChanged();
                             /*HorizontalChooseMenuAdapter   horizontalChooseMenuAdapter = new HorizontalChooseMenuAdapter(getApplicationContext(), googleLoginModel.getMenusSubmenus());
                                menuRecyclerView.setAdapter(horizontalChooseMenuAdapter);*/

                               /* horizontalChooseMenuAdapter.notifyDataSetChanged();*/
                           /* googleLoginModel = new Gson().fromJson(s, ChooseSubMenuModel.class);
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {*/
                               /* JSONArray jsonArray = jsonObject.getJSONArray("ChefmenuList");*/
                               /* for (int i = 0; i < jsonArray.length(); i++) {*/
                                   /* String menu_title = jsonArray.getJSONObject(i).getString("menu_title");
                                    String menu_image = jsonArray.getJSONObject(i).getString("menu_image");
                                    String price_per_person = jsonArray.getJSONObject(i).getString("price_per_person");
                                    String currency = jsonArray.getJSONObject(i).getString("currency");
                                    String whatshot = jsonArray.getJSONObject(i).getString("whatshot");*/

                                    /*ProfileMenuModel profileMenuModel = new ProfileMenuModel();
                                    profileMenuModel.setDishName(menu_title);
                                    profileMenuModel.setWhatsHotKey(whatsHotKey);
                                    profileMenuModel.setUrl(menu_image);
                                    profileMenuModel.setPerGuest(currency + price_per_person);
                                    profileMenuModel.setWhatshot(whatshot);*/
                                  /*  Log.e("whatsHotKeyChefProfile", "" + profileMenuModel.getWhatsHotKey());
                                    profileMenuModels.add(profileMenuModel);*/
                                     /*------------------------------------ Menu Recycler View----------------------------------------------------*/
                              /*  Log.e("S", "" + s);
                                     LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(ChefProfile.this, LinearLayoutManager.VERTICAL, false);
                                menuRecyclerView.setLayoutManager(horizontalLayoutManagaer);

                                menuRecyclerView.setItemAnimator(new DefaultItemAnimator());

                           *//*     horizontalChooseMenuAdapter = new HorizontalChooseMenuAdapter(getApplicationContext(), googleLoginModel.getMenusSubmenus());
                                recyclerView.setAdapter(horizontalChooseMenuAdapter);

                                horizontalChooseMenuAdapter.notifyDataSetChanged();*//*
                                profileMenuAdapter = new ProfileMenuAdapter(getApplicationContext(), googleLoginModel.getMenusSubmenus());
                                menuRecyclerView.setAdapter(profileMenuAdapter);

                                profileMenuAdapter.notifyDataSetChanged();*/

/*
                                profileMenuAdapter = new ProfileMenuAdapter(getApplicationContext(), profileMenuModels);
                                menuRecyclerView.setAdapter(profileMenuAdapter);*/
                                //  profileMenuAdapter.notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //  String image = getStringImage(bitmap);
                map.put("user_id", chef_id);
                map.put("main_user_id", user_id);

                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /*----------------------------------Get Calendar API-----------------------------------------*/
    public void getCalDateAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.CALENDAR_DETAILS_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Class", "" + s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            GetDateModel paymentModel = new Gson().fromJson(s, GetDateModel.class);
                            if (status == 1) {

                                Log.e("D", "" + s);
                                for (int i = 0; i < paymentModel.getGetchefCalendar().size(); i++) {
                                    getchefCalendarBeans.add(paymentModel.getGetchefCalendar().get(i));
                                    GetchefCalendarBean.add(paymentModel.getGetchefCalendar().get(i).getDate());
                                    dateAvail.add(paymentModel.getGetchefCalendar().get(i).getAva_status());
                                    calendarView.refreshCalendar(currentCalendar);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getApplicationContext() != null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("Error", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", chef_id);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    private class ColorDecorator implements DayDecorator {
        @Override
        public void decorate(final DayView dayView) {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            String date = df.format(dayView.getDate());
            int color = 0;
            if (CalendarUtils.isPastDay(dayView.getDate())) {
                color = getResources().getColor(R.color.filter_color);
                dayView.setTextColor(color);
                //  dayView.setBackgroundResource(R.drawable.ic_substract);
            }
            for (int i = 0; i < getchefCalendarBeans.size(); i++) {
                if (date.equalsIgnoreCase(getchefCalendarBeans.get(i).getDate())) {

                    if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("2")) {
                        try {
                            Date d = df.parse(getchefCalendarBeans.get(i).getDate());
                            if (CalendarUtils.isPastDay(d)) {
                                color = getResources().getColor(R.color.filter_color);
                                dayView.setTextColor(color);
                                dayView.setBackgroundResource(R.drawable.ic_substract);
                            } else {
                                dayView.setTextColor(getResources().getColor(R.color.green));
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        // dayView.setBackgroundResource(R.drawable.green_circle);

                    } else if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("0")) {
                        /*dayView.setBackgroundResource(R.drawable.red_circle);*/
                        dayView.setTextColor(getResources().getColor(R.color.filter_color));
                        dayView.setBackgroundResource(R.drawable.ic_substract);
                    }
                }
            }

            calendarView.setCalendarListener(new CalendarListener() {
                @Override
                public void onDateSelected(Date date) {
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String selecteDate = df.format(date);
                    if (CalendarUtils.isPastDay(date)) {
                        de = "";
                        Toast.makeText(ChefProfile.this, "Disabled Date", Toast.LENGTH_SHORT).show();
                    } else {
                        de = df.format(date);
                        @SuppressLint("SimpleDateFormat") final Format formatter = new SimpleDateFormat("EEE, MMM .dd");
                        seat_available.setText(getResources().getString(R.string.on) + " " + formatter.format(date) + getResources().getString(R.string.available_booking));
                        for (int i = 0; i < getchefCalendarBeans.size(); i++) {
                            if (selecteDate.equalsIgnoreCase(getchefCalendarBeans.get(i).getDate())) {
                                if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("2")) {
                                    de = "";
                                    seat_available.setText(getResources().getString(R.string.on) + " " + formatter.format(date) + getResources().getString(R.string.seats_sold_out));
                                    Log.e("AvailDate", "" + de);
                                } else if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("0")) {
                                    Log.e("UnAva", "" + de);
                                    de = "";
                                    seat_available.setText(getResources().getString(R.string.on) + " " + formatter.format(date) + getResources().getString(R.string.chef_not_available));
                                    // Toast.makeText(ChefProfile.this, "Chef is not available", Toast.LENGTH_SHORT).show();
                                } else if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("1")) {
                                    de = df.format(date);
                                    if (getchefCalendarBeans.get(i).getIs_book().equalsIgnoreCase("yes")) {
                                        seat_available.setText(getResources().getString(R.string.on) + " " + formatter.format(date) + getResources().getString(R.string.some_seats_available));
                                    } else {
                                        seat_available.setText(getResources().getString(R.string.on) + " " + formatter.format(date) + getResources().getString(R.string.available_booking));
                                    }
                                }

                            }
                          /*  else
                            {*/

                            //seat_available.setText("On "+ formatter.format(date)+", it is available for booking!");
                          /*  }*/
                        }

                    }
                }

                @Override
                public void onMonthChanged(Date date) {
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("MM-yyyy");
                }
            });
        }
    }

    /*------------------------------------Instagram Images---------------------------------------*/
    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            URL example;
            try {
                example = new URL("https://api.instagram.com/v1/users/self/media/recent?access_token="
                        + access_token);

                URLConnection tc = example.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        tc.getInputStream()));

                String line;
                while ((line = in.readLine()) != null) {
                    JSONObject ob = new JSONObject(line);
                    return line;

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e1) {
                e1.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            try {
                if (!TextUtils.isEmpty(result)) {
                    insta_text.setVisibility(View.GONE);
                    JSONObject obj = new JSONObject(result);
                    JSONArray object = obj.getJSONArray("data");
                    Log.e("Obj length", "" + object);
                    for (int i = 0; i < object.length(); i++) {
                        JSONObject jo = (JSONObject) object.get(i);
                        JSONObject nja = jo.getJSONObject("images");
                        JSONObject purl3 = nja.getJSONObject("standard_resolution");
                    /*Log.e("Chef", "" + purl3.getString("url"));*/
                        InstaModel instaModel = new InstaModel();
                        instaModel.setUri(purl3.getString("url"));
                        instaModels.add(instaModel);
                    }
                    LinearLayoutManager horizontalLayoutManagaer1
                            = new LinearLayoutManager(ChefProfile.this, LinearLayoutManager.HORIZONTAL, false);
                    instagramRecyclerView.setLayoutManager(horizontalLayoutManagaer1);
                    instagramAdapter = new InstagramAdapter(getApplicationContext(), instaModels);
                    instagramRecyclerView.setAdapter(instagramAdapter);
                    instagramAdapter.notifyDataSetChanged();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {

        }


        @Override
        protected void onProgressUpdate(String... text) {

        }
    }
}
