package com.vervelogic.chefdiner.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.app.CustomMultipartRequest;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UploadPictures extends AppCompatActivity implements View.OnClickListener {

    public static final String CustomGalleryIntentKey = "ImageArray";//Set Intent Key Value
    private static final String TAG = UploadPictures.class.getSimpleName();
    private static final int CustomGallerySelectId = 1;//Set Intent Id
    private static final int CAMERA_ENABLE_PERMISSION = 100;
    private static ImageView openCustomGallery;
    private static GridView selectedImageGridView;
    Context mContext = this;
    DbHelper dbHelper;
    /*------------------------20 image sections---------------------------------------------------------*/
    ImageView uploadImage2, uploadImage3, uploadImage4, uploadImage5, uploadImage6, uploadImage7, uploadImage8,
            uploadImage9, uploadImage10, uploadImage11, uploadImage12, uploadImage13, uploadImage14, uploadImage15, uploadImage16,
            uploadImage17, uploadImage18, uploadImage19, uploadImage20;
    ImageView uploadImage1;
    String fileToUpload1 = "", fileToUpload2 = "", fileToUpload3 = "", fileToUpload4 = "", fileToUpload5 = "", fileToUpload6 = "", fileToUpload7 = "",
            fileToUpload8 = "", fileToUpload9 = "", fileToUpload10 = "", fileToUpload11 = "", fileToUpload12 = "", fileToUpload13 = "", fileToUpload14 = "", fileToUpload15 = "", fileToUpload16 = "", fileToUpload17 = "", fileToUpload18 = "", fileToUpload19 = "", fileToUpload20 = "";
    /*----------------------------20 image from server--------------------------------------------------------*/
    String image1, image2, image3, image4, image5, image6, image7, image8, image9, image10, image11, image12, image13, image14, image15, image16, image17, image18, image19, image20;
    /*----------------------Image view Delete----------------------------------------------------------------*/
    ImageView delete1, delete2, delete3, delete4, delete5, delete6, delete7, delete8, delete9, delete10, delete11, delete12, delete13,
            delete14, delete15, delete16, delete17, delete18, delete19, delete20;
    Button uploadPictures;
    String sendImage, imageNumber = "0";
    String user_id, id = "0", chef_id;
    Activity context = this;
    int SELECT_PICTURE = 1;
    int TAKE_PICTURE = 21;
    Bitmap bitmap;
    private SharedPreference sharedPreference;
    // Runtime permissions
    private SharedPreferences permissionStatus;
    private boolean sentToSettings = false;

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_pictures);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.upload_pic);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // RUNTIME PERMISSION
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);

        if (ActivityCompat.checkSelfPermission(UploadPictures.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(UploadPictures.this, Manifest.permission.CAMERA)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(UploadPictures.this);
                builder.setTitle(getResources().getString(R.string.need_permission));
                builder.setMessage(getResources().getString(R.string.camera_need_access));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        ActivityCompat.requestPermissions(UploadPictures.this, new String[]{Manifest.permission.CAMERA}, CAMERA_ENABLE_PERMISSION);

                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        finish();
                    }
                });
                builder.show();

            } else {
                ActivityCompat.requestPermissions(UploadPictures.this, new String[]{Manifest.permission.CAMERA}, CAMERA_ENABLE_PERMISSION);
            }
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.CAMERA, true);
            editor.commit();
        } else {
            proceedAfterPermission();

        }

        //Shared preference def
        sharedPreference = new SharedPreference();

        //  user_id=sharedPreference.getUserId(context);
        /*------------------------------------DB Access-----------------------------------*/
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            // phone_number_chef.setText(cn.getPhoneNumber());
            chef_id = cn.getUser_id();
        }
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            getKitchenGalleryAPI();
        }


        initViews();
        setListeners();

    }

    @SuppressLint("LongLogTag")
    private void proceedAfterPermission() {
        Log.e("Got The Camera permission", "Got it");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent intent=new Intent(getApplicationContext(),BecomeAChef.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Init all views
    private void initViews() {
        /*openCustomGallery = (ImageView) findViewById(R.id.openCustomGallery);
        selectedImageGridView = (GridView) findViewById(R.id.selectedImagesGridView);*/

        /*--------------------------------------IDS------------------------------------------------*/
        uploadImage1 = (ImageView) findViewById(R.id.uploadImage1);
        uploadImage2 = (ImageView) findViewById(R.id.uploadImage2);
        uploadImage3 = (ImageView) findViewById(R.id.uploadImage3);
        uploadImage4 = (ImageView) findViewById(R.id.uploadImage4);
        uploadImage5 = (ImageView) findViewById(R.id.uploadImage5);
        uploadImage6 = (ImageView) findViewById(R.id.uploadImage6);
        uploadImage7 = (ImageView) findViewById(R.id.uploadImage7);
        uploadImage8 = (ImageView) findViewById(R.id.uploadImage8);
        uploadImage9 = (ImageView) findViewById(R.id.uploadImage9);
        uploadImage10 = (ImageView) findViewById(R.id.uploadImage10);
        uploadImage11 = (ImageView) findViewById(R.id.uploadImage11);
        uploadImage12 = (ImageView) findViewById(R.id.uploadImage12);
        uploadImage13 = (ImageView) findViewById(R.id.uploadImage13);
        uploadImage14 = (ImageView) findViewById(R.id.uploadImage14);
        uploadImage15 = (ImageView) findViewById(R.id.uploadImage15);
        uploadImage16 = (ImageView) findViewById(R.id.uploadImage16);
        uploadImage17 = (ImageView) findViewById(R.id.uploadImage17);
        uploadImage18 = (ImageView) findViewById(R.id.uploadImage18);
        uploadImage19 = (ImageView) findViewById(R.id.uploadImage19);
        uploadImage20 = (ImageView) findViewById(R.id.uploadImage20);
        /*uploadPictures=(Button)findViewById(R.id.uploadImages);*/

        delete1 = (ImageView) findViewById(R.id.delete1);
        delete2 = (ImageView) findViewById(R.id.delete2);
        delete3 = (ImageView) findViewById(R.id.delete3);
        delete4 = (ImageView) findViewById(R.id.delete4);
        delete5 = (ImageView) findViewById(R.id.delete5);
        delete6 = (ImageView) findViewById(R.id.delete6);
        delete7 = (ImageView) findViewById(R.id.delete7);
        delete8 = (ImageView) findViewById(R.id.delete8);
        delete9 = (ImageView) findViewById(R.id.delete9);
        delete10 = (ImageView) findViewById(R.id.delete10);
        delete11 = (ImageView) findViewById(R.id.delete11);
        delete12 = (ImageView) findViewById(R.id.delete12);
        delete13 = (ImageView) findViewById(R.id.delete13);
        delete14 = (ImageView) findViewById(R.id.delete14);
        delete15 = (ImageView) findViewById(R.id.delete15);
        delete16 = (ImageView) findViewById(R.id.delete16);
        delete17 = (ImageView) findViewById(R.id.delete17);
        delete18 = (ImageView) findViewById(R.id.delete18);
        delete19 = (ImageView) findViewById(R.id.delete19);
        delete20 = (ImageView) findViewById(R.id.delete20);

    }

    //set Listeners
    private void setListeners() {
        // openCustomGallery.setOnClickListener(this);
        uploadImage1.setOnClickListener(this);
        uploadImage2.setOnClickListener(this);
        uploadImage3.setOnClickListener(this);
        uploadImage4.setOnClickListener(this);
        uploadImage5.setOnClickListener(this);
        uploadImage6.setOnClickListener(this);
        uploadImage7.setOnClickListener(this);
        uploadImage8.setOnClickListener(this);
        uploadImage9.setOnClickListener(this);
        uploadImage10.setOnClickListener(this);
        uploadImage11.setOnClickListener(this);
        uploadImage12.setOnClickListener(this);
        uploadImage13.setOnClickListener(this);
        uploadImage14.setOnClickListener(this);
        uploadImage15.setOnClickListener(this);
        uploadImage16.setOnClickListener(this);
        uploadImage17.setOnClickListener(this);
        uploadImage18.setOnClickListener(this);
        uploadImage19.setOnClickListener(this);
        uploadImage20.setOnClickListener(this);
       /* uploadPictures.setOnClickListener(this);*/

        delete1.setOnClickListener(this);
        delete2.setOnClickListener(this);
        delete3.setOnClickListener(this);
        delete4.setOnClickListener(this);
        delete5.setOnClickListener(this);
        delete6.setOnClickListener(this);
        delete7.setOnClickListener(this);
        delete8.setOnClickListener(this);
        delete9.setOnClickListener(this);
        delete10.setOnClickListener(this);
        delete11.setOnClickListener(this);
        delete12.setOnClickListener(this);
        delete13.setOnClickListener(this);
        delete14.setOnClickListener(this);
        delete15.setOnClickListener(this);
        delete16.setOnClickListener(this);
        delete17.setOnClickListener(this);
        delete18.setOnClickListener(this);
        delete19.setOnClickListener(this);
        delete20.setOnClickListener(this);

    }

   /* protected void onActivityResult(int requestcode, int resultcode,
                                    Intent imagereturnintent) {
        super.onActivityResult(requestcode, resultcode, imagereturnintent);
        switch (requestcode) {
            case CustomGallerySelectId:
                if (resultcode == RESULT_OK) {
                    String imagesArray = imagereturnintent.getStringExtra(CustomGalleryIntentKey);//get Intent data
                    //Convert string array into List by splitting by ',' and substring after '[' and before ']'
                    List<String> selectedImages = Arrays.asList(imagesArray.substring(1, imagesArray.length() - 1).split(", "));
                    loadGridView(new ArrayList<String>(selectedImages));//call load gridview method by passing converted list into arrayList
                }
                break;

        }
    }

    //Load GridView
    private void loadGridView(ArrayList<String> imagesArray) {
        selectedImageGridView.setVisibility(View.VISIBLE);
        GridView_Adapter adapter = new GridView_Adapter(UploadPictures.this, imagesArray, false);
        selectedImageGridView.setAdapter(adapter);
    }

    //Read Shared Images
    private void getSharedImages() {

        //If Intent Action equals then proceed
        if (Intent.ACTION_SEND_MULTIPLE.equals(getIntent().getAction())
                && getIntent().hasExtra(Intent.EXTRA_STREAM)) {
            ArrayList<Parcelable> list =
                    getIntent().getParcelableArrayListExtra(Intent.EXTRA_STREAM);//get Parcelabe list
            ArrayList<String> selectedImages = new ArrayList<>();

            //Loop to all parcelable list
            for (Parcelable parcel : list) {
                Uri uri = (Uri) parcel;//get URI
                String sourcepath = getPath(uri);//Get Path of URI
                selectedImages.add(sourcepath);//add images to arraylist
            }
            loadGridView(selectedImages);//call load gridview
        }
    }


    //get actual path of uri
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        startManagingCursor(cursor);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }*/

    @Override
    public void onClick(View view) {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        switch (view.getId()) {
            case R.id.uploadImage1:
                SELECT_PICTURE = 1;
                TAKE_PICTURE = 21;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage2:
                SELECT_PICTURE = 2;
                TAKE_PICTURE = 22;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage3:
                SELECT_PICTURE = 3;
                TAKE_PICTURE = 23;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage4:
                SELECT_PICTURE = 4;
                TAKE_PICTURE = 24;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage5:
                SELECT_PICTURE = 5;
                TAKE_PICTURE = 25;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage6:
                TAKE_PICTURE = 26;
                SELECT_PICTURE = 6;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage7:
                SELECT_PICTURE = 7;
                TAKE_PICTURE = 27;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage8:
                SELECT_PICTURE = 8;
                TAKE_PICTURE = 28;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage9:
                SELECT_PICTURE = 9;
                TAKE_PICTURE = 29;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage10:
                SELECT_PICTURE = 10;
                TAKE_PICTURE = 30;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage11:
                SELECT_PICTURE = 11;
                TAKE_PICTURE = 31;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage12:
                SELECT_PICTURE = 12;
                TAKE_PICTURE = 32;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage13:
                SELECT_PICTURE = 13;
                TAKE_PICTURE = 33;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage14:
                SELECT_PICTURE = 14;
                TAKE_PICTURE = 34;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage15:
                SELECT_PICTURE = 15;
                TAKE_PICTURE = 35;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage16:
                SELECT_PICTURE = 16;
                TAKE_PICTURE = 36;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage17:
                SELECT_PICTURE = 17;
                TAKE_PICTURE = 37;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage18:
                SELECT_PICTURE = 18;
                TAKE_PICTURE = 38;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage19:
                SELECT_PICTURE = 19;
                TAKE_PICTURE = 39;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.uploadImage20:
                SELECT_PICTURE = 20;
                TAKE_PICTURE = 40;
                showFileChooser(SELECT_PICTURE, TAKE_PICTURE);
                break;
            case R.id.delete1:
                imageNumber = "1";

                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }

                break;
            case R.id.delete2:
                imageNumber = "2";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete3:
                imageNumber = "3";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete4:
                imageNumber = "4";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete5:
                imageNumber = "5";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete6:
                imageNumber = "6";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete7:
                imageNumber = "7";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete8:
                imageNumber = "8";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete9:
                imageNumber = "9";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete10:
                imageNumber = "10";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete11:
                imageNumber = "11";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete12:
                imageNumber = "12";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete13:
                imageNumber = "13";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete14:
                imageNumber = "14";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete15:
                imageNumber = "15";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete16:
                imageNumber = "16";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete17:
                imageNumber = "17";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete18:
                imageNumber = "18";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete19:
                imageNumber = "19";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
            case R.id.delete20:
                imageNumber = "20";
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    deleteImageAPI(imageNumber);
                }
                break;
        }
    }

    // Image sent in file object form
    private void showFileChooser(final int SELECT_PICTURE, final int TAKE_PICTURE) {

        final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.choose_photo),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.add_photo);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (options[which].equals(getString(R.string.take_photo))) {
                    Intent cameraIntent = new Intent(
                            android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, TAKE_PICTURE);

                } else if (options[which].equals(getString(R.string.choose_photo))) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, SELECT_PICTURE);
                } else if (options[which].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == CAMERA_ENABLE_PERMISSION) {
            if (ActivityCompat.checkSelfPermission(UploadPictures.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermission();

            }
        }
        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            Uri filePath = intent.getData();
            if (null != filePath) {

                if (SELECT_PICTURE == 1) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = sendImage;
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";
                    uploadImage1.setImageURI(filePath);
                    Log.e("FIle1", "" + fileToUpload1);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload1);
                    }

                } else if (SELECT_PICTURE == 2) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = sendImage;
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";
                    uploadImage2.setImageURI(filePath);
                    Log.e("FIle2", "" + fileToUpload2);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload2);
                    }

                } else if (SELECT_PICTURE == 3) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = sendImage;
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";
                    uploadImage3.setImageURI(filePath);
                    Log.e("FIle3", "" + fileToUpload3);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload3);
                    }

                } else if (SELECT_PICTURE == 4) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = sendImage;
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage4.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload4);
                    }

                } else if (SELECT_PICTURE == 5) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = sendImage;
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage5.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload5);
                    }

                } else if (SELECT_PICTURE == 6) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = sendImage;
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage6.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload6);
                    }

                } else if (SELECT_PICTURE == 7) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = sendImage;
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage7.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload7);
                    }

                } else if (SELECT_PICTURE == 8) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = sendImage;
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage8.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload8);
                    }

                } else if (SELECT_PICTURE == 9) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = sendImage;
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage9.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload9);
                    }

                } else if (SELECT_PICTURE == 10) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = sendImage;
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage10.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload10);
                    }

                } else if (SELECT_PICTURE == 11) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = sendImage;
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage11.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload11);
                    }

                } else if (SELECT_PICTURE == 12) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = sendImage;
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage12.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload12);
                    }

                } else if (SELECT_PICTURE == 13) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = sendImage;
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage13.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload13);
                    }

                } else if (SELECT_PICTURE == 14) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = sendImage;
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage14.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload14);
                    }

                } else if (SELECT_PICTURE == 15) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = sendImage;
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage15.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload15);
                    }

                } else if (SELECT_PICTURE == 16) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = sendImage;
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage16.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload16);
                    }

                } else if (SELECT_PICTURE == 17) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = sendImage;
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage17.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload17);
                    }

                } else if (SELECT_PICTURE == 18) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = sendImage;
                    fileToUpload19 = "";
                    fileToUpload20 = "";

                    uploadImage18.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload18);
                    }

                } else if (SELECT_PICTURE == 19) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = sendImage;
                    fileToUpload20 = "";

                    uploadImage19.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload19);
                    }

                } else if (SELECT_PICTURE == 20) {
                    // Get the path from the Uri
                    sendImage = getPathFromURI(filePath);
                    Log.e("Upload", "Image Path->Gallery :" + sendImage);
                    // Set the image in ImageView
                    fileToUpload1 = "";
                    fileToUpload2 = "";
                    fileToUpload3 = "";
                    fileToUpload4 = "";
                    fileToUpload5 = "";
                    fileToUpload6 = "";
                    fileToUpload7 = "";
                    fileToUpload8 = "";
                    fileToUpload9 = "";
                    fileToUpload10 = "";
                    fileToUpload11 = "";
                    fileToUpload12 = "";
                    fileToUpload13 = "";
                    fileToUpload14 = "";
                    fileToUpload15 = "";
                    fileToUpload16 = "";
                    fileToUpload17 = "";
                    fileToUpload18 = "";
                    fileToUpload19 = "";
                    fileToUpload20 = sendImage;

                    uploadImage20.setImageURI(filePath);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        kitchenGalleryAPI(fileToUpload20);
                    }

                }
            }
        } else if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK && intent != null && intent.getExtras().get("data") != null) {
            if (intent.getExtras() == null) {
                return;
            }


            if (TAKE_PICTURE == 21) {
                // Get the path from the Uri

                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();
                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = sendImage;
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";
                uploadImage1.setImageBitmap(bitmap);
                // uploadImage1.setImageURI(filePath);
                Log.e("FIle1", "" + fileToUpload1);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload1);
                }

            } else if (TAKE_PICTURE == 22) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();
                Log.e("Upload", "Image Path->Gallery :" + sendImage);

                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = sendImage;
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";
                uploadImage2.setImageBitmap(bitmap);
                Log.e("FIle2", "" + fileToUpload2);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload2);
                }

            } else if (TAKE_PICTURE == 23) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = sendImage;
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";
                uploadImage3.setImageBitmap(bitmap);
                Log.e("FIle3", "" + fileToUpload3);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload3);
                }

            } else if (TAKE_PICTURE == 24) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = sendImage;
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage4.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload4);
                }

            } else if (TAKE_PICTURE == 25) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = sendImage;
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage5.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload5);
                }

            } else if (TAKE_PICTURE == 26) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = sendImage;
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage6.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload6);
                }

            } else if (TAKE_PICTURE == 27) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = sendImage;
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage7.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload7);
                }

            } else if (TAKE_PICTURE == 28) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = sendImage;
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage8.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload8);
                }

            } else if (TAKE_PICTURE == 29) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = sendImage;
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage9.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload9);
                }

            } else if (TAKE_PICTURE == 30) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = sendImage;
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage10.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload10);
                }

            } else if (TAKE_PICTURE == 31) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = sendImage;
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage11.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload11);
                }

            } else if (TAKE_PICTURE == 32) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = sendImage;
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage12.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload12);
                }

            } else if (TAKE_PICTURE == 33) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = sendImage;
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage13.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload13);
                }

            } else if (TAKE_PICTURE == 34) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = sendImage;
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage14.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload14);
                }

            } else if (TAKE_PICTURE == 35) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = sendImage;
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage15.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload15);
                }


            } else if (TAKE_PICTURE == 36) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = sendImage;
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage16.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload16);
                }

            } else if (TAKE_PICTURE == 37) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = sendImage;
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage17.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload17);
                }

            } else if (TAKE_PICTURE == 38) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = sendImage;
                fileToUpload19 = "";
                fileToUpload20 = "";

                uploadImage18.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload18);
                }

            } else if (TAKE_PICTURE == 39) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = sendImage;
                fileToUpload20 = "";

                uploadImage19.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload19);
                }

            } else if (TAKE_PICTURE == 40) {
                // Get the path from the Uri
                bitmap = (Bitmap) intent.getExtras().get("data");
                Uri tempUri = getImageUri(this, bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                sendImage = finalFile.toString();

                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                fileToUpload1 = "";
                fileToUpload2 = "";
                fileToUpload3 = "";
                fileToUpload4 = "";
                fileToUpload5 = "";
                fileToUpload6 = "";
                fileToUpload7 = "";
                fileToUpload8 = "";
                fileToUpload9 = "";
                fileToUpload10 = "";
                fileToUpload11 = "";
                fileToUpload12 = "";
                fileToUpload13 = "";
                fileToUpload14 = "";
                fileToUpload15 = "";
                fileToUpload16 = "";
                fileToUpload17 = "";
                fileToUpload18 = "";
                fileToUpload19 = "";
                fileToUpload20 = sendImage;

                uploadImage20.setImageBitmap(bitmap);
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new android.support.v7.app.AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    kitchenGalleryAPI(fileToUpload20);
                }

            }
        }
          /* try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                img_profile.setImageBitmap(bitmap);
                sendImage = getStringImage(bitmap);
                Log.e(TAG, "onActivityResult: "+ sendImage );

               updateProfile(sendImage);
            } catch (IOException e) {
                e.printStackTrace();
            }*/


    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null,
                null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    public void kitchenGalleryAPI(String sendImage) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        UserDetail userDetail = new UserDetail();

        // API sends data in multipart form
        //Auth header
        Map<String, String> mHeaderPart = new HashMap<>();
        mHeaderPart.put("Content-type", "multipart/form-data;");

        //File part
        Map<String, File> mFilePartData = new HashMap<>();
        if (!fileToUpload1.equals("") && !fileToUpload1.isEmpty() && fileToUpload1 != null) {
            mFilePartData.put("fileToUpload1", new File(sendImage));
            Log.e("fileToUpload1", "" + new File(sendImage));
        } else if (!fileToUpload2.equals("") && !fileToUpload2.isEmpty() && fileToUpload2 != null) {
            mFilePartData.put("fileToUpload2", new File(sendImage));
            Log.e("fileToUpload2", "" + new File(sendImage));
        } else if (!fileToUpload3.equals("") && !fileToUpload3.isEmpty() && fileToUpload3 != null) {
            mFilePartData.put("fileToUpload3", new File(sendImage));
            Log.e("fileToUpload3", "" + new File(sendImage));
        } else if (!fileToUpload4.equals("") && !fileToUpload4.isEmpty() && fileToUpload4 != null) {
            mFilePartData.put("fileToUpload4", new File(sendImage));
            Log.e("fileToUpload4", "" + new File(sendImage));
        } else if (!fileToUpload5.equals("") && !fileToUpload5.isEmpty() && fileToUpload5 != null) {
            mFilePartData.put("fileToUpload5", new File(sendImage));
            Log.e("fileToUpload5", "" + new File(sendImage));
        } else if (!fileToUpload6.equals("") && !fileToUpload6.isEmpty() && fileToUpload6 != null) {
            mFilePartData.put("fileToUpload6", new File(sendImage));
            Log.e("fileToUpload6", "" + new File(sendImage));
        } else if (!fileToUpload7.equals("") && !fileToUpload7.isEmpty() && fileToUpload7 != null) {
            mFilePartData.put("fileToUpload7", new File(sendImage));
            Log.e("fileToUpload7", "" + new File(sendImage));
        } else if (!fileToUpload8.equals("") && !fileToUpload8.isEmpty() && fileToUpload8 != null) {
            mFilePartData.put("fileToUpload8", new File(sendImage));
            Log.e("fileToUpload8", "" + new File(sendImage));
        } else if (!fileToUpload9.equals("") && !fileToUpload9.isEmpty() && fileToUpload9 != null) {
            mFilePartData.put("fileToUpload9", new File(sendImage));
            Log.e("fileToUpload9", "" + new File(sendImage));
        } else if (!fileToUpload10.equals("") && !fileToUpload10.isEmpty() && fileToUpload10 != null) {
            mFilePartData.put("fileToUpload10", new File(sendImage));
            Log.e("fileToUpload10", "" + new File(sendImage));
        } else if (!fileToUpload11.equals("") && !fileToUpload11.isEmpty() && fileToUpload11 != null) {
            mFilePartData.put("fileToUpload11", new File(sendImage));
            Log.e("fileToUpload11", "" + new File(sendImage));
        } else if (!fileToUpload12.equals("") && !fileToUpload12.isEmpty() && fileToUpload12 != null) {
            mFilePartData.put("fileToUpload12", new File(sendImage));
            Log.e("fileToUpload12", "" + new File(sendImage));
        } else if (!fileToUpload13.equals("") && !fileToUpload13.isEmpty() && fileToUpload13 != null) {
            mFilePartData.put("fileToUpload13", new File(sendImage));
            Log.e("fileToUpload13", "" + new File(sendImage));
        } else if (!fileToUpload14.equals("") && !fileToUpload14.isEmpty() && fileToUpload14 != null) {
            mFilePartData.put("fileToUpload14", new File(sendImage));
            Log.e("fileToUpload14", "" + new File(sendImage));
        } else if (!fileToUpload15.equals("") && !fileToUpload15.isEmpty() && fileToUpload15 != null) {
            mFilePartData.put("fileToUpload15", new File(sendImage));
            Log.e("fileToUpload15", "" + new File(sendImage));
        } else if (!fileToUpload16.equals("") && !fileToUpload16.isEmpty() && fileToUpload16 != null) {
            mFilePartData.put("fileToUpload16", new File(sendImage));
            Log.e("fileToUpload16", "" + new File(sendImage));
        } else if (!fileToUpload17.equals("") && !fileToUpload17.isEmpty() && fileToUpload17 != null) {
            mFilePartData.put("fileToUpload17", new File(sendImage));
            Log.e("fileToUpload17", "" + new File(sendImage));
        } else if (!fileToUpload18.equals("") && !fileToUpload18.isEmpty() && fileToUpload18 != null) {
            mFilePartData.put("fileToUpload18", new File(sendImage));
            Log.e("fileToUpload18", "" + new File(sendImage));
        } else if (!fileToUpload19.equals("") && !fileToUpload19.isEmpty() && fileToUpload19 != null) {
            mFilePartData.put("fileToUpload19", new File(sendImage));
            Log.e("fileToUpload19", "" + new File(sendImage));
        } else if (!fileToUpload20.equals("") && !fileToUpload20.isEmpty() && fileToUpload20 != null) {
            mFilePartData.put("fileToUpload20", new File(sendImage));
            Log.e("fileToUpload20", "" + new File(sendImage));
        }

        //String part
        Map<String, String> mStringPart = new HashMap<>();
        mStringPart.put("chef_id", chef_id);
        mStringPart.put("id", id);
        Log.e("Data", "" + mStringPart);

        // Custom multipart class called
        CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, getApplicationContext(), AppConfig.KITCHEN_GALLERY_UPLOAD_URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                progressDialog.dismiss();

                try {
                    int status = jsonObject.getInt("status");
                    if (status == 1) {

                        Log.e(TAG, "" + jsonObject);
                        String message = jsonObject.getString("message");
                        String id = jsonObject.getString("id");
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            new android.support.v7.app.AlertDialog.Builder(UploadPictures.this)
                                    .setTitle(getResources().getString(R.string.app_name))
                                    .setMessage(getResources().getString(R.string.internet_error))
                                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
                        } else {
                            getKitchenGalleryAPI();
                        }


                    } else {
                        Log.e(TAG, "" + jsonObject);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();if (getApplicationContext()!=null)
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();if (getApplicationContext()!=null)
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }if (getApplicationContext()!=null)
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e(TAG, "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }

        }, mFilePartData, mStringPart, mHeaderPart);

        RequestQueue rQueue = Volley.newRequestQueue(this);
        rQueue.add(mCustomRequest);
    }

    public void getKitchenGalleryAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();

        String uri = String.format(AppConfig.GET_KITCHEN_GALLERY_URL + "?chef_id=%1$s", chef_id);
        Log.e("uri", "" + uri);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e(TAG, response);
                        try {
                            UserDetail userDetail = new UserDetail();
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            String message = jsonObject.getString("message");
                            id = jsonObject.getString("id");
                            userDetail.setId(id);
                            if (status == 1) {
                                JSONArray jsonArray = jsonObject.getJSONArray("getKitchengallery");
                                for (int i = 0; i <= jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    image1 = object.getString("image1");
                                    image2 = object.getString("image2");
                                    image3 = object.getString("image3");
                                    image4 = object.getString("image4");
                                    image5 = object.getString("image5");
                                    image6 = object.getString("image6");
                                    image7 = object.getString("image7");
                                    image8 = object.getString("image8");
                                    image9 = object.getString("image9");
                                    image10 = object.getString("image10");
                                    image11 = object.getString("image11");
                                    image12 = object.getString("image12");
                                    image13 = object.getString("image13");
                                    image14 = object.getString("image14");
                                    image15 = object.getString("image15");
                                    image16 = object.getString("image16");
                                    image17 = object.getString("image17");
                                    image18 = object.getString("image18");
                                    image19 = object.getString("image19");
                                    image20 = object.getString("image20");
                                    Log.e(TAG, response);
                                    if (!image1.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image1).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage1);
                                        delete1.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage1.setImageResource(R.drawable.add);
                                        delete1.setVisibility(View.GONE);
                                    }
                                    if (!image2.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image2).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage2);
                                        delete2.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage2.setImageResource(R.drawable.add);
                                        delete2.setVisibility(View.GONE);
                                    }
                                    if (!image3.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image3).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage3);
                                        delete3.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage3.setImageResource(R.drawable.add);
                                        delete3.setVisibility(View.GONE);
                                    }
                                    if (!image4.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image4).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage4);
                                        delete4.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage4.setImageResource(R.drawable.add);
                                        delete4.setVisibility(View.GONE);
                                    }
                                    if (!image5.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image5).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage5);
                                        delete5.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage5.setImageResource(R.drawable.add);
                                        delete5.setVisibility(View.GONE);
                                    }
                                    if (!image6.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image6).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage6);
                                        delete6.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage6.setImageResource(R.drawable.add);
                                        delete6.setVisibility(View.GONE);
                                    }
                                    if (!image7.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image7).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage7);
                                        delete7.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage7.setImageResource(R.drawable.add);
                                        delete7.setVisibility(View.GONE);
                                    }
                                    if (!image8.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image8).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage8);
                                        delete8.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage8.setImageResource(R.drawable.add);
                                        delete8.setVisibility(View.GONE);
                                    }
                                    if (!image9.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image9).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage9);
                                        delete9.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage9.setImageResource(R.drawable.add);
                                        delete9.setVisibility(View.GONE);
                                    }
                                    if (!image10.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image10).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage10);
                                        delete10.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage10.setImageResource(R.drawable.add);
                                        delete10.setVisibility(View.GONE);
                                    }
                                    if (!image11.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image11).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage11);
                                        delete11.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage11.setImageResource(R.drawable.add);
                                        delete11.setVisibility(View.GONE);
                                    }
                                    if (!image12.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image12).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage12);
                                        delete12.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage12.setImageResource(R.drawable.add);
                                        delete12.setVisibility(View.GONE);
                                    }
                                    if (!image13.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image13).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage13);
                                        delete13.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage13.setImageResource(R.drawable.add);
                                        delete13.setVisibility(View.GONE);
                                    }
                                    if (!image14.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image14).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage14);
                                        delete14.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage14.setImageResource(R.drawable.add);
                                        delete14.setVisibility(View.GONE);
                                    }
                                    if (!image15.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image15).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage15);
                                        delete15.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage15.setImageResource(R.drawable.add);
                                        delete15.setVisibility(View.GONE);
                                    }
                                    if (!image16.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image16).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage16);
                                        delete16.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage16.setImageResource(R.drawable.add);
                                        delete16.setVisibility(View.GONE);
                                    }
                                    if (!image17.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image17).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage17);
                                        delete17.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage17.setImageResource(R.drawable.add);
                                        delete17.setVisibility(View.GONE);
                                    }
                                    if (!image18.equals("")) {
                                        Picasso.with(mContext)
                                                .load(image18).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage18);
                                        delete18.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage18.setImageResource(R.drawable.add);
                                        delete18.setVisibility(View.GONE);
                                    }
                                    if (!image19.equals("")) {

                                        Picasso.with(mContext)
                                                .load(image19).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage19);
                                        delete19.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage19.setImageResource(R.drawable.add);
                                        delete19.setVisibility(View.GONE);
                                    }
                                    if (!image20.equals("")) {
                                        // uploadImage20.setImageURI(Uri.parse(image20));
                                        Picasso.with(mContext)
                                                .load(image20).fit()
                                                .error(R.drawable.profile)
                                                .into(uploadImage20);
                                        delete20.setVisibility(View.VISIBLE);
                                    } else {
                                        uploadImage20.setImageResource(R.drawable.add);
                                        delete20.setVisibility(View.GONE);
                                    }
                                }
                            } else if (status == 0) {
                                Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            //  Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }if (getApplicationContext()!=null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /*-----Delete Image-----*/
    public void deleteImageAPI(final String imageNumber) {
    /*    final ProgressDialog progressDialog = new ProgressDialog(UploadPictures.this);
        progressDialog.setMessage("Please Wait......");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.DELETE_IMAGE_GALLERY_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      /*  progressDialog.dismiss();*/
                        Log.e(TAG, "" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                String message = jsonObject.getString("message");
                                Log.e("message", "" + message);
                                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    new android.support.v7.app.AlertDialog.Builder(UploadPictures.this)
                                            .setTitle(getResources().getString(R.string.app_name))
                                            .setMessage(getResources().getString(R.string.internet_error))
                                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                } else {
                                    getKitchenGalleryAPI();
                                }

                                Toast.makeText(UploadPictures.this, "" + message, Toast.LENGTH_SHORT).show();
                                Log.e("Response", response);

                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                Log.e("message", "" + message);
                                Toast.makeText(UploadPictures.this, "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                         /*   progressDialog.dismiss();*/
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            /*progressDialog.dismiss();*/
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                     /*   progressDialog.dismiss();*/
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("id", id);
                map.put("chef_id", chef_id);
                map.put("imagenumber", imageNumber);
                Log.e("Key Delete", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
