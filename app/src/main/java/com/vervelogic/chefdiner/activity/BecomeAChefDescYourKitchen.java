package com.vervelogic.chefdiner.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;

import java.util.List;

public class BecomeAChefDescYourKitchen extends AppCompatActivity {

    /*-----------------Declarations--------------------------------*/
    EditText describe_kitche_chef;
    Button next9;
    String langaugeList, language, firstName, lastName, phoneNumber, oftenCook, country, city, noOfGuestMin,
            noOfGuestMax, kitchenTitle, country_code, latitude, longitude, selectedCurrency;

    DbHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_achef_desc_your_kitchen);
        /*---------------------Toolbar-------------------------------*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      /*  // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/

       /*----------------------------IDS----------------------------------*/
        describe_kitche_chef = (EditText) findViewById(R.id.describe_kitche_chef);
        next9 = (Button) findViewById(R.id.next9);

        /*--------------------------Getting data from intent---------------*/
        Intent intent = getIntent();
        langaugeList = intent.getStringExtra("LanguageList");
        language = intent.getStringExtra("language");
        firstName = intent.getStringExtra("firstName");
        lastName = intent.getStringExtra("lastName");
        phoneNumber = intent.getStringExtra("phoneNumberChef");
        oftenCook = intent.getStringExtra("Often Cook");
        country = intent.getStringExtra("country");
        city = intent.getStringExtra("city");
        noOfGuestMin = intent.getStringExtra("minGuest");
        noOfGuestMax = intent.getStringExtra("maxGuest");
        kitchenTitle = intent.getStringExtra("kitchenTitle");
        country_code = intent.getStringExtra("country_code");
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        selectedCurrency = intent.getStringExtra("selectedCurrency");


        /*----------------Listener-----------------------------------*/
        next9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validate()) {
                    return;
                }
            }
        });

        /*-------------------------DB access------------------------*/
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {

            describe_kitche_chef.setText(cn.getKitchenDescription());

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validate() {
        String chefKitchenDesc = describe_kitche_chef.getText().toString();
        if (chefKitchenDesc.isEmpty()) {
            describe_kitche_chef.setError(getString(R.string.please_enter_kitchen_desc));
        } else {
            Intent intent = new Intent(getApplicationContext(), BecomAChefServiceType.class);
            intent.putExtra("Often Cook", oftenCook);
            intent.putExtra("LanguageList", langaugeList);
            intent.putExtra("language", language);
            intent.putExtra("firstName", firstName);
            intent.putExtra("lastName", lastName);
            intent.putExtra("phoneNumberChef", phoneNumber);
            intent.putExtra("country", country);
            intent.putExtra("city", city);
            intent.putExtra("minGuest", noOfGuestMin);
            intent.putExtra("maxGuest", noOfGuestMax);
            intent.putExtra("kitchenTitle", kitchenTitle);
            intent.putExtra("kitchenDesc", chefKitchenDesc);
            intent.putExtra("country_code", country_code);
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitude);
            intent.putExtra("selectedCurrency", selectedCurrency);

            startActivity(intent);
        }
        return true;
    }
}
