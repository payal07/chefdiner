package com.vervelogic.chefdiner.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;
import com.stacktips.view.DayDecorator;
import com.stacktips.view.DayView;
import com.stacktips.view.utils.CalendarUtils;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.adapter.TimeAdapter;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.GetDateModel;
import com.vervelogic.chefdiner.model.RecyclerTouchListener;
import com.vervelogic.chefdiner.model.TimeModel;
import com.vervelogic.chefdiner.model.UserDetail;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RequestABook extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    Button next;
    ImageView plus, minus;
    TextView number_book,seat_available;
    TimeAdapter timeAdapter;
    RecyclerView recyclerView;
    List<TimeModel> timeModels = new ArrayList<>();
    int count;

    String language, chef_id, maximum_person, minimum_person;
    String timeStr = "";

    DbHelper dbHelper;
    String dateStr = "", noOfSeats = "", reservationType = "";
    CustomCalendarView calendarView;
    Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());
    List<GetDateModel.GetchefCalendarBean> getchefCalendarBeans = new ArrayList<>();
    List<String> GetchefCalendarBean = new ArrayList<>();
    List<String> dateAvail = new ArrayList<>();
    String de = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_abook);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.request_a_book);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent intent = getIntent();
        language = intent.getStringExtra("language");
        chef_id = intent.getStringExtra("chef_id");
        minimum_person = intent.getStringExtra("minimum_person");
        maximum_person = intent.getStringExtra("maximum_person");
        reservationType = intent.getStringExtra("reservationType");
        dateStr = intent.getStringExtra("date");

        // IDs
        plus = (ImageView) findViewById(R.id.plus_book);
        minus = (ImageView) findViewById(R.id.minus_book);
        next = (Button) findViewById(R.id.nextBook);
        number_book = (TextView) findViewById(R.id.number_book);
        recyclerView = (RecyclerView) findViewById(R.id.time_recyclerview);
        timeAdapter = new TimeAdapter(timeModels);
        calendarView = (CustomCalendarView) findViewById(R.id.calendarView);
        seat_available = (TextView) findViewById(R.id.seat_available);

        number_book.setText(minimum_person);

        //Listeners
        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        next.setOnClickListener(this);

        //Database
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllBookingDetails();
        for (UserDetail cn : contacts) {
            timeStr = cn.getTime();
            noOfSeats = cn.getNoOfSeats();
            if (!TextUtils.isEmpty(noOfSeats)) {
                number_book.setText(noOfSeats);
                count = Integer.valueOf(noOfSeats);
            } else {
                count = Integer.valueOf(minimum_person);
            }

        }
        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);
        List<DayDecorator> decorators = new ArrayList<>();
        decorators.add(new ColorDecorator());
        calendarView.setDecorators(decorators);
        calendarView.refreshCalendar(currentCalendar);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(RequestABook.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(timeAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //  HorizontalScrollModel movie=horizontalScrollModels.get(position);
                //   Toast.makeText(RequestABook.this, "", Toast.LENGTH_SHORT).show();
                TimeModel timeModel = timeModels.get(position);
                timeStr = timeModel.getTime();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        setData();
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new android.support.v7.app.AlertDialog.Builder(RequestABook.this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            getCalDateAPI();
        }

    }



    /*----------------------------------Get Calendar API-----------------------------------------*/
    public void getCalDateAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.CALENDAR_DETAILS_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Class", "" + s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            GetDateModel paymentModel = new Gson().fromJson(s, GetDateModel.class);
                            if (status == 1) {

                                Log.e("D", "" + s);
                                for (int i = 0; i < paymentModel.getGetchefCalendar().size(); i++) {
                                    getchefCalendarBeans.add(paymentModel.getGetchefCalendar().get(i));
                                    GetchefCalendarBean.add(paymentModel.getGetchefCalendar().get(i).getDate());
                                    dateAvail.add(paymentModel.getGetchefCalendar().get(i).getAva_status());
                                    calendarView.refreshCalendar(currentCalendar);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }if (getApplicationContext()!=null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("Error", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", chef_id);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }



    private class ColorDecorator implements DayDecorator {
        @Override
        public void decorate(final DayView dayView) {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            String date = df.format(dayView.getDate());
            int color = 0;
            if (CalendarUtils.isPastDay(dayView.getDate())) {
                color = getResources().getColor(R.color.filter_color);
                dayView.setTextColor(color);
                //  dayView.setBackgroundResource(R.drawable.ic_substract);
            }
            for (int i = 0; i < getchefCalendarBeans.size(); i++) {
                if (date.equalsIgnoreCase(getchefCalendarBeans.get(i).getDate())) {

                    if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("2")) {
                        try {
                            Date d = df.parse(getchefCalendarBeans.get(i).getDate());
                            if (CalendarUtils.isPastDay(d)) {
                                color = getResources().getColor(R.color.filter_color);
                                dayView.setTextColor(color);
                                dayView.setBackgroundResource(R.drawable.ic_substract);
                            } else {
                                dayView.setTextColor(getResources().getColor(R.color.green));
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        // dayView.setBackgroundResource(R.drawable.green_circle);

                    } else if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("0")) {
                        /*dayView.setBackgroundResource(R.drawable.red_circle);*/
                        dayView.setTextColor(getResources().getColor(R.color.filter_color));
                        dayView.setBackgroundResource(R.drawable.ic_substract);
                    }
                }
            }

            calendarView.setCalendarListener(new CalendarListener() {
                @Override
                public void onDateSelected(Date date) {
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String selecteDate = df.format(date);
                    if (CalendarUtils.isPastDay(date)) {
                        de = "";
                        Toast.makeText(RequestABook.this, "Disabled Date", Toast.LENGTH_SHORT).show();
                    } else {
                        de = df.format(date);
                        @SuppressLint("SimpleDateFormat") final Format formatter = new SimpleDateFormat("EEE, MMM .dd");
                        seat_available.setText(getResources().getString(R.string.on)+" "+ formatter.format(date) + getResources().getString(R.string.available_booking));
                        for (int i = 0; i < getchefCalendarBeans.size(); i++) {
                            if (selecteDate.equalsIgnoreCase(getchefCalendarBeans.get(i).getDate())) {
                                if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("2")) {
                                    de = "";
                                    seat_available.setText(getResources().getString(R.string.on)+" " + formatter.format(date) + getResources().getString(R.string.seats_sold_out));
                                    Log.e("AvailDate", "" + de);
                                } else if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("0")) {
                                    Log.e("UnAva", "" + de);
                                    de = "";
                                    seat_available.setText(getResources().getString(R.string.on) +" "+ formatter.format(date) + getResources().getString(R.string.chef_not_available));
                                    // Toast.makeText(ChefProfile.this, "Chef is not available", Toast.LENGTH_SHORT).show();
                                } else if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("1")) {
                                    de = df.format(date);
                                    if (getchefCalendarBeans.get(i).getIs_book().equalsIgnoreCase("yes")) {
                                        seat_available.setText(getResources().getString(R.string.on)+" "+ formatter.format(date) + getResources().getString(R.string.some_seats_available));
                                    } else {
                                        seat_available.setText(getResources().getString(R.string.on) +" "+ formatter.format(date) + getResources().getString(R.string.available_booking));
                                    }
                                }

                            }
                          /*  else
                            {*/

                            //seat_available.setText("On "+ formatter.format(date)+", it is available for booking!");
                          /*  }*/
                        }

                    }
                }

                @Override
                public void onMonthChanged(Date date) {
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("MM-yyyy");
                }
            });
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.plus_book:
                count = Integer.valueOf(number_book.getText().toString());
                if ((Integer.valueOf(number_book.getText().toString())) > Integer.valueOf(maximum_person)) {
                    //    Toast.makeText(this, "Not Allowed", Toast.LENGTH_LONG).show();
                } else {
                    Log.e("CountPlus", "" + count);
                    count = count + 1;
                    number_book.setText("" + count);
                }
                break;
            case R.id.minus_book:
                count = Integer.valueOf(number_book.getText().toString());
                if (count < Integer.valueOf(minimum_person)) {
                    //   Toast.makeText(this, "Not Allowed", Toast.LENGTH_LONG).show();
                } else {
                    count = count - 1;
                    number_book.setText("" + count);
                }
                break;
            case R.id.nextBook:
                if (!(Integer.valueOf(number_book.getText().toString()) < Integer.valueOf(minimum_person)) && !timeStr.equalsIgnoreCase("") && !TextUtils.isEmpty(de)) {
                        UserDetail userDetail = new UserDetail();

                        userDetail.setTime(timeStr);
                        userDetail.setChef_id(chef_id);
                        userDetail.setDate(de);
                        userDetail.setNoOfSeats(number_book.getText().toString());

                        dbHelper.addBookingDetails(userDetail);

                        Intent intent = new Intent(getApplicationContext(), ChooseAMenu.class);
                        intent.putExtra("language", language);
                        intent.putExtra("chef_id", chef_id);
                        intent.putExtra("count", number_book.getText().toString());
                        intent.putExtra("time", timeStr);
                        intent.putExtra("date", de);
                        intent.putExtra("reservationType", reservationType);
                        startActivity(intent);


                }
                else {
                    if (Integer.valueOf(number_book.getText().toString()) < Integer.valueOf(minimum_person)) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select), Toast.LENGTH_SHORT).show();
                    }
                    if (timeStr.equalsIgnoreCase("")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select), Toast.LENGTH_SHORT).show();
                    }
                    if (TextUtils.isEmpty(de)) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.select_another_date), Toast.LENGTH_SHORT).show();
                    }
                }
        }
    }

    private void setData() {
        for (int i = 0; i <= 7; i++) {
            TimeModel timeModel = new TimeModel("1:20");
            timeModels.add(timeModel);
        }
        timeAdapter.notifyDataSetChanged();
    }
}
