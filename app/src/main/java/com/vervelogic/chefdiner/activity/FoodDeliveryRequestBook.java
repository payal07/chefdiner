package com.vervelogic.chefdiner.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;

import java.util.List;

public class FoodDeliveryRequestBook extends AppCompatActivity implements View.OnClickListener {

    /*--------------------------------Declarations-----------------------*/
    Toolbar toolbar;
    Button nextFoodDelivery;
    TextView title_text;
    EditText first_name_food_delivery, last_name_food_delivery, email_food_delivery, contact_number_food_delivery, deliveryAddress_food_delivery;
    String language, chef_id, service_type, timeStr, count, menu_id, menu_title, menu_price, menu_currency, deliver_to_your_home, dateStr;
    DbHelper dbHelper;
    String f_name = "";
    String l_name = "";
    String emailString = "";
    String contact_numberStr = "", reservationType = "";
    String address_Str = "", Key = "", kitchen_tool = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_delivery_request_book);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.request_a_book);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*----------------------------IDS---------------------------------------------------*/
        nextFoodDelivery = (Button) findViewById(R.id.nextFoodDelivery);
        first_name_food_delivery = (EditText) findViewById(R.id.first_name_food_delivery);
        last_name_food_delivery = (EditText) findViewById(R.id.last_name_food_delivery);
        email_food_delivery = (EditText) findViewById(R.id.email_food_delivery);
        contact_number_food_delivery = (EditText) findViewById(R.id.contact_number_food_delivery);
        deliveryAddress_food_delivery = (EditText) findViewById(R.id.deliveryAddress_food_delivery);
        title_text = (TextView) findViewById(R.id.title_text);

        /*--------------------------------------Listener------------------------------------------*/
        nextFoodDelivery.setOnClickListener(this);


        Intent intent = getIntent();
        language = intent.getStringExtra("language");
        chef_id = intent.getStringExtra("chef_id");
        timeStr = intent.getStringExtra("timeStr");
        count = intent.getStringExtra("count");
        menu_id = intent.getStringExtra("menu_id");
        menu_title = intent.getStringExtra("menu_title");
        menu_price = intent.getStringExtra("menu_price");
        menu_currency = intent.getStringExtra("menu_currency");
        service_type = intent.getStringExtra("service_type");
        deliver_to_your_home = intent.getStringExtra("food_delivery");
        dateStr = intent.getStringExtra("date");
        kitchen_tool = intent.getStringExtra("kitchen_tool");
        Key = intent.getStringExtra("Key");
        reservationType = intent.getStringExtra("reservationType");
        if (!TextUtils.isEmpty(Key)) {
            if (Key.equalsIgnoreCase("0")) {
                title_text.setText(getResources().getString(R.string.food_delivery));
            } else if (Key.equalsIgnoreCase("1")) {
                title_text.setText(getResources().getString(R.string.chef_onsite_cooking));
            }
        }


        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllBookingDetails();
        for (UserDetail cn : contacts) {
            f_name = cn.getFirstName();
            l_name = cn.getLastName();
            emailString = cn.getEmailId();
            contact_numberStr = cn.getPhoneNumber();
            address_Str = cn.getDeliveryAddress();

            first_name_food_delivery.setText(f_name);
            last_name_food_delivery.setText(l_name);
            email_food_delivery.setText(emailString);
            contact_number_food_delivery.setText(contact_numberStr);
            deliveryAddress_food_delivery.setText(address_Str);
        }
        Log.e("food_delivery", "" + deliver_to_your_home);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }
    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (!validate()) {
            return;
        }

    }

    public boolean validate() {
        boolean valid = true;
        f_name = first_name_food_delivery.getText().toString();
        l_name = last_name_food_delivery.getText().toString();
        emailString = email_food_delivery.getText().toString();
        contact_numberStr = contact_number_food_delivery.getText().toString();
        address_Str = deliveryAddress_food_delivery.getText().toString();
        String regex = "^\\+[0-9]{10,13}$";

        if (f_name.isEmpty()) {
            first_name_food_delivery.setError(getResources().getString(R.string.enter_first_name));
            // f_name_ly.setFocusable(true);
        } else if (l_name.isEmpty()) {
            last_name_food_delivery.setError(getResources().getString(R.string.enter_last_name));
            // l_name_ly.setFocusable(true);
        } else if (emailString.isEmpty()) {
            email_food_delivery.setError(getResources().getString(R.string.enter_email));
            // email_ly.setFocusable(true);
        }else if (!isValidEmail(emailString))
        {
            email_food_delivery.setError(getResources().getString(R.string.please_enter_valid_email));
        }
        else if (contact_numberStr.isEmpty()) {
            contact_number_food_delivery.setError(getResources().getString(R.string.enter_contact_number));
            // contact_number_ly.setFocusable(true);
        } else if (!isValidMobile(contact_numberStr)) {
            contact_number_food_delivery.setError(getResources().getString(R.string.enter_valid_number));
            // contact_number_ly.setFocusable(true);
        } else if (address_Str.isEmpty()) {
            deliveryAddress_food_delivery.setError(getResources().getString(R.string.enter_address));
        } else {

            Intent intent = new Intent(getApplicationContext(), ReservationDetailsFoodDelivery.class);
            intent.putExtra("language", language);
            intent.putExtra("chef_id", chef_id);
            intent.putExtra("timeStr", timeStr);
            intent.putExtra("count", count);
            intent.putExtra("menu_id", menu_id);
            intent.putExtra("menu_title", menu_title);
            intent.putExtra("menu_price", menu_price);
            intent.putExtra("menu_currency", menu_currency);
            intent.putExtra("service_type", service_type);
            intent.putExtra("food_delivery", deliver_to_your_home);
            intent.putExtra("f_name", f_name);
            intent.putExtra("l_name", l_name);
            intent.putExtra("emailString", emailString);
            intent.putExtra("contact_numberStr", contact_numberStr);
            intent.putExtra("address_Str", address_Str);
            intent.putExtra("kitchen_tool", "");
            intent.putExtra("date", dateStr);
            intent.putExtra("Key", Key);
            intent.putExtra("reservationType", reservationType);
            intent.putExtra("kitchen_tool", kitchen_tool);
            startActivity(intent);
        }
        return valid;
    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }
}
