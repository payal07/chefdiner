package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.AvailCreditModel;
import com.vervelogic.chefdiner.model.BookingOutput;
import com.vervelogic.chefdiner.model.ChatDetailsModel;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReservationDetailsFoodDelivery extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = ReservationDetailsFoodDelivery.class.getSimpleName();
    /*-----------------------Declarations------------------------------------*/
    Toolbar toolbar;
    boolean BUTTON_CLICK = false;
    TextView date_reservation, guest_count, menu_reservation, contact_number_reservation, reservation_price,
            message_to_chef;/*who_is_joining_you,do_you_have_food_pref*/
    Button next;
    EditText tell_about_yourself;
    /*    ImageView edit_date, edit_guest_count, edit_menu, edit_contact_number;*/
    String language;
    String reservationType = "";
    String chef_id = "";
    String timeStr = "";
    String count = "";
    String menu_id = "";
    String menu_title = "";
    String menu_price = "";
    String menu_currency = "";
    String food_delivery = "";
    String service_type = "";
    String kitchen_tool = "";
    String deliveryAddress = "";
    String firstName = "";
    String lastName = "";
    String email = "";
    String contactNumber = "";
    String user_id = "";
    String date;
    float MenuPrice;
    int availCredit;
    String currency, diduct_price, total_price;
    UserDetail userDetail = new UserDetail();
    int booknow_id;
    DbHelper dbHelper;
    Activity context = this;
    private SharedPreference sharedPreference;
    private String chef_fname, chef_lname, chef_address, profile_pic, kitchen_title, kitchen_desc, upto_guest, languageSpeak, lunchStr, from_guest,
            dinnerStr, currencyStr, allImages, priceStr, countryChef, brunchStr, userProfile, userName, emailUser, chefEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_details_food_delivery);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.request_a_book);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*------------------------IDs-------------------------------------------------------*/
        date_reservation = (TextView) findViewById(R.id.date_reservation);
        guest_count = (TextView) findViewById(R.id.guest_count);
        menu_reservation = (TextView) findViewById(R.id.menu_reservation);
        contact_number_reservation = (TextView) findViewById(R.id.contact_number_reservation);
        reservation_price = (TextView) findViewById(R.id.reservation_price);
        message_to_chef = (TextView) findViewById(R.id.message_to_chef);
        tell_about_yourself = (EditText) findViewById(R.id.tell_about_yourself);
        next = (Button) findViewById(R.id.nextFoodDelivery);

        tell_about_yourself.setHint(getResources().getString(R.string.tell_about_yourself)+"\n"+getResources().getString(R.string.who_is_joining_you)
        +"\n"+getResources().getString(R.string.do_you_have_food_pref));
        /*---------------------Listeners------------------------------*/
        next.setOnClickListener(this);

        Intent intent = getIntent();
        language = intent.getStringExtra("language");
        reservationType = intent.getStringExtra("reservationType");
        date = intent.getStringExtra("date");

        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            language = sharedPreference.getLanguage(context);
            user_id = sharedPreference.getUserId(context);
        } else {
            language = "english";
            user_id = "";
        }


        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contact = dbHelper.getAllBookingDetails();
        for (UserDetail cn : contact) {
            if (!TextUtils.isEmpty(cn.getTime())) {
                timeStr = cn.getTime();
            } else {
                timeStr = intent.getStringExtra("timeStr");
            }
            if (!TextUtils.isEmpty(cn.getNoOfSeats())) {
                count = cn.getNoOfSeats();
            } else {
                count = intent.getStringExtra("count");
            }
            if (!TextUtils.isEmpty(cn.getChef_id())) {
                chef_id = cn.getChef_id();
            } else {
                chef_id = intent.getStringExtra("chef_id");
            }
            if (!TextUtils.isEmpty(cn.getMenuId())) {
                menu_id = cn.getMenuId();
            } else {
                menu_id = intent.getStringExtra("menu_id");
            }
            if (!TextUtils.isEmpty(cn.getMenuPrice())) {
                menu_price = cn.getMenuPrice();
                total_price = cn.getMenuPrice();
            } else {
                menu_price = intent.getStringExtra("menu_price");
                total_price = intent.getStringExtra("menu_price");
                userDetail.setMenuPrice(menu_price);
            }
            if (!TextUtils.isEmpty(cn.getMenuTitle())) {
                menu_title = cn.getMenuTitle();
            } else {
                menu_title = intent.getStringExtra("menu_title");
            }
            if (!TextUtils.isEmpty(cn.getCurrency())) {
                menu_currency = cn.getCurrency();
            } else {
                menu_currency = intent.getStringExtra("menu_currency");
            }
            if (!TextUtils.isEmpty(cn.getServiceType())) {
                service_type = cn.getServiceType();
            } else {
                service_type = intent.getStringExtra("service_type");
            }
            if (!TextUtils.isEmpty(cn.getFoodDelivery())) {
                food_delivery = cn.getFoodDelivery();
            } else {
                food_delivery = intent.getStringExtra("food_delivery");
            }
            if (!TextUtils.isEmpty(cn.getKitchenTool())) {
                kitchen_tool = cn.getKitchenTool();
            } else {
                kitchen_tool = intent.getStringExtra("kitchen_tool");
            }
            if (!TextUtils.isEmpty(cn.getFirstName())) {
                firstName = cn.getFirstName();
            } else {
                firstName = intent.getStringExtra("f_name");
            }
            if (!TextUtils.isEmpty(cn.getLastName())) {
                lastName = cn.getLastName();
            } else {
                lastName = intent.getStringExtra("l_name");
            }
            if (!TextUtils.isEmpty(cn.getEmailId())) {
                email = cn.getEmailId();
            } else {
                email = intent.getStringExtra("emailString");
            }
            if (!TextUtils.isEmpty(cn.getPhoneNumber())) {
                contactNumber = cn.getPhoneNumber();
            } else {
                contactNumber = intent.getStringExtra("contact_numberStr");
            }
            if (!TextUtils.isEmpty(cn.getDeliveryAddress())) {
                deliveryAddress = cn.getDeliveryAddress();
            } else {
                deliveryAddress = intent.getStringExtra("address_Str");
            }

        }
        List<UserDetail> c = dbHelper.getAllDetails();
        for (UserDetail cn : c) {
            if (!TextUtils.isEmpty(cn.getProfilePic()))
            {
                userProfile = cn.getProfilePic();
            }
            else
            {
                userProfile = "";
            }
            if (!TextUtils.isEmpty(cn.getEmailId()))
            {
                emailUser = cn.getEmailId();
            }
            else
            {
                emailUser = "";
            }
            if (!TextUtils.isEmpty(cn.getFirstName()))
            {
                userName = cn.getFirstName() + " " + cn.getLastName();
            }
            else
            {
                userName = "";
            }
            Log.e("userProfile", "" + cn.getProfilePic());
            Log.e("emailUser", "" + cn.getEmailId());
            Log.e("userName", "" + cn.getFirstName() + " " + cn.getLastName());
        }

        Cursor cursor = dbHelper.getChefDetails(Integer.parseInt(chef_id));
        if (cursor.moveToFirst()) {
            chef_fname = cursor.getString(1);
            chef_lname = cursor.getString(2);
            chef_address = cursor.getString(12);
            profile_pic = cursor.getString(7);
            kitchen_title = cursor.getString(19);
            kitchen_desc = cursor.getString(20);
            from_guest = cursor.getString(13);
            upto_guest = cursor.getString(14);
            languageSpeak = cursor.getString(9);
            lunchStr = cursor.getString(23);
            brunchStr = cursor.getString(24);
            dinnerStr = cursor.getString(25);
            currencyStr = cursor.getString(26);
            allImages = cursor.getString(36);
            priceStr = cursor.getString(35);
            countryChef = cursor.getString(11);
            chefEmail = cursor.getString(3);
        }


        if (!food_delivery.equalsIgnoreCase("") && food_delivery != null && !food_delivery.isEmpty()) {
            if (food_delivery.equalsIgnoreCase("Pick up from chef"))
            {
                List<UserDetail> contacts = dbHelper.getAllDetails();
                for (UserDetail cn : contacts) {
                    if (!TextUtils.isEmpty( cn.getFirstName()))
                    {
                        firstName = cn.getFirstName();
                    }
                    else {
                        firstName = "";
                    }
                    if (!TextUtils.isEmpty(cn.getLastName()))
                    {
                        lastName = cn.getLastName();
                    }
                    else
                    {
                        lastName = "";
                    }
                    if (!TextUtils.isEmpty(cn.getEmailId()))
                    {
                        email = cn.getEmailId();
                    }
                    else
                    {
                        email = "";
                    }
                    if (!TextUtils.isEmpty(cn.getPhoneNumber()))
                    {
                        contactNumber = cn.getPhoneNumber();
                    }
                    else
                    {
                        contactNumber = "";
                    }

                    deliveryAddress = "";
                }
            }
            else if (food_delivery.equalsIgnoreCase("Deliver to your home"))
            {
                firstName = intent.getStringExtra("f_name");
                contactNumber = intent.getStringExtra("contact_numberStr");
                deliveryAddress = intent.getStringExtra("address_Str");
                email = intent.getStringExtra("emailString");
                lastName = intent.getStringExtra("l_name");
            }

        } else {
            List<UserDetail> contacts = dbHelper.getAllDetails();
            for (UserDetail cn : contacts) {
                if (!TextUtils.isEmpty( cn.getFirstName()))
                {
                    firstName = cn.getFirstName();
                }
                else {
                    firstName = "";
                }
                if (!TextUtils.isEmpty(cn.getLastName()))
                {
                    lastName = cn.getLastName();
                }
                else
                {
                    lastName = "";
                }
                if (!TextUtils.isEmpty(cn.getEmailId()))
                {
                    email = cn.getEmailId();
                }
                else
                {
                    email = "";
                }
                if (!TextUtils.isEmpty(cn.getPhoneNumber()))
                {
                    contactNumber = cn.getPhoneNumber();
                }
                else
                {
                    contactNumber = "";
                }

                deliveryAddress = intent.getStringExtra("address_Str");
            }
        }
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new android.support.v7.app.AlertDialog.Builder(ReservationDetailsFoodDelivery.this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton( getResources().getString(R.string.ok), null).show();
        } else {
            availCreditAPI();
        }
        date_reservation.setText(date + "," + timeStr);
        guest_count.setText(count);
        menu_reservation.setText(menu_title+" | "+menu_currency + " " + menu_price+" "+getResources().getString(R.string.per_guest));
        contact_number_reservation.setText(contactNumber);

        MenuPrice= ((Float.valueOf(menu_price))*(Integer.valueOf(count)));
        reservation_price.setText(menu_currency + " " + MenuPrice);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.nextFoodDelivery:
                if (!BUTTON_CLICK) {

                    if (tell_about_yourself.getText().toString().isEmpty()) {
                        tell_about_yourself.setError( getResources().getString(R.string.please_enter_comment));

                    } else {
                        if (!TextUtils.isEmpty(reservationType)) {
                            if (reservationType.equalsIgnoreCase("0")) {
                                String message = userName + " has requested to book.The chef has 24 hours to respond to your booking.You will " +
                                        "be charged only when the chef accepts booking." + "\n" + "Date" + "\n" + date + "," + "\n" +
                                        "Time" + "\n" + timeStr + "\n" + "Guests " + "\n" + count + " guests" + "\n" + "Menu Name" +
                                        "\n" + menu_title+"\n"+"Message From User"+"\n"+tell_about_yourself.getText().toString();
                                ChatDetailsModel chatDetailsModel = new ChatDetailsModel();
                                chatDetailsModel.setChefId(chef_id);
                                chatDetailsModel.setUserId(user_id);
                                chatDetailsModel.setUserPicture(userProfile);
                                chatDetailsModel.setChefPicture(profile_pic);
                                chatDetailsModel.setChefName(chef_fname + "" + chef_lname);
                                chatDetailsModel.setUserName(userName);
                                chatDetailsModel.setUserEmail(emailUser);
                                chatDetailsModel.setChefEmail(chefEmail);
                                chatDetailsModel.setDefaultMessage(message);
                                chatDetailsModel.setIsArchive("0");
                                /*chatDetailsModel.setChefName(chef_id);*/
                                Toast.makeText(context, "A message is sent to the chef regarding your enquiry", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MessageOpenActivity.class);
                                intent.putExtra("Model", chatDetailsModel);
                                startActivity(intent);
                            }
                            if (reservationType.equalsIgnoreCase("1")) {
                                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    new android.support.v7.app.AlertDialog.Builder(ReservationDetailsFoodDelivery.this)
                                            .setTitle(getResources().getString(R.string.app_name))
                                            .setMessage(getResources().getString(R.string.internet_error))
                                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                } else {
                                    bookingAPI();

                                }

                            }
                        }
                    }
                } else {
                    next.setText(getResources().getString(R.string.pay_now));
                    String message = userName + " has requested to book.The chef has 24 hours to respond to your booking.You will " +
                            "be charged only when the chef accepts booking." + "\n" + "Date" + "\n" + date + "," + "\n" +
                            "Time" + "\n" + timeStr + "\n" + "Guests " + "\n" + count + " guests" + "\n" + "Menu Name" +
                            "\n" + menu_title+"\n"+"Message From User"+"\n"+tell_about_yourself.getText().toString();
                    ChatDetailsModel chatDetailsModel = new ChatDetailsModel();
                    chatDetailsModel.setChefId(chef_id);
                    chatDetailsModel.setUserId(user_id);
                    chatDetailsModel.setUserPicture(userProfile);
                    chatDetailsModel.setChefPicture(profile_pic);
                    chatDetailsModel.setChefName(chef_fname + "" + chef_lname);
                    chatDetailsModel.setUserName(userName);
                    chatDetailsModel.setUserEmail(emailUser);
                    chatDetailsModel.setChefEmail(chefEmail);
                    chatDetailsModel.setDefaultMessage(message);
                    chatDetailsModel.setIsArchive("0");
                    Intent intent = new Intent(getApplicationContext(), PaymentClass.class);
                    intent.putExtra("booknow_id", booknow_id);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("Model", chatDetailsModel);
                    startActivity(intent);
                }
                break;

        }
    }

    /*----------------------------------Booking Api--------------------------------------------------*/

    public void bookingAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.BOOKING_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e(TAG, "" + s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            BookingOutput bookingOutput = new Gson().fromJson(s.toString(), BookingOutput.class);
                            if (status == 1) {
                                String booking_id = bookingOutput.getBooking_id();
                                booknow_id = bookingOutput.getBooknow_id();
                                Log.e(TAG, "" + booking_id + "\n" + booknow_id);

                                userDetail.setBookingId(booking_id);
                                userDetail.setUser_id(user_id);
                                userDetail.setChef_id(chef_id);
                                userDetail.setDate(date);
                                userDetail.setTime(timeStr);
                                userDetail.setNoOfSeats(count);
                                userDetail.setMenuTitle(menu_title);
                                userDetail.setMenuId(menu_id);
                                //userDetail.setMenuPrice(menu_price);
                                userDetail.setCurrency(menu_currency);
                                userDetail.setServiceType(service_type);
                                userDetail.setFoodDelivery(food_delivery);
                                userDetail.setKitchenTool(kitchen_tool);
                                userDetail.setFirstName(firstName);
                                userDetail.setLastName(lastName);
                                userDetail.setEmailId(email);
                                userDetail.setPhoneNumber(contactNumber);
                                userDetail.setDeliveryAddress(deliveryAddress);
                                userDetail.setUserMsg(tell_about_yourself.getText().toString());
                                userDetail.setBooNowId("" + booknow_id);


                                dbHelper.updateBooking(userDetail);
                                BUTTON_CLICK = true;
                                next.setText(getResources().getString(R.string.pay_now));

                                Toast.makeText(getApplicationContext(), "Booking Successful", Toast.LENGTH_SHORT).show();
                                String message = userName + " has requested to book.The chef has 24 hours to respond to your booking.You will " +
                                        "be charged only when the chef accepts booking." + "\n" + "Date" + "\n" + date + "," + "\n" +
                                        "Time" + "\n" + timeStr + "\n" + "Guests " + "\n" + count + " guests" + "\n" + "Menu Name" +
                                        "\n" + menu_title+"\n"+"Message From User"+"\n"+tell_about_yourself.getText().toString();
                                ChatDetailsModel chatDetailsModel = new ChatDetailsModel();
                                chatDetailsModel.setChefId(chef_id);
                                chatDetailsModel.setUserId(user_id);
                                chatDetailsModel.setUserPicture(userProfile);
                                chatDetailsModel.setChefPicture(profile_pic);
                                chatDetailsModel.setChefName(chef_fname + "" + chef_lname);
                                chatDetailsModel.setUserName(userName);
                                chatDetailsModel.setUserEmail(emailUser);
                                chatDetailsModel.setChefEmail(chefEmail);
                                chatDetailsModel.setDefaultMessage(message);
                                chatDetailsModel.setIsArchive("0");

                                Intent intent = new Intent(getApplicationContext(), PaymentClass.class);
                                intent.putExtra("booknow_id", booknow_id);
                                intent.putExtra("user_id", user_id);
                                intent.putExtra("Model", chatDetailsModel);
                                startActivity(intent);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }if (getApplicationContext()!=null)
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e(TAG, "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("booking_id", "");
                map.put("booknow_id", "");
                map.put("chef_id", chef_id);
                map.put("user_id", user_id);
                map.put("date", date);
                map.put("time", timeStr);
                map.put("total_seats", count);
                map.put("menu_id", menu_id);
                map.put("menu_price", String.valueOf(MenuPrice));
                map.put("menu_currency", menu_currency);
                map.put("service_type", service_type);
                map.put("food_delivery", food_delivery);
                map.put("kitchen_tool", kitchen_tool);
                map.put("first_name", firstName);
                map.put("last_name", lastName);
                map.put("email", email);
                map.put("diduct_price", diduct_price);
                map.put("total_price", total_price);
                map.put("contact_number", contactNumber);
                map.put("delivery_address", deliveryAddress);
                map.put("user_msg", tell_about_yourself.getText().toString());

                Log.e("Map", "" + map);

                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void availCreditAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.AVAIL_CREDIT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Class", "" + s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            AvailCreditModel availCreditModel = new Gson().fromJson(s, AvailCreditModel.class);
                            if (status == 1) {
                                for (int i = 0; i < availCreditModel.getGetReferralAmount().size(); i++) {
                                    availCredit = availCreditModel.getGetReferralAmount().get(i).getCredited_amount();
                                    diduct_price = String.valueOf(availCreditModel.getGetReferralAmount().get(i).getCredited_amount());
                                    currency = availCreditModel.getGetReferralAmount().get(i).getCurrency();
                                    MenuPrice = (Float.valueOf(MenuPrice) - Float.valueOf(availCredit));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            // Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }if (getApplicationContext()!=null)
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("Error", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("main_user_id", user_id);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
