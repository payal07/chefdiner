package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.chatModel.UserModel;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.firebase.FirebaseConstant;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SessionManager;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterScreen extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = RegisterScreen.class.getSimpleName();
    // Firebase chatiing objects for registration on firebase by vishnu saini
    private static DatabaseReference databaseReferenceSender;
    Toolbar toolbar;
    Button register, fb_register;
    EditText first_name, last_name, email, password, confirm_password, contact_number;
    TextInputLayout f_name_ly, l_name_ly, email_ly, password_ly, confirm_password_ly, contact_number_ly;
    String f_name, l_name, emailString, passwordStr, confirm_passwordStr, contact_numberStr;
    String userId;
    String language;
    String is_chef_verify, is_chef, referal_code;
    LoginButton loginButton;
    // Session Manager Class
    SessionManager session;
    DbHelper dbHelper;
    String first_name_str, last_name_str, email_str, phone_number;
    Activity context = this;
    FirebaseStorage firebaseStorage;
    // Facebook
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private SharedPreference sharedPreference;
    private FirebaseAuth.AuthStateListener mAuthListener;


    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Log.e(TAG, "Accesstoken " + accessToken);
            Profile profile = Profile.getCurrentProfile();
            displayMessage(profile);
            getUserDetails(loginResult);
        }

        void getUserDetails(LoginResult loginResult) {
            GraphRequest data_request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject json_object,
                                GraphResponse response) {
                            try {
                                String userName = json_object.getString("name");
                                String pswd = json_object.getString("email");
                                String id = json_object.getString("id");
                                String email = json_object.getString("email");
                                JSONObject jsonObject = json_object.getJSONObject("picture");
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String url = jsonObject1.getString("url");

                                String[] splited = userName.split("\\s+");
                                String f_name = splited[0];
                                String l_name = splited[1];
                                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    new android.support.v7.app.AlertDialog.Builder(RegisterScreen.this)
                                            .setTitle(getResources().getString(R.string.app_name))
                                            .setMessage(getResources().getString(R.string.internet_error))
                                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                } else {
                                    fbLoginAPI(id, email, url, f_name, l_name);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    });
            Bundle permission_param = new Bundle();
            permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");
            data_request.setParameters(permission_param);
            data_request.executeAsync();

        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        //final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_register_screen);
        callbackManager = CallbackManager.Factory.create();

        // ------------------------toolbar--------------------------------
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    /*    // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/

        // check firebase auth if already login or register and if register then save user data on firebase database. this function is use when after registration user go to home screen without login.
        checkFirebaseAuth();

        //------------------------------------------ IDs-----------------------------------------------
        first_name = (EditText) findViewById(R.id.firstname);
        last_name = (EditText) findViewById(R.id.lastname);
        email = (EditText) findViewById(R.id.emailRegister);
        password = (EditText) findViewById(R.id.passwordRegister);
        confirm_password = (EditText) findViewById(R.id.confirmPassword);
        contact_number = (EditText) findViewById(R.id.contactNumber);
        register = (Button) findViewById(R.id.register_button);
        fb_register = (Button) findViewById(R.id.facebook_register_btn);
        f_name_ly = (TextInputLayout) findViewById(R.id.f_name_ly);
        l_name_ly = (TextInputLayout) findViewById(R.id.l_name_ly);
        email_ly = (TextInputLayout) findViewById(R.id.email_ly);
        password_ly = (TextInputLayout) findViewById(R.id.password_ly);
        confirm_password_ly = (TextInputLayout) findViewById(R.id.confirm_password_ly);
        contact_number_ly = (TextInputLayout) findViewById(R.id.contact_number_ly);
        loginButton = (LoginButton) findViewById(R.id.lb);


        //------------------------------Listeners-------------------------------------------------
        register.setOnClickListener(this);
        fb_register.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        sharedPreference = new SharedPreference();
        //------------------------------- Session Manager--------------------------------
        session = new SessionManager(getApplicationContext());

        /*-----------------------Getting data from intent--------------------------------------*/


        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                displayMessage(currentProfile);
            }
        };
        Intent intent = getIntent();
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(context))) {
            language = sharedPreference.getLanguage(context);
        } else {
            language = "english";
        }
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };
        accessTokenTracker.startTracking();
        profileTracker.startTracking();
    }

    private void checkFirebaseAuth() {
        if (AppController.mAuth == null) {
            AppController.mAuth = FirebaseAuth.getInstance();
        }
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                    Log.d("onAuthStateChanged", "onAuthStateChanged:signed_in:" + user.getUid());
// Create a storage reference from our app
                    if (!email.getText().toString().isEmpty()) {
                        databaseReferenceSender = AppController.database.getReference(FirebaseConstant.TABLE_USER).child(user.getEmail());

                        UserModel userModel = new UserModel();
                        userModel.setUserId(user.getUid());
                        userModel.setUserQuickblockEmail(user.getEmail());
                        //userModel.setUserImage(String.valueOf(downloadUrl));
                        //userModel.setUserAddress(homeaddress);
                        userModel.setUserPhoneNo(phone_number);
                        userModel.setUserEmail(user.getEmail());
                        databaseReferenceSender.setValue(userModel);
                    }


                    // Utils.gotTodashBord(SignUpActivity.this);
                } else {
                    // User is signed out
                    Log.d("signed_out", "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void fbLoginAPI(final String id, final String emailStr, final String url, final String firstname, final String lastname) {
        final ProgressDialog progressDialog = new ProgressDialog(RegisterScreen.this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.FBLOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "" + response);
                        progressDialog.dismiss();
                        dbHelper.deleteChef();
                        dbHelper.deleteUser();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                String message = jsonObject.getString("message");
                                Log.e(TAG, "" + message);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("userdata");
                           /*     for (int i = 0; i < jsonObject1.length(); i++) {*/
                                userId = jsonObject1.getString("userId");
                                first_name_str = jsonObject1.getString("first_name");
                                   /* authtoken = jsonObject1.getString("authtoken");*/
                                last_name_str = jsonObject1.getString("last_name");
                                phone_number = jsonObject1.getString("phone_number");
                                email_str = jsonObject1.getString("email");
                                   /* profile_pic = jsonObject1.getString("profile_pic");*/
                                is_chef_verify = jsonObject1.getString("is_verify_chef");
                                is_chef = jsonObject1.getString("is_chef");
                                referal_code = jsonObject1.getString("referal_code");
                               /* }*/
                                Toast.makeText(RegisterScreen.this, "" + message, Toast.LENGTH_SHORT).show();
                                Log.e(TAG, response);
                                session.createLoginSession(first_name_str, email_str);

                                UserDetail userDetail = new UserDetail();
                                userDetail.setUser_id(userId);
                                userDetail.setFirstName(first_name_str);
                                userDetail.setLastName(last_name_str);
                                userDetail.setPhoneNumber(phone_number);
                                userDetail.setEmailId(email_str);
                               /* userDetail.setAuthToken(authtoken);*/
                             /*   userDetail.setProfilePic(profile_pic);*/
                                userDetail.setLanguage(language);
                               /* userDetail.setPassword(paswdSignIn.getText().toString());*/
                                userDetail.setIsChefVerify(is_chef_verify);
                                userDetail.setIsChef(is_chef);
                                dbHelper.addUser(userDetail);
                                dbHelper.addChefDetails(userDetail);

                                //-------------------------Save the text in SharedPreference---------------------------------
                                sharedPreference.save(context, userId, emailStr);
                                Log.e(TAG, "AuthToken " + sharedPreference.getAuthToken(context));
                                Log.e(TAG, "UserId " + sharedPreference.getUserId(context));
                                Intent intent = new Intent(getApplicationContext(), HomePage.class);
                                startActivity(intent);

                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                Log.e(TAG, "" + message);
                                Toast.makeText(RegisterScreen.this, "" + message, Toast.LENGTH_SHORT).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("id", id);
                map.put("first_name", firstname);
                map.put("email", emailStr);
                map.put("language", language);
                map.put("last_name ", lastname);
                map.put("picture", url);

                Log.e("Map", "" + map);

                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register_button:
                if (!validate()) {
                    return;
                }

                break;
            case R.id.facebook_register_btn:
                loginButton.performClick();
                loginButton.setReadPermissions("user_friends");
                //loginButton.setFragment();
                loginButton.registerCallback(callbackManager, callback);


        }
    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
    }

    private void displayMessage(Profile profile) {
        if (profile != null) {
            //    textView.setText(profile.getName());
        }
    }

    public boolean validate() {
        boolean valid = true;
        String f_name = first_name.getText().toString();
        String l_name = last_name.getText().toString();
        String emailString = email.getText().toString();
        String passwordStr = password.getText().toString();
        String confirm_passwordStr = confirm_password.getText().toString();
        String contact_numberStr = contact_number.getText().toString();
        String regex = "^\\+[0-9]{10,13}$";

        if (f_name.isEmpty()) {
            first_name.setError(getResources().getString(R.string.enter_first_name));
            f_name_ly.setFocusable(true);
        } else if (l_name.isEmpty()) {
            last_name.setError(getResources().getString(R.string.enter_last_name));
            l_name_ly.setFocusable(true);
        } else if (emailString.isEmpty()) {
            email.setError(getResources().getString(R.string.enter_email));
            email_ly.setFocusable(true);
        } else if (!isValidEmail(emailString)) {
            email.setError(getResources().getString(R.string.please_enter_valid_email));
        } else if (passwordStr.isEmpty()) {
            password.setError(getResources().getString(R.string.enter_password));
            password_ly.setFocusable(true);
        } else if (password.getText().toString().length() < 8 && !isValidPassword(password.getText().toString())) {
            password.setError(getResources().getString(R.string.password_validation));
            password_ly.setFocusable(true);
        } else if (confirm_passwordStr.isEmpty()) {
            confirm_password.setError(getResources().getString(R.string.enter_confirm_paswd));
            confirm_password_ly.setFocusable(true);
        } else if (contact_numberStr.isEmpty()) {
            contact_number.setError(getResources().getString(R.string.enter_contact_number));
            contact_number_ly.setFocusable(true);
        } else if (!isValidMobile(contact_numberStr)) {
            contact_number.setError(getResources().getString(R.string.enter_valid_number));
            contact_number_ly.setFocusable(true);
        } else if (!confirm_passwordStr.equals(passwordStr)) {
            confirm_password.setError(getResources().getString(R.string.paswword_dont_match));
            confirm_password_ly.setFocusable(true);
        } else if (confirm_password.getText().toString().length() < 8 && !isValidPassword(confirm_password.getText().toString())) {
            confirm_password.setError(getResources().getString(R.string.password_validation));
            confirm_password_ly.setFocusable(true);
        } else {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            if (netInfo == null) {
                new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.app_name))
                        .setMessage(getResources().getString(R.string.internet_error))
                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
            } else {
                RegisterAPI();
            }


        }
        return valid;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    //------------------------------------ Registration API-----------------------------------------
    public void RegisterAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(RegisterScreen.this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                String message = jsonObject.getString("message");
                                Log.e(TAG, "" + message);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("userData");
                                for (int i = 0; i < jsonObject1.length(); i++) {
                                    userId = jsonObject1.getString("id");
                                  /*  Toast.makeText(RegisterScreen.this, "Registration Successful", Toast.LENGTH_SHORT).show();*/
                                }
                                String emails = email.getText().toString();
                                String pass = password.getText().toString();


                                registerOnfireBase(emails, pass, progressDialog);


                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                Toast.makeText(RegisterScreen.this, "" + message, Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("firstname", first_name.getText().toString());
                map.put("lastname", last_name.getText().toString());
                map.put("email", email.getText().toString());
                map.put("password", password.getText().toString());
                map.put("contact", contact_number.getText().toString());
                map.put("language", language);
                Log.e("Map", "" + map);

                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void registerOnfireBase(final String email, String Password, final ProgressDialog progressDialog) {
        AppController.mAuth.createUserWithEmailAndPassword(email, Password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("createUserWithEmail", "createUserWithEmail:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Log.d("Registration fail", task.getException().toString());
                            progressDialog.dismiss();
                        } else {

                            Log.d("Registration success", task.getResult().toString());
                            Log.d("rahul firebase phone no", "rahul firebase phone no");
                            progressDialog.dismiss();

                            //   Toast.makeText(RegisterScreen.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                            final Dialog dialog = new Dialog(RegisterScreen.this);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                            Window window = dialog.getWindow();
                            window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.setContentView(R.layout.registration_completed_popup);

                            TextView email_received = dialog.findViewById(R.id.email_received);
                            TextView mail_resend = dialog.findViewById(R.id.mail_resend);
                            Button checkMail = dialog.findViewById(R.id.checkMail);

                            email_received.setText(getResources().getString(R.string.emailString) + email + getResources().getString(R.string.restData));

                            checkMail.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                   /* Intent cal = new Intent(getApplicationContext(), MessageOpenActivity.class);
                                    startActivity(cal);*/
                                }
                            });
                            dialog.show();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = new Intent(getApplicationContext(), SignInScreen.class);
                                    intent.putExtra("language", language);
                                    startActivity(intent);
                                }
                            }, 5000);

                        }

                    }
                });
    }
}
