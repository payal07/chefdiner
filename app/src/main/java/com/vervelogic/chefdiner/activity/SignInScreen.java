package com.vervelogic.chefdiner.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.bottomFragment.Home;
import com.vervelogic.chefdiner.chatModel.DeviceTokenModel;
import com.vervelogic.chefdiner.chatModel.UserdataModel;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.firebase.FirebaseFunctions;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.services.AppService;
import com.vervelogic.chefdiner.utils.NotificationUtils;
import com.vervelogic.chefdiner.utils.SessionManager;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignInScreen extends AppCompatActivity implements View.OnClickListener {

    private static final int LOCATION_ENABLE_PERMISSION = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final String TAG = SignInScreen.class.getSimpleName();
    Toolbar toolbar;
    EditText emailSignIn, paswdSignIn;
    Button login, fb_Login;
    LoginButton loginButton;
    TextInputLayout email_login_ly, password_login_ly;
    String uid;
    String is_chef_verify, is_chef, referal_code;
    String userId, first_name, authtoken, last_name, phone_number, email, profile_pic;
    String type, Chef_id;
    String regId;
    DbHelper dbHelper;
    String language;
    // Session Manager Class
    SessionManager session;
    Activity context = this;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private SharedPreference sharedPreference;
    // Facebook
    private CallbackManager callbackManager;

    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    // firebasse auth for check already login or not
    private FirebaseAuth.AuthStateListener mAuthListener;
     ProgressDialog progressDialog;

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Log.e(TAG, "AccessToken" + accessToken);
            Profile profile = Profile.getCurrentProfile();
            displayMessage(profile);
            getUserDetails(loginResult);
        }

        void getUserDetails(LoginResult loginResult) {
            GraphRequest data_request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject json_object,
                                GraphResponse response) {
                            Log.e(TAG, json_object.toString());
                            try {
                                String userName = json_object.getString("name");
                                String pswd = json_object.getString("email");
                                String id = json_object.getString("id");
                                String email = json_object.getString("email");
                                JSONObject jsonObject = json_object.getJSONObject("picture");
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String url = jsonObject1.getString("url");

                                String[] splited = userName.split("\\s+");
                                String f_name = splited[0];
                                String l_name = splited[1];
                                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    new android.support.v7.app.AlertDialog.Builder(SignInScreen.this)
                                            .setTitle(getResources().getString(R.string.app_name))
                                            .setMessage(getResources().getString(R.string.internet_error))
                                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                } else {
                                    fbLoginAPI(id, email, url, f_name, l_name);

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    });
            Bundle permission_param = new Bundle();
            permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");
            data_request.setParameters(permission_param);
            data_request.executeAsync();

        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        //final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }
    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());


        setContentView(R.layout.activity_sign_in_screen);
        callbackManager = CallbackManager.Factory.create();
    /*    facebook = new Facebook(APP_ID);
        mAsyncRunner = new AsyncFacebookRunner(facebook);*/

        // -------------------------------------toolbar------------------------------------
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      /*  // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/
        progressDialog = new ProgressDialog(SignInScreen.this);

        //check firebase auth
        checkFirebaseAuth();

        sharedPreference = new SharedPreference();
        //------------------------------- Session Manager--------------------------------
        session = new SessionManager(getApplicationContext());

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals("registrationComplete")) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic("global");
                  /*  String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                    Log.e("Broadcast",""+refreshedToken);*/

                    displayFirebaseRegId();

                } else if (intent.getAction().equals("pushNotification")) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "" + message);
                    // txtMessage.setText(message);
                }
            }
        };

        displayFirebaseRegId();
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);

        if (ActivityCompat.checkSelfPermission(SignInScreen.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(SignInScreen.this, Manifest.permission.READ_PHONE_STATE)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SignInScreen.this);
                builder.setTitle(getResources().getString(R.string.phone_state));
                builder.setMessage(getResources().getString(R.string.read_phone_state));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        ActivityCompat.requestPermissions(SignInScreen.this, new String[]{Manifest.permission.READ_PHONE_STATE}, LOCATION_ENABLE_PERMISSION);

                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        finish();
                    }
                });
                builder.show();

            } else {
                ActivityCompat.requestPermissions(SignInScreen.this, new String[]{Manifest.permission.READ_PHONE_STATE}, LOCATION_ENABLE_PERMISSION);
            }
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.READ_PHONE_STATE, true);
            editor.commit();
        } else {
            getDeviceId();
        }

        //------------------------------IDs----------------------------------------------
        emailSignIn = (EditText) findViewById(R.id.emailLogin);
        paswdSignIn = (EditText) findViewById(R.id.passwordLogin);
        login = (Button) findViewById(R.id.login_button);
        fb_Login = (Button) findViewById(R.id.facebook_login_btn);
        email_login_ly = (TextInputLayout) findViewById(R.id.email_login_ly);
        password_login_ly = (TextInputLayout) findViewById(R.id.password_login_ly);
        loginButton = (LoginButton) findViewById(R.id.lb);

        // ---------------------------------setting listener-------------------------------
        login.setOnClickListener(this);
        fb_Login.setOnClickListener(this);
        loginButton.setOnClickListener(this);


        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };


        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                displayMessage(currentProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();
        dbHelper = new DbHelper(this);
        Intent intent = getIntent();
        type = intent.getStringExtra("type");
        language = intent.getStringExtra("language");
        Chef_id = intent.getStringExtra("Chef_id");

    }

    private void checkFirebaseAuth() {
        if (AppController.mAuth == null) {
            AppController.mAuth = FirebaseAuth.getInstance();
        }
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d("onAuthStateChanged", "onAuthStateChanged:signed_in:" + user.getUid());
                    String userid = user.getUid();
                    String email = user.getEmail();
                    Log.d("onAuthStateChanged", "onAuthStateChanged:signed_in:" + user.getUid());

                } else {
                    // User is signed out
                    Log.d("signed_out", "onAuthStateChanged:signed_out");
                }
            }
        };

    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("firebase", 0);
        //regId = pref.getString("regId", null);
        regId = FirebaseInstanceId.getInstance().getToken();

        Log.e(TAG, "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId)) {
            //txtRegId.setText("Firebase Reg Id: " + regId);
            Log.e(TAG, "" + regId);
            /*String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.e("TOKEN", "" + refreshedToken);*/
        } else {
            //txtRegId.setText("Firebase Reg Id is not received yet!");
            Log.e(TAG, "Firebase Reg Id is not received yet!");
           /* String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.e("TOKEN", "" + refreshedToken);*/
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.login_button:
                if (!validate()) {
                    return;
                }
                break;
            case R.id.facebook_login_btn:
                loginButton.performClick();
                loginButton.setReadPermissions("user_friends");
                //loginButton.setFragment();
                loginButton.registerCallback(callbackManager, callback);
                break;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter("registrationComplete"));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter("pushNotification"));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    private void displayMessage(Profile profile) {
        if (profile != null) {
            //    textView.setText(profile.getName());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validate() {
        boolean valid = true;
        String emailStr = emailSignIn.getText().toString();
        String passwordStr = paswdSignIn.getText().toString();

        if (emailStr.isEmpty()) {
            emailSignIn.setError(getResources().getString(R.string.enter_first_name));
            email_login_ly.setFocusable(true);
        }
        else if (!isValidEmail(emailStr))
        {
            emailSignIn.setError(getResources().getString(R.string.please_enter_valid_email));
        }
        else if (passwordStr.isEmpty()) {
            paswdSignIn.setError(getResources().getString(R.string.enter_password));
            password_login_ly.setFocusable(true);
            password_login_ly.setFocusable(true);
        } else {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            if (netInfo == null) {
                Log.e(TAG, "No Internet connection");
            } else {
                LoginApi();
            }

        }
        return valid;
    }

    public void LoginApi() {

        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "" + response);
                        //progressDialog.dismiss();
                        dbHelper.deleteUser();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                progressDismiss();
                                String message = jsonObject.getString("message");
                                Log.e(TAG, "" + message);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("userData");
                                String pass = paswdSignIn.getText().toString();
                                loginOnFirebase(emailSignIn.getText().toString(), pass, progressDialog, jsonObject1, message, regId);

                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                Log.e(TAG, "" + message);
                                Toast.makeText(SignInScreen.this, "" + message, Toast.LENGTH_SHORT).show();
                                progressDismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDismiss();if (getApplicationContext()!=null)
                            Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDismiss();if (getApplicationContext()!=null)
                            Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.d("Error: ", error.toString());
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("useremail", emailSignIn.getText().toString());
                map.put("password", paswdSignIn.getText().toString());
                map.put("deviceId", regId);
                map.put("deviceType", "2");

                Log.e("Map", "" + map);

                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void fbLoginAPI(final String id, final String emailStr, final String url, final String firstname, final String lastname) {

        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.FBLOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "" + response);
                        progressDismiss();
                        dbHelper.deleteChef();
                        dbHelper.deleteUser();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                String message = jsonObject.getString("message");
                                Log.e(TAG, "" + message);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("userdata");
                             /*   for (int i = 0; i < jsonObject1.length(); i++) {*/
                                userId = jsonObject1.getString("userId");
                                first_name = jsonObject1.getString("first_name");
                                   /* authtoken = jsonObject1.getString("authtoken");*/
                                last_name = jsonObject1.getString("last_name");
                                phone_number = jsonObject1.getString("phone_number");
                                email = jsonObject1.getString("email");
                                   /* profile_pic = jsonObject1.getString("profile_pic");*/
                                is_chef_verify = jsonObject1.getString("is_verify_chef");
                                is_chef = jsonObject1.getString("is_chef");
                                referal_code = jsonObject1.getString("referal_code");
                           /*     }*/
                                Toast.makeText(SignInScreen.this, "" + message, Toast.LENGTH_SHORT).show();
                                Log.e(TAG, response);
                                session.createLoginSession(first_name, email);

                                UserDetail userDetail = new UserDetail();
                                userDetail.setUser_id(userId);
                                userDetail.setFirstName(first_name);
                                userDetail.setLastName(last_name);
                                userDetail.setPhoneNumber(phone_number);
                                userDetail.setEmailId(email);
                                userDetail.setReferralCode(referal_code);
                               /* userDetail.setAuthToken(authtoken);*/
                             /*   userDetail.setProfilePic(profile_pic);*/
                                userDetail.setLanguage(language);
                               /* userDetail.setPassword(paswdSignIn.getText().toString());*/
                                userDetail.setIsChefVerify(is_chef_verify);
                                userDetail.setIsChef(is_chef);
                                dbHelper.addUser(userDetail);

                                //-------------------------Save the text in SharedPreference---------------------------------
                                sharedPreference.save(context, userId, authtoken);
                                Log.e(TAG, "AuthToken " + sharedPreference.getAuthToken(context));
                                Log.e(TAG, "UserID " + sharedPreference.getUserId(context));
                                Intent intent = new Intent(getApplicationContext(), HomePage.class);
                                startActivity(intent);

                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                Log.e(TAG, "" + message);
                                Toast.makeText(SignInScreen.this, "" + message, Toast.LENGTH_SHORT).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.d("Error: ", error.toString());

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("id", id);
                map.put("first_name", firstname);
                map.put("email", emailStr);
                map.put("language", language);
                map.put("last_name ", lastname);
                /*map.put("picture", url);*/

                Log.e("Map", "" + map);

                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void proceedAfterPermission() {

        Toast.makeText(this, "Thank You!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_ENABLE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getDeviceId();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(SignInScreen.this, Manifest.permission.READ_PHONE_STATE)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SignInScreen.this);
                    builder.setTitle(getResources().getString(R.string.phone_state));
                    builder.setMessage(getResources().getString(R.string.read_phone_state));
                    builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            ActivityCompat.requestPermissions(SignInScreen.this, new String[]{Manifest.permission.READ_PHONE_STATE}, LOCATION_ENABLE_PERMISSION);


                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            finish();
                        }
                    });
                    builder.show();

                } else {
                    Toast.makeText(this, getResources().getString(R.string.unable_to_get_permission), Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOCATION_ENABLE_PERMISSION) {
            if (ActivityCompat.checkSelfPermission(SignInScreen.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                getDeviceId();
            }
        }
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(SignInScreen.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                getDeviceId();
            }
        }
    }

    @SuppressLint("HardwareIds")
    public void getDeviceId() {
        // Device ID
        TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        uid = tManager.getDeviceId();
        Log.e("Device Id", "" + uid);
    }

    public void loginOnFirebase(final String emails, String passsword, final ProgressDialog progressDialog,
                                final JSONObject jsonObject1, final String message, final String token) {
        AppController.mAuth.signInWithEmailAndPassword(emails, passsword)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("createUserWithEmail", "createUserWithEmail:onComplete:" + task.isSuccessful());
                        progressDismiss();
                        if (!task.isSuccessful()) {

                            Toast.makeText(SignInScreen.this, "Login Fail", Toast.LENGTH_SHORT).show();
                        } else {

                            DeviceTokenModel deviceTokenModel = new DeviceTokenModel();
                            Gson gson = new Gson();

                            /*User Updation Call-=====---------------------*/
                            UserdataModel userModel = gson.fromJson(String.valueOf(jsonObject1), UserdataModel.class);
                            deviceTokenModel.setMobileNo(userModel.getPhone_number());
                            deviceTokenModel.setProfilepic(userModel.getProfile_pic());
                            deviceTokenModel.setDevicetoken(token);
                            deviceTokenModel.setEmail(userModel.getEmail());
                            deviceTokenModel.setUserId(userModel.getUserId());
                            deviceTokenModel.setUserName(userModel.getFirst_name()+" "+userModel.getLast_name());
                            deviceTokenModel.setDeviceType("A");
                            FirebaseFunctions.saveDeviceTokenIntoServer(deviceTokenModel);

                            for (int i = 0; i < jsonObject1.length(); i++) {
                                try {
                                    userId = jsonObject1.getString("userId");
                                    first_name = jsonObject1.getString("first_name");
                                   /* authtoken = jsonObject1.getString("authtoken");*/
                                    last_name = jsonObject1.getString("last_name");
                                    phone_number = jsonObject1.getString("phone_number");
                                    email = jsonObject1.getString("email");
                                    profile_pic = jsonObject1.getString("profile_pic");
                                    is_chef_verify = jsonObject1.getString("is_verify_chef");
                                    is_chef = jsonObject1.getString("is_chef");
                                    referal_code = jsonObject1.getString("referal_code");



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            Intent service = new Intent(SignInScreen.this, AppService.class);
                            startService(service);
                            Toast.makeText(SignInScreen.this, "" + message, Toast.LENGTH_SHORT).show();

                            session.createLoginSession(userId, paswdSignIn.getText().toString());

                            UserDetail userDetail = new UserDetail();
                            userDetail.setUser_id(userId);
                            userDetail.setFirstName(first_name);
                            userDetail.setLastName(last_name);
                            userDetail.setPhoneNumber(phone_number);
                            userDetail.setEmailId(email);
                            userDetail.setReferralCode(referal_code);
                            Log.e("referal_code", "" + referal_code);
                               /* userDetail.setAuthToken(authtoken);*/
                            userDetail.setProfilePic(profile_pic);
                            userDetail.setLanguage(language);
                            userDetail.setPassword(paswdSignIn.getText().toString());
                            userDetail.setIsChefVerify(is_chef_verify);
                            userDetail.setIsChef(is_chef);
                            dbHelper.addUser(userDetail);
                            //-------------------------Save the text in SharedPreference---------------------------------
                            sharedPreference.save(context, userId, authtoken);
                            Log.e(TAG, "AuthToken " + sharedPreference.getAuthToken(context));
                            Log.e(TAG, "UserID " + sharedPreference.getUserId(context));

                            if (!TextUtils.isEmpty(type)) {
                                if (type.equalsIgnoreCase("ChefProfile")) {
                                /*    if (!TextUtils.isEmpty(userId))
                                    {*/
                                      /*  if (Chef_id.equalsIgnoreCase(userId))
                                        {*/
                                            Intent intent = new Intent(getApplicationContext(), HomePage.class);
                                            startActivity(intent);
                                     /*   }*/
                                      /*  else
                                        {
                                            Intent intent = new Intent(getApplicationContext(), ChefProfile.class);
                                            intent.putExtra("Chef_id", Chef_id);
                                            startActivity(intent);
                                        }*/
                                 /*   }*/
                                }
                                if (type.equalsIgnoreCase("homePage")) {
                                    Intent intent = new Intent(getApplicationContext(), HomePage.class);
                                    startActivity(intent);
                                }
                                if (type.equalsIgnoreCase("becomeAChef")) {
                                    if (!TextUtils.isEmpty(is_chef))
                                    {
                                        if (is_chef.equalsIgnoreCase("1"))
                                        {
                                            Intent intent = new Intent(getApplicationContext(), HomePage.class);
                                            startActivity(intent);
                                        }
                                        else
                                        {
                                            Intent intent = new Intent(getApplicationContext(), BecomeAChefSelectLanguage.class);
                                            startActivity(intent);
                                        }
                                    }
                                }
                            } else {
                                Intent intent = new Intent(getApplicationContext(), HomePage.class);
                                startActivity(intent);
                            }


                        }
                    }
                });

    }
    public void progressDismiss()
    {
        if (progressDialog.isShowing() && progressDialog!=null)
        {
            progressDialog.dismiss();
        }
    }

}
