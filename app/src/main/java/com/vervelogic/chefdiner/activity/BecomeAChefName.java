package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.util.List;

public class BecomeAChefName extends AppCompatActivity {

    /*---------------------------------Declaration----------------------------------*/
    EditText fName, lName;
    Button nextName;
    TextInputLayout fName_ly, lName_ly;
    UserDetail userDetail;
    String language;
    DbHelper dbHelper;
    Activity context = this;
    int user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_achef_name);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

     /*   // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/

        /*-------------------------------------Shared preference def----------------------*/
        SharedPreference sharedPreference = new SharedPreference();

        /*------------------getting user id from shared pref-------------------------------*/
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            language = sharedPreference.getLanguage(context);
            user_id = Integer.parseInt(sharedPreference.getUserId(context));
        } else {
            language = "";
            user_id = 0;
        }
        /*------------------------------------IDS-------------------------------------------------*/
        fName = (EditText) findViewById(R.id.first_name_chef);
        lName = (EditText) findViewById(R.id.last_name_chef);
        fName_ly = (TextInputLayout) findViewById(R.id.first_name_ly);
        lName_ly = (TextInputLayout) findViewById(R.id.last_name_ly);
        nextName = (Button) findViewById(R.id.next2);

        Intent intent = getIntent();
        language = intent.getStringExtra("language");

        nextName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validate()) {
                    return;
                }
            }
        });

        /*----------------------------------DB Access------------------------------------------------*/
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {

            fName.setText(cn.getFirstName());
            lName.setText(cn.getLastName());

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validate() {
        boolean valid = true;
        String firstName = fName.getText().toString();
        String lastName = lName.getText().toString();

        if (firstName.isEmpty()) {
            fName.setError(getResources().getString(R.string.enter_first_name));
            fName_ly.setFocusable(true);
        } else if (lastName.isEmpty()) {
            lName.setError(getResources().getString(R.string.enter_last_name));
            lName_ly.setFocusable(true);
        } else {
            Intent intent = new Intent(getApplicationContext(), BecomeAChefLanguageChoose.class);
            intent.putExtra("firstName", firstName);
            intent.putExtra("lastName", lastName);
            intent.putExtra("language", language);
            startActivity(intent);
        }
        return valid;
    }
}
