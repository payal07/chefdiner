package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.adapter.HorizontalChooseMenuAdapter;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.ChooseSubMenuModel;
import com.vervelogic.chefdiner.model.RecyclerTouchListener;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChooseAMenu extends AppCompatActivity {

    private static final String TAG = ChooseAMenu.class.getSimpleName();
    /*--------------------------------------Declarations-----------------------------------------*/
    Toolbar toolbar;
    RecyclerView recyclerView;
    HorizontalChooseMenuAdapter horizontalChooseMenuAdapter;
    Button next;
    List<ChooseSubMenuModel> horizontalScrollModels = new ArrayList<>();
    ChooseSubMenuModel googleLoginModel;
    String language;
    TextView no_image_available;
    String menu_title, menu_id = "", menu_price, menu_currency;
    Activity context = this;
    String user_id = "", chef_id, timeStr, count, reservationType, dateStr;
    DbHelper dbHelper;
    private SharedPreference sharedPreference;
     ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_amenu);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.request_a_book);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        language = intent.getStringExtra("language");
        chef_id = intent.getStringExtra("chef_id");
        timeStr = intent.getStringExtra("time");
        count = intent.getStringExtra("count");
        reservationType = intent.getStringExtra("reservationType");
        dateStr = intent.getStringExtra("date");

        progressDialog = new ProgressDialog(this);
        /*---------------------------------------------------IDs------------------------------*/
        recyclerView = (RecyclerView) findViewById(R.id.horizontal_recycler_view);
        next = (Button) findViewById(R.id.nextChooseMenu);
        no_image_available = (TextView) findViewById(R.id.no_data);

        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            user_id = sharedPreference.getUserId(context);
        } else {
            user_id = "";
        }


      /*  dbHelper=new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllBookingDetails();
        for (UserDetail cn : contacts) {
            menu_id=cn.getMenuId();
            menu_title=cn.getMenuTitle();
            menu_price=cn.getMenuPrice();
            menu_currency=cn.getCurrency();
            chef_id=cn.getChef_id();

        }
        Log.e("menu",""+menu_id+"\n"+menu_currency+"\n"+menu_price+"\n"+menu_title);
        Log.e("Choose",""+chef_id);*/
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new android.support.v7.app.AlertDialog.Builder(ChooseAMenu.this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            menuSubMenuAPI();
        }


        /*-------------------------------Recycler View--------------------------------------------*/

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Log.e("Menu", "" + googleLoginModel.getMenusSubmenus().get(position).get(0).getMenu_title());

                menu_id = googleLoginModel.getMenusSubmenus().get(position).get(0).getMenu_id();
                menu_title = googleLoginModel.getMenusSubmenus().get(position).get(0).getMenu_title();
                menu_price = String.valueOf(googleLoginModel.getMenusSubmenus().get(position).get(0).getMenu_price());
                menu_currency = googleLoginModel.getMenusSubmenus().get(position).get(0).getCurrency();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        /*-------------------------------Listener-----------------------------------------------------*/
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!menu_id.equalsIgnoreCase("")) {
                    Intent intent = new Intent(getApplicationContext(), DeliveryType.class);
                    intent.putExtra("language", language);
                    intent.putExtra("chef_id", chef_id);
                    intent.putExtra("timeStr", timeStr);
                    intent.putExtra("count", count);
                    intent.putExtra("menu_id", menu_id);
                    intent.putExtra("date", dateStr);
                    intent.putExtra("menu_title", menu_title);
                    intent.putExtra("menu_price", menu_price);
                    intent.putExtra("menu_currency", menu_currency);
                    intent.putExtra("reservationType", reservationType);
                    startActivity(intent);
                } else {
                    Toast.makeText(ChooseAMenu.this, getResources().getString(R.string.select_menu), Toast.LENGTH_SHORT).show();
                }

            }
        });
        //setData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*--------------------------------------------API MENU--------------------------------------------------------------*/
    public void progressDismiss()
    {
        if (progressDialog.isShowing() && progressDialog!=null)
        {
            progressDialog.dismiss();
        }
    }
    public void menuSubMenuAPI() {
        if (getApplicationContext()!=null)
        {
            progressDialog.setMessage(getResources().getString(R.string.please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        String uri = String.format(AppConfig.GET_MENU_SUBMENU_URL + "?user_id=%1$s", chef_id);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.GET_MENU_SUBMENU_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDismiss();
                        try {
                            Log.e(TAG, "" + s);
                            googleLoginModel = new Gson().fromJson(s, ChooseSubMenuModel.class);

                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {

                                LinearLayoutManager horizontalLayoutManager
                                        = new LinearLayoutManager(ChooseAMenu.this, LinearLayoutManager.HORIZONTAL, false);
                                recyclerView.setLayoutManager(horizontalLayoutManager);

                                recyclerView.setItemAnimator(new DefaultItemAnimator());

                                horizontalChooseMenuAdapter = new HorizontalChooseMenuAdapter(getApplicationContext(), googleLoginModel.getMenusSubmenus());
                                recyclerView.setAdapter(horizontalChooseMenuAdapter);

                                horizontalChooseMenuAdapter.notifyDataSetChanged();
                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                no_image_available.setVisibility(View.VISIBLE);
                                no_image_available.setText(message);
                                recyclerView.setVisibility(View.GONE);
                                if (getApplicationContext()!=null)
                                Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDismiss();
                            if (getApplicationContext()!=null)
                                Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                            progressDismiss();
                            if (getApplicationContext()!=null)
                                Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDismiss();
                            if (getApplicationContext()!=null)
                                Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }if (getApplicationContext()!=null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //  String image = getStringImage(bitmap);
                map.put("user_id", chef_id);
                map.put("main_user_id", user_id);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

}
