package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.List;

public class BecomeAChefMealType extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {

    CheckBox brunch, lunch, dinner;
    TextView brunchTime, lunchTime, dinnerTime;
    Button next11;
    String brunchStr, lunchStr, dinnerStr;
    String langaugeList, language, firstName, lastName, phoneNumber, oftenCook, country, city, noOfGuestMin, noOfGuestMax, kitchenTitle, kitchenDesc, serviceList, country_code, latitude, longitude, selectedCurrency;
    DbHelper dbHelper;
    String user_id, lunchTimeDB = "", brunchTimeDB = "", dinnerTimeDB = "";
    UserDetail userDetail;
    Activity context = this;
    private int TIME_VALUE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_achef_meal_type);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
/*
        // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/

        /*-----------------------------Data fetch--------------------------*/
        //Shared preference def
        SharedPreference sharedPreference = new SharedPreference();
        //Getting data from intent
        final Intent intent = getIntent();
        langaugeList = intent.getStringExtra("LanguageList");
        language = intent.getStringExtra("language");
        firstName = intent.getStringExtra("firstName");
        lastName = intent.getStringExtra("lastName");
        phoneNumber = intent.getStringExtra("phoneNumberChef");
        oftenCook = intent.getStringExtra("Often Cook");
        country = intent.getStringExtra("country");
        city = intent.getStringExtra("city");
        noOfGuestMin = intent.getStringExtra("minGuest");
        noOfGuestMax = intent.getStringExtra("maxGuest");
        kitchenTitle = intent.getStringExtra("kitchenTitle");
        kitchenDesc = intent.getStringExtra("kitchenDesc");
        serviceList = intent.getStringExtra("serviceList");
        country_code = intent.getStringExtra("country_code");
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        selectedCurrency = intent.getStringExtra("selectedCurrency");

        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            //language = sharedPreference.getLanguage(context);
            user_id = sharedPreference.getUserId(context);
        } else {
            user_id = "";
        }



        /*----------------------------ID--------------------------------*/
        brunch = (CheckBox) findViewById(R.id.brunch_checkbox);
        lunch = (CheckBox) findViewById(R.id.lunch_checkbox);
        dinner = (CheckBox) findViewById(R.id.dinner_checkbox);
        brunchTime = (TextView) findViewById(R.id.brunch_time);
        lunchTime = (TextView) findViewById(R.id.lunch_time);
        dinnerTime = (TextView) findViewById(R.id.dinner_time);
        next11 = (Button) findViewById(R.id.next11);


        /*--------------------------DB-------------------------------*/
        //Database
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            lunchTimeDB = cn.getLunch();
            brunchTimeDB = cn.getBrunch();
            dinnerTimeDB = cn.getDinner();
        }

        if (!TextUtils.isEmpty(lunchTimeDB)) {
            lunch.setChecked(true);
            lunchTime.setEnabled(true);
            lunchTime.setText(lunchTimeDB);
        }
        if (!TextUtils.isEmpty(brunchTimeDB)) {
            brunch.setChecked(true);
            brunchTime.setEnabled(true);
            brunchTime.setText(brunchTimeDB);
        }
        if (!TextUtils.isEmpty(dinnerTimeDB)) {
            dinner.setChecked(true);
            dinnerTime.setEnabled(true);
            dinnerTime.setText(dinnerTimeDB);
        }

        /*-------------------Listener-----------------------*/
        next11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!brunch.isChecked() && !lunch.isChecked() && !dinner.isChecked()) {
                    Toast.makeText(context, getResources().getString(R.string.select_time_zone), Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent1 = new Intent(getApplicationContext(), BecomeAChefUploadPhoto.class);
                    intent1.putExtra("Often Cook", oftenCook);
                    intent1.putExtra("LanguageList", langaugeList);
                    intent1.putExtra("language", language);
                    intent1.putExtra("firstName", firstName);
                    intent1.putExtra("lastName", lastName);
                    intent1.putExtra("phoneNumberChef", phoneNumber);
                    intent1.putExtra("country", country);
                    intent1.putExtra("city", city);
                    intent1.putExtra("minGuest", noOfGuestMin);
                    intent1.putExtra("maxGuest", noOfGuestMax);
                    intent1.putExtra("kitchenTitle", kitchenTitle);
                    intent1.putExtra("kitchenDesc", kitchenDesc);
                    intent1.putExtra("serviceList", serviceList);
                    intent1.putExtra("country_code", country_code);
                    intent1.putExtra("latitude", latitude);
                    intent1.putExtra("longitude", longitude);
                    intent1.putExtra("brunch", brunchTimeDB);
                    intent1.putExtra("lunch", lunchTimeDB);
                    intent1.putExtra("dinner", dinnerTimeDB);
                    intent1.putExtra("selectedCurrency", selectedCurrency);
                    startActivity(intent1);
                }
            }
        });
    }

    public void validate(View view) {
        if (brunch.isChecked()) {

            brunchTime.setEnabled(true);
            brunchTime.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    TIME_VALUE = 0;
                    Calendar now = Calendar.getInstance();
                    TimePickerDialog timepickerdialog = TimePickerDialog.newInstance(BecomeAChefMealType.this,
                            now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
                    timepickerdialog.setThemeDark(false); //Dark Theme?
                    timepickerdialog.vibrate(false); //vibrate on choosing time?
                    timepickerdialog.dismissOnPause(false); //dismiss the dialog onPause() called?
                    timepickerdialog.enableSeconds(true); //show seconds?

                    //Handling cancel event
                    timepickerdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            Toast.makeText(BecomeAChefMealType.this, getResources().getString(R.string.cancel), Toast.LENGTH_SHORT).show();
                        }
                    });
                    timepickerdialog.show(getFragmentManager(), "Timepickerdialog"); //show time picker dialog
                }
            });
        } else {
            brunchTime.setEnabled(false);
        }
        if (lunch.isChecked()) {

            lunchTime.setEnabled(true);
            lunchTime.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    TIME_VALUE = 1;
                    Calendar now = Calendar.getInstance();
                    TimePickerDialog timepickerdialog = TimePickerDialog.newInstance(BecomeAChefMealType.this,
                            now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
                    timepickerdialog.setThemeDark(false); //Dark Theme?
                    timepickerdialog.vibrate(false); //vibrate on choosing time?
                    timepickerdialog.dismissOnPause(false); //dismiss the dialog onPause() called?
                    timepickerdialog.enableSeconds(true); //show seconds?

                    //Handling cancel event
                    timepickerdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            Toast.makeText(BecomeAChefMealType.this, getResources().getString(R.string.cancel), Toast.LENGTH_SHORT).show();
                        }
                    });
                    timepickerdialog.show(getFragmentManager(), "Timepickerdialog"); //show time picker dialog

                }
            });
        } else {
            lunchTime.setEnabled(false);
        }
        if (dinner.isChecked()) {

            dinnerTime.setEnabled(true);
            dinnerTime.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    TIME_VALUE = 2;
                    Calendar now = Calendar.getInstance();
                    TimePickerDialog timepickerdialog = TimePickerDialog.newInstance(BecomeAChefMealType.this,
                            now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
                    timepickerdialog.setThemeDark(false); //Dark Theme?
                    timepickerdialog.vibrate(false); //vibrate on choosing time?
                    timepickerdialog.dismissOnPause(false); //dismiss the dialog onPause() called?
                    timepickerdialog.enableSeconds(true); //show seconds?

                    //Handling cancel event
                    timepickerdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            Toast.makeText(BecomeAChefMealType.this, getResources().getString(R.string.cancel), Toast.LENGTH_SHORT).show();
                        }
                    });
                    timepickerdialog.show(getFragmentManager(), "Timepickerdialog"); //show time picker dialog
                }
            });

        } else {
            dinnerTime.setEnabled(false);
        }
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String time = hourOfDay + ":" + minute;
        if (TIME_VALUE == 0) {
            brunchTime.setText(time);
            brunchStr = time;
            brunchTimeDB = time;
        }
        if (TIME_VALUE == 1) {
            lunchTime.setText(time);
            lunchStr = time;
            lunchTimeDB = time;
        }
        if (TIME_VALUE == 2) {
            dinnerTime.setText(time);
            dinnerStr = time;
            dinnerTimeDB = time;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
