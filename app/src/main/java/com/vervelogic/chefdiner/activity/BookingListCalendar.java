package com.vervelogic.chefdiner.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;
import com.stacktips.view.DayDecorator;
import com.stacktips.view.DayView;
import com.stacktips.view.utils.CalendarUtils;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.model.CalendarModelDateAvail;
import com.vervelogic.chefdiner.model.GetDateModel;
import com.vervelogic.chefdiner.model.GuestListModel;
import com.vervelogic.chefdiner.model.RecyclerTouchListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class BookingListCalendar extends AppCompatActivity {

    CustomCalendarView calendarView;
    Toolbar toolbar;
    String Chef_id, d, selectedDate;
    List<String> GetchefCalendarBean = new ArrayList<>();
    List<String> dateAvail = new ArrayList<>();
    List<GuestListModel> guestListModels = new ArrayList<>();
    RecyclerView guest_list_recyclerView;
    List<GetDateModel.GetchefCalendarBean> getchefCalendarBeans = new ArrayList<>();

    TextView name, email, noOfGuestAcceptReject, timeAndMeal, menuType, serviceType, priceTotal;
    Button accept, reject, completed;
    ToggleButton take_order_on_date;
    GuestListAdapter guestListAdapter;
    Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_list_calendar);
        //   setContentView(R.layout.activity_booking_list_calendar);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.manage_booking);

        Intent intent = getIntent();
        Chef_id = intent.getStringExtra("Chef_id");
        calendarView = (CustomCalendarView) findViewById(R.id.calendar_view);
        guest_list_recyclerView = (RecyclerView) findViewById(R.id.guest_list_recyclerView);
        take_order_on_date = (ToggleButton) findViewById(R.id.take_order_on_date);
        //Initialize calendar with date

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            Log.e("", "No Internet connection");
        } else {
            getCalDateAPI();
        }
        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);
        take_order_on_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (take_order_on_date.isChecked()) {
                    if (!TextUtils.isEmpty(selectedDate)) {
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            Log.e("", "No Internet connection");
                        } else {

                            calendarDateAPI(selectedDate, "1");
                        }

                    } else {
                        Toast.makeText(BookingListCalendar.this, getResources().getString(R.string.select_another_date), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (!TextUtils.isEmpty(selectedDate)) {
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            Log.e("", "No Internet connection");
                        } else {

                            calendarDateAPI(selectedDate, "2");
                        }
                    } else {
                        Toast.makeText(BookingListCalendar.this, getResources().getString(R.string.select_another_date), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        //Handling custom calendar events
        calendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String d = df.format(date);
                for (int i = 0; i < getchefCalendarBeans.size(); i++) {
                    if (d.equalsIgnoreCase(getchefCalendarBeans.get(i).getDate())) {
                        if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("1")) {
                            // dayView.setBackgroundResource(R.drawable.green_circle);
                            take_order_on_date.setChecked(true);
                            take_order_on_date.setEnabled(true);
                        } else if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("0")) {
                        /*dayView.setBackgroundResource(R.drawable.red_circle);*/
                            take_order_on_date.setEnabled(false);
                            take_order_on_date.setChecked(false);
                        } else if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("2")) {
                            take_order_on_date.setEnabled(false);
                            take_order_on_date.setChecked(false);
                        }
                    }
                }

                if (CalendarUtils.isPastDay(date)) {
                    selectedDate = "";
                    //  Toast.makeText(BookingListCalendar.this, "Disabled Date", Toast.LENGTH_SHORT).show();
                } else {
                    selectedDate = df.format(date);
                    //  Toast.makeText(BookingListCalendar.this, df.format(date), Toast.LENGTH_SHORT).show();
                }
                if (CalendarUtils.isPastDay(date)) {
                    d = df.format(date);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        Log.e("", "No Internet connection");
                    } else {
                        getBookingListAPI(d);
                    }

                    //  Toast.makeText(BookingListCalendar.this, "Disabled Date", Toast.LENGTH_SHORT).show();
                } else {
                   /* int color = getResources().getColor(R.color.blue);
                   calendarView.setba(color);*/
                    d = df.format(date);
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        Log.e("", "No Internet connection");
                    } else {
                        getBookingListAPI(d);
                    }
                    //Toast.makeText(BookingListCalendar.this, df.format(date), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onMonthChanged(Date date) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("MM-yyyy");
                Toast.makeText(BookingListCalendar.this, df.format(date), Toast.LENGTH_SHORT).show();
            }
        });

       /* //Setting custom font
        final Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Arch_Rival_Bold.ttf");
        if (null != typeface) {
            calendarView.setCustomTypeface(typeface);
            calendarView.refreshCalendar(currentCalendar);
        }*/

        //adding calendar day decorators
        List<DayDecorator> decorators = new ArrayList<>();
        // decorators.add(new DisabledColorDecorator());
        decorators.add(new ColorDecorator());
        calendarView.setDecorators(decorators);
        calendarView.refreshCalendar(currentCalendar);

        guestListAdapter = new GuestListAdapter(guestListModels);

        guest_list_recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), guest_list_recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //  HorizontalScrollModel movie=horizontalScrollModels.get(position);
                //   Toast.makeText(RequestABook.this, "", Toast.LENGTH_SHORT).show();
                GuestListModel timeModel = guestListModels.get(position);
                // timeStr = timeModel.getTime();
                final Dialog dialog = new Dialog(BookingListCalendar.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.reject_accept_screen);

                name = dialog.findViewById(R.id.name);
                email = dialog.findViewById(R.id.email);
                noOfGuestAcceptReject = dialog.findViewById(R.id.noOfGuestAcceptReject);
                timeAndMeal = dialog.findViewById(R.id.timeAndMeal);
                menuType = dialog.findViewById(R.id.menuType);
                serviceType = dialog.findViewById(R.id.serviceTypeText);
                accept = dialog.findViewById(R.id.accept);
                reject = dialog.findViewById(R.id.reject);
                completed = dialog.findViewById(R.id.completed);
                priceTotal = dialog.findViewById(R.id.priceTotal);

                name.setText(timeModel.getGetBookingList().get(position).getFirst_name() + " " + timeModel.getGetBookingList().get(position).getFirst_name());
                email.setText(timeModel.getGetBookingList().get(position).getEmail());
                noOfGuestAcceptReject.setText(timeModel.getGetBookingList().get(position).getTotal_seats());
                timeAndMeal.setText(timeModel.getGetBookingList().get(position).getTime() + ", " + timeModel.getGetBookingList().get(position).getMenu_title());
                menuType.setText(timeModel.getGetBookingList().get(position).getMenu_title());
                serviceType.setText(timeModel.getGetBookingList().get(position).getService_type());
                priceTotal.setText(timeModel.getGetBookingList().get(position).getMenu_currency() + " " + timeModel.getGetBookingList().get(position).getMenu_price());

                String bookingStatus = timeModel.getGetBookingList().get(position).getBooking_status();
                final String bookingId = timeModel.getGetBookingList().get(position).getBooknow_id();

                String dateStr = timeModel.getGetBookingList().get(position).getDate();
                Calendar c = Calendar.getInstance();
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                int result = formattedDate.compareTo(dateStr);
                Log.e("Date", "" + dateStr);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date strDate = null;
                if (!TextUtils.isEmpty(bookingStatus)) {
                    if (bookingStatus.equalsIgnoreCase("Accepted")) {
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);
                        // completed.setVisibility(View.VISIBLE);
                    }
                }
                try {
                    strDate = df.parse(dateStr);
                    Log.e("Payal", "" + strDate);
                    long yourmilliseconds = System.currentTimeMillis();
                    Date resultdate = new Date(yourmilliseconds);
                    String currentDate = df.format(resultdate);
                    Date d = df.parse(currentDate);

                    if (d.getDate() > strDate.getDate()) {
                        completed.setVisibility(View.VISIBLE);
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);
                        completed.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String bookingStatus = "completed";
                                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    Log.e("", "No Internet connection");
                                } else {
                                    completedAPI(bookingId, bookingStatus);
                                }

                                dialog.dismiss();
                            }
                        });
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                final String date = timeModel.getGetBookingList().get(position).getDate();
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        String bookingStatus = "Accepted";
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            Log.e("", "No Internet connection");
                        } else {
                            acceptRejectAPI(bookingId, bookingStatus, date);
                        }

                    }
                });
                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        String bookingStatus = "Rejected";
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            Log.e("", "No Internet connection");
                        } else {

                            acceptRejectAPI(bookingId, bookingStatus, date);
                        }
                    }
                });
                dialog.show();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }

    public void calendarDateAPI(final String date, final String availStatus) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.CALENDAR_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("BookingListCalendar", "" + s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            CalendarModelDateAvail paymentModel = new Gson().fromJson(s, CalendarModelDateAvail.class);
                            if (status == 1) {
                                Log.e("BookingListCalendar", "" + s);
                                String s1 = paymentModel.getChefCalendar().get(0).getDate();
                                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    Log.e("", "No Internet connection");
                                } else {
                                    getCalDateAPI();
                                    getBookingListAPI(s1);
                                }


                                // webView.setVisibility(View.GONE);
                                // ---------------------------custom dialog---------------------------------

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getApplicationContext() != null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("BookingListCalendar", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("calendar_id", "");
                map.put("chef_id", Chef_id);
                map.put("date", date);
                map.put("ava_status", availStatus);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getCalDateAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.CALENDAR_DETAILS_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Class", "" + s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            GetDateModel paymentModel = new Gson().fromJson(s, GetDateModel.class);
                            if (status == 1) {

                                Log.e("D", "" + s);
                                for (int i = 0; i < paymentModel.getGetchefCalendar().size(); i++) {
                                    getchefCalendarBeans.add(paymentModel.getGetchefCalendar().get(i));
                                    GetchefCalendarBean.add(paymentModel.getGetchefCalendar().get(i).getDate());
                                    dateAvail.add(paymentModel.getGetchefCalendar().get(i).getAva_status());
                                    calendarView.refreshCalendar(currentCalendar);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getApplicationContext() != null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("Error", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", Chef_id);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void completedAPI(final String bookingId, final String bookingStatus) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.COMPLETED_DINNER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Class", "" + s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                Log.e("D", "" + s);
                                CalendarModelDateAvail paymentModel = new Gson().fromJson(s, CalendarModelDateAvail.class);
                                if (status == 1) {
                                    Log.e("BookingListCalendar", "" + s);
                                    //  String s1 = paymentModel.getChefCalendar().get(0).getDate();
                                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                    if (netInfo == null) {
                                        Log.e("", "No Internet connection");
                                    } else {
                                        getBookingListAPI(d);
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getApplicationContext() != null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("Error", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", Chef_id);
                map.put("bookingid", bookingId);
                map.put("booking_status", bookingStatus);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void getBookingListAPI(final String date) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.GET_BOOKING_DETAILS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Class", "" + s);
                        guestListModels.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            GuestListModel paymentModel = new Gson().fromJson(s, GuestListModel.class);
                            if (status == 1) {
                                guestListModels.add(paymentModel);
                                Log.e("D", "" + s);
                                /*for (int i=0;i<paymentModel.getGetBookingList().size();i++) {*/
                                LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(BookingListCalendar.this, LinearLayoutManager.VERTICAL, false);
                                guest_list_recyclerView.setLayoutManager(horizontalLayoutManagaer);

                                guest_list_recyclerView.setItemAnimator(new DefaultItemAnimator());
                                guest_list_recyclerView.setAdapter(guestListAdapter);
                                guestListAdapter.notifyDataSetChanged();
                               /* }*/
                            }
                            if (status == 0) {
                               /* guestListModels.clear();*/
                                guestListAdapter.notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getApplicationContext() != null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("Error", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", Chef_id);
                map.put("date", date);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void acceptRejectAPI(final String booknow_id, final String booking_status, final String d) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.ACCEPT_REJECT_GUEST_DETAILS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Class", "" + s);
                        guestListModels.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            //GuestListModel paymentModel = new Gson().fromJson(s, GuestListModel.class);
                            if (status == 1) {

                                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    Log.e("", "No Internet connection");
                                } else {

                                    getBookingListAPI(d);
                                }
                            }

                                /*guestListModels.add(paymentModel);
                                Log.e("D", "" + s);
                                *//*for (int i=0;i<paymentModel.getGetBookingList().size();i++) {*//*
                                LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(BookingListCalendar.this, LinearLayoutManager.VERTICAL, false);
                                guest_list_recyclerView.setLayoutManager(horizontalLayoutManagaer);

                                guest_list_recyclerView.setItemAnimator(new DefaultItemAnimator());
                                guest_list_recyclerView.setAdapter(guestListAdapter);
                                guestListAdapter.notifyDataSetChanged();*/
                               /* }*/

                            if (status == 0) {
                               /* guestListModels.clear();*/
                                guestListAdapter.notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getApplicationContext() != null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("Error", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", Chef_id);
                map.put("date", d);
                map.put("booknow_id", booknow_id);
                map.put("booking_status", booking_status);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private class ColorDecorator implements DayDecorator {
        @SuppressLint("ResourceType")
        @Override
        public void decorate(DayView dayView) {

            @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            String date = df.format(dayView.getDate());


            if (CalendarUtils.isPastDay(dayView.getDate())) {
                int color = Color.parseColor("#a9afb9");
                dayView.setTextColor(color);
            }
           /* for (int i = 0; i < getchefCalendarBeans.size(); i++) {
                if (date.equalsIgnoreCase(getchefCalendarBeans.get(i).getDate())) {
                    if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("1")) {
                        int color = Color.parseColor("#5ec639");
                        dayView.setBackgroundColor(color);
                        dayView.setBackgroundResource(R.drawable.green_circle);
                        dayView.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        int color = Color.parseColor("#ff0000");
                        dayView.setBackgroundColor(color);
                        dayView.setBackgroundResource(R.drawable.red_circle);
                        dayView.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            }*/
            int color = 0;
            for (int i = 0; i < getchefCalendarBeans.size(); i++) {
                if (date.equalsIgnoreCase(getchefCalendarBeans.get(i).getDate())) {

                    if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("2")) {
                        try {
                            Date d = df.parse(getchefCalendarBeans.get(i).getDate());
                            if (CalendarUtils.isPastDay(d)) {
                                color = getResources().getColor(R.color.filter_color);
                                dayView.setTextColor(color);
                                dayView.setBackgroundResource(R.drawable.ic_substract);
                            } else {
                                dayView.setTextColor(getResources().getColor(R.color.green));
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        // dayView.setBackgroundResource(R.drawable.green_circle);

                    } else if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("0")) {
                        dayView.setBackgroundResource(R.drawable.red_circle);
                        dayView.setTextColor(getResources().getColor(R.color.filter_color));
                        dayView.setBackgroundResource(R.drawable.ic_substract);
                    }
                }
            }

            /*for (String d:GetchefCalendarBean)
            {
                if (!TextUtils.isEmpty(d))
                {
                    if (date.equalsIgnoreCase(d))
                    {

                        for (String ava:dateAvail)
                        {
                            if (!TextUtils.isEmpty(ava))
                            {
                                if (ava.equalsIgnoreCase("1"))
                                {
                                    Log.e("D1",""+d);
                                    Log.e("AVA","1");
                                    int color = Color.parseColor("#5ec639");
                                    dayView.setTextColor(color);
                                }
                                else *//*if (ava.equalsIgnoreCase("0"))*//*
                                {
                                    Log.e("D0",""+d);
                                    Log.e("AVA","0");
                                    int color = Color.parseColor("#a9afb9");
                                    dayView.setTextColor(color);
                                }
                            }
                        }

                    }
                }

            }*/
            /*if (dayView.getDate().getDate()==1 || dayView.getDate().getDate()==2 || dayView.getDate().getDate()==9)
            {

                int color = Color.parseColor("#9fffc2");
                dayView.setBackgroundColor(color);
            }*/
        }
    }

    public class GuestListAdapter extends RecyclerView.Adapter<GuestListAdapter.MyViewHolder> {

        private List<GuestListModel> timeModels = new ArrayList<>();
        private int row_index = -1;

        public GuestListAdapter(List<GuestListModel> timeModels) {
            this.timeModels = timeModels;
        }

        @Override
        public GuestListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.guest_list_screen, parent, false);
            return new GuestListAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(GuestListAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
            final GuestListModel timeModel = timeModels.get(position);
            holder.guestName.setText(timeModel.getGetBookingList().get(position).getBooking_status());
            holder.guestMenuName.setText(timeModel.getGetBookingList().get(position).getMenu_title());
            holder.noOfGuestCalendar.setText(timeModel.getGetBookingList().get(position).getTotal_seats());

         /*   holder.cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    row_index = position;
                    Log.e("Cancel","Cancelled");
                    notifyDataSetChanged();
                    notifyItemChanged(row_index);
                    String bookingId=timeModel.getGetBookingList().get(position).getBooknow_id();
                    String bookingStatus="rejected";
                    String date=timeModel.getGetBookingList().get(position).getDate();
                    //String chefId=timeModel.getGetBookingList().get(position).getChef_id();
                    acceptRejectAPI(bookingId,bookingStatus,date);

                }
            });
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    row_index = position;
                    Log.e("Confirm","Confirmed");
                    notifyDataSetChanged();
                    notifyItemChanged(row_index);
                    String bookingId=timeModel.getGetBookingList().get(position).getBooknow_id();
                    String bookingStatus="accepted";
                    String date=timeModel.getGetBookingList().get(position).getDate();
                    //String chefId=timeModel.getGetBookingList().get(position).getChef_id();
                    acceptRejectAPI(bookingId,bookingStatus,date);


                }
            });*/
        /*holder.guest_ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                row_index = position;

                notifyDataSetChanged();
                notifyItemChanged(row_index);

            }
        });*/
        /*if (row_index == position) {
            holder.text_ly.setBackgroundResource(R.drawable.custom_button);
        } else {
            Log.e("TAGIndex", "" + row_index);
            holder.text_ly.setBackgroundResource(R.color.radio_btn);
        }*/
        }

        @Override
        public int getItemCount() {
            return timeModels.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView guestName, guestMenuName, noOfGuestCalendar;
            /* ImageView cancel,confirm;*/
            LinearLayout guest_ly;
            private SparseBooleanArray selectedItems = new SparseBooleanArray();

            MyViewHolder(View itemView) {
                super(itemView);
                guestName = itemView.findViewById(R.id.bookingStatus);
                guestMenuName = itemView.findViewById(R.id.guestMenuName);
                noOfGuestCalendar = itemView.findViewById(R.id.noOfGuestCalendar);
               /* cancel=itemView.findViewById(R.id.cancelGuest);
                confirm=itemView.findViewById(R.id.confirm);*/
                guest_ly = itemView.findViewById(R.id.guest_ly);
            }
        }
    }

}
