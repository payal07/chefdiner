package com.vervelogic.chefdiner.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;

import java.util.List;

public class BecomeAChefCook extends AppCompatActivity implements View.OnClickListener {

    /*-------------------------Declarations----------------------------*/
    RadioGroup cook_often_group;
    RadioButton once_a_week, a_couple_times_a_week, everyday;
    String langaugeList, language, firstName, lastName, phoneNumber;

    DbHelper dbHelper;
    String oftenCookStr = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_achef_cook);

        /*------------Toolbar------------------------------------*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

       /* // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/

       /*-----------------------IDS---------------------------------*/
        cook_often_group = (RadioGroup) findViewById(R.id.often_cook_chef);
        once_a_week = (RadioButton) findViewById(R.id.once_a_week);
        a_couple_times_a_week = (RadioButton) findViewById(R.id.a_couple_times_a_week);
        everyday = (RadioButton) findViewById(R.id.everyday);
        /*-----------------Getting data from intent-----------------------*/
        Intent intent = getIntent();
        langaugeList = intent.getStringExtra("LanguageList");
        language = intent.getStringExtra("language");
        firstName = intent.getStringExtra("firstName");
        lastName = intent.getStringExtra("lastName");
        phoneNumber = intent.getStringExtra("phoneNumberChef");

        /*-----------------------Listeners------------------------------*/
        everyday.setOnClickListener(this);
        once_a_week.setOnClickListener(this);
        a_couple_times_a_week.setOnClickListener(this);

        /*-----------------------DB access-----------------------------*/
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            oftenCookStr = cn.getOftenCook();
        }
        if (!TextUtils.isEmpty(oftenCookStr)) {
            if (oftenCookStr.equalsIgnoreCase("Once a week")) {
                once_a_week.setChecked(true);
            }
            if (oftenCookStr.equalsIgnoreCase("Everyday")) {
                everyday.setChecked(true);
            }
            if (oftenCookStr.equalsIgnoreCase("A couple times a week")) {
                a_couple_times_a_week.setChecked(true);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.once_a_week:
                Intent intent = new Intent(getApplicationContext(), BecomAChefPlaceLocated.class);
                intent.putExtra("Often Cook", "Once a week");
                intent.putExtra("LanguageList", langaugeList);
                intent.putExtra("language", language);
                intent.putExtra("firstName", firstName);
                intent.putExtra("lastName", lastName);
                intent.putExtra("phoneNumberChef", phoneNumber);
                startActivity(intent);
                break;
            case R.id.everyday:
                Intent intent1 = new Intent(getApplicationContext(), BecomAChefPlaceLocated.class);
                intent1.putExtra("Often Cook", "Everyday");
                intent1.putExtra("LanguageList", langaugeList);
                intent1.putExtra("language", language);
                intent1.putExtra("firstName", firstName);
                intent1.putExtra("lastName", lastName);
                intent1.putExtra("phoneNumberChef", phoneNumber);
                startActivity(intent1);
                break;
            case R.id.a_couple_times_a_week:
                Intent intent2 = new Intent(getApplicationContext(), BecomAChefPlaceLocated.class);
                intent2.putExtra("Often Cook", "A couple times A week");
                intent2.putExtra("LanguageList", langaugeList);
                intent2.putExtra("language", language);
                intent2.putExtra("firstName", firstName);
                intent2.putExtra("lastName", lastName);
                intent2.putExtra("phoneNumberChef", phoneNumber);
                startActivity(intent2);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
