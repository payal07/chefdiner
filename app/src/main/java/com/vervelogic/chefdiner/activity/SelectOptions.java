package com.vervelogic.chefdiner.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;

public class SelectOptions extends AppCompatActivity implements View.OnClickListener {

    /*--------------------------Declarations----------------------------*/
    Toolbar toolbar;
    Spinner city_name;
    ArrayList<String> CITY_NAME = new ArrayList<>();
    ArrayList<Integer> CITY_ID = new ArrayList<>();
    Button ok;
    TextView date;
    ImageView plus, minus;
    TextView numberOfGuest;
    int count = 0;
    String selectedCountry = "", selectedNoOfGuest = "", minimumPriceStr = "", maximumPriceStr = "", selectedDate = "", formattedDate = "";
    String language;
    private HorizontalCalendar horizontalCalendar;
    private ArrayList<String> countriesCodeList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_options);

        //------------------------------ ToolBar-----------------------
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

     /*   // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
*/
        SharedPreference sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(getApplicationContext()))) {
            language = sharedPreference.getLanguage(getApplicationContext());
            Log.e("Lang", "" + language);
            if (!TextUtils.isEmpty(language)) {
                if (language.equalsIgnoreCase("english")) {
                    Locale current = Locale.ENGLISH;
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
                if (language.equalsIgnoreCase("traditional chinese")) {
                    Locale current = Locale.TRADITIONAL_CHINESE;
                    Log.e("TAG", "" + current);

                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
                if (language.equalsIgnoreCase("simplified chinese")) {
                    Locale current = Locale.SIMPLIFIED_CHINESE;
                    Log.e("TAG", "" + current);
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
            }

        } else {
            language = "english";
            Locale current = Locale.ENGLISH;
            Locale.setDefault(current);
            Configuration config = new Configuration();
            config.locale = current;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        }

        final Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        final Calendar todayDate = Calendar.getInstance();
        final Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        final Calendar defaultDate = Calendar.getInstance();
        defaultDate.add(Calendar.MONTH, -1);
        defaultDate.add(Calendar.DAY_OF_WEEK, +5);

        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .datesNumberOnScreen(5)
                .dayNameFormat("EEE")
                .dayNumberFormat("dd")
                .monthFormat("MMM")
                .showDayName(true)
                .showMonthName(true)
                .defaultSelectedDate(todayDate.getTime())
                .textColor(Color.GRAY, Color.WHITE)
                // .selectorColor(R.color.button_color)
                .selectedDateBackground(Color.TRANSPARENT)
                .build();
        horizontalCalendar.goToday(false);

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date d, int position) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                formattedDate = df.format(d);
                if (d.equals(todayDate.getTime())) {
                    date.setText(R.string.today);
                    selectedDate = "" + DateFormat.getDateInstance().format(d);
                } else {
                    date.setText("" + DateFormat.getDateInstance().format(d));
                    selectedDate = "" + DateFormat.getDateInstance().format(d);
                    //  Toast.makeText(SelectOptions.this, DateFormat.getDateInstance().format(d) + " is selected!", Toast.LENGTH_SHORT).show();
                }

            }

        });

        // ------------------------------Ids-----------------------------
        city_name = (Spinner) findViewById(R.id.city);
        ok = (Button) findViewById(R.id.ok);
        date = (TextView) findViewById(R.id.date);
        plus = (ImageView) findViewById(R.id.plus);
        minus = (ImageView) findViewById(R.id.minus);
        numberOfGuest = (TextView) findViewById(R.id.number);
        date.setText(R.string.today);

        numberOfGuest.setText("0");

        //--------------------------listeners----------------------------
        plus.setOnClickListener(this);
        minus.setOnClickListener(this);


        //----------------------- get min and max text view-------------------------------
        final TextView tvMin = (TextView) findViewById(R.id.min);
        final TextView tvMax = (TextView) findViewById(R.id.max);
        /*----------------------------Price Range Seekbar-------------------------------*/
      /*  MultiSlider multiSlider5 = (MultiSlider) findViewById(R.id.range_slider5);

        multiSlider5.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                if (thumbIndex == 0) {
                    tvMin.setText("$" + value);
                } else {
                    tvMax.setText("$" + value);
                }
            }
        });*/

// get seekbar from view
        final CrystalRangeSeekbar rangeSeekbar = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbar1);


// set listener
        rangeSeekbar.setMaxValue(1000);
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tvMin.setText(String.valueOf(minValue));
                minimumPriceStr = "" + minValue;
                maximumPriceStr = "" + maxValue;
                //    tvMax.setText(String.valueOf(maxValue));
            }
        });
        /*---------------------Spinner listner---------------------------*/
        city_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCountry = CITY_NAME.get(position);
                // Log.e("Country", "onItemSelected: " + CITY_NAME.get(position) + "   " + position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        try {
            getCountries(new JSONArray(loadJSONFromAsset()));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedNoOfGuest = numberOfGuest.getText().toString();
                Intent intent = new Intent(getApplicationContext(), HomePage.class);
                intent.putExtra("selectedCountry", selectedCountry);
                intent.putExtra("minimumPriceStr", minimumPriceStr);
                intent.putExtra("maximumPriceStr", maximumPriceStr);
                intent.putExtra("selectedDate", selectedDate);
                intent.putExtra("selectedNoOfGuest", selectedNoOfGuest);
                intent.putExtra("formattedDate", formattedDate);
                startActivity(intent);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.plus:
                count++;
                Log.e("CounterPlus", "" + count);
                numberOfGuest.setText("" + count);
                break;
            case R.id.minus:
                if (count <= 0) {
                    //   Toast.makeText(this, "Not Allowed", Toast.LENGTH_LONG).show();
                } else {
                    count--;
                    Log.e("CounterMinus", "" + count);
                    numberOfGuest.setText("" + count);
                }
                break;

        }
    }

    /*----------------------JSON Load-----------------------------------------*/
    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getAssets().open("countries.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void getCountries(JSONArray j) {
        //Traversing through all the items in the json array
        for (int i = 0; i < j.length(); i++) {
            try {
                //Getting json object
                JSONObject json = j.getJSONObject(i);

                //Adding the name of the student to array list
                CITY_NAME.add(json.getString("name"));
                countriesCodeList.add(json.getString("dial_code"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //Setting adapter to show the items in the spinner
        ArrayAdapter arrayAdapter = new
                ArrayAdapter<>(SelectOptions.this, R.layout.spinner_text,
                CITY_NAME);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        city_name.setAdapter(arrayAdapter);

        city_name.setSelection(86);
    }
}
