package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.util.Locale;

public class SelectLanguage extends AppCompatActivity {

    /*------------------------Declarations----------------------------*/
    RadioGroup radioGroup;
    RadioButton languageButton;
    Button next;
    TextView skipForNow;
    Activity context = this;
    private SharedPreference sharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);

        // -----------------------Transparent toolbar-----------------------------
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

       /* // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/
        sharedPreference = new SharedPreference();
        /*---------------------IDS------------------------------------*/
        radioGroup = (RadioGroup) findViewById(R.id.language);
        skipForNow = (TextView) findViewById(R.id.skipLanguage);
        next = (Button) findViewById(R.id.next);

        /*-------------------------Listeners--------------------------------*/
        skipForNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Locale current = Locale.ENGLISH;
                Log.e("TAG", "" + current);

                Locale.setDefault(current);
                Configuration config = new Configuration();
                config.locale = current;
                getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                sharedPreference.saveLanguage(context, "english");
                Intent intent = new Intent(SelectLanguage.this, EatAndCook.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();
                languageButton = (RadioButton) findViewById(selectedId);

                //   String lang = "en";//Default Language
                String c = languageButton.getText().toString();
                if (c.equalsIgnoreCase("English") || c.equalsIgnoreCase("英语") || c.equalsIgnoreCase("英語")) {
                    Locale current = Locale.ENGLISH;
                    Log.e("TAG", "" + current);

                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                    sharedPreference.saveLanguage(context, "english");
                    Intent intent = new Intent(SelectLanguage.this, EatAndCook.class);
                    intent.putExtra("language", "english");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else if (c.equalsIgnoreCase("中文簡體")) {
                    Locale current = Locale.SIMPLIFIED_CHINESE;
                    Log.e("TAG", "" + current);
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                    sharedPreference.saveLanguage(context, "simplified chinese");
                    Intent intent = new Intent(SelectLanguage.this, EatAndCook.class);
                    intent.putExtra("language", "simplified chinese");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } else if (c.equalsIgnoreCase("中文繁體")) {
                    Locale current = Locale.TRADITIONAL_CHINESE;
                    Log.e("TAG", "" + current);

                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                    sharedPreference.saveLanguage(context, "traditional chinese");
                    Intent intent = new Intent(SelectLanguage.this, EatAndCook.class);
                    intent.putExtra("language", "traditional chinese");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
