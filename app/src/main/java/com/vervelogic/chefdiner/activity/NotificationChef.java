package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.app.InstagramApp;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NotificationChef extends AppCompatActivity {

    private static final String TAG = NotificationChef.class.getSimpleName();
    Toolbar toolbar;
    ToggleButton email_promotion_toggle, email_message_toggle, sms_messages_toggle, mobile_messages_toggle, mobile_promotions_toggle,
            instagram_notify_toggle;
    String email_promotion_toggleValue = "0", email_message_toggleValue = "0", sms_messages_toggleValue = "0", mobile_messages_toggleValue = "0", mobile_promotions_toggleValue = "0", instagramValue = "0";
    String user_id = "";
    String id = "", email_pro, email_msg, sms_msg, mobile_msg, mobile_pro, instagram_login;
    DbHelper dbHelper;
    Activity context = this;
    private SharedPreference sharedPreference;
    /* Button save;*/
    private InstagramApp mApp;
    InstagramApp.OAuthAuthenticationListener listener = new InstagramApp.OAuthAuthenticationListener() {

        @Override
        public void onSuccess() {
            String access_token = mApp.getmAccessToken();
            Log.e(TAG, "AccessToken " + mApp.getmAccessToken());

            Toast.makeText(NotificationChef.this, mApp.getUserName(), Toast.LENGTH_SHORT).show();
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            if (netInfo == null) {
                new android.support.v7.app.AlertDialog.Builder(NotificationChef.this)
                        .setTitle(getResources().getString(R.string.app_name))
                        .setMessage(getResources().getString(R.string.internet_error))
                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
            } else {
                InstaLogin(access_token);
            }

        }

        @Override
        public void onFail(String error) {
            Toast.makeText(NotificationChef.this, "Insta" + error, Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_chef);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.notification);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*---------------------------------------------IDs-------------------------------------------*/
        email_promotion_toggle = (ToggleButton) findViewById(R.id.email_promotion_toggle);
        email_message_toggle = (ToggleButton) findViewById(R.id.email_message_toggle);
        sms_messages_toggle = (ToggleButton) findViewById(R.id.sms_messages_toggle);
        mobile_messages_toggle = (ToggleButton) findViewById(R.id.mobile_messages_toggle);
        mobile_promotions_toggle = (ToggleButton) findViewById(R.id.mobile_promotions_toggle);
        instagram_notify_toggle = (ToggleButton) findViewById(R.id.instagram_notify_toggle);

/*-----------------------------------------//Database------------------------------------------------------*/
        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            user_id = sharedPreference.getUserId(context);
        } else {
            user_id = "";
        }


        mApp = new InstagramApp(this, AppController.CLIENT_ID,
                AppController.CLIENT_SECRET, AppController.CALLBACK_URL);
        mApp.setListener(listener);
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            getPermissionInfoAPI();
        }


        instagram_notify_toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mApp.hasAccessToken()) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(
                            NotificationChef.this);
                    builder.setMessage("Disconnect from Instagram?")
                            .setCancelable(false)
                            .setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            instagram_notify_toggle.setChecked(false);
                                            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                            if (netInfo == null) {
                                                new android.support.v7.app.AlertDialog.Builder(NotificationChef.this)
                                                        .setTitle(getResources().getString(R.string.app_name))
                                                        .setMessage(getResources().getString(R.string.internet_error))
                                                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                            } else {
                                                InstaLogin("");
                                                mApp.resetAccessToken();

                                            }


                                        }
                                    })
                            .setNegativeButton(getResources().getString(R.string.cancel),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            instagram_notify_toggle.setChecked(true);
                                        }
                                    });
                    final AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    mApp.authorize();
                }
            }
        });
        if (mApp.hasAccessToken()) {
            instagram_notify_toggle.setChecked(true);
        }
    }

    @Override
    public void onBackPressed() {
        if (!validate()) {
            return;
        }
        super.onBackPressed();
        // finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*---------------------------------------------API---------------------------------------------------------*/
    public void getPermissionInfoAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.GET_PERMISSION_URL + "?user_id=%1$s", user_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e(TAG, response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            String message = jsonObject.getString("message");
                            if (status == 1) {
                                JSONArray jsonArray = jsonObject.getJSONArray("getPermission");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    id = object.getString("id");
                                    email_pro = object.getString("email_pro");
                                    email_msg = object.getString("email_msg");
                                    sms_msg = object.getString("sms_msg");
                                    mobile_msg = object.getString("mobile_msg");
                                    mobile_pro = object.getString("mobile_pro");
                                    instagram_login = object.getString("instagram_login");
                                }

                                if (email_pro.equals("1")) {
                                    email_promotion_toggle.setChecked(true);
                                }
                                if (email_msg.equals("1")) {
                                    email_message_toggle.setChecked(true);
                                }
                                if (sms_msg.equals("1")) {
                                    sms_messages_toggle.setChecked(true);
                                }
                                if (mobile_msg.equals("1")) {
                                    mobile_messages_toggle.setChecked(true);
                                }
                                if (mobile_pro.equals("1")) {
                                    mobile_promotions_toggle.setChecked(true);
                                }
                                if (instagram_login.equals("1")) {
                                    instagram_notify_toggle.setChecked(true);
                                } else {
                                    instagram_notify_toggle.setChecked(false);
                                    mApp.resetAccessToken();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }if (getApplicationContext()!=null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void updatePermissionInfoAPI() {
       /* final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.ADD_PERMISSION_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      /*  progressDialog.dismiss();*/
                        Log.e(TAG, response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            String message = jsonObject.getString("message");
                            if (status == 1) {
                                Toast.makeText(NotificationChef.this, "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                           /* progressDialog.dismiss();*/
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                          /*  progressDialog.dismiss();*/
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       /* progressDialog.dismiss();*/
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }if (getApplicationContext()!=null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("id", id);
                map.put("user_id", user_id);
                map.put("email_pro", email_promotion_toggleValue);
                map.put("email_msg", email_message_toggleValue);
                map.put("sms_msg", sms_messages_toggleValue);
                map.put("mobile_msg", mobile_messages_toggleValue);
                map.put("mobile_pro", mobile_promotions_toggleValue);
                map.put("instagram_login", instagramValue);

                Log.e("MapData", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public boolean validate() {
        email_promotion_toggleValue = "0";
        email_message_toggleValue = "0";
        sms_messages_toggleValue = "0";
        mobile_messages_toggleValue = "0";
        mobile_promotions_toggleValue = "0";
        if (email_promotion_toggle.isChecked()) {
            email_promotion_toggleValue = "1";
        }
        if (email_message_toggle.isChecked()) {
            email_message_toggleValue = "1";
        }
        if (sms_messages_toggle.isChecked()) {
            sms_messages_toggleValue = "1";
        }
        if (mobile_messages_toggle.isChecked()) {
            mobile_messages_toggleValue = "1";
        }
        if (mobile_promotions_toggle.isChecked()) {
            mobile_promotions_toggleValue = "1";
        }
        if (instagram_notify_toggle.isChecked()) {
            instagramValue = "1";
        }
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            updatePermissionInfoAPI();
        }

        return true;
    }

    public void InstaLogin(final String access_token) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.INSTA_LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e(TAG, response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            String message = jsonObject.getString("message");
                            JSONObject jsonObject1 = jsonObject.getJSONObject("updatetoken");
                            if (status == 1) {
                                String id = jsonObject1.getString("id");
                                Toast.makeText(NotificationChef.this, "" + id, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }if (getApplicationContext()!=null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("access_token", access_token);
                map.put("user_id", user_id);

                Log.e("MapData", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
