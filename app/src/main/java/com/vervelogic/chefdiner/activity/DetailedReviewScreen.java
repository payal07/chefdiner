package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.model.ReviewModelClass;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class DetailedReviewScreen extends AppCompatActivity {

    Toolbar toolbar;
    ReviewAdapter reviewAdapter;
    RecyclerView recyclerView;
    SimpleRatingBar taste_ratingReview, environment_ratingReview, serviceQuality_ratingReview, cleaniness_ratingReview, price_ratingReview,
            avg_ratingReview;
    TextView rate_count_review, review_text;
    String Chef_id, userId = "", language;
    Activity context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_review_screen);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.rating_and_review);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Chef_id = intent.getStringExtra("Chef_id");
        Log.e("Chef", "" + Chef_id);


        SharedPreference sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            userId = sharedPreference.getUserId(context);
            language = sharedPreference.getLanguage(context);
        } else {
            userId = "";
            language = "";
        }

        review_text = (TextView) findViewById(R.id.review_text);
        rate_count_review = (TextView) findViewById(R.id.rate_count_review);
        taste_ratingReview = (SimpleRatingBar) findViewById(R.id.taste_ratingReview);
        environment_ratingReview = (SimpleRatingBar) findViewById(R.id.environment_ratingReview);
        serviceQuality_ratingReview = (SimpleRatingBar) findViewById(R.id.serviceQuality_ratingReview);
        cleaniness_ratingReview = (SimpleRatingBar) findViewById(R.id.cleaniness_ratingReview);
        price_ratingReview = (SimpleRatingBar) findViewById(R.id.price_ratingReview);
        avg_ratingReview = (SimpleRatingBar) findViewById(R.id.avgRating);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewReview);

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            Log.e("", "No Internet connection");
        } else {
            getReviewAPI();
            getReviewCommentAPI();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getReviewAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.GET_REVIEW_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        try {
                            Log.e("DetailedReviewScreen", "" + s);

                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                JSONObject object = jsonObject.getJSONObject("ratingarr");
                                int avrTaste = object.getInt("avrTaste");
                                int avr_environment = object.getInt("avr_environment");
                                int avr_service_quality = object.getInt("avr_service_quality");
                                int avr_cleanliness = object.getInt("avr_cleanliness");
                                int avr_review = object.getInt("avr_review");
                                int avr_price = object.getInt("avr_price");
                                String totalreview = object.getString("totalreview");

                                taste_ratingReview.setRating(avrTaste);
                                environment_ratingReview.setRating(avr_environment);
                                serviceQuality_ratingReview.setRating(avr_service_quality);
                                cleaniness_ratingReview.setRating(avr_cleanliness);
                                avg_ratingReview.setRating(avr_review);
                                price_ratingReview.setRating(avr_price);
                                rate_count_review.setText("" + avr_review);
                                review_text.setText(totalreview + " " + getResources().getString(R.string.review));

                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                Toast.makeText(DetailedReviewScreen.this, "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e("Detailed Screen Review", "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", Chef_id);
                Log.e("Review", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void likeReviewAPI(final String reviewId, final String like_rev) {
      /*  final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.setCancelable(false);
        progressDialog.show();*/

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.LIKE_REVIEW_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        // progressDialog.dismiss();
                        try {
                            Log.e("DetailedReviewScreen", "" + s);

                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                //  String message = jsonObject.getString("message");
                                getReviewCommentAPI();
                                // Toast.makeText(DetailedReviewScreen.this, "" + message, Toast.LENGTH_SHORT).show();
                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                Toast.makeText(DetailedReviewScreen.this, "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            // progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            // progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e("Detailed Screen Review", "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("review_id", reviewId);
                map.put("main_user_id", userId);
                map.put("like_rev", like_rev);
                Log.e("Review", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void getReviewCommentAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.GET_REVIEW_COMMENT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        try {
                            Log.e("Comment", "" + s);
                            ReviewModelClass reviewModelClass = new Gson().fromJson(s, ReviewModelClass.class);
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {


                                LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(DetailedReviewScreen.this, LinearLayoutManager.VERTICAL, false);
                                recyclerView.setLayoutManager(horizontalLayoutManagaer);

                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                reviewAdapter = new ReviewAdapter(getApplicationContext(), reviewModelClass.getGetRRlist());
                                recyclerView.setAdapter(reviewAdapter);
                                reviewAdapter.notifyDataSetChanged();
                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                Toast.makeText(DetailedReviewScreen.this, "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e("Detailed Screen Review", "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", Chef_id);
                map.put("main_user_id", userId);
                Log.e("Comment", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder> {

        List<ReviewModelClass.GetRRlistBean> historyMeals;
        Context context;
        int likeAdd = 0;
        String favorite = "0";

        ReviewAdapter(Context context, List<ReviewModelClass.GetRRlistBean> historyMeals) {
            this.historyMeals = historyMeals;
            this.context = context;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.review_details_screen, viewGroup, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            ReviewModelClass.GetRRlistBean reviewModelClass = historyMeals.get(position);

            holder.userName.setText(historyMeals.get(position).getFirst_name() + " " + historyMeals.get(position).getLast_name());
            holder.avgCount.setText(historyMeals.get(position).getAvr_rating());
            holder.dateText.setText(historyMeals.get(position).getNewdate());
            float rate = Float.parseFloat((historyMeals.get(position).getAvr_rating()));
            /*Log.e("V", "" + v);
            if (0 <= Float.parseFloat(historyMeals.get(position).getAvr_rating()) && Float.parseFloat(historyMeals.get(position).getAvr_rating()) <= 2) {
                holder.imageSmiley.setImageResource(R.drawable.ic_sad);
            } else if (Float.parseFloat(historyMeals.get(position).getAvr_rating()) > 2 && Float.parseFloat(historyMeals.get(position).getAvr_rating()) <= 4) {
                holder.imageSmiley.setImageResource(R.drawable.ic_smiling);
            } else if (Float.parseFloat(historyMeals.get(position).getAvr_rating()) > 4 && Float.parseFloat(historyMeals.get(position).getAvr_rating()) <= 5) {
                holder.imageSmiley.setImageResource(R.drawable.ic_tongue_out);
            }*/

            if (rate <= 2.4) {
                holder.imageSmiley.setImageResource(R.drawable.ic_sad);
            } else if (rate <= 3.9) {
                holder.imageSmiley.setImageResource(R.drawable.ic_smiling);
            } else if (rate > 3.9) {
                holder.imageSmiley.setImageResource(R.drawable.ic_tongue_out);
            }


            if (!TextUtils.isEmpty(historyMeals.get(position).getProfile_pic())) {
                Picasso.with(context)
                        .load(historyMeals.get(position).getProfile_pic()).fit()
                        .error(R.drawable.profile)
                        .into(holder.circleImageView);
            }
            holder.simpleRatingBar.setBorderColor(getResources().getColor(R.color.button_color));
            holder.simpleRatingBar.setFillColor(getResources().getColor(R.color.button_color));
            holder.simpleRatingBar.setIndicator(true);
            holder.simpleRatingBar.setStarSize(40);
            holder.simpleRatingBar.setStarCornerRadius(0);
            holder.simpleRatingBar.setRating(Float.parseFloat(historyMeals.get(position).getAvr_rating()));
            holder.reviewText.setText(historyMeals.get(position).getReview());
            final List<String> newImages = new ArrayList<>();

            if (reviewModelClass.getNewimages().size() != 0) {
                for (int k = 0; k < reviewModelClass.getNewimages().size(); k++) {
                    if (!TextUtils.isEmpty(reviewModelClass.getNewimages().get(k))) {
                        newImages.add(historyMeals.get(position).getNewimages().get(k));
                    }

                }
            }
            String likeCount = "" + historyMeals.get(position).getLikecount();
            if (!TextUtils.isEmpty(likeCount)) {
                holder.no_of_likes.setText("" + historyMeals.get(position).getLikecount());
            }
            if (!TextUtils.isEmpty(historyMeals.get(position).getLikeKey())) {
                if (historyMeals.get(position).getLikeKey().equalsIgnoreCase("yes")) {
                    holder.like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_heart_red));
                    likeAdd = 0;
                } else if (historyMeals.get(position).getLikeKey().equalsIgnoreCase("no")) {
                    holder.like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_gray_heart));
                    likeAdd = 1;
                }
            }

            final String reviewId = historyMeals.get(position).getRr_id();
            holder.like_ly.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!TextUtils.isEmpty(userId)) {
                        if (likeAdd == 0) {

                            holder.like_img.setImageResource(R.drawable.ic_gray_heart);
                            favorite = "0";
                            likeAdd = 1;
                            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                            if (netInfo == null) {
                                new android.support.v7.app.AlertDialog.Builder(getApplicationContext())
                                        .setTitle(getResources().getString(R.string.app_name))
                                        .setMessage(getResources().getString(R.string.internet_error))
                                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
                            } else {
                                likeReviewAPI(reviewId, favorite);
                            }

                        } else {
                            holder.like_img.setImageResource(R.drawable.ic_heart_red);
                            favorite = "1";
                            likeAdd = 0;
                            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                            if (netInfo == null) {
                                new android.support.v7.app.AlertDialog.Builder(getApplicationContext())
                                        .setTitle(getResources().getString(R.string.app_name))
                                        .setMessage(getResources().getString(R.string.internet_error))
                                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
                            } else {
                                likeReviewAPI(reviewId, favorite);
                            }
                        }
                    } else {
                        Intent intent = new Intent(getApplicationContext(), RegisterSignUpScreen.class);
                        intent.putExtra("language", language);
                        intent.putExtra("type", "ChefProfile");
                        intent.putExtra("Chef_id", Chef_id);
                        startActivity(intent);

                    }

                }
            });

            for (int i = 0; i < newImages.size(); i++) {
                if (!TextUtils.isEmpty(newImages.get(i))) {
                    holder.image1Review.setVisibility(View.VISIBLE);
                    Picasso.with(context)
                            .load(newImages.get(0)).fit()
                          /*  .error(R.drawable.profile)*/
                            .into(holder.image1Review);
                    if (i > 0) {
                        holder.image2Review.setVisibility(View.VISIBLE);
                        Picasso.with(context)
                                .load(newImages.get(1)).fit()
                             /*   .error(R.drawable.profile)*/
                                .into(holder.image2Review);
                    }

                    if (i > 1) {
                        holder.image3Review.setVisibility(View.VISIBLE);
                        Picasso.with(context)
                                .load(newImages.get(2)).fit()
                                /*.error(R.drawable.profile)*/
                                .into(holder.image3Review);
                    }

                    if (i > 2) {
                        holder.image4Review.setVisibility(View.VISIBLE);
                        Picasso.with(context)
                                .load(newImages.get(3)).fit()
                              /*  .error(R.drawable.profile)*/
                                .into(holder.image4Review);
                    }
                    /*else
                    {
                        holder.image4Review.setVisibility(View.GONE);
                    }*/
                    if (i > 3) {
                        holder.image5Review.setVisibility(View.VISIBLE);
                        Picasso.with(context)
                                .load(newImages.get(4)).fit()
                              /*  .error(R.drawable.profile)*/
                                .into(holder.image5Review);
                    }
                   /* else
                    {
                        holder.image5Review.setVisibility(View.GONE);
                    }*/
                }
                holder.image1Review.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Dialog dialog = new Dialog(view.getRootView().getContext());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                        Window window = dialog.getWindow();
                        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.choose_menu_content);
                        dialog.getWindow().setLayout(700, 900);

                        LinearLayout card_view_layout_menu = dialog.findViewById(R.id.card_view_layout_menu);
                        ImageView imageView = dialog.findViewById(R.id.imageView);


                        card_view_layout_menu.setVisibility(View.GONE);

                        Picasso.with(context)
                                .load(newImages.get(0))
                                .fit()
                                .into(imageView);

                        dialog.show();
                    }
                });
                holder.image2Review.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Dialog dialog = new Dialog(view.getRootView().getContext());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                        Window window = dialog.getWindow();
                        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.choose_menu_content);
                        dialog.getWindow().setLayout(700, 900);

                        LinearLayout card_view_layout_menu = dialog.findViewById(R.id.card_view_layout_menu);
                        ImageView imageView = dialog.findViewById(R.id.imageView);


                        card_view_layout_menu.setVisibility(View.GONE);

                        Picasso.with(context)
                                .load(newImages.get(1))
                                .fit()
                                .into(imageView);

                        dialog.show();
                    }
                });
                holder.image3Review.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Dialog dialog = new Dialog(view.getRootView().getContext());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                        Window window = dialog.getWindow();
                        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.choose_menu_content);
                        dialog.getWindow().setLayout(700, 900);

                        LinearLayout card_view_layout_menu = dialog.findViewById(R.id.card_view_layout_menu);
                        ImageView imageView = dialog.findViewById(R.id.imageView);


                        card_view_layout_menu.setVisibility(View.GONE);

                        Picasso.with(context)
                                .load(newImages.get(2))
                                .fit()
                                .into(imageView);

                        dialog.show();
                    }
                });
                holder.image4Review.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Dialog dialog = new Dialog(view.getRootView().getContext());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                        Window window = dialog.getWindow();
                        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.choose_menu_content);
                        dialog.getWindow().setLayout(700, 900);

                        LinearLayout card_view_layout_menu = dialog.findViewById(R.id.card_view_layout_menu);
                        ImageView imageView = dialog.findViewById(R.id.imageView);


                        card_view_layout_menu.setVisibility(View.GONE);

                        Picasso.with(context)
                                .load(newImages.get(3))
                                .fit()
                                .into(imageView);

                        dialog.show();
                    }
                });
                holder.image5Review.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Dialog dialog = new Dialog(view.getRootView().getContext());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                        Window window = dialog.getWindow();
                        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.choose_menu_content);
                        dialog.getWindow().setLayout(700, 900);

                        LinearLayout card_view_layout_menu = dialog.findViewById(R.id.card_view_layout_menu);
                        ImageView imageView = dialog.findViewById(R.id.imageView);


                        card_view_layout_menu.setVisibility(View.GONE);

                        Picasso.with(context)
                                .load(newImages.get(4))
                                .fit()
                                .into(imageView);

                        dialog.show();
                    }
                });
            }


        }

        @Override
        public int getItemCount() {
            return historyMeals.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView reviewText, userName, dateText, avgCount, like_btn, no_of_likes;
            SimpleRatingBar simpleRatingBar;
            ImageView image1Review, image2Review, image3Review, image4Review, image5Review, imageSmiley, like_img;
            CircleImageView circleImageView;
            LinearLayout like_ly;

            MyViewHolder(View itemView) {
                super(itemView);
                reviewText = itemView.findViewById(R.id.review);
                userName = itemView.findViewById(R.id.user_name);
                dateText = itemView.findViewById(R.id.dateReview);
/*
                timeText = itemView.findViewById(R.id.timeReview);
*/
                avgCount = itemView.findViewById(R.id.avgCount);
                simpleRatingBar = itemView.findViewById(R.id.avgRatingReview);
                imageSmiley = itemView.findViewById(R.id.imageSmiley);
                image1Review = itemView.findViewById(R.id.image1Review);
                image2Review = itemView.findViewById(R.id.image2Review);
                image3Review = itemView.findViewById(R.id.image3Review);
                image4Review = itemView.findViewById(R.id.image4Review);
                image5Review = itemView.findViewById(R.id.image5Review);
                circleImageView = itemView.findViewById(R.id.user_image);
                like_btn = itemView.findViewById(R.id.like_btn);
                no_of_likes = itemView.findViewById(R.id.no_of_likes);
                like_img = itemView.findViewById(R.id.like_img);
                like_ly = itemView.findViewById(R.id.like_ly);

            }
        }
    }

}

