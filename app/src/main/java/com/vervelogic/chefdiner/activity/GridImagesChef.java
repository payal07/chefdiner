package com.vervelogic.chefdiner.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.adapter.ImageAdapter;
import com.vervelogic.chefdiner.adapter.MyPagerAdapter;
import com.vervelogic.chefdiner.model.GridItem;
import com.vervelogic.chefdiner.model.RecyclerTouchListener;
import com.vervelogic.chefdiner.utils.CarouselEffectTransformer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class GridImagesChef extends AppCompatActivity {

    public static final int ADAPTER_TYPE_TOP = 1;
    /*------------------------------------Declarations-------------------------------*/
    String allImages, imageCount;
    int imagesLength;
    TextView no_image_available;
    RecyclerView recyclerView;
    ArrayList<String> listView = new ArrayList<>();
    ArrayList<GridItem> android_version = new ArrayList<>();
    ArrayList<GridItem> androidVersions;
    GridItem gridItem;
    Toolbar toolbar;
    int pos;
    private ViewPager viewpagerTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_images_chef);

        /*-------------------Toolbar------------------------------------------------------*/
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.upload_pic);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*---------------------------Getting data from intent---------------------------------*/
        Intent intent = getIntent();
        allImages = intent.getStringExtra("allImages");
        imageCount = intent.getStringExtra("imageCount");

        List<String> items = Arrays.asList(allImages.split("\\|"));


        for (int j = 0; j < items.size(); j++) {
            if (!items.get(j).equals("")) {
                listView.add(items.get(j));

            }
        }
        imagesLength = listView.size();

        no_image_available = (TextView) findViewById(R.id.no_image_available);

        /*----------------------------------Recycler View----------------------------------------*/
        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        androidVersions = prepareData();
        if (!androidVersions.isEmpty()) {
            ImageAdapter adapter = new ImageAdapter(getApplicationContext(), androidVersions);
            recyclerView.setAdapter(adapter);
        } else {
            no_image_available.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }


        // ------------------------setting listener on recycler view------------------------------
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View view, int position) {
                pos = position;
                gridItem = androidVersions.get(position);
                // custom dialog
                final Dialog dialog = new Dialog(GridImagesChef.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.single_image_chef);
/*---------------------View pager for single image-------------------------------------------------------------------------*/
                viewpagerTop = dialog.findViewById(R.id.viewpagerTop);
                viewpagerTop.setClipChildren(false);
                viewpagerTop.setOffscreenPageLimit(3);
                viewpagerTop.setPageTransformer(false, new CarouselEffectTransformer(getApplicationContext())); // Set transformer
                setupViewPager(pos);

                final Button cancel = dialog.findViewById(R.id.cancel);
                // if button is clicked, close the custom dialog
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    /*----------------------------------- Data in recycler view-----------------------------------------------*/
    private ArrayList<GridItem> prepareData() {

        for (int i = 0; i < imagesLength; i++) {
            GridItem androidVersion = new GridItem();

            androidVersion.setImage(listView.get(i));
            android_version.add(androidVersion);
        }
        return android_version;
    }

    private void setupViewPager(final int pos) {

        // Set Top ViewPager Adapter
        MyPagerAdapter adapter = new MyPagerAdapter(this, androidVersions, ADAPTER_TYPE_TOP);
        viewpagerTop.setAdapter(adapter);
        viewpagerTop.setCurrentItem(pos);

        viewpagerTop.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int index = pos;

            @Override
            public void onPageSelected(int position) {
                index = position;
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
              /*  int width = viewPagerBackground.getWidth();
                viewPagerBackground.scrollTo((int) (width * position + width * positionOffset), 0);*/
            }

            @Override
            public void onPageScrollStateChanged(int state) {
               /* if (state == ViewPager.SCROLL_STATE_IDLE) {
                    viewPagerBackground.setCurrentItem(index);
                }*/

            }
        });
    }

    /**
     * Handle all click event of activity
     */
    public void clickEvent(View view) {
        switch (view.getId()) {
            case R.id.linMain:
                if (view.getTag() != null) {
                    int position = Integer.parseInt(view.getTag().toString());
                    /*Toast.makeText(getApplicationContext(), "Position: " + position, Toast.LENGTH_LONG).show();*/
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}