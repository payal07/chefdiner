package com.vervelogic.chefdiner.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.model.RecyclerTouchListener;
import com.vervelogic.chefdiner.model.SubCuisineModel;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubCuisine extends AppCompatActivity {


    Toolbar toolbar;
    RecyclerView cuisine_listView;
    CuisinesAdapter cuisinesAdapter;
    String language;
    String user_id, cuisine = "", cuisine_name;
    Activity context = this;
    TextView no_data;
    SubCuisineModel googleLoginModel;
    private SharedPreference sharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_cuisine);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.cuisines);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cuisine_listView = (RecyclerView) findViewById(R.id.sub_cuisine_list);
        no_data = (TextView) findViewById(R.id.no_data);
        //-------------------  Recycler View.......--------------------------------------------------------------

        Intent intent = getIntent();
        cuisine = intent.getStringExtra("cuisine");
        cuisine_name = intent.getStringExtra("cuisine_name");

        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(context))) {
            language = sharedPreference.getLanguage(context);
        } else {
            language = "";
        }


        cuisine_listView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), cuisine_listView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                SubCuisineModel.FiltersubCuisineBean filterCuisineBean = googleLoginModel.getFiltersubCuisine().get(position);
                Intent intent = new Intent(getApplicationContext(), FavoriteChefList.class);
                intent.putExtra("cuisine", "1");
                intent.putExtra("sub_cuisine", filterCuisineBean.getSubcuisinename());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        setData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void setData() {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new android.support.v7.app.AlertDialog.Builder(SubCuisine.this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            getFilterCuisineListAPI();

        }
    }

    public void getFilterCuisineListAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.FILTERED_SUBCUISINE_LIST_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            Log.e("Response", response);
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                googleLoginModel = new Gson().fromJson(response, SubCuisineModel.class);
                                LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(SubCuisine.this, LinearLayoutManager.VERTICAL, false);
                                cuisine_listView.setLayoutManager(horizontalLayoutManagaer);
                                cuisine_listView.setVisibility(View.VISIBLE);
                                cuisine_listView.setNestedScrollingEnabled(false);
                                cuisinesAdapter = new CuisinesAdapter(getApplicationContext(), googleLoginModel.getFiltersubCuisine());
                                cuisine_listView.setAdapter(cuisinesAdapter);
                                cuisinesAdapter.notifyDataSetChanged();

                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                no_data.setVisibility(View.VISIBLE);
                                no_data.setText(message);
                                cuisine_listView.setVisibility(View.GONE);
                                Toast.makeText(SubCuisine.this, "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }if (getApplicationContext()!=null)
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //  String image = getStringImage(bitmap);
                map.put("language", language);
                map.put("cuisinename", cuisine_name);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    class CuisinesAdapter extends RecyclerView.Adapter<CuisinesAdapter.MyViewHolder> {
        List<SubCuisineModel.FiltersubCuisineBean> profileMenuModels = new ArrayList<>();
        Context mcontext;

        CuisinesAdapter(Context mcontext, List<SubCuisineModel.FiltersubCuisineBean> profileMenuModels) {
            this.mcontext = mcontext;
            this.profileMenuModels = profileMenuModels;

        }

        @Override
        public CuisinesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cuisines_list, parent, false);
            return new CuisinesAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final CuisinesAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
            //CuisinesModel.FilterCuisineBean profileMenuModel = profileMenuModels.get(position);

            holder.next.setVisibility(View.GONE);
            Picasso.with(mcontext)
                    .load(R.drawable.cuisine_img)
                    .error(R.drawable.cuisine_img)
                    .into(holder.cuisine_ly);

           /* if (! profileMenuModel.getUrl().equals(""))
                Picasso.with(mcontext)
                        .load( profileMenuModel.getUrl())
                        .error(R.drawable.bg_img)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                              holder.cuisine_ly.setBackground(new BitmapDrawable(bitmap));
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });*/
          /*  holder.cuisine_type.setText(profileMenuModel.getCusineTitle());
            holder.chef_list.setText(profileMenuModel.getChefList());*/
            holder.cuisine_type.setText(profileMenuModels.get(position).getSubcuisinename());
            holder.chef_list.setText(profileMenuModels.get(position).getCounter() +" "+ getResources().getString(R.string.chef_listed));
            holder.next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext(), "Next" + position, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return profileMenuModels.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView cuisine_type, chef_list;
            ImageView next;
            RoundedImageView cuisine_ly;

            MyViewHolder(View itemView) {
                super(itemView);
                next = itemView.findViewById(R.id.img_next);
                cuisine_type = itemView.findViewById(R.id.cuisine_type);
                chef_list = itemView.findViewById(R.id.chef_list);
                cuisine_ly = itemView.findViewById(R.id.bg_image);

            }
        }
    }
}
