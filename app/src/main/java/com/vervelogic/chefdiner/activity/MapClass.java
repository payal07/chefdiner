package com.vervelogic.chefdiner.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.adapter.MapClassAdapter;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.HomeFragmentModel;
import com.vervelogic.chefdiner.model.RecyclerTouchListener;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.GPSTracker;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapClass extends AppCompatActivity implements OnMapReadyCallback {

    //----------------------------- GPS-----------------------------
    GPSTracker gps;
    String latitude = "", longitude = "";

    RecyclerView recyclerView;
    TextView no_image_available;
    List<HomeFragmentModel> homeFragmentModelList = new ArrayList<>();
    MapClassAdapter homeFragmentAdapter;
    Toolbar toolbar;
    LinearLayout filter_ly;
    RadioButton highestRecommended, highestBookmarked, priceLowToHigh, priceHighToLow;
    ArrayList<LatLng> markerPoints = new ArrayList<>();
    String userId;
    SharedPreference sharedPreference;
    DbHelper dbHelper;
    /*------------------------------------Google Map--------------------------*/
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_class);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.map);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //--------------------------- create class object--------------------------------
        gps = new GPSTracker(this);

        dbHelper = new DbHelper(getApplicationContext());
        // check if GPS enabled
        if (gps.canGetLocation()) {

            double lat = gps.getLatitude();
            double lon = gps.getLongitude();
            latitude = String.valueOf(lat);
            longitude = String.valueOf(lon);

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }

        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(getApplicationContext()))) {
            userId = sharedPreference.getUserId(getApplicationContext());
        } else {
            userId = "";
        }
        GetChefAPI();
         /*------------ Obtain the SupportMapFragment and get notified when the map is ready to be used.-----------*/
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        recyclerView = (RecyclerView) findViewById(R.id.map_recyclerview);
        no_image_available = (TextView) findViewById(R.id.no_data);


       /* filter_ly= (LinearLayout)findViewById(R.id.filter_ly_map);
        filter_ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // custom dialog
                final Dialog dialog = new Dialog(MapClass.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.custom_filter_dialog);
                Button dialogButton = dialog.findViewById(R.id.applyfilter);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                TextView cancel = dialog.findViewById(R.id.cancel);
                // if button is clicked, close the custom dialog
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });*/
        //  filter = view.findViewById(R.id.filter);
        filter_ly = (LinearLayout) findViewById(R.id.filter_ly_map);
        filter_ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // custom dialog
                final Dialog dialog = new Dialog(MapClass.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.custom_filter_dialog);
                Button dialogButton = dialog.findViewById(R.id.applyfilter);
                highestRecommended = dialog.findViewById(R.id.highestRecommended);
                highestBookmarked = dialog.findViewById(R.id.highestBookmarked);
                priceHighToLow = dialog.findViewById(R.id.priceHighToLow);
                priceLowToHigh = dialog.findViewById(R.id.priceLowToHigh);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (highestBookmarked.isChecked()) {
                            Collections.sort(homeFragmentModelList, new Comparator<HomeFragmentModel>() {
                                @Override
                                public int compare(HomeFragmentModel userDetail, HomeFragmentModel t1) {
                                    String stringName1 = userDetail.getBookmarkedCount();
                                    String stringName2 = t1.getBookmarkedCount();

                                    return stringName2.compareToIgnoreCase(stringName1);//Desc Order
                                }
                            });
                            if (!homeFragmentModelList.isEmpty()) {
                                // ---------------Adapter---------------------
                                homeFragmentAdapter = new MapClassAdapter(getApplicationContext(), homeFragmentModelList);
                                // setting adapter on recycler view
                                recyclerView.setAdapter(homeFragmentAdapter);
                                homeFragmentAdapter.notifyDataSetChanged();
                            } else {
                                no_image_available.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                        /*Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();*/
                            }

                        }
                        if (highestRecommended.isChecked()) {
                            Collections.sort(homeFragmentModelList, new Comparator<HomeFragmentModel>() {
                                @Override
                                public int compare(HomeFragmentModel userDetail, HomeFragmentModel t1) {
                                    String stringName1 = userDetail.getHappyCount();
                                    String stringName2 = t1.getHappyCount();

                                    return stringName2.compareToIgnoreCase(stringName1);//Desc Order
                                }
                            });
                            if (!homeFragmentModelList.isEmpty()) {
                                // ---------------Adapter---------------------
                                homeFragmentAdapter = new MapClassAdapter(getApplicationContext(), homeFragmentModelList);
                                // setting adapter on recycler view
                                recyclerView.setAdapter(homeFragmentAdapter);
                                homeFragmentAdapter.notifyDataSetChanged();
                            } else {
                                no_image_available.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                        /*Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();*/
                            }


                        }
                        if (priceHighToLow.isChecked()) {
                            Collections.sort(homeFragmentModelList, new Comparator<HomeFragmentModel>() {
                                @Override
                                public int compare(HomeFragmentModel userDetail, HomeFragmentModel t1) {
                                    float stringName1 = Float.parseFloat(userDetail.getPrice());
                                    float stringName2 = Float.parseFloat(t1.getPrice());
                                    if (stringName1 < stringName2) {
                                        return 1;
                                    } else {
                                        return -1;
                                    }
                                }
                            });
                            if (!homeFragmentModelList.isEmpty()) {
                                // ---------------Adapter---------------------

                                homeFragmentAdapter = new MapClassAdapter(getApplicationContext(), homeFragmentModelList);
                                // setting adapter on recycler view
                                recyclerView.setAdapter(homeFragmentAdapter);
                                homeFragmentAdapter.notifyDataSetChanged();
                            } else {
                                no_image_available.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                        /*Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();*/
                            }


                        }
                        if (priceLowToHigh.isChecked()) {
                            Collections.sort(homeFragmentModelList, new Comparator<HomeFragmentModel>() {
                                @Override
                                public int compare(HomeFragmentModel userDetail, HomeFragmentModel t1) {
                                    float stringName1 = Float.parseFloat(userDetail.getPrice());
                                    float stringName2 = Float.parseFloat(t1.getPrice());
                                    if (stringName2 < stringName1) {
                                        return 1;
                                    } else {
                                        return -1;
                                    }
                                }
                            });
                            if (!homeFragmentModelList.isEmpty()) {
                                // ---------------Adapter---------------------
                                homeFragmentAdapter = new MapClassAdapter(getApplicationContext(), homeFragmentModelList);
                                // setting adapter on recycler view
                                recyclerView.setAdapter(homeFragmentAdapter);
                                homeFragmentAdapter.notifyDataSetChanged();
                            } else {
                                no_image_available.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                        /*Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();*/
                            }


                        }
                    }
                });
                TextView cancel = dialog.findViewById(R.id.cancel);
                // if button is clicked, close the custom dialog
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
        // ------------------------setting listener on recycler view------------------------------
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //  HomeFragmentModel movie = homeFragmentModelList.get(position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void GetChefAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.GET_CHEF_URL + "?latitude=%1$s", latitude + "&longitude=" + longitude);
        // String uri = String.format(AppConfig.GET_CHEF_URL + "?latitude=26.911974&longitude=75.789882");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.GET_CHEF_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            //Log.e("Response", response);
                            dbHelper.deleteChef();

                            UserDetail userDetail = new UserDetail();
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {


                                JSONArray jsonArray = jsonObject.getJSONArray("getChef");
                                for (int i = 0; i <= jsonArray.length(); i++) {
                                    final HomeFragmentModel homeFragmentModel = new HomeFragmentModel();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    String id = jsonObject1.getString("id");
                                    String language = jsonObject1.getString("language");
                                    String fName = jsonObject1.getString("chef_fname");
                                    String last_name = jsonObject1.getString("chef_lname");
                                    String language_speak = jsonObject1.getString("language_speak");
                                    String serviceType = jsonObject1.getString("service_type");
                                    String phoneNumber = jsonObject1.getString("phone_number");
                                    String oftenCook = jsonObject1.getString("oftencook");
                                    String chefCountry = jsonObject1.getString("chef_country");
                                    String fromGuest = jsonObject1.getString("from_guest");
                                    String uptoGuest = jsonObject1.getString("upto_guest");
                                    String kitchen_title = jsonObject1.getString("kitchen_title");
                                    String kitchen_desc = jsonObject1.getString("kitchen_descrition");
                                    String profile_pic = jsonObject1.getString("profile_pic");
                                    String chef_latitude = jsonObject1.getString("chef_latitude");
                                    String chef_longitude = jsonObject1.getString("chef_longitude");
                                    String favorite = jsonObject1.getString("favouritecount");
                                    String userF_name = jsonObject1.getString("first_name");
                                    String userL_name = jsonObject1.getString("last_name");
                                    String email = jsonObject1.getString("email");
                                    String is_verify = jsonObject1.getString("is_verify");
                                    String is_chef_verify = jsonObject1.getString("is_verify_chef");
                                    String is_user = jsonObject1.getString("is_user");
                                    String device_ID = jsonObject1.getString("deviceId");
                                    String user_latitude = jsonObject1.getString("latitude");
                                    String user_longitude = jsonObject1.getString("longitude");
                                    String chef_city = jsonObject1.getString("chef_city");
                                    String all_Images = jsonObject1.getString("allimage");
                                    String price = jsonObject1.getString("price");
                                    String is_chef = jsonObject1.getString("is_chef");
                                    String lunch = jsonObject1.getString("lunch");
                                    String brunch = jsonObject1.getString("brunch");
                                    String dinner = jsonObject1.getString("dinner");
                                    String currency = jsonObject1.getString("currency");
                                    String cuisine = jsonObject1.getString("cuisine");
                                    String access_token = jsonObject1.getString("access_token");
                                    String totalreview = jsonObject1.getString("totalreview");
                                    String ok = jsonObject1.getString("ok");
                                    String no_recommended = jsonObject1.getString("no_recommended");
                                    String recommended = jsonObject1.getString("recommended");
                                    String average_rating = jsonObject1.getString("average_rating");
                                    String maxprice = jsonObject1.getString("maxprice");


                                    Double lat = Double.valueOf(chef_latitude);
                                    Double lon = Double.valueOf(chef_longitude);
                                    LatLng loc = new LatLng(lat, lon);
                                    markerPoints.add(loc);

                                    homeFragmentModel.setLanguage(language);
                                    homeFragmentModel.setPrice(price);
                                    homeFragmentModel.setLanguageSpeak(language_speak);
                                    homeFragmentModel.setServiceType(serviceType);
                                    homeFragmentModel.setPhoneNumber(phoneNumber);
                                    homeFragmentModel.setOftenCook(oftenCook);
                                    homeFragmentModel.setChefCountry(chefCountry);
                                    homeFragmentModel.setFromGuest(fromGuest);
                                    homeFragmentModel.setUptoGuest(uptoGuest);
                                    homeFragmentModel.setKitchen_description(kitchen_desc);
                                    homeFragmentModel.setChef_latitude(chef_latitude);
                                    homeFragmentModel.setChef_longiude(chef_longitude);
                                    homeFragmentModel.setLunch(lunch);
                                    homeFragmentModel.setDinner(dinner);
                                    homeFragmentModel.setBrunch(brunch);
                                    //   homeFragmentModel.setBookmarkedCount(favorite);
                                    homeFragmentModel.setEmail(email);
                                    homeFragmentModel.setCuisine(cuisine);
                                    homeFragmentModel.setIs_verify(is_verify);
                                    homeFragmentModel.setIs_chef_verify(is_chef_verify);
                                    homeFragmentModel.setIs_chef(is_chef);
                                    homeFragmentModel.setUserLat(user_latitude);
                                    homeFragmentModel.setUserLong(user_longitude);
                                    homeFragmentModel.setChefName(fName);
                                    homeFragmentModel.setChefLName(last_name);
                                    homeFragmentModel.setChefAddress(chef_city);
                                    homeFragmentModel.setKitchen_title(kitchen_title);
                                    homeFragmentModel.setProfileUrl(profile_pic);
                                    homeFragmentModel.setHKD(currency + price);
                                    homeFragmentModel.setAllImages(all_Images);
                                    homeFragmentModel.setFoodType1("Indian");
                                    homeFragmentModel.setFoodType2("Italian");
                                    homeFragmentModel.setBookmarkedCount(favorite);
                                    homeFragmentModel.setChef_id(id);
                                    homeFragmentModel.setChefRate(average_rating);
                                    homeFragmentModel.setReviewCount(totalreview + " Review");
                                    homeFragmentModel.setRateCount(average_rating);
                                    homeFragmentModel.setHappyCount(recommended);
                                    homeFragmentModel.setSadCount(no_recommended);

                                    List<String> items = Arrays.asList(all_Images.split("\\|"));

                                    ArrayList<String> listView = new ArrayList<>();
                                    for (int j = 0; j < items.size(); j++) {
                                        if (!items.get(j).equals("")) {
                                            listView.add(items.get(j));
                                        }
                                    }
                                    int imagesLength = listView.size();
                                    homeFragmentModel.setImageLength(imagesLength);
                                    if (!listView.isEmpty()) {
                                        homeFragmentModel.setImageUrl(listView.get(0));
                                        homeFragmentModel.setImage2Url(listView.get(1));
                                        homeFragmentModel.setImage3Url(listView.get(2));
                                    }
                                    userDetail.setLanguage(language);
                                    userDetail.setAccessToken(access_token);
                                    userDetail.setLanguageList(language_speak);
                                    userDetail.setServiceType(serviceType);
                                    userDetail.setPhoneNumber(phoneNumber);
                                    userDetail.setOftenCook(oftenCook);
                                    userDetail.setChefCountry(chefCountry);
                                    userDetail.setMin_guest(fromGuest);
                                    userDetail.setMax_guest(uptoGuest);
                                    userDetail.setKitchenDescription(kitchen_desc);
                                    userDetail.setLatitude(chef_latitude);
                                    userDetail.setLongitude(chef_longitude);
                                    userDetail.setLunch(lunch);
                                    userDetail.setDinner(dinner);
                                    userDetail.setBrunch(brunch);
                                    userDetail.setCuisine(cuisine);
                                    userDetail.setEmailId(email);
                                    userDetail.setIsVerifyUser(is_verify);
                                    userDetail.setIsChefVerify(is_chef_verify);
                                    userDetail.setIsChef(is_chef);
                                   /* userDetail.setLatitude(user_latitude);
                                    userDetail.setLongitude(user_longitude);*/
                                    userDetail.setFirstName(fName);
                                    userDetail.setLastName(last_name);
                                    userDetail.setCity(chef_city);
                                    userDetail.setKitchenTitle(kitchen_title);
                                    userDetail.setProfilePic(profile_pic);
                                    userDetail.setPrice(price);
                                    userDetail.setAllImages(all_Images);
                                    /*userDetail.setChefRate("4.5");*/
                                    userDetail.setTotalReview(totalreview + " " + getResources().getString(R.string.review));
                                    userDetail.setAvgRating(average_rating);
                                    userDetail.setRecommended(recommended);
                                    userDetail.setNotRecommended(no_recommended);
                                    userDetail.setOk(ok);
                                   /* userDetail.setFoodType2("Italian");*/
                                    userDetail.setFavCount(favorite);
                                    userDetail.setUser_id(id);
                                    userDetail.setCurrency(currency);
                                    userDetail.setCountry(chefCountry);
                                    userDetail.setMaxPrice(maxprice);

                                    dbHelper.addChefDetails(userDetail);

                                    homeFragmentModelList.add(homeFragmentModel);
                                    // -------------Layout setting on recycler view---------------
                                    LinearLayoutManager horizontalLayoutManagaer
                                            = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                    recyclerView.setLayoutManager(horizontalLayoutManagaer);
                                    recyclerView.setNestedScrollingEnabled(false);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());

                                    if (!homeFragmentModelList.isEmpty()) {
                                        // ---------------Adapter---------------------
                                        homeFragmentAdapter = new MapClassAdapter(getApplicationContext(), homeFragmentModelList);
                                        // setting adapter on recycler view
                                        recyclerView.setAdapter(homeFragmentAdapter);
                                    } else {
                                        no_image_available.setVisibility(View.VISIBLE);
                                        recyclerView.setVisibility(View.GONE);
                                    }


                                    for (int j = 0; j < markerPoints.size(); j++) {
                                        LatLng loct = markerPoints.get(j);
                                        // create marker
                                        MarkerOptions marker = new MarkerOptions().position(loct).title(homeFragmentModel.getKitchen_title());

                                        marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker_map));


                                        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                                                new LatLng(lat, lon)).zoom(10).build();

                                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                        mMap.addMarker(marker);

                                      /*  mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                            @Override
                                            public void onInfoWindowClick(Marker marker) {
                                                String title = marker.getTitle();
                                                for (int i=0;i<homeFragmentModelList.size();i++)
                                                {
                                                    if (title.equalsIgnoreCase(homeFragmentModelList.get(i).getKitchen_title()))
                                                    {
                                                        recyclerView.scrollToPosition(i);
                                                    }

                                                }

                                            }
                                        });*/
                                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                            @Override
                                            public boolean onMarkerClick(Marker marker) {
                                                String title = marker.getTitle();
                                                for (int i = 0; i < markerPoints.size(); i++) {
                                                    if (title.equalsIgnoreCase(homeFragmentModelList.get(i).getKitchen_title())) {
                                                        recyclerView.scrollToPosition(i);
                                                    }

                                                }
                                                return true;
                                            }
                                        });
                                    }
                                }

                                homeFragmentAdapter.notifyDataSetChanged();
                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                no_image_available.setVisibility(View.VISIBLE);
                                no_image_available.setText(message);
                                recyclerView.setVisibility(View.GONE);
                                Toast.makeText(MapClass.this, "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            // Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("getActivity", "onErrorResponse: " + message);
                Log.d("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //  String image = getStringImage(bitmap);
                map.put("latitude", latitude);
                map.put("longitude", longitude);
                map.put("userid", userId);

                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style);
        mMap.setMapStyle(style);
        mMap.getUiSettings().setZoomControlsEnabled(false);
    }
}
