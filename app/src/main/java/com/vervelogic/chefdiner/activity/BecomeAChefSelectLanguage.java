package com.vervelogic.chefdiner.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;

public class BecomeAChefSelectLanguage extends AppCompatActivity implements View.OnClickListener {

    /*-------------------------------------Declarations----------------------------------------*/
    RadioGroup langGroup;
    RadioButton englishChef, traditonalChef, simplifiedChef;
    UserDetail userDetail;
    DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_achef_select_language);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

       /* // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/

        /*------------------------------Ids----------------------*/
        langGroup = (RadioGroup) findViewById(R.id.language);
        englishChef = (RadioButton) findViewById(R.id.englishLang_Chef);
        traditonalChef = (RadioButton) findViewById(R.id.traditionalChinese_Chef);
        simplifiedChef = (RadioButton) findViewById(R.id.simplifiedChinese_Chef);

        /*-------------------------------DB-------------------------------------*/
        dbHelper = new DbHelper(this);
        userDetail = new UserDetail();

        /*---------------------Listeners-----------------------------------------*/
        englishChef.setOnClickListener(this);
        traditonalChef.setOnClickListener(this);
        simplifiedChef.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.englishLang_Chef:
                userDetail.setLanguage("english");
                Intent intent = new Intent(getApplicationContext(), BecomeAChefName.class);
                intent.putExtra("language", "english");
                startActivity(intent);
                break;
            case R.id.traditionalChinese_Chef:
                userDetail.setLanguage("traditional-chinese");
                Intent intent1 = new Intent(getApplicationContext(), BecomeAChefName.class);
                intent1.putExtra("language", "traditional-chinese");
                startActivity(intent1);
                break;
            case R.id.simplifiedChinese_Chef:
                userDetail.setLanguage("simplified-chinese");
                Intent intent2 = new Intent(getApplicationContext(), BecomeAChefName.class);
                intent2.putExtra("language", "simplified-chinese");
                startActivity(intent2);
                break;
            default:
                userDetail.setLanguage("english");
                Intent intent3 = new Intent(getApplicationContext(), BecomeAChefName.class);
                intent3.putExtra("language", "english");
                startActivity(intent3);
                break;
        }

    }
}
