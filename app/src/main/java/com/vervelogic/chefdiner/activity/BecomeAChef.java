package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.firebase.FirebaseFunctions;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BecomeAChef extends AppCompatActivity implements View.OnClickListener {

    /*---------------------Declarations---------------------------*/
    Toolbar toolbar;
    LinearLayout upload_picture_ly, create_menu_ly, basic_info_ly;
    ImageView picture_image_veryify, menu_image_verify;

    DbHelper dbHelper;
    String is_verify;
    String user_id;
    SharedPreference sharedPreference;
    Activity context = this;
    String id_chef, first_name_chef, last_name_chef, email_chef, phone_number_chef, profile_pic_chef, is_verify_chef_chef, is_user_chef,
            is_chef_chef, language_chef, authtoken_chef, chef_fname, chef_lname, language_speak, service_type, oftencook, chef_country,
            chef_city, from_guest, upto_guest, kitchen_title, kitchen_descrition, favorite, brunchStr, lunchStr, dinnerStr, currency,
            selectedCur = "", referal_code;
    ProgressDialog progressDialog;
    Button preview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_achef);

        /*-------------------------Toolbar----------------------------*/
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.become_a_chef);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

       /*-----------------------IDS---------------------------------------*/
        basic_info_ly = (LinearLayout) findViewById(R.id.basic_info_ly);
        upload_picture_ly = (LinearLayout) findViewById(R.id.upload_picture_ly);
        create_menu_ly = (LinearLayout) findViewById(R.id.create_menu_ly);
        menu_image_verify = (ImageView) findViewById(R.id.menu_image_verify);
        picture_image_veryify = (ImageView) findViewById(R.id.picture_image_veryify);
        preview = (Button) findViewById(R.id.preview);

        /*-----------------------Listener-----------------------------*/
        basic_info_ly.setOnClickListener(this);
        create_menu_ly.setOnClickListener(this);
        upload_picture_ly.setOnClickListener(this);
        preview.setOnClickListener(this);
        sharedPreference = new SharedPreference();
        user_id = sharedPreference.getUserId(context);
        dbHelper = new DbHelper(getApplicationContext());

        progressDialog = new ProgressDialog(this);
        /*--------------------------DB access-------------------------*/
      /*
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            is_verify = cn.getIsChef();
        }

        if (!TextUtils.isEmpty(is_verify)) {
            if (is_verify.equals("1")) {
                create_menu_ly.setEnabled(true);
                upload_picture_ly.setEnabled(true);
                picture_image_veryify.setImageResource(R.drawable.ic_check_circle_green);
                menu_image_verify.setImageResource(R.drawable.ic_check_circle_green);
            } else {
                create_menu_ly.setEnabled(false);
                upload_picture_ly.setEnabled(false);
                picture_image_veryify.setImageResource(R.drawable.ic_uncheck_mark);
                menu_image_verify.setImageResource(R.drawable.ic_uncheck_mark);
            }
        }*/
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton("OK", null).show();
        } else {
            getChefInfoAPI();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent intent = new Intent(getApplicationContext(), HomePage.class);
        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.upload_picture_ly:
                Intent intent2 = new Intent(getApplicationContext(), UploadPictures.class);
                startActivity(intent2);
                break;
            case R.id.basic_info_ly:
                Intent intent = new Intent(getApplicationContext(), BecomeAChefSelectLanguage.class);
                startActivity(intent);
                break;
            case R.id.create_menu_ly:
                Intent intent1 = new Intent(getApplicationContext(), AddAMenu.class);
                startActivity(intent1);
                break;
            case R.id.preview:
                Intent chefProfile = new Intent(view.getContext(), ChefProfile.class);
                chefProfile.putExtra("Chef_id", user_id);
              /*intent.putExtra("maxprice", homeFragmentModel.getMaxprice());
                intent.putExtra("whatsHotKey", homeFragmentModel.getWhatsHotKey());*/
                chefProfile.putExtra("keyChef", "preview");
                view.getContext().startActivity(chefProfile);
        }

    }

    // --------------IS CHEF VALIDATION API FOR BECOME A CHEF BUTTON------------
    public void getChefInfoAPI() {

        progressDialog.setMessage("Please Wait.....");
        progressDialog.setCancelable(false);
        progressDialog.show();

        String uri = String.format(AppConfig.GET_CHEF_INFO_URL + "?user_id=%1$s", user_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDismiss();

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            String message = jsonObject.getString("message");
                            if (status == 1) {
                                dbHelper.deleteUser();
                                Log.e("Become A chef ", response);
                                UserDetail userDetail = new UserDetail();
                                JSONArray jsonArray = jsonObject.getJSONArray("getchefinfo");
                                for (int i = 0; i <= jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    id_chef = object.getString("id");
                                    first_name_chef = object.getString("first_name");
                                    last_name_chef = object.getString("last_name");
                                    email_chef = object.getString("email");
                                    phone_number_chef = object.getString("phone_number");
                                    profile_pic_chef = object.getString("profile_pic");
                                    is_verify_chef_chef = object.getString("is_verify_chef");
                                    is_user_chef = object.getString("is_user");
                                    is_chef_chef = object.getString("is_chef");
                                    language_chef = object.getString("language");
                                    authtoken_chef = object.getString("authtoken");
                                    chef_fname = object.getString("chef_fname");
                                    chef_lname = object.getString("chef_lname");
                                    language_speak = object.getString("language_speak");
                                    service_type = object.getString("service_type");
                                    oftencook = object.getString("oftencook");
                                    chef_country = object.getString("chef_country");
                                    chef_city = object.getString("chef_city");
                                    from_guest = object.getString("from_guest");
                                    upto_guest = object.getString("upto_guest");
                                    kitchen_title = object.getString("kitchen_title");
                                    kitchen_descrition = object.getString("kitchen_descrition");
                                    favorite = object.getString("favorite");
                                    brunchStr = object.getString("brunch");
                                    lunchStr = object.getString("lunch");
                                    dinnerStr = object.getString("dinner");
                                    currency = object.getString("currency");
                                    String average_rating = object.getString("average_rating");
                                    String totalreview = object.getString("totalreview");
                                    String ok = object.getString("ok");
                                    String no_recommended = object.getString("no_recommended");
                                    String recommended = object.getString("recommended");
                                    String access_token = object.getString("access_token");
                                    String chef_latitude = object.getString("chef_latitude");
                                    String chef_longitude = object.getString("chef_longitude");
                                    String maxprice = object.getString("maxprice");
                                    String all_Images = object.getString("allimage");
                                    String price = object.getString("price");
                                    String cuisine = object.getString("cuisine");
                                    String is_menu = object.getString("is_menu");
                                    String is_pic = object.getString("is_pic");
                                    String is_verify = object.getString("is_verify");

                                /*    userDetail.setUser_id(user_id);
                                    userDetail.setFirstName(first_name_chef);
                                    userDetail.setLastName(last_name_chef);
                                    userDetail.setEmailId(email_chef);
                                    userDetail.setPhoneNumber(phone_number_chef);
                                    userDetail.setIsChefVerify(is_verify_chef_chef);
                                    userDetail.setLanguage(language_chef);
                                    userDetail.setIsChef(is_chef_chef);
                                    userDetail.setAuthToken(authtoken_chef);
                                    userDetail.setProfilePic(profile_pic_chef);
                                    userDetail.setLanguageList(language_speak);
                                    userDetail.setServiceType(service_type);
                                    userDetail.setOftenCook(oftencook);
                                    userDetail.setChefCountry(chef_country);
                                    userDetail.setChefCity(chef_city);
                                    userDetail.setMax_guest(upto_guest);
                                    userDetail.setMin_guest(from_guest);
                                    userDetail.setKitchenTitle(kitchen_title);
                                    userDetail.setKitchenDescription(kitchen_descrition);
                                    userDetail.setLunch(lunchStr);
                                    userDetail.setBrunch(brunchStr);
                                    userDetail.setDinner(dinnerStr);
                                    userDetail.setCurrency(currency);
                                    userDetail.setReferralCode(referal_code);
                                    dbHelper.addUser(userDetail);*/


                                    userDetail.setUser_id(user_id);
                                    userDetail.setFirstName(first_name_chef);
                                    userDetail.setLastName(last_name_chef);
                                    userDetail.setEmailId(email_chef);
                                    userDetail.setPhoneNumber(phone_number_chef);
                                    userDetail.setIsChefVerify(is_verify_chef_chef);
                                    userDetail.setIsVerifyUser(is_verify);
                                    userDetail.setLanguage(language_chef);
                                    userDetail.setIsChef(is_chef_chef);
                                    userDetail.setAuthToken(authtoken_chef);
                                    userDetail.setProfilePic(profile_pic_chef);
                                    userDetail.setLanguageList(language_speak);
                                    userDetail.setServiceType(service_type);
                                    userDetail.setOftenCook(oftencook);
                                    userDetail.setChefCountry(chef_country);
                                    userDetail.setChefCity(chef_city);
                                    userDetail.setCity(chef_city);
                                    userDetail.setMax_guest(upto_guest);
                                    userDetail.setMin_guest(from_guest);
                                    userDetail.setKitchenTitle(kitchen_title);
                                    userDetail.setKitchenDescription(kitchen_descrition);
                                    userDetail.setLunch(lunchStr);
                                    userDetail.setBrunch(brunchStr);
                                    userDetail.setDinner(dinnerStr);
                                    userDetail.setCurrency(currency);
                                    userDetail.setReferralCode(referal_code);
                                    userDetail.setTotalReview(totalreview + " " + getResources().getString(R.string.review));
                                    userDetail.setAvgRating(average_rating);
                                    userDetail.setRecommended(recommended);
                                    userDetail.setNotRecommended(no_recommended);
                                    userDetail.setOk(ok);
                                    userDetail.setLanguage(language_chef);
                                    userDetail.setAccessToken(access_token);
                                    userDetail.setLatitude(chef_latitude);
                                    userDetail.setLongitude(chef_longitude);

                                    userDetail.setCuisine(cuisine);
                                    userDetail.setIsVerifyUser(is_verify);
                                    userDetail.setKitchenTitle(kitchen_title);
                                    userDetail.setPrice(price);
                                    userDetail.setAllImages(all_Images);
                                    /*userDetail.setChefRate("4.5");*/
                                   /* userDetail.setFoodType2("Italian");*/
                                    userDetail.setFavCount(favorite);
                                    userDetail.setCountry(chef_country);
                                    userDetail.setMaxPrice(maxprice);

                                    dbHelper.addUser(userDetail);
                                    dbHelper.updateChefDetails(userDetail);

                                    /*Updating data on firebase*/
                                    String UserName = first_name_chef + " " + last_name_chef;
                                    FirebaseFunctions.updateUserName(user_id, UserName);
                                    FirebaseFunctions.updateUserImage(user_id, profile_pic_chef);
                                    FirebaseFunctions.updateUserMobileNo(user_id, phone_number_chef);

                                    if (!TextUtils.isEmpty(is_chef_chef)) {
                                        if (is_chef_chef.equals("1")) {
                                            create_menu_ly.setEnabled(true);
                                            upload_picture_ly.setEnabled(true);
                                           /* picture_image_veryify.setImageResource(R.drawable.ic_check_circle_green);
                                            menu_image_verify.setImageResource(R.drawable.ic_check_circle_green);*/
                                        } else {
                                            create_menu_ly.setEnabled(false);
                                            upload_picture_ly.setEnabled(false);
                                          /*  picture_image_veryify.setImageResource(R.drawable.ic_uncheck_mark);
                                            menu_image_verify.setImageResource(R.drawable.ic_uncheck_mark);*/
                                        }
                                    }
                                    if (!TextUtils.isEmpty(is_menu)) {
                                        if (is_menu.equals("1")) {
                                            create_menu_ly.setEnabled(true);
                                           /* upload_picture_ly.setEnabled(true);
                                            picture_image_veryify.setImageResource(R.drawable.ic_check_circle_green);*/
                                            menu_image_verify.setImageResource(R.drawable.ic_check_circle_green);
                                        } else {
                                            create_menu_ly.setEnabled(true);
                                            menu_image_verify.setVisibility(View.GONE);
                                          /*  upload_picture_ly.setEnabled(false);
                                            picture_image_veryify.setImageResource(R.drawable.ic_uncheck_mark);*/
                                            //menu_image_verify.setImageResource(R.drawable.ic_uncheck_mark);
                                        }
                                    }
                                    if (!TextUtils.isEmpty(is_pic)) {
                                        if (is_pic.equals("1")) {
                                          /*  create_menu_ly.setEnabled(true);*/
                                            upload_picture_ly.setEnabled(true);
                                            picture_image_veryify.setImageResource(R.drawable.ic_check_circle_green);
                                            /*menu_image_verify.setImageResource(R.drawable.ic_check_circle_green);*/
                                        } else {
                                           /* create_menu_ly.setEnabled(false);*/
                                            upload_picture_ly.setEnabled(true);
                                            picture_image_veryify.setVisibility(View.GONE);
                                           /* picture_image_veryify.setImageResource(R.drawable.ic_uncheck_mark);
                                            menu_image_verify.setImageResource(R.drawable.ic_uncheck_mark);*/
                                        }
                                    }

                                 /*   Intent intent = new Intent(getApplicationContext(), BecomeAChef.class);
                                    startActivity(intent);*/
                                }
                            } else if (status == 0) {
                                Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDismiss();
                            //Toast.makeText(getActivity(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e("Become A chef", "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void progressDismiss() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
