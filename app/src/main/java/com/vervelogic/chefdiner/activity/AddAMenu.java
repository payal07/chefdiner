package com.vervelogic.chefdiner.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.adapter.FilterRecyclerAdapter;
import com.vervelogic.chefdiner.adapter.FilterValRecyclerAdapter;
import com.vervelogic.chefdiner.adapter.ItemResponse;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.app.CustomMultipartRequest;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.CuisineSubCuisineModel;
import com.vervelogic.chefdiner.model.MenuListModel;
import com.vervelogic.chefdiner.model.RecyclerTouchListener;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddAMenu extends AppCompatActivity implements ItemResponse {


    // Declarations....
    private static final String TAG = AddAMenu.class.getSimpleName();
    private static final int CAMERA_ENABLE_PERMISSION = 100;
    Toolbar toolbar;
    Button add_menu, saveMenu, saveDish;
    EditText menuTitle, dishTitle, price;
    TextView cancel, min, max, empty_text;
    String menuTitleStr, dishTitleStr;
    SeekBar multiSlider;
    TextView cuisine_add_menu;
    CircleImageView add_menu_image, add_dish_image;
    ImageView add_image_chef, add_image_dish;
    RecyclerView editMenuRecyclerView;
    Bitmap bitmap;
    MenuListAdapter menuListAdapter;
    List<MenuListModel> menulist = new ArrayList<>();
    String list,serviceList;
    String dishCourse;
    String pricePerPerson = "10";
    CheckBox food_delivery_chef, home_private_meal_chef, restaurant_private_meal_chef,
            chef_onsite_cooking_chef, cooking_workshop_chef, daily_meal;
    String id = "0", user_id, menu_title, menu_image, service_type, price_per_person;
    int cuisine_id;
    String selectedCurrency;
    LinearLayout cusine_ly;
    ArrayAdapter<String> CuisineAdapter;
    ArrayAdapter<String> subCuisineAdapter;
    String sendImage = "";
    String sendDishImage = "";
    String menu_id = "0";
    DbHelper dbHelper;
    String language;
    /*------------------ LIST-------------------------SPINNER---------------CUISINE-----------------------*/
    List<String> CUISINE_LIST = new ArrayList<>();
    List<Integer> CUISINE_INT = new ArrayList<>();
    Activity context = this;
    RecyclerView filterListView = null;
    RecyclerView filterValListView;
    FilterRecyclerAdapter adapter;
    FilterValRecyclerAdapter filterValAdapter;
    List<CuisineSubCuisineModel.CuisineBean> cuisineBeans;
    CuisineSubCuisineModel cuisineSubCuisine;
    List<CuisineSubCuisineModel.CuisineBean.SubCuisneBean> subCuisneBeans;
    Set<String> cuisineList = new HashSet<>();
    List<String> subCuisineList = new ArrayList<>();
    TextView ok_btn;
    ImageView clearbtn;
    String cuisineListStr = "", subcuisineListStr;
    TextWatcher searchtext = new TextWatcher() {
        int name = 0;
        String s1 = "0";

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

           // multiSlider.setLeft(123);
          //  Log.e("RRRRRR",""+s);
        }

        @Override
        public void afterTextChanged(final Editable s) {
            if (!TextUtils.isEmpty(s))
            {
                    int value=Integer.valueOf(s.toString());
                    multiSlider.setProgress(value);
            }
        }
    };
    private int TAKE_PICTURE = 2;
    private int SELECT_PICTURE = 1;
    private int GALLERY_DISH = 3;
    private int CAMERA_DISH = 4;
    //----------------------------------------- Runtime permissions--------------------------------------
    private SharedPreferences permissionStatus;
    private boolean sentToSettings = false;
    private SharedPreference sharedPreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_amenu);

        /*------------------------Toolbar---------------------------------------------------*/
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.edit_menu);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //------------------------------ RUNTIME PERMISSION--------------------------------------------
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);

        if (ActivityCompat.checkSelfPermission(AddAMenu.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(AddAMenu.this, Manifest.permission.CAMERA)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddAMenu.this);
                builder.setTitle(R.string.need_permission);
                builder.setMessage("This app needs camera access");
                builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        ActivityCompat.requestPermissions(AddAMenu.this, new String[]{Manifest.permission.CAMERA}, CAMERA_ENABLE_PERMISSION);

                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        finish();
                    }
                });
                builder.show();

            } else {
                ActivityCompat.requestPermissions(AddAMenu.this, new String[]{Manifest.permission.CAMERA}, CAMERA_ENABLE_PERMISSION);
            }
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.CAMERA, true);
            editor.commit();
        } else {
            proceedAfterPermission();

        }

        /*------------------------shared preference for user id and language---------------------------*/
        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {

            user_id = sharedPreference.getUserId(context);
        } else {

            user_id = "";
        }
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(context))) {
            language = sharedPreference.getLanguage(context);
        } else {
            language = "english";
        }

        /*----------------------------------------DB--------------------------------------------*/
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            selectedCurrency = cn.getCurrency();
        }


        //--------------------------------- IDs-------------------------------------------
        add_menu = (Button) findViewById(R.id.add_menu);

        //--------------------------- GET MENU API CALL--------------------------------------------
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            getMenuURL();

        }

        //------------------SETTING LISTENER ON ADD MENU BUTTON PREVIEW DIALOG BOX-----------------------------
        add_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // ------------------------------custom dialog--------------------------------------
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.add_menu_layout);

                //------------------------------DIALOG BOX ID------------------------------------------
                saveMenu = dialog.findViewById(R.id.save_menu);
                menuTitle = dialog.findViewById(R.id.menu_title);
                menuTitleStr = menuTitle.getText().toString();
                min = dialog.findViewById(R.id.min);
                max = dialog.findViewById(R.id.max);
                price = dialog.findViewById(R.id.price);
                multiSlider = dialog.findViewById(R.id.range_slider_add_menu);
                price.addTextChangedListener(searchtext);
                cuisine_add_menu = dialog.findViewById(R.id.cuisine_add_menu);
                cusine_ly = dialog.findViewById(R.id.cusine_ly);

                add_image_chef = dialog.findViewById(R.id.add_image_chef);
                add_menu_image = dialog.findViewById(R.id.add_menu_image);

                //------------------CHECKBOX IDS---------------------------------------------------
                food_delivery_chef = dialog.findViewById(R.id.food_delivery_add_menu);
                home_private_meal_chef = dialog.findViewById(R.id.home_private_meal_add_menu);
                restaurant_private_meal_chef = dialog.findViewById(R.id.restaurant_private_meal_add_menu);
                chef_onsite_cooking_chef = dialog.findViewById(R.id.chef_onsite_cooking_add_menu);
                cooking_workshop_chef = dialog.findViewById(R.id.cooking_workshop_add_menu);
                daily_meal = dialog.findViewById(R.id.daily_meal_add_menu);


                dbHelper = new DbHelper(getApplicationContext());
                List<UserDetail> contacts = dbHelper.getAllDetails();
                for (UserDetail cn : contacts) {
                    serviceList = cn.getServiceType();
                    Log.e("List", "" + cn.getLanguageList());

                }

              if (!TextUtils.isEmpty(serviceList)) {
                            List<String> items = Arrays.asList(serviceList.split("\\s*,\\s*"));
                            Log.e("ListItem", "" + items);
                            for (int i = 0; i < items.size(); i++) {
                                String stringList = items.get(i);
                                if (stringList.equalsIgnoreCase("Food delivery")) {
                                    food_delivery_chef.setClickable(true);
                                    food_delivery_chef.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    food_delivery_chef.setPadding(20,0,20,0);
                                }
                                if (stringList.equalsIgnoreCase("Home private meal")) {
                                    home_private_meal_chef.setClickable(true);
                                    home_private_meal_chef.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    home_private_meal_chef.setPadding(20,0,20,0);
                                }
                                if (stringList.equalsIgnoreCase("Restaurant private meal")) {
                                    restaurant_private_meal_chef.setClickable(true);
                                    restaurant_private_meal_chef.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    daily_meal.setPadding(20,0,20,0);
                                }
                                if (stringList.equalsIgnoreCase("Chef onsite cooking")) {
                                    chef_onsite_cooking_chef.setClickable(true);
                                    chef_onsite_cooking_chef.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    chef_onsite_cooking_chef.setPadding(20,0,20,0);
                                }
                                if (stringList.equalsIgnoreCase("Cooking workshop")) {
                                    cooking_workshop_chef.setClickable(true);
                                    cooking_workshop_chef.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    cooking_workshop_chef.setPadding(20,0,20,0);
                                }
                                if (stringList.equalsIgnoreCase("Daily meal")) {
                                    daily_meal.setClickable(true);
                                    daily_meal.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    daily_meal.setPadding(20,0,20,0);
                                }

                            }
                        }

                    add_image_chef.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showPictureDialog();
                    }
                });
                cusine_ly.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // ------------------------------custom dialog--------------------------------------

                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            new AlertDialog.Builder(AddAMenu.this)
                                    .setTitle(getResources().getString(R.string.app_name))
                                    .setMessage(getResources().getString(R.string.internet_error))
                                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
                        } else {
                            getCuisineList();
                        }

                    }
                });
               /* multiSlider.setMaxValue(1000);*/
             /*   final int priceInt = Integer.parseInt(price.getText().toString());
                price.addTextChangedListener(searchtext);
            *//*    if (priceInt<=1000)
                {
                  multiSlider.setPosition(priceInt);
                }*/
               /* multiSlider.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
                    @Override
                    public void valueChanged(Number value) {
                        pricePerPerson = "" + value;
                        min.setText(R.string.min_cur);
                        price.setText("" + value);
                    }
                });*/
               multiSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                   @Override
                   public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                       pricePerPerson = ""+i;
                       min.setText(R.string.min_cur);

                   }

                   @Override
                   public void onStartTrackingTouch(SeekBar seekBar) {

                   }

                   @Override
                   public void onStopTrackingTouch(SeekBar seekBar) {
                       price.setText(""+seekBar.getProgress());
                   }
               });
               /* multiSlider.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
                    @Override
                    public void valueChanged(Number minValue, Number maxValue) {
                      *//*  if (minValue == 0) {*//*

                      *//*  }*//*
                        //    tvMax.setText(String.valueOf(maxValue));
                    }
                });*/

              /*  multiSlider.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
                    @Override
                    public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                        if (thumbIndex == 0) {
                            pricePerPerson = "" + value;
                            min.setText(R.string.min_cur);
                            price.setText("" + value);
                        } else {
                            max.setText("" + value);
                        }
                    }
                });*/

                //------------------------------ if button is clicked, close the custom dialog----------------------
                saveMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        menuTitleStr = menuTitle.getText().toString();
                        // CALLING METHOD TO GET CHECKBOX LIST AND API CALL
                        validate();

                        dialog.dismiss();
                    }
                });
                cancel = dialog.findViewById(R.id.cancel);

                // if button is clicked, close the custom dialog
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    @SuppressLint("LongLogTag")
    private void proceedAfterPermission() {
        Log.e("Got The Camera permission", "Got it");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(AddAMenu.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermission();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_ENABLE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermission();

            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(AddAMenu.this, Manifest.permission.CAMERA)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddAMenu.this);
                    builder.setTitle(R.string.need_permission);
                    builder.setMessage(getResources().getString(R.string.camera_need_access));
                    builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            ActivityCompat.requestPermissions(AddAMenu.this, new String[]{Manifest.permission.CAMERA}, CAMERA_ENABLE_PERMISSION);

                        }
                    });
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            finish();
                        }
                    });
                    builder.show();

                } else {
                    Toast.makeText(this, "Unable to get permission", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent intent=new Intent(getApplicationContext(),BecomeAChef.class);
        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showPictureDialog() {
        final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.choose_photo),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.add_photo);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (options[which].equals(getString(R.string.take_photo))) {
                    Intent cameraIntent = new Intent(
                            android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, TAKE_PICTURE);


                } else if (options[which].equals(getString(R.string.choose_photo))) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, SELECT_PICTURE);
                } else if (options[which].equals(R.string.cancel)) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();
    }

    private void showPictureDialogAddDishImage() {

        final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.choose_photo),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.add_photo);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (options[which].equals(R.string.take_photo)) {
                    Intent cameraIntent = new Intent(
                            android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_DISH);


                } else if (options[which].equals(R.string.choose_photo)) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, GALLERY_DISH);
                } else if (options[which].equals(R.string.cancel)) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == CAMERA_ENABLE_PERMISSION) {
            if (ActivityCompat.checkSelfPermission(AddAMenu.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermission();

            }
        }
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            Uri filePath = intent.getData();
            if (null != filePath) {
                // Get the path from the Uri
                sendImage = getPathFromURI(filePath);
                // Set the image in ImageView
                add_menu_image.setImageURI(filePath);
                add_image_chef.setImageResource(android.R.color.transparent);

            }

        } else if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            if (intent.getExtras() == null) {
                return;
            }
            bitmap = (Bitmap) intent.getExtras().get("data");
            add_menu_image.setImageBitmap(bitmap);
            add_image_chef.setImageResource(android.R.color.transparent);
            Uri tempUri = getImageUri(this, bitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            sendImage = finalFile.toString();

        } else if (requestCode == GALLERY_DISH && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            Uri filePath = intent.getData();
            if (null != filePath) {
                // Get the path from the Uri
                sendDishImage = getPathFromURI(filePath);
                // Set the image in ImageView
                add_dish_image.setImageURI(filePath);
                add_image_dish.setImageResource(android.R.color.transparent);
            }
        } else if (requestCode == CAMERA_DISH && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            if (intent.getExtras() == null) {
                return;
            }
            bitmap = (Bitmap) intent.getExtras().get("data");
            add_dish_image.setImageBitmap(bitmap);
            add_image_dish.setImageResource(android.R.color.transparent);
            Uri tempUri = getImageUri(this, bitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            sendDishImage = finalFile.toString();
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null,
                null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    /*---------------------------Get Cuisine List----------------------------------------*/
    public void getCuisineList() {
      /*  final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        String uri = "";
        if (!TextUtils.isEmpty(language)) {
            if (language.equalsIgnoreCase("english")) {
                uri = String.format(AppConfig.GET_CUISINE_URL + "?language=%1$s", "english");
            }
            if (language.equalsIgnoreCase("simplified chinese")) {
                uri = String.format(AppConfig.GET_CUISINE_URL + "?language=%1$s", "simplified");
            }
            if (language.equalsIgnoreCase("traditional chinese")) {
                uri = String.format(AppConfig.GET_CUISINE_URL + "?language=%1$s", "traditional");
            }
        }
        // String uri = String.format(AppConfig.GET_CUISINE_URL+"?language=%1$s",language);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                     /*   progressDialog.dismiss();*/
                        try {
                            cuisineSubCuisine = new Gson().fromJson(s, CuisineSubCuisineModel.class);

                            JSONObject jsonObject = new JSONObject(s);

                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                final Dialog dialog = new Dialog(context);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                                Window window = dialog.getWindow();
                                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog.setContentView(R.layout.cuisine_subcuisine_ly);

                                filterListView = dialog.findViewById(R.id.filter_dialog_listview);
                                filterValListView = dialog.findViewById(R.id.filter_value_listview);
                                cuisineBeans = cuisineSubCuisine.getCuisine();

                                LinearLayoutManager horizontalLayoutManager
                                        = new LinearLayoutManager(AddAMenu.this, LinearLayoutManager.VERTICAL, false);
                                filterListView.setLayoutManager(horizontalLayoutManager);

                                filterListView.setItemAnimator(new DefaultItemAnimator());
                                subCuisneBeans = cuisineBeans.get(0).getSubCuisne();

                                adapter = new FilterRecyclerAdapter(AddAMenu.this, cuisineBeans);
                                filterListView.setAdapter(adapter);
                                adapter.setOnItemClickListener(new FilterRecyclerAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View v, int position) {
                                        filterItemListClicked(position, v, cuisineBeans.get(position).getCuisine_id());
                                        adapter.setItemSelected(position);
                                    }
                                });

                                filterListView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), filterListView, new RecyclerTouchListener.ClickListener() {
                                    @Override
                                    public void onClick(View view, int position) {
                                        CuisineSubCuisineModel.CuisineBean cuisineBean = cuisineBeans.get(position);

                                        cuisineList.add(cuisineBean.getCuisine_name());
                                    }

                                    @Override
                                    public void onLongClick(View view, int position) {

                                    }
                                }));

                                filterItemListClicked(0, null, "");
                                adapter.setItemSelected(0);
                                ok_btn = dialog.findViewById(R.id.btn_filter);
                                clearbtn = dialog.findViewById(R.id.btn_clear);

                                ok_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        for (CuisineSubCuisineModel.CuisineBean.SubCuisneBean cuisineSubCuisineModel : subCuisneBeans) {
                                            if (cuisineSubCuisineModel.isChecked()) {

                                                subcuisineListStr = TextUtils.join(",", subCuisineList);
                                                cuisineListStr = TextUtils.join(",", cuisineList);
                                                Log.e(TAG, "SubCuisine " + subcuisineListStr);
                                                Log.e(TAG, "Cuisine " + cuisineListStr);
                                                cuisine_add_menu.setText(cuisineListStr);
                                            }
                                        }
                                        if (subCuisineList.isEmpty() && cuisineList.isEmpty()) {
                                            Toast.makeText(AddAMenu.this, getResources().getString(R.string.select_cuisine_subcuisine)
                                                    , Toast.LENGTH_SHORT).show();
                                        }
                                        dialog.dismiss();
                                    }
                                });

                                clearbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        for (CuisineSubCuisineModel.CuisineBean.SubCuisneBean cuisineSubCuisineModel : subCuisneBeans) {
                                            cuisineSubCuisineModel.setChecked(false);
                                        }

                                        subCuisineList.clear();
                                        cuisineList.clear();
                                        adapter.notifyDataSetChanged();
                                        filterValAdapter.notifyDataSetChanged();
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                          /*  progressDialog.dismiss();*/
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            //  Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                          /*  progressDialog.dismiss();*/
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        volleyError.printStackTrace();
                    }
                });
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Only if you need to restore open/close state when
        // the orientation is changed
        if (menuListAdapter != null) {
            menuListAdapter.saveStates(outState);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Only if you need to restore open/close state when
        // the orientation is changed
        if (menuListAdapter != null) {
            menuListAdapter.restoreStates(savedInstanceState);
        }
    }

    private void filterItemListClicked(int position, View v, String cuisine_id) {
        LinearLayoutManager horizontalLayoutManager1
                = new LinearLayoutManager(AddAMenu.this, LinearLayoutManager.VERTICAL, false);
        filterValListView.setLayoutManager(horizontalLayoutManager1);

        filterValListView.setItemAnimator(new DefaultItemAnimator());
        subCuisneBeans = cuisineBeans.get(position).getSubCuisne();


        filterValAdapter = new FilterValRecyclerAdapter(AddAMenu.this,
                subCuisneBeans, this, cuisine_id);


        filterValListView.setAdapter(filterValAdapter);

        filterValAdapter.setOnItemClickListener(new FilterValRecyclerAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                filterValitemListClicked(position);
                CuisineSubCuisineModel.CuisineBean.SubCuisneBean cuisineBeans = subCuisneBeans.get(position);
                if (cuisineBeans.isChecked()) {
                    subCuisineList.add(cuisineBeans.getSubcuisine_name());
                }
            }
        });

        filterValAdapter.notifyDataSetChanged();
    }

    private void filterValitemListClicked(int position) {
        filterValAdapter.setItemSelected(position);

    }

    @Override
    public void itemClickResponse(Object status, Object response, String pos) {
        boolean s = (boolean) status;
        if (s == true) {

            List<CuisineSubCuisineModel.CuisineBean.SubCuisneBean> subCuisneBeans = (List<CuisineSubCuisineModel.CuisineBean.SubCuisneBean>) response;
            // subCuisneBean.getSubcuisine_id();
            int count = 0;
            for (int j = 0; j < subCuisneBeans.size(); j++) {
                if (subCuisneBeans.get(j).isChecked()) {
                    count = count + 1;
                }
            }

            for (int i = 0; i < cuisineBeans.size(); i++) {
                if (cuisineBeans.get(i).getCuisine_id().equalsIgnoreCase(pos)) {
                    cuisineBeans.get(i).setC(count);
                    adapter.notifyDataSetChanged();
                }
            }


        }
    }

    /*-----------------------------------------------------ADD MENU AND GET MENU-------------------------------------------------------------------------------------*/
    public void AddMenuAPI(String sendImage) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        if (!sendImage.isEmpty() && sendImage != null && !sendImage.equals("")) {
            //Auth header
            Map<String, String> mHeaderPart = new HashMap<>();
            mHeaderPart.put("Content-type", "multipart/form-data;");

            //File part
            Map<String, File> mFilePartData = new HashMap<>();
            mFilePartData.put("fileToUpload", new File(sendImage));


            //String part
            Map<String, String> mStringPart = new HashMap<>();
            mStringPart.put("user_id", user_id);
            mStringPart.put("menu_id", "0");
            mStringPart.put("menu_title", menuTitleStr);
            mStringPart.put("service_type", list);
            mStringPart.put("price_per_person", price.getText().toString());
            mStringPart.put("currency", selectedCurrency);
            mStringPart.put("cusine", cuisineListStr);
            mStringPart.put("subcusine", subcuisineListStr);
            Log.e("Data", "" + mStringPart);


            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, getApplicationContext(), AppConfig.ADD_MENU_URL, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    progressDialog.dismiss();
                    Log.e(TAG, "" + jsonObject);
                    try {

                        menu_id = jsonObject.getString("id");
                        Log.e(TAG, "" + jsonObject);
                        String message = jsonObject.getString("message");
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            new AlertDialog.Builder(AddAMenu.this)
                                    .setTitle(getResources().getString(R.string.app_name))
                                    .setMessage(getResources().getString(R.string.internet_error))
                                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
                        } else {
                            getMenuURL();
                        }

                        Log.e(TAG, "" + message);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    if (getApplicationContext() != null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());
                }

            }, mFilePartData, mStringPart, mHeaderPart);

            RequestQueue rQueue = Volley.newRequestQueue(this);
            rQueue.add(mCustomRequest);
        } else {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.ADD_MENU_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.e(TAG, "" + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                menu_id = jsonObject.getString("id");
                                String message = jsonObject.getString("message");
                                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    new AlertDialog.Builder(AddAMenu.this)
                                            .setTitle(getResources().getString(R.string.app_name))
                                            .setMessage(getResources().getString(R.string.internet_error))
                                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                } else {
                                    getMenuURL();
                                }
                                Log.e(TAG, "" + message);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (getApplicationContext() != null)
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                if (getApplicationContext() != null)
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    if (getApplicationContext() != null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("user_id", user_id);
                    map.put("menu_id", "0");
                    map.put("menu_title", menuTitleStr);
                    map.put("service_type", list);
                    map.put("price_per_person", price.getText().toString());
                    map.put("currency", selectedCurrency);
                    map.put("cusine", cuisineListStr);
                    map.put("subcusine", subcuisineListStr);
                    Log.e("Map", "" + map);
                    return map;
                }
            };
            int socketTimeout = 30000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(stringRequest);
        }
    }

    /*-----------------------------Edit menu api--------------------------------------------------*/
    public void editMenuAPI(final String menuID, String sendImage, final String cuisineListStr, final String subcuisineListStr) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        if (!sendImage.isEmpty() && sendImage != null && !sendImage.equals("")) {
            //Auth header
            Map<String, String> mHeaderPart = new HashMap<>();
            mHeaderPart.put("Content-type", "multipart/form-data;");

            //File part
            Map<String, File> mFilePartData = new HashMap<>();
            mFilePartData.put("fileToUpload", new File(sendImage));


            //String part
            Map<String, String> mStringPart = new HashMap<>();
            mStringPart.put("user_id", user_id);
            mStringPart.put("menu_id", menuID);
            mStringPart.put("menu_title", menuTitleStr);
            mStringPart.put("service_type", list);
            mStringPart.put("price_per_person", price.getText().toString());
            mStringPart.put("currency", selectedCurrency);
            mStringPart.put("cusine", cuisineListStr);
            mStringPart.put("subcusine", subcuisineListStr);
            Log.e("Data", "" + mStringPart);


            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, getApplicationContext(), AppConfig.ADD_MENU_URL, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    progressDialog.dismiss();
                    Log.e(TAG, "" + jsonObject);
                    try {

                        menu_id = jsonObject.getString("id");
                        Log.e(TAG, "" + jsonObject);
                        String message = jsonObject.getString("message");
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            new AlertDialog.Builder(AddAMenu.this)
                                    .setTitle(getResources().getString(R.string.app_name))
                                    .setMessage(getResources().getString(R.string.internet_error))
                                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
                        } else {
                            getMenuURL();
                        }
                        Log.e(TAG, "" + message);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    if (getApplicationContext() != null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());
                }

            }, mFilePartData, mStringPart, mHeaderPart);
            RequestQueue rQueue = Volley.newRequestQueue(this);
            rQueue.add(mCustomRequest);
        } else {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.ADD_MENU_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.e(TAG, "" + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                menu_id = jsonObject.getString("id");
                                String message = jsonObject.getString("message");
                                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    new AlertDialog.Builder(AddAMenu.this)
                                            .setTitle(getResources().getString(R.string.app_name))
                                            .setMessage(getResources().getString(R.string.internet_error))
                                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                } else {
                                    getMenuURL();
                                }
                                Log.e(TAG, "" + message);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                if (getApplicationContext() != null)
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                if (getApplicationContext() != null)
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    if (getApplicationContext() != null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.d("Error: ", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("user_id", user_id);
                    map.put("menu_id", menuID);
                    map.put("menu_title", menuTitleStr);
                    map.put("service_type", list);
                    map.put("price_per_person", price.getText().toString());
                    map.put("currency", selectedCurrency);
                    map.put("cusine", cuisineListStr);
                    map.put("subcusine", subcuisineListStr);
                    Log.e("Map", "" + map);
                    return map;
                }
            };
            int socketTimeout = 30000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(stringRequest);
        }
    }

    /*---------------------------------Get All Menu-------------------------------------------------*/
    public void getMenuURL() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.GET_MENU_URL + "?user_id=%1$s", user_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                menulist.clear();
                                JSONArray jsonArray = jsonObject.getJSONArray("getMenus");
                                for (int i = 0; i <= jsonArray.length(); i++) {

                                    MenuListModel menuListModel = new MenuListModel();
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    menu_id = object.getString("id");
                                    // user_id=object.getString("user_id");
                                    menu_title = object.getString("menu_title");
                                    service_type = object.getString("service_type");
                                    price_per_person = object.getString("actualprice");
                                    String cuisineList = object.getString("cusine");
                                    String subCuisineList = object.getString("subcusine");
                                    String countsubmenu = object.getString("countsubmenu");
                                    String currency = object.getString("currency");
                                    String status1 = object.getString("status");
                                    menu_image = object.getString("menu_image");
                                    menuListModel.setCuisineList(cuisineList);
                                    menuListModel.setSubCuisineList(subCuisineList);
                                    menuListModel.setMenuTitle(menu_title);
                                    menuListModel.setMenuId(menu_id);
                                    menuListModel.setPrice(currency + price_per_person);
                                    menuListModel.setActualPrice(currency + price_per_person);
                                    menuListModel.setImageUrl(menu_image);
                                    menuListModel.setMoney(price_per_person);
                                    menuListModel.setServiceList(service_type);
                                    menuListModel.setDishesCount(countsubmenu);
                                    menuListModel.setStatus1(status1);
                                    menulist.add(menuListModel);
                                    editMenuRecyclerView = (RecyclerView) findViewById(R.id.edit_menu_ly);
                                    empty_text = (TextView) findViewById(R.id.empty_text);
                                    //---------------- Menu Recycler View........---------------------------------------
                                    LinearLayoutManager horizontalLayoutManagaer
                                            = new LinearLayoutManager(AddAMenu.this, LinearLayoutManager.VERTICAL, false);
                                    editMenuRecyclerView.setLayoutManager(horizontalLayoutManagaer);
                                    editMenuRecyclerView.setVisibility(View.VISIBLE);
                                    editMenuRecyclerView.setNestedScrollingEnabled(false);
                                    empty_text.setVisibility(View.GONE);
                                    menuListAdapter = new MenuListAdapter(getApplicationContext(), menulist);
                                    editMenuRecyclerView.setAdapter(menuListAdapter);

                                }
                                menuListAdapter.notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            //  Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        progressDialog.dismiss();
                    }
                });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /*--------------------------------Delete Menu------------------------------------------------*/
    public void deleteMenuAPI(String menu_id) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.DELETE_MENU_URL + "?menu_id=%1$s", menu_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                String message = jsonObject.getString("message");
                                if (getApplicationContext() != null)
                                    Toast.makeText(AddAMenu.this, "" + message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void draftAPI(final String menu_id, final String draftStatus) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.DELETE_MENU_URL + "?menu_id=%1$s", menu_id);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.DRAFT_MENU,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                String message = jsonObject.getString("message");
                                if (getApplicationContext() != null)
                                    Toast.makeText(AddAMenu.this, "" + message, Toast.LENGTH_SHORT).show();
                                getMenuURL();
                            }

                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        if (getApplicationContext() != null)
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("menuid", menu_id);
                map.put("draftstatus", draftStatus);

                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /*--------------------------------------------------ADD SUB MENU-----------------------------------------------------------------------------------------*/
    public void addSubMenuURL(String sendDishImage, final String menuID) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        if (!sendImage.isEmpty() && sendImage != null && !sendImage.equals("")) {
            //Auth header
            Map<String, String> mHeaderPart = new HashMap<>();
            mHeaderPart.put("Content-type", "multipart/form-data;");

            //File part
            Map<String, File> mFilePartData = new HashMap<>();
            mFilePartData.put("fileToUpload", new File(sendDishImage));
            Log.e("Image File", "" + new File(sendDishImage));

            //String part
            Map<String, String> mStringPart = new HashMap<>();
            mStringPart.put("user_id", user_id);
            mStringPart.put("id", id);
            mStringPart.put("dish_name", dishTitleStr);
            mStringPart.put("dish_category", dishCourse);
            mStringPart.put("menu_id", menuID);
            Log.e("Data", "" + mStringPart);

            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, getApplicationContext(), AppConfig.ADD_SUBMENU_URL, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    progressDialog.dismiss();
                    Log.e(TAG, "" + jsonObject);
                    try {
                        JSONObject jsonObject1 = new JSONObject();
                        int status = jsonObject1.getInt("status");
                        if (status == 1) {
                            String message = jsonObject1.getString("message");
                            id = jsonObject1.getString("id");
                            Log.e(TAG, "" + message);

                        } else {
                            Log.e(TAG, "" + jsonObject);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    if (getApplicationContext() != null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());
                }

            }, mFilePartData, mStringPart, mHeaderPart);

            RequestQueue rQueue = Volley.newRequestQueue(this);
            rQueue.add(mCustomRequest);
        } else {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.ADD_SUBMENU_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.e(TAG, "" + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                id = jsonObject.getString("id");
                                String message = jsonObject.getString("message");
                                Log.e(TAG, "" + message);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();

                    map.put("menu_id", menuID);
                    map.put("user_id", user_id);
                    map.put("id", id);
                    map.put("dish_name", dishTitleStr);
                    map.put("dish_category", dishCourse);
                    return map;
                }
            };

            AppController.getInstance().addToRequestQueue(stringRequest);
        }

    }

    /*------------------------------------------Validation Of All ---------------------------------------------------------*/

    public boolean validate() {
        String food_delivery_chefStr = food_delivery_chef.getText().toString();
        String home_private_meal_chefStr = home_private_meal_chef.getText().toString();
        String restaurant_private_meal_chefStr = restaurant_private_meal_chef.getText().toString();
        String chef_onsite_cooking_chefStr = chef_onsite_cooking_chef.getText().toString();
        String cooking_workshop_chefStr = cooking_workshop_chef.getText().toString();
        String daily_mealStr = daily_meal.getText().toString();

        List<String> serviceList = new ArrayList<>();
        if (food_delivery_chef.isChecked()) {
            serviceList.add("Food delivery");
        }
        if (home_private_meal_chef.isChecked()) {
            serviceList.add("Home private meal");
        }
        if (restaurant_private_meal_chef.isChecked()) {
            serviceList.add("Restaurant private meal");
        }
        if (chef_onsite_cooking_chef.isChecked()) {
            serviceList.add("Chef onsite cooking");
        }
        if (cooking_workshop_chef.isChecked()) {
            serviceList.add("Cooking workshop");
        }
        if (daily_meal.isChecked()) {
            serviceList.add("Daily meal");
        }
        if (serviceList.isEmpty()) {
            Toast.makeText(this, R.string.please_select, Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(menuTitleStr)) {
            menuTitle.setError(getResources().getString(R.string.please_enter_menu_title));
        } else if (TextUtils.isEmpty(price.getText().toString())) {
            Toast.makeText(this, R.string.please_select_price_per_person, Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(cuisineListStr)) {
            Toast.makeText(this, R.string.please_select_cuisine, Toast.LENGTH_SHORT).show();
        } else {
            list = TextUtils.join(",", serviceList);
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            if (netInfo == null) {
                new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.app_name))
                        .setMessage(getResources().getString(R.string.internet_error))
                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
            } else {
                AddMenuAPI(sendImage);
            }

        }

        return true;
    }

    public boolean validateEDIT(String menu_id, String cuisineListStr, String subcuisineListStr) {
        String food_delivery_chefStr = food_delivery_chef.getText().toString();
        String home_private_meal_chefStr = home_private_meal_chef.getText().toString();
        String restaurant_private_meal_chefStr = restaurant_private_meal_chef.getText().toString();
        String chef_onsite_cooking_chefStr = chef_onsite_cooking_chef.getText().toString();
        String cooking_workshop_chefStr = cooking_workshop_chef.getText().toString();
        String daily_mealStr = daily_meal.getText().toString();


        List<String> serviceList = new ArrayList<>();
        if (food_delivery_chef.isChecked()) {
            serviceList.add("Food delivery");
        }
        if (home_private_meal_chef.isChecked()) {
            serviceList.add("Home private meal");
        }
        if (restaurant_private_meal_chef.isChecked()) {
            serviceList.add("Restaurant private meal");
        }
        if (chef_onsite_cooking_chef.isChecked()) {
            serviceList.add("Chef onsite cooking");
        }
        if (cooking_workshop_chef.isChecked()) {
            serviceList.add("Cooking workshop");
        }
        if (daily_meal.isChecked()) {
            serviceList.add("Daily meal");
        }
        if (serviceList.isEmpty()) {
            Toast.makeText(this, R.string.please_select, Toast.LENGTH_SHORT).show();
        } else {
            list = TextUtils.join(",", serviceList);
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            if (netInfo == null) {
                new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.app_name))
                        .setMessage(getResources().getString(R.string.internet_error))
                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
            } else {
                editMenuAPI(menu_id, sendImage, cuisineListStr, subcuisineListStr);
            }


        }

        return true;
    }


    /*-------------------------------Add Cuisine API--------------------------------------------------------------------------------------*/

    public void AddCuisineAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.GET_CUISINE_URL + "?language=%1$s", language);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.e(TAG, "" + s);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                CUISINE_LIST.clear();
                                JSONArray jsonArray = jsonObject.getJSONArray("cuisine");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    cuisine_id = jsonObject2.getInt("cuisine_id");
                                    CUISINE_INT.add(cuisine_id);
                                    String cuisine_name = jsonObject2.getString("cuisine_name");
                                    CUISINE_LIST.add(cuisine_name);
                                }

                               /*  CuisineAdapter= new ArrayAdapter<>(getApplicationContext(), R.layout.custom_spinner_currency, CUISINE_LIST);

                                cuisine_add_menu.setAdapter(CuisineAdapter);

                                cuisine_add_menu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        int id = CUISINE_INT.get(i);
                                     // int  ID=cuisine_add_menu.getSelectedItemPosition();
                                        cuisine_name=cuisine_add_menu.getSelectedItem().toString();
                                      //  AddSubCuisineAPI(id);
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });*/
                            } else if (status == 0) {
                                CUISINE_LIST.clear();
                                //CuisineAdapter.notifyDataSetChanged();
                            }
                            CuisineAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        volleyError.printStackTrace();
                    }
                });
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /*----------------------------------------------Add Sub Cuisine API-----------------------------------------------------------------*/

    public void AddSubCuisineAPI(int cuisine_id) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.GET_SUB_CUISINE_URL + "?cuisine_id=%1$s", cuisine_id + "&language=" + language);
        Log.e("URL", "" + uri);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e(TAG, "Subcuisine " + s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {

                                JSONArray jsonArray = jsonObject.getJSONArray("subcuisine");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    int sub_cuisine_id = jsonObject2.getInt("subcuisine_id");
                                    // SUB_CUISINE_INT.add(sub_cuisine_id);
                                    String sub_cuisine_name = jsonObject2.getString("subcuisine_name");
                                    // SUB_CUISINE_LIST.add(sub_cuisine_name);

                                }

/*
                                subCuisineAdapter= new ArrayAdapter<>(getApplicationContext(), R.layout.custom_spinner_currency, SUB_CUISINE_LIST);
*/

                              /*  cuisine_type_add_menu.setAdapter(subCuisineAdapter);

                                cuisine_type_add_menu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                        //int id = SUB_CUISINE_INT.get(i);
                                        cuisine_name=cuisine_add_menu.getSelectedItem().toString();

                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });*/

                            } else if (status == 0) {
                                // SUB_CUISINE_LIST.clear();
                                //subCuisineAdapter.notifyDataSetChanged();
                            }
                            subCuisineAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        volleyError.printStackTrace();
                    }
                });
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /*--------------------------Menu List Adapter---------------------------------------*/
    class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.MyMenuListViewHolder> {
        private final ViewBinderHelper binderHelper = new ViewBinderHelper();
        List<MenuListModel> profileMenuModels = new ArrayList<>();
        Context mContext;

        MenuListAdapter(Context mContext, List<MenuListModel> profileMenuModels) {
            this.profileMenuModels = profileMenuModels;
            this.mContext = mContext;
        }

        public void saveStates(Bundle outState) {
            binderHelper.saveStates(outState);
        }

        /**
         * Only if you need to restore open/close state when the orientation is changed.
         * Call this method in {@link android.app.Activity#onRestoreInstanceState(Bundle)}
         */
        public void restoreStates(Bundle inState) {
            binderHelper.restoreStates(inState);
        }

        @Override
        public MenuListAdapter.MyMenuListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_menu_listview, parent, false);
            return new MenuListAdapter.MyMenuListViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MenuListAdapter.MyMenuListViewHolder holder, int position) {
            final MenuListModel profileMenuModel = profileMenuModels.get(position);
            holder.title_menu.setText(profileMenuModel.getMenuTitle());
            holder.price_guest.setText(profileMenuModel.getActualPrice());
            holder.dishes_count.setText(profileMenuModel.getDishesCount() + getResources().getString(R.string.dishes_count));
            final String serviceList = profileMenuModel.getServiceList();
            final String menuID = profileMenuModel.getMenuId();

            if (!TextUtils.isEmpty(profileMenuModel.getStatus1())) {
                if (profileMenuModel.getStatus1().equalsIgnoreCase("1")) {
                    holder.draftText.setText(getResources().getString(R.string.draft));
                    holder.draftText.setBackgroundColor(getResources().getColor(R.color.purple));
                }
                if (profileMenuModel.getStatus1().equalsIgnoreCase("0")) {
                    holder.draftText.setText(getResources().getString(R.string.publish));
                    holder.draftText.setBackgroundColor(getResources().getColor(R.color.greenPublish));
                }
            }

            List<String> items = Arrays.asList(serviceList.split("\\s*,\\s*"));

            String s;
            if (!items.isEmpty() && items != null && !items.equals("")) {
                for (int i = 0; i < items.size(); i++) {
                    s = items.get(i);
                    if (s.equalsIgnoreCase("Food delivery")) {
                        holder.food_delivery_image.setImageResource(R.drawable.ic_check_square);
                    } else if (s.equalsIgnoreCase("Home private meal")) {
                        holder.home_private_meal_profile_image.setImageResource(R.drawable.ic_check_square);
                    } else if (s.equalsIgnoreCase("Restaurant private meal")) {
                        holder.restaurant_private_meal_profile_image.setImageResource(R.drawable.ic_check_square);
                    } else if (s.equalsIgnoreCase("Chef onsite cooking")) {
                        holder.chef_onsite_cooking_profile_image.setImageResource(R.drawable.ic_check_square);
                    } else if (s.equalsIgnoreCase("Cooking workshop")) {
                        holder.cooking_workshop_profile_image.setImageResource(R.drawable.ic_check_square);
                    } else if (s.equalsIgnoreCase("Daily meal")) {
                        holder.daily_meal_profile_image.setImageResource(R.drawable.ic_check_square);
                    }
                }
            }


            if (!profileMenuModel.getImageUrl().equals("")) {
                Picasso.with(mContext)
                        .load(profileMenuModel.getImageUrl())
                        .error(R.drawable.briyani).fit()
                        .into(holder.dish_image);
            }
            binderHelper.bind(holder.swipeLayout, String.valueOf(profileMenuModel));
            // Bind your data here
            holder.bind(profileMenuModel);
            holder.bindEdit(profileMenuModel);
            holder.bindDraft(profileMenuModel);

            holder.clickToAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // custom dialog
                    Intent intent = new Intent(AddAMenu.this, DishList.class);
                    intent.putExtra("menu_id", menuID);
                    intent.putExtra("menuName", profileMenuModel.getMenuTitle());
                    startActivity(intent);
               /*     final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    Window window = dialog.getWindow();
                    window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setContentView(R.layout.add_dish_layout);


                    saveDish= dialog.findViewById(R.id.save_dish);
                    dishTitle= dialog.findViewById(R.id.dish_title);
                    add_image_dish= dialog.findViewById(R.id.add_image_dish);
                    add_dish_image= dialog.findViewById(R.id.add_dish_image);
                    pickerView = dialog.findViewById(R.id.pickerView);

                    ArrayList<String> items = new ArrayList<>();
                    items.add("Starter");
                    items.add("Salad");
                    items.add("Main Course");
                    items.add("Dessert");
                    items.add("Drink");
                    pickerView.setItems(items);
                    pickerView.setPickerListener(new SGPickerView.SGPickerViewListener() {
                        @Override
                        public void itemSelected(String item, int index) {
                          pickerView.getCurrentSelectedItemIndex();
                             dishCourse=  pickerView.getCurrentSelectedItem();
                            //Toast.makeText(AddAMenu.this, " Index = " + String.valueOf(index) + " Item name " + item, Toast.LENGTH_SHORT).show();
                            Toast.makeText(AddAMenu.this, " Item name " + dishCourse, Toast.LENGTH_SHORT).show();
                        }
                    });

                    add_image_dish.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showPictureDialogAddDishImage();
                        }
                    });

                    // if button is clicked, close the custom dialog
                    saveDish.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dishTitleStr=dishTitle.getText().toString();

                            addSubMenuURL(sendDishImage,menuID);
                            dialog.dismiss();
                        }
                    });
                    cancel = dialog.findViewById(R.id.cancel);
                    // if button is clicked, close the custom dialog
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();*/
                }
            });
            holder.menu_title_ly.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }

        @Override
        public int getItemCount() {
            return profileMenuModels.size();
        }

        class MyMenuListViewHolder extends RecyclerView.ViewHolder {
            TextView title_menu, dishes_count, clickToAdd, price_guest;
            ImageView dish_image;
            LinearLayout menu_title_ly;
            ImageView food_delivery_image, home_private_meal_profile_image, daily_meal_profile_image, cooking_workshop_profile_image, restaurant_private_meal_profile_image,
                    chef_onsite_cooking_profile_image;
            TextView draftText;
            private View deleteLayout, edit_layout, draft_ly;
            private SwipeRevealLayout swipeLayout;

            MyMenuListViewHolder(View itemView) {
                super(itemView);
                swipeLayout = itemView.findViewById(R.id.swipe_layout);
                deleteLayout = itemView.findViewById(R.id.delete_layout);
                draft_ly = itemView.findViewById(R.id.draft_ly);
                edit_layout = itemView.findViewById(R.id.edit_layout);
                draftText = itemView.findViewById(R.id.draftText);
                dish_image = itemView.findViewById(R.id.dish_image);
                title_menu = itemView.findViewById(R.id.title_menu);
                dishes_count = itemView.findViewById(R.id.dishes_count);
                clickToAdd = itemView.findViewById(R.id.clickToAdd);
                price_guest = itemView.findViewById(R.id.price_guest);
              /*  service_list=(ListView)itemView.findViewById(R.id.service_list);*/
                menu_title_ly = itemView.findViewById(R.id.menu_title_ly);
                food_delivery_image = itemView.findViewById(R.id.food_delivery_image);
                home_private_meal_profile_image = itemView.findViewById(R.id.home_private_meal_profile_image);
                daily_meal_profile_image = itemView.findViewById(R.id.daily_meal_profile_image);
                cooking_workshop_profile_image = itemView.findViewById(R.id.cooking_workshop_profile_image);
                restaurant_private_meal_profile_image = itemView.findViewById(R.id.restaurant_private_meal_profile_image);
                chef_onsite_cooking_profile_image = itemView.findViewById(R.id.chef_onsite_cooking_profile_image);

            }

            public void bindDraft(final MenuListModel profileMenuModel) {
                String draftStatus;
                if (profileMenuModel.getStatus1().equalsIgnoreCase("0")) {
                    draftStatus = "0";
                    final String finalDraftStatus1 = draftStatus;
                    draft_ly.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                                        if (netInfo == null) {
                                                            new AlertDialog.Builder(AddAMenu.this)
                                                                    .setTitle(getResources().getString(R.string.app_name))
                                                                    .setMessage(getResources().getString(R.string.internet_error))
                                                                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                                        } else {
                                                            draftAPI(profileMenuModel.getMenuId(), finalDraftStatus1);
                                                            //  notifyItemRemoved(getAdapterPosition());
                                                        }
                                                    }
                                                }
                    );
                }
                if (profileMenuModel.getStatus1().equalsIgnoreCase("1")) {
                    draftStatus = "1";
                    final String finalDraftStatus = draftStatus;
                    draft_ly.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                                        if (netInfo == null) {
                                                            new AlertDialog.Builder(AddAMenu.this)
                                                                    .setTitle(getResources().getString(R.string.app_name))
                                                                    .setMessage(getResources().getString(R.string.internet_error))
                                                                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                                        } else {
                                                            draftAPI(profileMenuModel.getMenuId(), finalDraftStatus);
                                                            //notifyItemRemoved(getAdapterPosition());
                                                        }
                                                    }
                                                }
                    );
                }


            }

            public void bind(final MenuListModel profileMenuModel) {
                deleteLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        profileMenuModels.remove(getAdapterPosition());
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            new AlertDialog.Builder(AddAMenu.this)
                                    .setTitle(getResources().getString(R.string.app_name))
                                    .setMessage(getResources().getString(R.string.internet_error))
                                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
                        } else {
                            deleteMenuAPI(profileMenuModel.getMenuId());
                            notifyItemRemoved(getAdapterPosition());
                        }


                    }
                });

                //   textView.setText(data);
            }

            public void bindEdit(final MenuListModel profileMenuModel) {
                edit_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // ------------------------------custom dialog--------------------------------------
                        final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                        Window window = dialog.getWindow();
                        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.add_menu_layout);

                        //------------------------------DIALOG BOX ID------------------------------------------
                        saveMenu = dialog.findViewById(R.id.save_menu);
                        menuTitle = dialog.findViewById(R.id.menu_title);
                        menuTitleStr = menuTitle.getText().toString();
                        min = dialog.findViewById(R.id.min);
                        max = dialog.findViewById(R.id.max);
                        price = dialog.findViewById(R.id.price);
                        multiSlider = dialog.findViewById(R.id.range_slider_add_menu);

                        cuisine_add_menu = dialog.findViewById(R.id.cuisine_add_menu);
                        cusine_ly = dialog.findViewById(R.id.cusine_ly);

                        add_image_chef = dialog.findViewById(R.id.add_image_chef);
                        add_menu_image = dialog.findViewById(R.id.add_menu_image);


                        //------------------CHECKBOX IDS---------------------------------------------------
                        food_delivery_chef = dialog.findViewById(R.id.food_delivery_add_menu);
                        home_private_meal_chef = dialog.findViewById(R.id.home_private_meal_add_menu);
                        restaurant_private_meal_chef = dialog.findViewById(R.id.restaurant_private_meal_add_menu);
                        chef_onsite_cooking_chef = dialog.findViewById(R.id.chef_onsite_cooking_add_menu);
                        cooking_workshop_chef = dialog.findViewById(R.id.cooking_workshop_add_menu);
                        daily_meal = dialog.findViewById(R.id.daily_meal_add_menu);

                      dbHelper = new DbHelper(getApplicationContext());
                        List<UserDetail> contacts = dbHelper.getAllDetails();
                        for (UserDetail cn : contacts) {
                            serviceList = cn.getServiceType();
                            Log.e("List", "" + cn.getLanguageList());

                        }

                        if (!TextUtils.isEmpty(serviceList)) {
                            List<String> items = Arrays.asList(serviceList.split("\\s*,\\s*"));
                            Log.e("ListItem", "" + items);
                            for (int i = 0; i < items.size(); i++) {
                                String stringList = items.get(i);
                                if (stringList.equalsIgnoreCase("Food delivery")) {
                                    food_delivery_chef.setClickable(true);
                                    food_delivery_chef.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    food_delivery_chef.setPadding(20,0,20,0);
                                }
                                if (stringList.equalsIgnoreCase("Home private meal")) {
                                    home_private_meal_chef.setClickable(true);
                                    home_private_meal_chef.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    home_private_meal_chef.setPadding(20,0,20,0);
                                }
                                if (stringList.equalsIgnoreCase("Restaurant private meal")) {
                                    restaurant_private_meal_chef.setClickable(true);
                                    restaurant_private_meal_chef.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    daily_meal.setPadding(20,0,20,0);
                                }
                                if (stringList.equalsIgnoreCase("Chef onsite cooking")) {
                                    chef_onsite_cooking_chef.setClickable(true);
                                    chef_onsite_cooking_chef.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    chef_onsite_cooking_chef.setPadding(20,0,20,0);
                                }
                                if (stringList.equalsIgnoreCase("Cooking workshop")) {
                                    cooking_workshop_chef.setClickable(true);
                                    cooking_workshop_chef.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    cooking_workshop_chef.setPadding(20,0,20,0);
                                }
                                if (stringList.equalsIgnoreCase("Daily meal")) {
                                    daily_meal.setClickable(true);
                                    daily_meal.setBackground(getResources().getDrawable(R.drawable.checkbox_selector));
                                    daily_meal.setPadding(20,0,20,0);
                                }

                            }
                        }


                        menuTitle.setText(profileMenuModel.getMenuTitle());
                        cuisine_add_menu.setText(profileMenuModel.getCuisineList());
                        String serviceList = profileMenuModel.getServiceList();
                        if (!TextUtils.isEmpty(serviceList)) {
                            List<String> items = Arrays.asList(serviceList.split("\\s*,\\s*"));
                            Log.e("ListItem", "" + items);
                            for (int i = 0; i < items.size(); i++) {
                                String stringList = items.get(i);
                                if (stringList.equalsIgnoreCase("Food delivery")) {
                                    food_delivery_chef.setChecked(true);
                                } else if (stringList.equalsIgnoreCase("Home private meal")) {
                                    home_private_meal_chef.setChecked(true);
                                } else if (stringList.equalsIgnoreCase("Restaurant private meal")) {
                                    restaurant_private_meal_chef.setChecked(true);
                                } else if (stringList.equalsIgnoreCase("Chef onsite cooking")) {
                                    chef_onsite_cooking_chef.setChecked(true);
                                } else if (stringList.equalsIgnoreCase("Cooking workshop")) {
                                    cooking_workshop_chef.setChecked(true);
                                } else if (stringList.equalsIgnoreCase("Daily meal")) {
                                    daily_meal.setChecked(true);
                                }

                            }
                        }
                        if (!TextUtils.isEmpty(profileMenuModel.getMoney()))
                        {
                            price.setText(profileMenuModel.getMoney());
                            multiSlider.setProgress(Integer.parseInt(profileMenuModel.getMoney()));

                        }

                        add_image_chef.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                // CALLING POP UP TO CHOOSE GALLERY / CAMERA
                                showPictureDialog();
                            }
                        });
                        cusine_ly.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // ------------------------------custom dialog--------------------------------------
                              /*  final Dialog dialog = new Dialog(context);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                                Window window = dialog.getWindow();
                                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog.setContentView(R.layout.cuisine_subcuisine_ly);


                                filterListView = dialog.findViewById(R.id.filter_dialog_listview);
                                filterValListView = dialog.findViewById(R.id.filter_value_listview);
*/
                                getCuisineList();
/*
                                ok_btn = dialog.findViewById(R.id.btn_filter);
                                clearbtn = dialog.findViewById(R.id.btn_clear);

                                ok_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        for (CuisineSubCuisineModel.CuisineBean.SubCuisneBean cuisineSubCuisineModel : subCuisneBeans) {
                                            if (cuisineSubCuisineModel.isChecked()) {

                                                subcuisineListStr = TextUtils.join(",", subCuisineList);
                                                cuisineListStr = TextUtils.join(",", cuisineList);
                                                Log.e(TAG, "SubCuisine " + subcuisineListStr);
                                                Log.e(TAG, "Cuisine " + cuisineListStr);
                                                cuisine_add_menu.setText(cuisineListStr);
                                            }
                                        }
                                        if (subCuisineList.isEmpty() && cuisineList.isEmpty()) {
                                            Toast.makeText(AddAMenu.this, "Please select cuisine and sub-cuisine", Toast.LENGTH_SHORT).show();
                                        }
                                        dialog.dismiss();
                                    }
                                });

                                clearbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        for (CuisineSubCuisineModel.CuisineBean.SubCuisneBean cuisineSubCuisineModel : subCuisneBeans) {
                                            cuisineSubCuisineModel.setChecked(false);
                                        }
                                        subCuisineList.clear();
                                        cuisineList.clear();
                                        adapter.notifyDataSetChanged();
                                        filterValAdapter.notifyDataSetChanged();
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();*/
                            }
                        });
                        multiSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                pricePerPerson = ""+i;
                                min.setText(R.string.min_cur);
                              //  price.setText(""+i);
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {

                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {
                                price.setText(""+seekBar.getProgress());
                            }
                        });
                      /*  multiSlider.setMaxValue(1000);
//                        final int priceInt = Integer.parseInt(price.getText().toString());
                        //     price.addTextChangedListener(searchtext);

                       *//* if (priceInt <= 1000) {
                            multiSlider.setPosition(priceInt);
                        }*//*



                        multiSlider.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
                            @Override
                            public void valueChanged(Number value) {

                                pricePerPerson = "" + value;
                                min.setText(R.string.min_cur);
                                price.setText("" + value);
                       *//* } else {*//*
                        *//*max.setText("" + maxValue);*//*
                            }
                        });*/

                        //------------------------------ if button is clicked, close the custom dialog----------------------
                        saveMenu.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                menuTitleStr = menuTitle.getText().toString();
                                // CALLING METHOD TO GET CHECKBOX LIST AND API CALL
                                if (!TextUtils.isEmpty(cuisineListStr) && !TextUtils.isEmpty(subcuisineListStr)) {
                                    validateEDIT(profileMenuModel.getMenuId(), cuisineListStr, subcuisineListStr);
                                } else {
                                    validateEDIT(profileMenuModel.getMenuId(), profileMenuModel.getCuisineList(), profileMenuModel.getSubCuisineList());
                                }

                                dialog.dismiss();
                            }
                        });
                        cancel = dialog.findViewById(R.id.cancel);

                        // if button is clicked, close the custom dialog
                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        dialog.show();
                        //  Toast.makeText(mContext, "Edit clicked", Toast.LENGTH_SHORT).show();
                    }
                });

                //   textView.setText(data);
            }
        }
    }

}
