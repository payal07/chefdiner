package com.vervelogic.chefdiner.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.vervelogic.chefdiner.R;

public class EatAndCook extends AppCompatActivity implements View.OnClickListener {

    Button wantToEat, wantToCook;
    String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eat_and_cook);

        /*----------------------------getting data from intent---------------------------------*/
        Intent intent = getIntent();
        language = intent.getStringExtra("language");

        /*---------------------------IDS--------------------------------------------------------*/
        wantToCook = (Button) findViewById(R.id.wantToCook);
        wantToEat = (Button) findViewById(R.id.wantToEat);
        /*-------------------------------------Listeners-------------------------------------*/
        wantToCook.setOnClickListener(this);
        wantToEat.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.wantToCook:
                Intent intent = new Intent(EatAndCook.this, RegisterSignUpScreen.class);
                intent.putExtra("language", language);
                intent.putExtra("type", "becomeAChef");
                startActivity(intent);
                break;
            case R.id.wantToEat:
                Intent intent1 = new Intent(EatAndCook.this, HomePage.class);
                intent1.putExtra("language", language);
                startActivity(intent1);
                break;
            default:
                Intent intent2 = new Intent(EatAndCook.this, HomePage.class);
                intent2.putExtra("language", language);
                startActivity(intent2);
        }
    }
}
