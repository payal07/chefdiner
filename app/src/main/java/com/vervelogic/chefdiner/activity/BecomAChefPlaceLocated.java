package com.vervelogic.chefdiner.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.GeocodingLocation;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import static com.hbb20.CountryCodePicker.Language.CHINESE_SIMPLIFIED;
import static com.hbb20.CountryCodePicker.Language.CHINESE_TRADITIONAL;


public class BecomAChefPlaceLocated extends AppCompatActivity {

    Button next6;
    EditText city_name;
    /* TextView selectedCountryText;*/
    String langaugeList, language, firstName, lastName, phoneNumber, oftenCook, country, city;
    LinearLayout country_spinner_ly;
    CountryCodePicker countryCodePicker;
    String countryCode;

    DbHelper dbHelper;
    Activity context = this;
    String country_code;
    Spinner currency_chef;
    String selectedCurrency;

    public static List<Currency> getAllCurrencies() {
        List<Currency> toret = new ArrayList<>();
        Locale[] locs = Locale.getAvailableLocales();
        List<String> currencySysmbol = new ArrayList<>();

        for (Locale loc : locs) {

            try {
                Currency currency = Currency.getInstance(loc);
                if (currency != null) {
                    toret.add(currency);
                    currencySysmbol.add(currency.getSymbol());

                }
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }

        return toret;
    }

    public static List<String> getAllCurrencySymbol() {
        List<Currency> toret = new ArrayList<>();
        Locale[] locs = Locale.getAvailableLocales();
        List<String> currencySysmbol = new ArrayList<>();

        for (Locale loc : locs) {

            try {
                Currency currency = Currency.getInstance(loc);
                if (currency != null) {
                    toret.add(currency);
                    currencySysmbol.add(currency.getSymbol());
                }
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }

        return currencySysmbol;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_becom_achef_place_located);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*// Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/
        /*--------------------------------IDS-------------------------------------------------------*/
        next6 = (Button) findViewById(R.id.next6);
        city_name = (EditText) findViewById(R.id.city_text_chef);
        country_spinner_ly = (LinearLayout) findViewById(R.id.country_name_ly);
        countryCodePicker = (CountryCodePicker) findViewById(R.id.ccp);
        currency_chef = (Spinner) findViewById(R.id.currency_chef);
      /*  selectedCountryText = (TextView) findViewById(R.id.selectedCountry);*/
        /*-----------------------------Getting data from intent---------------------------------------*/
        Intent intent = getIntent();
        langaugeList = intent.getStringExtra("LanguageList");
        language = intent.getStringExtra("language");
        firstName = intent.getStringExtra("firstName");
        lastName = intent.getStringExtra("lastName");
        phoneNumber = intent.getStringExtra("phoneNumberChef");
        oftenCook = intent.getStringExtra("Often Cook");

        /*------------------------------Database Access-------------------------------------------------*/


        List<Currency> strings = getAllCurrencies();

        // add elements to al, including duplicates
        Set<Currency> hs = new HashSet<>();
        hs.addAll(strings);
        strings.clear();
        strings.addAll(hs);

        Collections.sort(strings, new Comparator<Currency>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(Currency lhs, Currency rhs) {
                String stringName1 = lhs.getCurrencyCode();
                String stringName2 = rhs.getCurrencyCode();

                return stringName1.compareToIgnoreCase(stringName2);
                //  return lhs.getDisplayName().compareTo(rhs.getDisplayName());
            }
        });

        SharedPreference sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(getApplicationContext()))) {
            language = sharedPreference.getLanguage(getApplicationContext());
            Log.e("Lang", "" + language);
            if (!TextUtils.isEmpty(language)) {
                if (language.equalsIgnoreCase("english")) {
                    Locale current = Locale.ENGLISH;
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());

                }
                if (language.equalsIgnoreCase("traditional chinese")) {
                    Locale current = Locale.TRADITIONAL_CHINESE;
                    Log.e("TAG", "" + current);

                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                    countryCodePicker.changeDefaultLanguage(CHINESE_TRADITIONAL);
                }
                if (language.equalsIgnoreCase("simplified chinese")) {
                    Locale current = Locale.SIMPLIFIED_CHINESE;
                    Log.e("TAG", "" + current);
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                    countryCodePicker.changeDefaultLanguage(CHINESE_SIMPLIFIED);
                }
            }

        } else {
            language = "english";
            Locale current = Locale.ENGLISH;
            Locale.setDefault(current);
            Configuration config = new Configuration();
            config.locale = current;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        }
        ArrayAdapter<Currency> stringArrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.custom_spinner_currency, strings);

        currency_chef.setAdapter(stringArrayAdapter);

        /*-----------------------Listener--------------------------------------------------*/
        next6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validate()) {
                    return;
                }
            }
        });
        country_code = sharedPreference.getCountryCode(context);

        // --------------------------------LISTENER ON SPINNER----------------------------
        currency_chef.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String first = currency_chef.getSelectedItem().toString();
                StringTokenizer tokens = new StringTokenizer(first, "(");
                //  selectedCurrency = tokens.nextToken();// this will contain "Fruit"
                //  String second = tokens.nextToken();// this will contain " they taste good"
                selectedCurrency = currency_chef.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            city_name.setText(cn.getChefCity());
            String c = cn.getCurrency();
            if (!TextUtils.isEmpty(c)) {
                Log.e("C", "" + currency_chef.getSelectedItem());
                String cd = cn.getChefCountry();

                if (!TextUtils.isEmpty(cd)) {
                    StringTokenizer tokens = new StringTokenizer(cd, "(");
                /*if (!TextUtils.isEmpty(tokens.nextToken()))
                {*/
                    selectedCurrency = tokens.nextToken();// this will contain "Fruit"
                    while (tokens.hasMoreTokens()) {
                        String second = tokens.nextToken();// this will contain " they taste good"
                    }
                    selectedCurrency = selectedCurrency.trim();
                    getCountryCode(selectedCurrency);
                }
                currency_chef.setSelection(stringArrayAdapter.getPosition(Currency.getInstance(c)));
                selectedCurrency = cn.getCurrency();
            }
        }
    }

    public String getCountryCode(String countryName) {

        // Get all country codes in a string array.
        Locale current = Locale.ENGLISH;
        Locale.setDefault(current);
        String[] isoCountryCodes = Locale.getISOCountries();
        Map<String, String> countryMap = new HashMap<>();

        // Iterate through all country codes:
        for (String code : isoCountryCodes) {
            // Create a locale using each country code
            Locale locale = new Locale("", code);
            // Get country name for each code.
            String name = locale.getDisplayCountry();
            // Map all country names and codes in key - value pairs.
            countryMap.put(name, code);
        }
        // Get the country code for the given country name using the map.
        // Here you will need some validation or better yet
        // a list of countries to give to user to choose from.
        String countryCode = countryMap.get(countryName); // "NL" for Netherlands.
        countryCodePicker.setCountryForNameCode(String.valueOf(countryCode));
        country = countryCodePicker.getSelectedCountryName();
        return countryCode;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validate() {
        city = city_name.getText().toString();
        country = countryCodePicker.getSelectedCountryName();
        countryCode = countryCodePicker.getSelectedCountryNameCode();

        if (TextUtils.isEmpty(city) || TextUtils.isEmpty(country) || TextUtils.isEmpty(selectedCurrency)) {
            if (city.isEmpty()) {
                city_name.setError(getString(R.string.please_enter_address));
            }
            if (TextUtils.isEmpty(country)) {
                Toast.makeText(this, getResources().getString(R.string.please_select_country), Toast.LENGTH_SHORT).show();
            }
            if (TextUtils.isEmpty(selectedCurrency)) {
                Toast.makeText(this, getResources().getString(R.string.please_select_currency), Toast.LENGTH_SHORT).show();
            }
        } else {

            GeocodingLocation locationAddress = new GeocodingLocation();
            GeocodingLocation.getAddressFromLocation(city,
                    getApplicationContext(), new GeocoderHandler());

        }
        return true;
    }

    @SuppressLint("HandlerLeak")
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            StringTokenizer tokens = new StringTokenizer(locationAddress, " ");
            String latitude = tokens.nextToken();// this will contain "Latitude"
            String longitude = tokens.nextToken();// this will contain " Longitude"

            Intent intent = new Intent(getApplicationContext(), BecomeAChefHostGuest.class);
            intent.putExtra("Often Cook", oftenCook);
            intent.putExtra("LanguageList", langaugeList);
            intent.putExtra("language", language);
            intent.putExtra("firstName", firstName);
            intent.putExtra("lastName", lastName);
            intent.putExtra("phoneNumberChef", phoneNumber);
            intent.putExtra("country", country);
            intent.putExtra("country_code", countryCode);
            intent.putExtra("city", city);
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitude);
            intent.putExtra("selectedCurrency", selectedCurrency);
            startActivity(intent);

        }
    }

}
