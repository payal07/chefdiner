package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.utils.SharedPreference;

public class RegisterSignUpScreen extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    Button register, signin;
    String language;
    String type, Chef_id;
    SharedPreference sharedPreference;
    Activity context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_sign_up_screen);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      /*  // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/

        // ---------------------------------------IDs--------------------------------
        register = (Button) findViewById(R.id.register_btn);
        signin = (Button) findViewById(R.id.signin_btn);
        register.setOnClickListener(this);
        signin.setOnClickListener(this);
        Intent intent = getIntent();
        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(context))) {
            language = sharedPreference.getLanguage(context);
        } else {

            language = "english";
        }


        type = intent.getStringExtra("type");
        Chef_id = intent.getStringExtra("Chef_id");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register_btn:
                Intent intent = new Intent(getApplicationContext(), RegisterScreen.class);
                intent.putExtra("language", language);
                startActivity(intent);
                Toast.makeText(this, "Redirect to register screen", Toast.LENGTH_SHORT).show();
                break;
            case R.id.signin_btn:
                Intent intent1 = new Intent(getApplicationContext(), SignInScreen.class);
                intent1.putExtra("language", language);
                intent1.putExtra("type", type);
                intent1.putExtra("Chef_id", Chef_id);
                startActivity(intent1);
                break;
        }
    }
}
