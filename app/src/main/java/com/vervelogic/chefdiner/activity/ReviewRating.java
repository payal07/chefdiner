package com.vervelogic.chefdiner.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.app.CustomMultipartRequest;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ReviewRating extends AppCompatActivity {

    private static final String TAG = ReviewRating.class.getSimpleName();
    Toolbar toolbar;

    Button submitRating, uploadPhotos;
    EditText reviewRatingText;
    SimpleRatingBar taste_rating, environment_rating, serviceQuality_rating, cleaniness_rating, price_rating;
    private ArrayList<File> multipleImagesPath = new ArrayList<>();
    String fileToUpload, fileToUpload1, fileToUpload2, fileToUpload3, fileToUpload4;
    DbHelper dbHelper;
    String userId, reviewRatingTextStr, chefId, bookingId;
    int taste = 0, environment = 0, serviceQuality = 0, cleaniness = 0, price, avg = 0;
    String language;
    SharedPreference sharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_rating);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.rating_and_review);

        Intent intent = getIntent();
        chefId = intent.getStringExtra("Chef_id");
        bookingId = intent.getStringExtra("bookingId");
       /* cal.putExtra("Chef_id",chef_id);
        cal.putExtra("bookingId",bookingId);
        cal.putExtra("mainUserId",mainUserId);*/
       /* intent1.putExtra("bookingid",bookingId);
        intent1.putExtra("chefid",chefId);*/

        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            userId = cn.getUser_id();
        }

        reviewRatingText = (EditText) findViewById(R.id.reviewRatingText);
        submitRating = (Button) findViewById(R.id.submitRating);
        uploadPhotos = (Button) findViewById(R.id.uploadPhotos);
        taste_rating = (SimpleRatingBar) findViewById(R.id.taste_rating);
        environment_rating = (SimpleRatingBar) findViewById(R.id.environment_rating);
        serviceQuality_rating = (SimpleRatingBar) findViewById(R.id.serviceQuality_rating);
        cleaniness_rating = (SimpleRatingBar) findViewById(R.id.cleaniness_rating);
        price_rating = (SimpleRatingBar) findViewById(R.id.price_rating);

        sharedPreference=new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(getApplicationContext()))){
            language = sharedPreference.getLanguage(getApplicationContext());
            Log.e("Lang",""+language);
            if (!TextUtils.isEmpty(language))
            {
                if (language.equalsIgnoreCase("english"))
                {
                    Locale current = Locale.ENGLISH;
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
                if (language.equalsIgnoreCase("traditional chinese"))
                {
                    Locale current = Locale.TRADITIONAL_CHINESE;
                    Log.e("TAG", "" + current);

                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
                if(language.equalsIgnoreCase("simplified chinese"))
                {
                    Locale current = Locale.SIMPLIFIED_CHINESE;
                    Log.e("TAG", "" + current);
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
            }

        }
        else
        {
            language="english";
            Locale current = Locale.ENGLISH;
            Locale.setDefault(current);
            Configuration config = new Configuration();
            config.locale = current;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        }
        uploadPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReviewRating.this, AlbumSelectActivity.class);
//set limit on number of images that can be selected, default is 10
                intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 5);
                startActivityForResult(intent, Constants.REQUEST_CODE);
            }
        });

        submitRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (multipleImagesPath != null) {
                    Log.e("UploadImages", "" + multipleImagesPath.toString());
                    for (int i = 0; i < multipleImagesPath.size(); i++) {
                          /*  File destination = saveFileToSDcard(multipleImagesPath.get(0).g);
                            String filePath = FileUtils.getPath(ReviewRating.this, Uri.fromFile(destination));
                            imageFile= new File(filePath);*/
                        fileToUpload = String.valueOf(multipleImagesPath.get(0));
                        Log.e("fileToUpload", "" + fileToUpload);
                        if (i > 0) {
                            if (i == 1) {
                                fileToUpload1 = String.valueOf(multipleImagesPath.get(1));
                                Log.e("fileToUpload1", "" + fileToUpload1);
                            } else {
                                fileToUpload1 = "";
                            }
                            if (i == 2) {
                                fileToUpload2 = String.valueOf(multipleImagesPath.get(2));
                                Log.e("fileToUpload2", "" + fileToUpload2);
                            } else {
                                fileToUpload2 = "";
                            }
                            if (i == 3) {
                                fileToUpload3 = String.valueOf(multipleImagesPath.get(3));
                                Log.e("fileToUpload3", "" + fileToUpload3);
                            } else {
                                fileToUpload3 = "";
                            }
                            if (i == 4) {
                                fileToUpload4 = String.valueOf(multipleImagesPath.get(4));
                                Log.e("fileToUpload4", "" + fileToUpload4);
                            } else {
                                fileToUpload4 = "";
                            }
                        }

                    }

                }
                reviewRatingTextStr = reviewRatingText.getText().toString();
                taste = (int) taste_rating.getRating();
                environment = (int) environment_rating.getRating();
                serviceQuality = (int) serviceQuality_rating.getRating();
                cleaniness = (int) cleaniness_rating.getRating();
                price = (int) price_rating.getRating();
                avg = (taste + environment + serviceQuality + cleaniness + price) / 5;
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    Log.e(TAG, "No Internet connection");
                } else {
                    saveReviewAPI();
                }
                     /*if (taste==0 || environment==0 || serviceQuality==0 || cleaniness==0 || price==0 || avg==0 || !TextUtils.isEmpty(reviewRatingTextStr))
                     {
                         saveReviewAPI();
                     }
                     else
                     {
                         Toast.makeText(ReviewRating.this, "Please Rate", Toast.LENGTH_SHORT).show();
                     }*/

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomePage.class);
        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private File saveFileToSDcard(Bitmap thumbnail) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(getApplicationFolder(Environment
                .getExternalStorageDirectory()
                + File.separator
                + getResources().getString(R.string.app_name) + File.separator),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return destination;
    }

    public static File getApplicationFolder(String path) {
        File dir = new File(path);
        try {
            if (dir.mkdirs()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dir;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            for (int i = 0, l = images.size(); i < l; i++) {
//                stringBuffer.append(images.get(i).path + "\n");
                File file = new File(images.get(i).path);
                multipleImagesPath.add(file);
                //  Log.e(TAG, "onActivityResult: " + multipleImagesPath.get(i).getName());

            }
        }
    }

    public void saveReviewAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        if (!multipleImagesPath.isEmpty() && multipleImagesPath != null && !multipleImagesPath.equals("")) {
            /*----------------------------------------- API sends data in multipart form-----------------------------------*/
            //Auth header
            Map<String, String> mHeaderPart = new HashMap<>();
            mHeaderPart.put("Content-type", "multipart/form-data;");

            //File part
            Map<String, File> mFilePartData = new HashMap<>();
            mFilePartData.put("fileToUpload[0]", new File(fileToUpload));
            if (!TextUtils.isEmpty(fileToUpload1)) {
                mFilePartData.put("fileToUpload[1]", new File(fileToUpload1));
            }
            if (!TextUtils.isEmpty(fileToUpload2)) {
                mFilePartData.put("fileToUpload[2]", new File(fileToUpload2));
            }
            if (!TextUtils.isEmpty(fileToUpload3)) {
                mFilePartData.put("fileToUpload[3]", new File(fileToUpload3));
            }
            if (!TextUtils.isEmpty(fileToUpload4)) {
                mFilePartData.put("fileToUpload[4]", new File(fileToUpload4));
            }

            Log.e("mFilePartData", "" + mFilePartData);
            //String part
            Map<String, String> mStringPart = new HashMap<>();
            mStringPart.put("chef_id", chefId);
            mStringPart.put("main_user_id", userId);
            mStringPart.put("booking_id", bookingId);
            mStringPart.put("taste", String.valueOf(taste));
            mStringPart.put("environment", String.valueOf(environment));
            mStringPart.put("service_quality", String.valueOf(serviceQuality));
            mStringPart.put("cleanliness", String.valueOf(cleaniness));
            mStringPart.put("price", String.valueOf(price));
            mStringPart.put("avr_rating", String.valueOf(avg));
            mStringPart.put("review", reviewRatingTextStr);

            Log.e("Data", "" + mStringPart);

            // Custom multipart class called
            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, getApplicationContext(), AppConfig.SAVE_REVIEW_URL,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            progressDialog.dismiss();
                            Log.e(TAG, "" + jsonObject);
                            try {

                        /*JSONObject jsonObject1 = new JSONObject();*/
                                int status = jsonObject.getInt("status");
                                if (status == 1) {
                                    String message = jsonObject.getString("message");
                                    Log.e(TAG, "" + message);
                                    Intent intent = new Intent(getApplicationContext(), HomePage.class);
                                    startActivity(intent);
                                   /* Intent intent = new Intent(getApplicationContext(), HomePage.class);
                                    startActivity(intent);*/
                                } else {
                                    Log.e(TAG, "" + jsonObject);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();if (getApplicationContext()!=null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                progressDialog.dismiss();if (getApplicationContext()!=null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }if (getApplicationContext()!=null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());
                }

            }, mFilePartData, mStringPart, mHeaderPart);

            RequestQueue rQueue = Volley.newRequestQueue(this);
            rQueue.add(mCustomRequest);
        } else {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.SAVE_REVIEW_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.e(TAG, "" + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                int status = jsonObject.getInt("status");
                                if (status == 1) {
                                    String message = jsonObject.getString("message");
                                    Log.e(TAG, "" + message);
                                    Intent intent = new Intent(getApplicationContext(), HomePage.class);
                                    startActivity(intent);

                                } else {
                                    Log.e("Response Chef", "" + response);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();if (getApplicationContext()!=null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                progressDialog.dismiss();if (getApplicationContext()!=null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }if (getApplicationContext()!=null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();

                    map.put("chef_id", chefId);
                    map.put("main_user_id", userId);
                    map.put("booking_id", bookingId);
                    map.put("taste", String.valueOf(taste));
                    map.put("environment", String.valueOf(environment));
                    map.put("service_quality", String.valueOf(serviceQuality));
                    map.put("cleanliness", String.valueOf(cleaniness));
                    map.put("price", String.valueOf(price));
                    map.put("avr_rating", String.valueOf(avg));
                    map.put("review", reviewRatingTextStr);
                    Log.e("map", "" + map);

                    return map;
                }
            };
            int socketTimeout = 30000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);

            AppController.getInstance().addToRequestQueue(stringRequest);
        }
    }
}
