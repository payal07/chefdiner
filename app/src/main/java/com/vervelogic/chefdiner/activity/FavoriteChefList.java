package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.adapter.HomeFragmentAdapter;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.HomeFragmentModel;
import com.vervelogic.chefdiner.model.RecyclerTouchListener;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.GPSTracker;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FavoriteChefList extends AppCompatActivity {

    RecyclerView recyclerView;
    String user_id, cuisine = "", cuisine_name;
    DbHelper dbHelper;
    List<HomeFragmentModel> homeFragmentModelList = new ArrayList<>();
    HomeFragmentAdapter homeFragmentAdapter;
    TextView no_image_available;
    Activity context = this;
    private SharedPreference sharedPreference;
    Toolbar toolbar;
    private static final String TAG = FavoriteChefList.class.getSimpleName();
    GPSTracker gps;
    String latitude = "", longitude = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_chef_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            user_id = sharedPreference.getUserId(context);
        } else {
            user_id = "";
        }

        dbHelper=new DbHelper(getApplicationContext());

        gps = new GPSTracker(FavoriteChefList.this);

        // check if GPS enabled
        if (gps.canGetLocation()) {

            double lat = gps.getLatitude();
            double lon = gps.getLongitude();
            latitude = String.valueOf(lat);
            longitude = String.valueOf(lon);

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }

        Intent intent = getIntent();
        cuisine = intent.getStringExtra("cuisine");
        cuisine_name = intent.getStringExtra("sub_cuisine");


        //---------------------------------ID----------------------------------------------------
        recyclerView = (RecyclerView) findViewById(R.id.home_recycler_view);
        no_image_available = (TextView) findViewById(R.id.no_data);

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new android.support.v7.app.AlertDialog.Builder(FavoriteChefList.this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            if (!TextUtils.isEmpty(cuisine)) {
                if (cuisine.equalsIgnoreCase("1")) {
                    getSupportActionBar().setTitle(cuisine_name);
                    filteredChefListAPI();
                } else if (cuisine.equalsIgnoreCase("0")) {
                    getSupportActionBar().setTitle(R.string.favorites);
                    chefFavListAPI();
                }
            }

        }

        // ------------------------setting listener on recycler view------------------------------
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                HomeFragmentModel movie = homeFragmentModelList.get(position);

                Intent intent = new Intent(getApplicationContext(), ChefProfile.class);
                intent.putExtra("Chef_id", movie.getChef_id());
                intent.putExtra("keyChef", "home");

                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    /*-----------------------Chef Fav List API--------------------------*/

    public void chefFavListAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.GET_CHEF_FAV_URL + "?user_id=%1$s", user_id);
        Log.e("URI", "" + uri);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            // Log.e("Response",response);
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                dbHelper.deleteChef();
                                UserDetail userDetail = new UserDetail();
                                JSONArray jsonArray = jsonObject.getJSONArray("getchefprofile");
                                for (int i = 0; i <= jsonArray.length(); i++) {
                                    HomeFragmentModel homeFragmentModel = new HomeFragmentModel();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    String id = jsonObject1.getString("id");
                                    String language = jsonObject1.getString("language");
                                    String fName = jsonObject1.getString("chef_fname");
                                    String last_name = jsonObject1.getString("chef_lname");
                                    String language_speak = jsonObject1.getString("language_speak");
                                    String serviceType = jsonObject1.getString("service_type");
                                    String phoneNumber = jsonObject1.getString("phone_number");
                                    String oftenCook = jsonObject1.getString("oftencook");
                                    String chefCountry = jsonObject1.getString("chef_country");
                                    String fromGuest = jsonObject1.getString("from_guest");
                                    String uptoGuest = jsonObject1.getString("upto_guest");
                                    String kitchen_title = jsonObject1.getString("kitchen_title");
                                    String kitchen_desc = jsonObject1.getString("kitchen_descrition");
                                    String profile_pic = jsonObject1.getString("profile_pic");
                                    String chef_latitude = jsonObject1.getString("chef_latitude");
                                    String chef_longitude = jsonObject1.getString("chef_longitude");
                                    String favorite = jsonObject1.getString("favouritecount");
                                    String userF_name = jsonObject1.getString("first_name");
                                    String userL_name = jsonObject1.getString("last_name");
                                    String email = jsonObject1.getString("email");
                                    String is_verify = jsonObject1.getString("is_verify");
                                    String is_chef_verify = jsonObject1.getString("is_verify_chef");
                                    String is_user = jsonObject1.getString("is_user");
                                    String device_ID = jsonObject1.getString("deviceId");
                                    String user_latitude = jsonObject1.getString("latitude");
                                    String user_longitude = jsonObject1.getString("longitude");
                                    String chef_city = jsonObject1.getString("chef_city");
                                    String all_Images = jsonObject1.getString("allimage");
                                    String price = jsonObject1.getString("price");
                                    String is_chef = jsonObject1.getString("is_chef");
                                    String lunch = jsonObject1.getString("lunch");
                                    String brunch = jsonObject1.getString("brunch");
                                    String dinner = jsonObject1.getString("dinner");
                                    String currency = jsonObject1.getString("currency");
                                    String cuisine = jsonObject1.getString("cuisine");
                                    String access_token = jsonObject1.getString("access_token");
                                    String totalreview = jsonObject1.getString("totalreview");
                                    String ok = jsonObject1.getString("ok");
                                    String no_recommended = jsonObject1.getString("no_recommended");
                                    String recommended = jsonObject1.getString("recommended");
                                    String average_rating = jsonObject1.getString("average_rating");
                                    String maxprice = jsonObject1.getString("maxprice");


                                    homeFragmentModel.setLanguage(language);
                                    homeFragmentModel.setLanguageSpeak(language_speak);
                                    homeFragmentModel.setServiceType(serviceType);
                                    homeFragmentModel.setPhoneNumber(phoneNumber);
                                    homeFragmentModel.setOftenCook(oftenCook);
                                    homeFragmentModel.setChefCountry(chefCountry);
                                    homeFragmentModel.setFromGuest(fromGuest);
                                    homeFragmentModel.setUptoGuest(uptoGuest);
                                    homeFragmentModel.setKitchen_description(kitchen_desc);
                                    homeFragmentModel.setChef_latitude(chef_latitude);
                                    homeFragmentModel.setChef_longiude(chef_longitude);
                                    homeFragmentModel.setLunch(lunch);
                                    homeFragmentModel.setDinner(dinner);
                                    homeFragmentModel.setBrunch(brunch);
                                    homeFragmentModel.setCuisine(cuisine);
                                      homeFragmentModel.setBookmarkedCount(favorite);
                                    homeFragmentModel.setEmail(email);
                                    homeFragmentModel.setIs_verify(is_verify);
                                    homeFragmentModel.setIs_chef_verify(is_chef_verify);
                                    homeFragmentModel.setIs_chef(is_chef);
                                    homeFragmentModel.setUserLat(user_latitude);
                                    homeFragmentModel.setUserLong(user_longitude);
                                    homeFragmentModel.setChefName(fName);
                                    homeFragmentModel.setChefLName(last_name);
                                    homeFragmentModel.setChefAddress(chef_city);
                                    homeFragmentModel.setKitchen_title(kitchen_title);
                                    homeFragmentModel.setProfileUrl(profile_pic);
                                    homeFragmentModel.setHKD(currency + " " + price);
                                    homeFragmentModel.setAllImages(all_Images);
                                    homeFragmentModel.setChefRate(average_rating);
                                    homeFragmentModel.setReviewCount(totalreview + " Review");
                                    homeFragmentModel.setRateCount(average_rating);
                                    homeFragmentModel.setHappyCount(recommended);
                                    homeFragmentModel.setSadCount(no_recommended);
                                    homeFragmentModel.setFoodType1("Indian");
                                    homeFragmentModel.setFoodType2("Italian");
                                    homeFragmentModel.setBookmarkedCount(favorite);
                                    homeFragmentModel.setChef_id(id);

                                    List<String> items = Arrays.asList(all_Images.split("\\|"));

                                    ArrayList<String> listView = new ArrayList<>();
                                    for (int j = 0; j < items.size(); j++) {
                                        if (!items.get(j).equals("")) {
                                            listView.add(items.get(j));
                                        }
                                    }
                                    int imagesLength = listView.size();
                                    homeFragmentModel.setImageLength(imagesLength);
                                    if (!listView.isEmpty()) {
                                        homeFragmentModel.setImageUrl(listView.get(0));
                                        homeFragmentModel.setImage2Url(listView.get(1));
                                        homeFragmentModel.setImage3Url(listView.get(2));
                                    }
                                    homeFragmentModelList.add(homeFragmentModel);
                                    // -------------Layout setting on recycler view---------------
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerView.setLayoutManager(mLayoutManager);
                                    recyclerView.setNestedScrollingEnabled(false);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    // ---------------Adapter---------------------
                                    homeFragmentAdapter = new HomeFragmentAdapter(getApplicationContext(), homeFragmentModelList);
                                    // setting adapter on recycler view
                                    recyclerView.setAdapter(homeFragmentAdapter);
                                    userDetail.setLanguage(language);
                                    userDetail.setAccessToken(access_token);
                                    userDetail.setLanguageList(language_speak);
                                    userDetail.setServiceType(serviceType);
                                    userDetail.setPhoneNumber(phoneNumber);
                                    userDetail.setOftenCook(oftenCook);
                                    userDetail.setChefCountry(chefCountry);
                                    userDetail.setMin_guest(fromGuest);
                                    userDetail.setMax_guest(uptoGuest);
                                    userDetail.setKitchenDescription(kitchen_desc);
                                    userDetail.setLatitude(chef_latitude);
                                    userDetail.setLongitude(chef_longitude);
                                    userDetail.setLunch(lunch);
                                    userDetail.setDinner(dinner);
                                    userDetail.setBrunch(brunch);
                                    userDetail.setCuisine(cuisine);
                                    userDetail.setEmailId(email);
                                    userDetail.setIsVerifyUser(is_verify);
                                    userDetail.setIsChefVerify(is_chef_verify);
                                    userDetail.setIsChef(is_chef);
                                   /* userDetail.setLatitude(user_latitude);
                                    userDetail.setLongitude(user_longitude);*/
                                    userDetail.setFirstName(fName);
                                    userDetail.setLastName(last_name);
                                    userDetail.setCity(chef_city);
                                    userDetail.setKitchenTitle(kitchen_title);
                                    userDetail.setProfilePic(profile_pic);
                                    userDetail.setPrice(price);
                                    userDetail.setAllImages(all_Images);
                                    /*userDetail.setChefRate("4.5");*/
                                    userDetail.setTotalReview(totalreview + " " + getResources().getString(R.string.review));
                                    userDetail.setAvgRating(average_rating);
                                    userDetail.setRecommended(recommended);
                                    userDetail.setNotRecommended(no_recommended);
                                    userDetail.setOk(ok);
                                   /* userDetail.setFoodType2("Italian");*/
                                    userDetail.setFavCount(favorite);
                                    userDetail.setUser_id(id);
                                    userDetail.setCurrency(currency);
                                    userDetail.setCountry(chefCountry);
                                    userDetail.setMaxPrice(maxprice);

                                    dbHelper.addChefDetails(userDetail);
                                }
                                homeFragmentAdapter.notifyDataSetChanged();
                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                no_image_available.setVisibility(View.VISIBLE);
                                no_image_available.setText(message);
                                recyclerView.setVisibility(View.GONE);
                                Toast.makeText(FavoriteChefList.this, "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            //  Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }if (getApplicationContext()!=null)
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e(TAG, "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void filteredChefListAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        //  String uri = String.format(AppConfig.GET_CHEF_FAV_URL + "?user_id=%1$s", user_id);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.FILTERED_CUISINE_LIST_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            Log.e("Response", response);
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                dbHelper.deleteChef();
                                UserDetail userDetail = new UserDetail();

                                JSONArray jsonArray = jsonObject.getJSONArray("getChefCuisines");
                                for (int i = 0; i <= jsonArray.length(); i++) {
                                    HomeFragmentModel homeFragmentModel = new HomeFragmentModel();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    String id = jsonObject1.getString("id");
                                    String language = jsonObject1.getString("language");
                                    String fName = jsonObject1.getString("chef_fname");
                                    String last_name = jsonObject1.getString("chef_lname");
                                    String language_speak = jsonObject1.getString("language_speak");
                                    String serviceType = jsonObject1.getString("service_type");
                                    String phoneNumber = jsonObject1.getString("phone_number");
                                    String oftenCook = jsonObject1.getString("oftencook");
                                    String chefCountry = jsonObject1.getString("chef_country");
                                    String fromGuest = jsonObject1.getString("from_guest");
                                    String uptoGuest = jsonObject1.getString("upto_guest");
                                    String kitchen_title = jsonObject1.getString("kitchen_title");
                                    String kitchen_desc = jsonObject1.getString("kitchen_descrition");
                                    String profile_pic = jsonObject1.getString("profile_pic");
                                    String chef_latitude = jsonObject1.getString("chef_latitude");
                                    String chef_longitude = jsonObject1.getString("chef_longitude");
                                    String favorite = jsonObject1.getString("favouritecount");
                                    String userF_name = jsonObject1.getString("first_name");
                                    String userL_name = jsonObject1.getString("last_name");
                                    String email = jsonObject1.getString("email");
                                    String is_verify = jsonObject1.getString("is_verify");
                                    String is_chef_verify = jsonObject1.getString("is_verify_chef");
                                    String is_user = jsonObject1.getString("is_user");
                                    String device_ID = jsonObject1.getString("deviceId");
                                    String user_latitude = jsonObject1.getString("latitude");
                                    String user_longitude = jsonObject1.getString("longitude");
                                    String chef_city = jsonObject1.getString("chef_city");
                                    String all_Images = jsonObject1.getString("allimage");
                                    String price = jsonObject1.getString("price");
                                    String is_chef = jsonObject1.getString("is_chef");
                                    String lunch = jsonObject1.getString("lunch");
                                    String brunch = jsonObject1.getString("brunch");
                                    String dinner = jsonObject1.getString("dinner");
                                    String currency = jsonObject1.getString("currency");
                                    String cuisine = jsonObject1.getString("cuisine");
                                    String access_token = jsonObject1.getString("access_token");
                                    String totalreview = jsonObject1.getString("totalreview");
                                    String ok = jsonObject1.getString("ok");
                                    String no_recommended = jsonObject1.getString("no_recommended");
                                    String recommended = jsonObject1.getString("recommended");
                                    String average_rating = jsonObject1.getString("average_rating");
                                    String maxprice = jsonObject1.getString("maxprice");


                                    homeFragmentModel.setLanguage(language);
                                    homeFragmentModel.setLanguageSpeak(language_speak);
                                    homeFragmentModel.setServiceType(serviceType);
                                    homeFragmentModel.setPhoneNumber(phoneNumber);
                                    homeFragmentModel.setOftenCook(oftenCook);
                                    homeFragmentModel.setChefCountry(chefCountry);
                                    homeFragmentModel.setFromGuest(fromGuest);
                                    homeFragmentModel.setUptoGuest(uptoGuest);
                                    homeFragmentModel.setKitchen_description(kitchen_desc);
                                    homeFragmentModel.setChef_latitude(chef_latitude);
                                    homeFragmentModel.setChef_longiude(chef_longitude);
                                    homeFragmentModel.setLunch(lunch);
                                    homeFragmentModel.setDinner(dinner);
                                    homeFragmentModel.setBrunch(brunch);
                                    homeFragmentModel.setCuisine(cuisine);
                                    //   homeFragmentModel.setBookmarkedCount(favorite);
                                  /*  homeFragmentModel.setEmail(email);
                                    homeFragmentModel.setIs_verify(is_verify);
                                    homeFragmentModel.setIs_chef_verify(is_chef_verify);
                                    homeFragmentModel.setIs_chef(is_chef);
                                    homeFragmentModel.setUserLat(user_latitude);
                                    homeFragmentModel.setUserLong(user_longitude);*/
                                    homeFragmentModel.setChefName(fName);
                                    homeFragmentModel.setChefLName(last_name);
                                    homeFragmentModel.setChefAddress(chef_city);
                                    homeFragmentModel.setKitchen_title(kitchen_title);
                                    homeFragmentModel.setProfileUrl(profile_pic);
                                    homeFragmentModel.setHKD(currency + " " + price);
                                    homeFragmentModel.setAllImages(all_Images);
                                    homeFragmentModel.setChefRate(average_rating);
                                    homeFragmentModel.setReviewCount(totalreview + " Review");
                                    homeFragmentModel.setRateCount(average_rating);
                                    homeFragmentModel.setHappyCount(recommended);
                                    homeFragmentModel.setSadCount(no_recommended);
                                    homeFragmentModel.setFoodType1("Indian");
                                    homeFragmentModel.setFoodType2("Italian");
                                    homeFragmentModel.setBookmarkedCount(favorite);
                                    homeFragmentModel.setChef_id(id);

                                    List<String> items = Arrays.asList(all_Images.split("\\|"));

                                    ArrayList<String> listView = new ArrayList<>();
                                    for (int j = 0; j < items.size(); j++) {
                                        if (!items.get(j).equals("")) {
                                            listView.add(items.get(j));
                                        }
                                    }
                                    int imagesLength = listView.size();
                                    homeFragmentModel.setImageLength(imagesLength);
                                    if (!listView.isEmpty()) {
                                        homeFragmentModel.setImageUrl(listView.get(0));
                                        homeFragmentModel.setImage2Url(listView.get(1));
                                        homeFragmentModel.setImage3Url(listView.get(2));
                                    }
                                    userDetail.setLanguage(language);
                                    userDetail.setAccessToken(access_token);
                                    userDetail.setLanguageList(language_speak);
                                    userDetail.setServiceType(serviceType);
                                    userDetail.setPhoneNumber(phoneNumber);
                                    userDetail.setOftenCook(oftenCook);
                                    userDetail.setChefCountry(chefCountry);
                                    userDetail.setMin_guest(fromGuest);
                                    userDetail.setMax_guest(uptoGuest);
                                    userDetail.setKitchenDescription(kitchen_desc);
                                    userDetail.setLatitude(chef_latitude);
                                    userDetail.setLongitude(chef_longitude);
                                    userDetail.setLunch(lunch);
                                    userDetail.setDinner(dinner);
                                    userDetail.setBrunch(brunch);
                                    userDetail.setCuisine(cuisine);
                                    userDetail.setEmailId(email);
                                    userDetail.setIsVerifyUser(is_verify);
                                    userDetail.setIsChefVerify(is_chef_verify);
                                    userDetail.setIsChef(is_chef);
                                   /* userDetail.setLatitude(user_latitude);
                                    userDetail.setLongitude(user_longitude);*/
                                    userDetail.setFirstName(fName);
                                    userDetail.setLastName(last_name);
                                    userDetail.setCity(chef_city);
                                    userDetail.setKitchenTitle(kitchen_title);
                                    userDetail.setProfilePic(profile_pic);
                                    userDetail.setPrice(price);
                                    userDetail.setAllImages(all_Images);
                                    /*userDetail.setChefRate("4.5");*/
                                    userDetail.setTotalReview(totalreview + " " + getResources().getString(R.string.review));
                                    userDetail.setAvgRating(average_rating);
                                    userDetail.setRecommended(recommended);
                                    userDetail.setNotRecommended(no_recommended);
                                    userDetail.setOk(ok);
                                   /* userDetail.setFoodType2("Italian");*/
                                    userDetail.setFavCount(favorite);
                                    userDetail.setUser_id(id);
                                    userDetail.setCurrency(currency);
                                    userDetail.setCountry(chefCountry);
                                    userDetail.setMaxPrice(maxprice);

                                    dbHelper.addChefDetails(userDetail);
                                    homeFragmentModelList.add(homeFragmentModel);
                                    // -------------Layout setting on recycler view---------------
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerView.setLayoutManager(mLayoutManager);
                                    recyclerView.setNestedScrollingEnabled(false);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    // ---------------Adapter---------------------
                                    homeFragmentAdapter = new HomeFragmentAdapter(getApplicationContext(), homeFragmentModelList);
                                    // setting adapter on recycler view
                                    recyclerView.setAdapter(homeFragmentAdapter);

                                }
                                homeFragmentAdapter.notifyDataSetChanged();
                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                no_image_available.setVisibility(View.VISIBLE);
                                no_image_available.setText(message);
                                recyclerView.setVisibility(View.GONE);
                                Toast.makeText(FavoriteChefList.this, "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            //  Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }if (getApplicationContext()!=null)
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //  String image = getStringImage(bitmap);
                map.put("userid", user_id);
                map.put("cuisinename", cuisine_name);
                map.put("latitude", latitude);
                map.put("longitude", longitude);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*finish();*/
//        Intent intent = new Intent(getApplicationContext(), HomePage.class);
//        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
