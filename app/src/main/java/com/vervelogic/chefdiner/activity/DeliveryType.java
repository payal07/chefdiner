package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.adapter.OnsiteCookingAdapter;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.OnsiteCookingModel;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DeliveryType extends AppCompatActivity implements View.OnClickListener {

    /*--------------------------------------Declarations----------------------------------------*/
    Toolbar toolbar;
    Button nextService;
    RadioGroup radioGroup;
    RadioButton food_delivery, home_private_meal, restaurant_private_meal, chef_onsite_booking, cooking_workshop, daily_meal;
    Activity context = this;
    String language, chef_id, timeStr, count, menu_id, menu_title, menu_price, menu_currency,dateStr;
    OnsiteCookingAdapter onsiteCookingAdapter;
    ArrayList<OnsiteCookingModel> onsiteCookingModels = new ArrayList<>();
    RecyclerView recyclerView;
    String serviceType = "",reservationType;

    DbHelper dbHelper;
    private static final String TAG = DeliveryType.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_type);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.request_a_book);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*-------------------------------------------IDs-----------------------------------------*/
        nextService = (Button) findViewById(R.id.nextService);
        radioGroup = (RadioGroup) findViewById(R.id.serviceType);
        food_delivery = (RadioButton) findViewById(R.id.food_delivery);
        home_private_meal = (RadioButton) findViewById(R.id.home_private_meal);
        restaurant_private_meal = (RadioButton) findViewById(R.id.restaurant_private_meal);
        chef_onsite_booking = (RadioButton) findViewById(R.id.chef_onsite_cooking);
        cooking_workshop = (RadioButton) findViewById(R.id.cooking_workshop);
        daily_meal = (RadioButton) findViewById(R.id.daily_meal);

        /*--------------------------------------Listener---------------------------------------------*/
        nextService.setOnClickListener(this);
        SharedPreference sharedPreference=new SharedPreference();

        Intent intent = getIntent();
        language = sharedPreference.getLanguage(context);
        chef_id = intent.getStringExtra("chef_id");
        timeStr = intent.getStringExtra("timeStr");
        count = intent.getStringExtra("count");
        menu_id = intent.getStringExtra("menu_id");
        menu_title = intent.getStringExtra("menu_title");
        menu_price = intent.getStringExtra("menu_price");
        menu_currency = intent.getStringExtra("menu_currency");
        reservationType = intent.getStringExtra("reservationType");
        dateStr = intent.getStringExtra("date");


        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllBookingDetails();
        for (UserDetail cn : contacts) {
            serviceType = cn.getServiceType();
            Log.e("Servicetype", "" + serviceType);
        }

        if (!TextUtils.isEmpty(serviceType)) {
            List<String> items = Arrays.asList(serviceType.split("\\s*,\\s*"));
            Log.e("ListItem", "" + items);
            for (int i = 0; i < items.size(); i++) {
                String stringList = items.get(i);
                if (stringList.equalsIgnoreCase(getString(R.string.food_delivery))) {
                    food_delivery.setChecked(true);
                } else if (stringList.equalsIgnoreCase(getString(R.string.home_private_meal))) {
                    home_private_meal.setChecked(true);
                } else if (stringList.equalsIgnoreCase(getString(R.string.restaurant_private_meal))) {
                    restaurant_private_meal.setChecked(true);
                } else if (stringList.equalsIgnoreCase(getString(R.string.chef_onsite_cooking))) {
                    chef_onsite_booking.setChecked(true);
                } else if (stringList.equalsIgnoreCase(getString(R.string.cooking_workshop))) {
                    cooking_workshop.setChecked(true);
                } else if (stringList.equalsIgnoreCase(getString(R.string.daily_meal))) {
                    daily_meal.setChecked(true);
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.nextService:
                if (food_delivery.isChecked()) {
                    /*------------------------------ custom dialog--------------------------------*/
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    Window window = dialog.getWindow();
                    window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setContentView(R.layout.custom_delivery_dialog);

                    /*---------------IDS------------------------------------------------------------*/
                    final RadioButton pickup_from_chef = dialog.findViewById(R.id.pickup_from_chef);
                    final RadioButton deliver_to_your_home = dialog.findViewById(R.id.deliver_to_your_home);

                    Button dialogButton = dialog.findViewById(R.id.ok_food_delivery);
                    // if button is clicked, close the custom dialog
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (pickup_from_chef.isChecked()) {
                                dialog.dismiss();

                               /* UserDetail userDetail=new UserDetail();

                                userDetail.setServiceType(food_delivery.getText().toString());
                                userDetail.setFoodDelivery(pickup_from_chef.getText().toString());


                                dbHelper.updateBooking(userDetail);
*/
                                Intent intent = new Intent(getApplicationContext(), ReservationDetailsFoodDelivery.class);
                                intent.putExtra("language", language);
                                intent.putExtra("chef_id", chef_id);
                                intent.putExtra("timeStr", timeStr);
                                intent.putExtra("count", count);
                                intent.putExtra("menu_id", menu_id);
                                intent.putExtra("menu_title", menu_title);
                                intent.putExtra("menu_price", menu_price);
                                intent.putExtra("menu_currency", menu_currency);
                                intent.putExtra("service_type", food_delivery.getText().toString());
                                intent.putExtra("food_delivery", pickup_from_chef.getText().toString());
                                intent.putExtra("kitchen_tool", "");
                                intent.putExtra("address_Str", "");
                                intent.putExtra("date", dateStr);
                                intent.putExtra("reservationType",reservationType);
                                startActivity(intent);
                            }
                            if (deliver_to_your_home.isChecked()) {

                                Intent intent = new Intent(getApplicationContext(), FoodDeliveryRequestBook.class);
                                intent.putExtra("language", language);
                                intent.putExtra("chef_id", chef_id);
                                intent.putExtra("timeStr", timeStr);
                                intent.putExtra("count", count);
                                intent.putExtra("menu_id", menu_id);
                                intent.putExtra("menu_title", menu_title);
                                intent.putExtra("menu_price", menu_price);
                                intent.putExtra("menu_currency", menu_currency);
                                intent.putExtra("service_type", food_delivery.getText().toString());
                                intent.putExtra("food_delivery", deliver_to_your_home.getText().toString());
                                intent.putExtra("kitchen_tool", "");
                                intent.putExtra("reservationType",reservationType);
                                intent.putExtra("address_Str", "");
                                intent.putExtra("Key","0");
                                intent.putExtra("date", dateStr);
                                startActivity(intent);
                                dialog.dismiss();
                            }
                        }
                    });
                    TextView cancel = dialog.findViewById(R.id.cancel_food);
                    // if button is clicked, close the custom dialog
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                } else if (home_private_meal.isChecked()) {

                    Intent intent = new Intent(getApplicationContext(), ReservationDetailsFoodDelivery.class);
                    intent.putExtra("language", language);
                    intent.putExtra("chef_id", chef_id);
                    intent.putExtra("timeStr", timeStr);
                    intent.putExtra("count", count);
                    intent.putExtra("menu_id", menu_id);
                    intent.putExtra("menu_title", menu_title);
                    intent.putExtra("menu_price", menu_price);
                    intent.putExtra("menu_currency", menu_currency);
                    intent.putExtra("service_type", home_private_meal.getText().toString());
                    intent.putExtra("food_delivery", "");
                    intent.putExtra("kitchen_tool", "");
                    intent.putExtra("address_Str", "");
                    intent.putExtra("reservationType",reservationType);
                    intent.putExtra("date", dateStr);
                    startActivity(intent);
                } else if (restaurant_private_meal.isChecked()) {

                    Intent intent = new Intent(getApplicationContext(), ReservationDetailsFoodDelivery.class);
                    intent.putExtra("language", language);
                    intent.putExtra("chef_id", chef_id);
                    intent.putExtra("timeStr", timeStr);
                    intent.putExtra("count", count);
                    intent.putExtra("menu_id", menu_id);
                    intent.putExtra("menu_title", menu_title);
                    intent.putExtra("menu_price", menu_price);
                    intent.putExtra("menu_currency", menu_currency);
                    intent.putExtra("service_type", restaurant_private_meal.getText().toString());
                    intent.putExtra("food_delivery", "");
                    intent.putExtra("kitchen_tool", "");
                    intent.putExtra("address_Str", "");
                    intent.putExtra("reservationType",reservationType);
                    intent.putExtra("date", dateStr);
                    startActivity(intent);
                } else if (chef_onsite_booking.isChecked()) {
                   /*------------------------------ custom dialog--------------------------------*/
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    Window window = dialog.getWindow();
                    window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setContentView(R.layout.custom_onsite_cooking);

                    /*---------------IDS------------------------------------------------------------*/
                    recyclerView = dialog.findViewById(R.id.card_recycler_view);
                    final Button send = dialog.findViewById(R.id.send_onsite);
                    ConnectivityManager conMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(DeliveryType.this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        getOnsiteCookingAPI();
                    }



                    final List<String> kitchenToolList = new ArrayList<>();
                    send.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            String data = "";
                            for (int i = 0; i < onsiteCookingModels.size(); i++) {
                                OnsiteCookingModel singleStudent = onsiteCookingModels.get(i);
                                if (singleStudent.isSelected()) {

                                    data = singleStudent.getCheckBox().toString();
                                    kitchenToolList.add(data);

                                    dialog.dismiss();
                                }

                            }
                            if (data.equalsIgnoreCase("")) {
                                Toast.makeText(DeliveryType.this, getResources().getString(R.string.please_select), Toast.LENGTH_SHORT).show();
                            } else {

                              /*  Toast.makeText(getApplicationContext(),
                                        "Selected : \n" + kitchenToolList, Toast.LENGTH_LONG)
                                        .show();*/
                                Log.e("Kitchen", "" + kitchenToolList);
                                String list = TextUtils.join(",", kitchenToolList);
                                Log.e("List", "" + list);

                             /*   UserDetail userDetail=new UserDetail();

                                userDetail.setServiceType(chef_onsite_booking.getText().toString());
                                userDetail.setKitchenTool(list);

                                dbHelper.updateBooking(userDetail);*/
                                Intent intent = new Intent(getApplicationContext(), FoodDeliveryRequestBook.class);
                                intent.putExtra("language", language);
                                intent.putExtra("chef_id", chef_id);
                                intent.putExtra("timeStr", timeStr);
                                intent.putExtra("count", count);
                                intent.putExtra("menu_id", menu_id);
                                intent.putExtra("menu_title", menu_title);
                                intent.putExtra("menu_price", menu_price);
                                intent.putExtra("menu_currency", menu_currency);
                                intent.putExtra("service_type", food_delivery.getText().toString());
                                intent.putExtra("food_delivery","Deliver to your home");
                                intent.putExtra("kitchen_tool", "");
                                intent.putExtra("reservationType",reservationType);
                                intent.putExtra("address_Str", "");
                                intent.putExtra("date", dateStr);
                                intent.putExtra("Key","1");
                                intent.putExtra("kitchen_tool", list);
                                startActivity(intent);
                                dialog.dismiss();
                               /* Intent intent = new Intent(getApplicationContext(), ReservationDetailsFoodDelivery.class);
                                intent.putExtra("language", language);
                                intent.putExtra("chef_id", chef_id);
                                intent.putExtra("timeStr", timeStr);
                                intent.putExtra("count", count);
                                intent.putExtra("menu_id", menu_id);
                                intent.putExtra("menu_title", menu_title);
                                intent.putExtra("menu_price", menu_price);
                                intent.putExtra("menu_currency", menu_currency);
                                intent.putExtra("service_type", chef_onsite_booking.getText().toString());
                                intent.putExtra("kitchen_tool", list);
                                intent.putExtra("reservationType",reservationType);
                                intent.putExtra("food_delivery", "");
                                intent.putExtra("address_Str", "");
                                intent.putExtra("date", dateStr);

                                startActivity(intent);*/
                            }

                        }
                    });
                    TextView cancel = dialog.findViewById(R.id.cancel);
                    // if button is clicked, close the custom dialog
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });


                    dialog.show();
                  //  Toast.makeText(this, "Chef", Toast.LENGTH_SHORT).show();
                } else if (cooking_workshop.isChecked()) {

                  /*  UserDetail userDetail=new UserDetail();

                    userDetail.setServiceType(cooking_workshop.getText().toString());


                    dbHelper.updateBooking(userDetail);*/

                    Intent intent = new Intent(getApplicationContext(), ReservationDetailsFoodDelivery.class);
                    intent.putExtra("language", language);
                    intent.putExtra("chef_id", chef_id);
                    intent.putExtra("timeStr", timeStr);
                    intent.putExtra("count", count);
                    intent.putExtra("menu_id", menu_id);
                    intent.putExtra("reservationType",reservationType);
                    intent.putExtra("menu_title", menu_title);
                    intent.putExtra("menu_price", menu_price);
                    intent.putExtra("menu_currency", menu_currency);
                    intent.putExtra("service_type", cooking_workshop.getText().toString());
                    intent.putExtra("food_delivery", "");
                    intent.putExtra("kitchen_tool", "");
                    intent.putExtra("address_Str", "");
                    intent.putExtra("date", dateStr);
                    startActivity(intent);
                } else if (daily_meal.isChecked()) {
                    Intent intent = new Intent(getApplicationContext(), ReservationDetailsFoodDelivery.class);
                    intent.putExtra("language", language);
                    intent.putExtra("chef_id", chef_id);
                    intent.putExtra("timeStr", timeStr);
                    intent.putExtra("count", count);
                    intent.putExtra("reservationType",reservationType);
                    intent.putExtra("menu_id", menu_id);
                    intent.putExtra("menu_title", menu_title);
                    intent.putExtra("menu_price", menu_price);
                    intent.putExtra("menu_currency", menu_currency);
                    intent.putExtra("service_type", daily_meal.getText().toString());
                    intent.putExtra("food_delivery", "");
                    intent.putExtra("kitchen_tool", "");
                    intent.putExtra("address_Str", "");
                    intent.putExtra("date", dateStr);
                    startActivity(intent);
                }
                break;
            default:
               // Toast.makeText(this, "Food Delivery", Toast.LENGTH_SHORT).show();
        }
    }

    /*-----------------------------------------Onsite Cooking API---------------------------------------------------------*/

    public void getOnsiteCookingAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.GET_ONSITE_COOKING_URL + "?language=%1$s", language);
        Log.e("URI", "" + uri);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                       /* Log.e("Response",""+s);*/
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {

                                JSONArray jsonArray = jsonObject.getJSONArray("onsiteCookingarr");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    String cooking_tool = jsonObject2.getString("cooking_tool");
                                    OnsiteCookingModel onsiteCookingModel = new OnsiteCookingModel();
                                    onsiteCookingModel.setCheckBox(cooking_tool);
                                    onsiteCookingModels.add(onsiteCookingModel);

                                }
                                int[] attrsArray = {
                                        android.R.attr.columnWidth
                                };
                                /*TypedArray array = context.obtainStyledAttributes(
                                        attrs, attrsArray);
                                columnWidth = array.getDimensionPixelSize(0, -1);
                                array.recycle();*/
                                RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);
                                recyclerView.setLayoutManager(layoutManager);
                                onsiteCookingAdapter = new OnsiteCookingAdapter(getApplicationContext(), onsiteCookingModels);
                                recyclerView.setAdapter(onsiteCookingAdapter);

                            } else if (status == 0) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }if (getApplicationContext()!=null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);

    }
}
