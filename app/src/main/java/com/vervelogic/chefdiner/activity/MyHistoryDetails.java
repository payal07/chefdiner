package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;

public class MyHistoryDetails extends AppCompatActivity {

    String kitchenImage, menuTitle, code, date, menuPrice, kitchenName, statusDinner, houseRules;
    TextView kitchen_name_history, history_status, code_history, date_history, guest_history, total_price_history, house_rules_history, menu_history;
    Button invite_earn_history;
    ImageView kitchen_image_history;
    Activity context = this;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_history_details);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent intent = getIntent();
        kitchenImage = intent.getStringExtra("kitchenImage");
        menuTitle = intent.getStringExtra("menuTitle");
        code = intent.getStringExtra("code");
        date = intent.getStringExtra("date");
        menuPrice = intent.getStringExtra("menuPrice");
        kitchenName = intent.getStringExtra("kitchenName");
        statusDinner = intent.getStringExtra("statusDinner");
        houseRules = intent.getStringExtra("houseRules");

        kitchen_name_history = (TextView) findViewById(R.id.kitchen_name_history);
        history_status = (TextView) findViewById(R.id.history_status);
        code_history = (TextView) findViewById(R.id.code_history);
        date_history = (TextView) findViewById(R.id.date_history);
        guest_history = (TextView) findViewById(R.id.guest_history);
        total_price_history = (TextView) findViewById(R.id.total_price_history);
        house_rules_history = (TextView) findViewById(R.id.house_rules_history);
        menu_history = (TextView) findViewById(R.id.menu_history);
        invite_earn_history = (Button) findViewById(R.id.invite_earn_history);
        kitchen_image_history = (ImageView) findViewById(R.id.kitchen_image_history);

        if (!TextUtils.isEmpty(kitchenImage)) {
            Picasso.with(context)
                    .load(kitchenImage)
                    .error(R.drawable.people).fit()
                    .into(kitchen_image_history);
        }
        if (!TextUtils.isEmpty(menuTitle)) {
            menu_history.setText(menuTitle);
        }
        if (!TextUtils.isEmpty(code)) {
            code_history.setText(code);
        }
        if (!TextUtils.isEmpty(date)) {
            date_history.setText(date);
        }
        if (!TextUtils.isEmpty(menuPrice)) {
            total_price_history.setText(menuPrice);
        }
        if (!TextUtils.isEmpty(kitchenName)) {
            kitchen_name_history.setText(kitchenName);
            getSupportActionBar().setTitle(kitchenName);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            toolbar.setTitleTextColor(0xFFFFFFFF);
        }
        if (!TextUtils.isEmpty(statusDinner)) {
            history_status.setText(statusDinner);
        }
        if (!TextUtils.isEmpty(houseRules)) {
            house_rules_history.setText(houseRules);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
