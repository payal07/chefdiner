package com.vervelogic.chefdiner.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;
import com.stacktips.view.DayDecorator;
import com.stacktips.view.DayView;
import com.stacktips.view.utils.CalendarUtils;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.model.CalendarModelDateAvail;
import com.vervelogic.chefdiner.model.GetDateModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/*import com.prolificinteractive.materialcalendarview.CalendarMode;*/

public class CalendarActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "PaymentClass";
    CustomCalendarView calendarView;
    Toolbar toolbar;
    Button avail, unavail;
    String selectedDate = "", Chef_id;
    List<String> GetchefCalendarBean = new ArrayList<>();
    List<GetDateModel.GetchefCalendarBean> getchefCalendarBeans = new ArrayList<>();
    List<String> dateAvail = new ArrayList<>();
    //Initialize calendar with date
    Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.manage_calendar);

        Intent intent = getIntent();
        Chef_id = intent.getStringExtra("Chef_id");

        //Initialize CustomCalendarView from layout
        calendarView = (CustomCalendarView) findViewById(R.id.calendar_view);
        avail = (Button) findViewById(R.id.avail);
        unavail = (Button) findViewById(R.id.unavail);

        avail.setOnClickListener(this);
        unavail.setOnClickListener(this);


        getCalDateAPI();
        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);


        //Handling custom calendar events
        calendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                if (CalendarUtils.isPastDay(date)) {
                    selectedDate = "";
                //    Toast.makeText(CalendarActivity.this, "Disabled Date", Toast.LENGTH_SHORT).show();
                } else {
                   /* int color = getResources().getColor(R.color.blue);
                   calendarView.setba(color);*/
                    selectedDate = df.format(date);
                    Toast.makeText(CalendarActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onMonthChanged(Date date) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("MM-yyyy");
                Toast.makeText(CalendarActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
            }
        });

        /*//Setting custom font
        final Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Arch_Rival_Bold.ttf");
        if (null != typeface) {
            calendarView.setCustomTypeface(typeface);
            calendarView.refreshCalendar(currentCalendar);
        }*/

        //adding calendar day decorators
        List<DayDecorator> decorators = new ArrayList<>();
        // decorators.add(new DisabledColorDecorator());
        decorators.add(new ColorDecorator());
        calendarView.setDecorators(decorators);
        calendarView.refreshCalendar(currentCalendar);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.avail:
                if (!TextUtils.isEmpty(selectedDate)) {
                    calendarDateAPI(selectedDate, "1");
                } else {
                    Toast.makeText(this, getResources().getString(R.string.select_another_date), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.unavail:
                if (!TextUtils.isEmpty(selectedDate)) {
                    calendarDateAPI(selectedDate, "0");
                } else {
                    Toast.makeText(this, getResources().getString(R.string.select_another_date), Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    public void getCalDateAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.CALENDAR_DETAILS_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Class", "" + s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            GetDateModel paymentModel = new Gson().fromJson(s, GetDateModel.class);
                            if (status == 1) {


                                Log.e("D", "" + s);
                                for (int i = 0; i < paymentModel.getGetchefCalendar().size(); i++) {
                                    getchefCalendarBeans.add(paymentModel.getGetchefCalendar().get(i));
                                    GetchefCalendarBean.add(paymentModel.getGetchefCalendar().get(i).getDate());
                                    dateAvail.add(paymentModel.getGetchefCalendar().get(i).getAva_status());
                                    calendarView.refreshCalendar(currentCalendar);
                                    Log.e("Status", "" + paymentModel.getGetchefCalendar().get(i).getAva_status());
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }if (getApplicationContext()!=null)
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("Error", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", Chef_id);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void calendarDateAPI(final String date, final String availStatus) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.CALENDAR_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e(TAG, "" + s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            CalendarModelDateAvail paymentModel = new Gson().fromJson(s, CalendarModelDateAvail.class);
                            if (status == 1) {
                                Log.e(TAG, "" + s);
                                String s1 = paymentModel.getChefCalendar().get(0).getAva_status();
                                getCalDateAPI();
                                // webView.setVisibility(View.GONE);
                                // ---------------------------custom dialog---------------------------------

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }if (getApplicationContext()!=null)
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e(TAG, "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("calendar_id", "");
                map.put("chef_id", Chef_id);
                map.put("date", date);
                map.put("ava_status", availStatus);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private class DisabledColorDecorator implements DayDecorator {
        @Override
        public void decorate(DayView dayView) {
            if (CalendarUtils.isPastDay(dayView.getDate())) {
                int color = Color.parseColor("#a9afb9");
                dayView.setTextColor(color);
                /*calendarView.state().edit()
                        .setFirstDayOfWeek(Calendar.SUNDAY)
                        .setMinimumDate(Calendar.getInstance())
                        .setCalendarDisplayMode(CalendarMode.MONTHS)
                        .commit();*/

            }
        }
    }

    private class ColorDecorator implements DayDecorator {
        @SuppressLint("ResourceType")
        @Override
        public void decorate(DayView dayView) {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            String date = df.format(dayView.getDate());


            if (CalendarUtils.isPastDay(dayView.getDate())) {
                int color = Color.parseColor("#a9afb9");
                dayView.setTextColor(color);
            }
           /* for (int i = 0; i < getchefCalendarBeans.size(); i++) {
                if (date.equalsIgnoreCase(getchefCalendarBeans.get(i).getDate())) {
                    if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("1")) {
                        int color = Color.parseColor("#5ec639");
                        dayView.setBackgroundColor(color);
                        dayView.setBackgroundResource(R.drawable.green_circle);
                        dayView.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        int color = Color.parseColor("#ff0000");
                        dayView.setBackgroundColor(color);
                        dayView.setBackgroundResource(R.drawable.red_circle);
                        dayView.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            }*/
            int color = 0;
            for (int i = 0; i < getchefCalendarBeans.size(); i++) {
                if (date.equalsIgnoreCase(getchefCalendarBeans.get(i).getDate())) {

                    if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("2")) {
                        try {
                            Date d = df.parse(getchefCalendarBeans.get(i).getDate());
                            if (CalendarUtils.isPastDay(d)) {
                                color = getResources().getColor(R.color.filter_color);
                                dayView.setTextColor(color);
                                dayView.setBackgroundResource(R.drawable.ic_substract);
                            } else {
                                dayView.setTextColor(getResources().getColor(R.color.green));
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        // dayView.setBackgroundResource(R.drawable.green_circle);

                    } else if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("0")) {
                        dayView.setBackgroundResource(R.drawable.red_circle);
                        dayView.setTextColor(getResources().getColor(R.color.filter_color));
                        dayView.setBackgroundResource(R.drawable.ic_substract);
                    }
                }
            }
            /*for (String d:GetchefCalendarBean)
            {
                if (!TextUtils.isEmpty(d))
                {
                    if (date.equalsIgnoreCase(d))
                    {

                        for (String ava:dateAvail)
                        {
                            if (!TextUtils.isEmpty(ava))
                            {
                                if (ava.equalsIgnoreCase("1"))
                                {
                                    Log.e("D1",""+d);
                                    Log.e("AVA","1");
                                    int color = Color.parseColor("#5ec639");
                                    dayView.setTextColor(color);
                                }
                                else *//*if (ava.equalsIgnoreCase("0"))*//*
                                {
                                    Log.e("D0",""+d);
                                    Log.e("AVA","0");
                                    int color = Color.parseColor("#a9afb9");
                                    dayView.setTextColor(color);
                                }
                            }
                        }

                    }
                }

            }*/
            /*if (dayView.getDate().getDate()==1 || dayView.getDate().getDate()==2 || dayView.getDate().getDate()==9)
            {

                int color = Color.parseColor("#9fffc2");
                dayView.setBackgroundColor(color);
            }*/
        }
    }
}


