package com.vervelogic.chefdiner.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BecomAChefServiceType extends AppCompatActivity implements View.OnClickListener {

    /*-----------------------Declarations---------------------------------*/
    CheckBox food_delivery_chef, home_private_meal_chef, restaurant_private_meal_chef,
            chef_onsite_cooking_chef, cooking_workshop_chef, daily_meal;
    Button next10;
    String langaugeList, language, firstName, lastName, phoneNumber, oftenCook, country, city, noOfGuestMin, noOfGuestMax, kitchenTitle, kitchenDesc, country_code, latitude, longitude, selectedCurrency;

    DbHelper dbHelper;
    String serviceList = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_becom_achef_service_type);

        /*---------------------Toolbar----------------------------*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      /*  // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/

       /*--------------------IDS------------------------------------------*/
        food_delivery_chef = (CheckBox) findViewById(R.id.food_delivery_chef);
        home_private_meal_chef = (CheckBox) findViewById(R.id.home_private_meal_chef);
        restaurant_private_meal_chef = (CheckBox) findViewById(R.id.restaurant_private_meal_chef);
        chef_onsite_cooking_chef = (CheckBox) findViewById(R.id.chef_onsite_cooking_chef);
        cooking_workshop_chef = (CheckBox) findViewById(R.id.cooking_workshop_chef);
        daily_meal = (CheckBox) findViewById(R.id.daily_meal);
        next10 = (Button) findViewById(R.id.next10);

        /*---------------------------Getting data from intent-----------------------*/
        Intent intent = getIntent();
        langaugeList = intent.getStringExtra("LanguageList");
        language = intent.getStringExtra("language");
        firstName = intent.getStringExtra("firstName");
        lastName = intent.getStringExtra("lastName");
        phoneNumber = intent.getStringExtra("phoneNumberChef");
        oftenCook = intent.getStringExtra("Often Cook");
        country = intent.getStringExtra("country");
        city = intent.getStringExtra("city");
        noOfGuestMin = intent.getStringExtra("minGuest");
        noOfGuestMax = intent.getStringExtra("maxGuest");
        kitchenTitle = intent.getStringExtra("kitchenTitle");
        kitchenDesc = intent.getStringExtra("kitchenDesc");
        country_code = intent.getStringExtra("country_code");
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        selectedCurrency = intent.getStringExtra("selectedCurrency");

        /*----------Listener--------------------------*/
        next10.setOnClickListener(this);

      /*-----------------------------DB access--------------------------------------*/
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            serviceList = cn.getServiceType();
            Log.e("List", "" + cn.getLanguageList());
        }
        if (!TextUtils.isEmpty(serviceList)) {
            List<String> items = Arrays.asList(serviceList.split("\\s*,\\s*"));
            Log.e("ListItem", "" + items);
            for (int i = 0; i < items.size(); i++) {
                String stringList = items.get(i);
                if (stringList.equalsIgnoreCase("Food delivery")) {
                    food_delivery_chef.setChecked(true);
                } else if (stringList.equalsIgnoreCase("Home private meal")) {
                    home_private_meal_chef.setChecked(true);
                } else if (stringList.equalsIgnoreCase("Restaurant private meal")) {
                    restaurant_private_meal_chef.setChecked(true);
                } else if (stringList.equalsIgnoreCase("Chef onsite cooking")) {
                    chef_onsite_cooking_chef.setChecked(true);
                } else if (stringList.equalsIgnoreCase("Cooking workshop")) {
                    cooking_workshop_chef.setChecked(true);
                } else if (stringList.equalsIgnoreCase("Daily meal")) {
                    daily_meal.setChecked(true);
                }

            }

        }

    }

    @Override
    public void onClick(View view) {
        if (!validate()) {
            return;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validate() {
        String food_delivery_chefStr = food_delivery_chef.getText().toString();
        String home_private_meal_chefStr = home_private_meal_chef.getText().toString();
        String restaurant_private_meal_chefStr = restaurant_private_meal_chef.getText().toString();
        String chef_onsite_cooking_chefStr = chef_onsite_cooking_chef.getText().toString();
        String cooking_workshop_chefStr = cooking_workshop_chef.getText().toString();
        String daily_mealStr = daily_meal.getText().toString();

        List<String> serviceList = new ArrayList<>();
        if (food_delivery_chef.isChecked()) {
            serviceList.add("Food delivery");
        }
        if (home_private_meal_chef.isChecked()) {
            serviceList.add("Home private meal");
        }
        if (restaurant_private_meal_chef.isChecked()) {
            serviceList.add("Restaurant private meal");
        }
        if (chef_onsite_cooking_chef.isChecked()) {
            serviceList.add("Chef onsite cooking");
        }
        if (cooking_workshop_chef.isChecked()) {
            serviceList.add("Cooking workshop");
        }
        if (daily_meal.isChecked()) {
            serviceList.add("Daily meal");
        }
        if (serviceList.isEmpty()) {
            Toast.makeText(this, R.string.please_select_service, Toast.LENGTH_SHORT).show();
        } else {
            String list = TextUtils.join(",", serviceList);

            Intent intent = new Intent(getApplicationContext(), BecomeAChefMealType.class);
            intent.putExtra("Often Cook", oftenCook);
            intent.putExtra("LanguageList", langaugeList);
            intent.putExtra("language", language);
            intent.putExtra("firstName", firstName);
            intent.putExtra("lastName", lastName);
            intent.putExtra("phoneNumberChef", phoneNumber);
            intent.putExtra("country", country);
            intent.putExtra("city", city);
            intent.putExtra("minGuest", noOfGuestMin);
            intent.putExtra("maxGuest", noOfGuestMax);
            intent.putExtra("kitchenTitle", kitchenTitle);
            intent.putExtra("kitchenDesc", kitchenDesc);
            intent.putExtra("serviceList", list);
            intent.putExtra("country_code", country_code);
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitude);
            intent.putExtra("selectedCurrency", selectedCurrency);
            startActivity(intent);
        }

        return true;
    }
}
