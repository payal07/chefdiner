package com.vervelogic.chefdiner.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.bottomFragment.Feed;
import com.vervelogic.chefdiner.bottomFragment.Home;
import com.vervelogic.chefdiner.bottomFragment.Meal;
import com.vervelogic.chefdiner.bottomFragment.Message;
import com.vervelogic.chefdiner.bottomFragment.Profile;
import com.vervelogic.chefdiner.utils.SessionManager;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.lang.reflect.Field;
import java.util.Locale;

public class HomePage extends AppCompatActivity {

    ImageView filter, sortOptions;
    Activity context = this;
    LinearLayout filter_ly;
    boolean doubleBackToExitPressedOnce = false;
    // Session Manager Class
    SessionManager session;
    Fragment selectedFragment = null;
    String language, page;
    /*--------------------------------Declarations----------------------------------------*/
    private Toolbar toolbar;
    private int mMenuId;

    public HomePage()
    {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        //--------------------------------------------- Session Manager--------------------------------
        session = new SessionManager(getApplicationContext());

        Intent intent = getIntent();
      //  language = intent.getStringExtra("language");
        page = intent.getStringExtra("page");

       SharedPreference sharedPreference=new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(getApplicationContext()))){
            language = sharedPreference.getLanguage(getApplicationContext());
            Log.e("Lang",""+language);
            if (!TextUtils.isEmpty(language))
            {
                if (language.equalsIgnoreCase("english"))
                {
                    language="english";
                    Locale current = Locale.ENGLISH;
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
                if (language.equalsIgnoreCase("traditional chinese"))
                {
                    Locale current = Locale.TRADITIONAL_CHINESE;
                    Log.e("TAG", "" + current);
                    language="traditional chinese";
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
                if(language.equalsIgnoreCase("simplified chinese"))
                {
                    language="simplified chinese";
                    Locale current = Locale.SIMPLIFIED_CHINESE;
                    Log.e("TAG", "" + current);
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
            }

        }
        else
        {
            language="english";
            Locale current = Locale.ENGLISH;
            Locale.setDefault(current);
            Configuration config = new Configuration();
            config.locale = current;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        }
        //    Log.e("Meal", "" + page);


/*----------------------------------Bottom Navigation-----------------------------------------------------*/
        final BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);
       /* if (!TextUtils.isEmpty(page)) {
            if (page.equalsIgnoreCase("3")) {
                bottomNavigationView.getMenu().findItem(R.id.meal).setChecked(true);
            }

        }*/


        BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                       /* session.checkLogin();*/
                       /* mMenuId = item.getItemId();
                        for (int i = 0; i < bottomNavigationView.getMenu().size(); i++) {
                            MenuItem menuItem = bottomNavigationView.getMenu().getItem(i);
                            boolean isChecked = menuItem.getItemId() == item.getItemId();
                            menuItem.setChecked(isChecked);
                        }
                        if (bottomNavigationView.getSelectedItemId() == R.id.home) {
                            bottomNavigationView.setClickable(true);
                        }*/
                        switch (item.getItemId()) {
                            case R.id.home:
                                selectedFragment = Home.newInstance();
                                break;
                            case R.id.feed:
                                if (session.isLoggedIn()) {
                                    selectedFragment = Feed.newInstance();
                                } else {
                                    //   selectedFragment = Home.newInstance();
                                    Intent intent = new Intent(getApplicationContext(), RegisterSignUpScreen.class);
                                    intent.putExtra("language", language);
                                    intent.putExtra("type", "homePage");
                                    startActivity(intent);
                                }
                                break;
                            case R.id.message:
                                //  session.checkLogin();
                                if (session.isLoggedIn()) {
                                    selectedFragment = Message.newInstance();
                                } else {
                                   /* selectedFragment = Home.newInstance();*/
                                    Intent intent = new Intent(getApplicationContext(), RegisterSignUpScreen.class);
                                    intent.putExtra("language", language);
                                    intent.putExtra("type", "homePage");
                                    startActivity(intent);
                                }
                                break;
                            case R.id.meal:
                                //  session.checkLogin();
                                if (session.isLoggedIn()) {
                                    selectedFragment = Meal.newInstance();

                                } else {
                                   /* selectedFragment = Home.newInstance();*/
                                    Intent intent = new Intent(getApplicationContext(), RegisterSignUpScreen.class);
                                    intent.putExtra("language", language);
                                    intent.putExtra("type", "homePage");
                                    startActivity(intent);
                                }
                                break;
                            case R.id.profile:
                                //  session.checkLogin();
                                if (session.isLoggedIn()) {
                                    selectedFragment = Profile.newInstance();
                                } else {
                                  /*  selectedFragment = Home.newInstance();*/
                                    Intent intent = new Intent(getApplicationContext(), RegisterSignUpScreen.class);
                                    intent.putExtra("language", language);
                                    intent.putExtra("type", "homePage");
                                    startActivity(intent);
                                }
                                break;
                        }
                        if (selectedFragment!=null)
                        {
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.viewpager, selectedFragment);
                            transaction.commit();
                        }

                        return true;
                    }
                });

        //------------------Manually displaying the first fragment - one time only--------------------------------
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.viewpager, Home.newInstance());
        transaction.commit();

    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.exit_double, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;

            }
        }, 2000);
    }


}
class  BottomNavigationViewHelper {
    public BottomNavigationViewHelper()
    {

    }

    @SuppressLint("RestrictedApi")
    static void removeShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("ERROR NO SUCH FIELD", "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            Log.e("ERROR ILLEGAL ALG", "Unable to change value of shift mode");
        }
    }
}


