package com.vervelogic.chefdiner.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;
import com.stacktips.view.DayDecorator;
import com.stacktips.view.DayView;
import com.stacktips.view.utils.CalendarUtils;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.model.GetDateModel;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CalendarMealTime extends AppCompatActivity implements View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    CustomCalendarView calendarView;
    Toolbar toolbar;
    String selectedDate = "", Chef_id;
    List<String> GetchefCalendarBean = new ArrayList<>();
    List<String> dateAvail = new ArrayList<>();
    TextView lunchStartTime, lunchEndTime, brunchStartTime, brunchEndTime, dinnerStartTime, dinnerEndTime;
    String lunchStartTimeStr = "", lunchEndTimeStr = "", brunchStartTimeStr = "", brunchEndTimeStr = "", dinnerStartTimeStr = "", dinnerEndTimeStr = "";
    Button save;
    List<GetDateModel.GetchefCalendarBean> getchefCalendarBeans = new ArrayList<>();
    //Initialize calendar with date
    Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());
    private int TIME_VALUE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_meal_time);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.manage_time_availabilty);

        Intent intent = getIntent();
        Chef_id = intent.getStringExtra("Chef_id");

        //Initialize CustomCalendarView from layout
        calendarView = (CustomCalendarView) findViewById(R.id.calendar_view);
        lunchStartTime = (TextView) findViewById(R.id.lunchStartTime);
        lunchEndTime = (TextView) findViewById(R.id.lunchEndTime);
        brunchStartTime = (TextView) findViewById(R.id.brunchStartTime);
        brunchEndTime = (TextView) findViewById(R.id.brunchEndTime);
        dinnerStartTime = (TextView) findViewById(R.id.dinnerStartTime);
        dinnerEndTime = (TextView) findViewById(R.id.dinnerEndTime);
        save = (Button) findViewById(R.id.save);

        lunchStartTime.setOnClickListener(this);
        lunchEndTime.setOnClickListener(this);
        brunchStartTime.setOnClickListener(this);
        brunchEndTime.setOnClickListener(this);
        dinnerStartTime.setOnClickListener(this);
        dinnerEndTime.setOnClickListener(this);
        save.setOnClickListener(this);

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            Log.e("", "No Internet connection");
        } else {
            getTimeAPI();
            getCalDateAPI();
        }


        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);


        //Handling custom calendar events
        calendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                selectedDate = df.format(date);
            }

            @Override
            public void onMonthChanged(Date date) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("MM-yyyy");
                Toast.makeText(CalendarMealTime.this, df.format(date), Toast.LENGTH_SHORT).show();
            }
        });

        /*//Setting custom font
        final Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Arch_Rival_Bold.ttf");
        if (null != typeface) {
            calendarView.setCustomTypeface(typeface);
            calendarView.refreshCalendar(currentCalendar);
        }
*/
        //adding calendar day decorators
        List<DayDecorator> decorators = new ArrayList<>();
        // decorators.add(new DisabledColorDecorator());
        decorators.add(new ColorDecorator());
        calendarView.setDecorators(decorators);
        calendarView.refreshCalendar(currentCalendar);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lunchStartTime:
                TIME_VALUE = 0;
                Calendar now = Calendar.getInstance();
                TimePickerDialog timepickerdialog = TimePickerDialog.newInstance(CalendarMealTime.this,
                        now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
                timepickerdialog.setThemeDark(false); //Dark Theme?
                timepickerdialog.vibrate(false); //vibrate on choosing time?
                timepickerdialog.dismissOnPause(false); //dismiss the dialog onPause() called?
                timepickerdialog.enableSeconds(true); //show seconds?

                //Handling cancel event
                timepickerdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Toast.makeText(CalendarMealTime.this, getResources().getString(R.string.cancel), Toast.LENGTH_SHORT).show();
                    }
                });
                timepickerdialog.show(getFragmentManager(), "Timepickerdialog"); //show time picker dialog
                break;
            case R.id.lunchEndTime:
                TIME_VALUE = 1;
                Calendar lunchEC = Calendar.getInstance();
                TimePickerDialog lunchEPicker = TimePickerDialog.newInstance(CalendarMealTime.this,
                        lunchEC.get(Calendar.HOUR_OF_DAY), lunchEC.get(Calendar.MINUTE), true);
                lunchEPicker.setThemeDark(false); //Dark Theme?
                lunchEPicker.vibrate(false); //vibrate on choosing time?
                lunchEPicker.dismissOnPause(false); //dismiss the dialog onPause() called?
                lunchEPicker.enableSeconds(true); //show seconds?

                //Handling cancel event
                lunchEPicker.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Toast.makeText(CalendarMealTime.this, getResources().getString(R.string.cancel), Toast.LENGTH_SHORT).show();
                    }
                });
                lunchEPicker.show(getFragmentManager(), "Timepickerdialog"); //show time picker dialog
                break;
            case R.id.brunchStartTime:
                TIME_VALUE = 2;
                Calendar brunchSC = Calendar.getInstance();
                TimePickerDialog brunchSPicker = TimePickerDialog.newInstance(CalendarMealTime.this,
                        brunchSC.get(Calendar.HOUR_OF_DAY), brunchSC.get(Calendar.MINUTE), true);
                brunchSPicker.setThemeDark(false); //Dark Theme?
                brunchSPicker.vibrate(false); //vibrate on choosing time?
                brunchSPicker.dismissOnPause(false); //dismiss the dialog onPause() called?
                brunchSPicker.enableSeconds(true); //show seconds?

                //Handling cancel event
                brunchSPicker.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Toast.makeText(CalendarMealTime.this, getResources().getString(R.string.cancel), Toast.LENGTH_SHORT).show();
                    }
                });
                brunchSPicker.show(getFragmentManager(), "Timepickerdialog"); //show time picker dialog
                break;
            case R.id.brunchEndTime:
                TIME_VALUE = 3;
                Calendar brunchEC = Calendar.getInstance();
                TimePickerDialog brunchEPicker = TimePickerDialog.newInstance(CalendarMealTime.this,
                        brunchEC.get(Calendar.HOUR_OF_DAY), brunchEC.get(Calendar.MINUTE), true);
                brunchEPicker.setThemeDark(false); //Dark Theme?
                brunchEPicker.vibrate(false); //vibrate on choosing time?
                brunchEPicker.dismissOnPause(false); //dismiss the dialog onPause() called?
                brunchEPicker.enableSeconds(true); //show seconds?

                //Handling cancel event
                brunchEPicker.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Toast.makeText(CalendarMealTime.this, getResources().getString(R.string.cancel), Toast.LENGTH_SHORT).show();
                    }
                });
                brunchEPicker.show(getFragmentManager(), "Timepickerdialog"); //show time picker dialog
                break;
            case R.id.dinnerStartTime:
                TIME_VALUE = 4;
                Calendar dinnerSC = Calendar.getInstance();
                TimePickerDialog dinnerSPicker = TimePickerDialog.newInstance(CalendarMealTime.this,
                        dinnerSC.get(Calendar.HOUR_OF_DAY), dinnerSC.get(Calendar.MINUTE), true);
                dinnerSPicker.setThemeDark(false); //Dark Theme?
                dinnerSPicker.vibrate(false); //vibrate on choosing time?
                dinnerSPicker.dismissOnPause(false); //dismiss the dialog onPause() called?
                dinnerSPicker.enableSeconds(true); //show seconds?

                //Handling cancel event
                dinnerSPicker.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Toast.makeText(CalendarMealTime.this, getResources().getString(R.string.cancel), Toast.LENGTH_SHORT).show();
                    }
                });
                dinnerSPicker.show(getFragmentManager(), "Timepickerdialog"); //show time picker dialog
                break;
            case R.id.dinnerEndTime:
                TIME_VALUE = 5;
                Calendar dinnerEC = Calendar.getInstance();
                TimePickerDialog dinnerEPicker = TimePickerDialog.newInstance(CalendarMealTime.this,
                        dinnerEC.get(Calendar.HOUR_OF_DAY), dinnerEC.get(Calendar.MINUTE), true);
                dinnerEPicker.setThemeDark(false); //Dark Theme?
                dinnerEPicker.vibrate(false); //vibrate on choosing time?
                dinnerEPicker.dismissOnPause(false); //dismiss the dialog onPause() called?
                dinnerEPicker.enableSeconds(true); //show seconds?

                //Handling cancel event
                dinnerEPicker.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Toast.makeText(CalendarMealTime.this, getResources().getString(R.string.cancel), Toast.LENGTH_SHORT).show();
                    }
                });
                dinnerEPicker.show(getFragmentManager(), "Timepickerdialog"); //show time picker dialog
                break;
            case R.id.save:
                if (!selectedDate.isEmpty()) {
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        Log.e("", "No Internet connection");
                    } else {
                        setTimeAPI();
                    }


                }
                break;
        }
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String time = hourOfDay + ":" + minute;
        if (TIME_VALUE == 0) {
            lunchStartTimeStr = time;
            lunchStartTime.setText(lunchStartTimeStr);
        }
        if (TIME_VALUE == 1) {
            lunchEndTimeStr = time;
            lunchEndTime.setText(lunchEndTimeStr);
        }
        if (TIME_VALUE == 2) {
            brunchStartTimeStr = time;
            brunchStartTime.setText(brunchStartTimeStr);
        }
        if (TIME_VALUE == 3) {
            brunchEndTimeStr = time;
            brunchEndTime.setText(brunchEndTimeStr);
        }
        if (TIME_VALUE == 4) {
            dinnerStartTimeStr = time;
            dinnerStartTime.setText(dinnerStartTimeStr);
        }
        if (TIME_VALUE == 5) {
            dinnerEndTimeStr = time;
            dinnerEndTime.setText(dinnerEndTimeStr);
        }
    }

    public void getCalDateAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.CALENDAR_DETAILS_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Class", "" + s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            GetDateModel paymentModel = new Gson().fromJson(s, GetDateModel.class);
                            if (status == 1) {

                                Log.e("D", "" + s);
                                for (int i = 0; i < paymentModel.getGetchefCalendar().size(); i++) {
                                    getchefCalendarBeans.add(paymentModel.getGetchefCalendar().get(i));
                                    GetchefCalendarBean.add(paymentModel.getGetchefCalendar().get(i).getDate());
                                    dateAvail.add(paymentModel.getGetchefCalendar().get(i).getAva_status());
                                    calendarView.refreshCalendar(currentCalendar);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getApplicationContext() != null)

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("Error", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", Chef_id);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void setTimeAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.SAVE_TIME_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Class", "" + s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                           /* GetDateModel paymentModel = new Gson().fromJson(s, GetDateModel.class);*/
                            if (status == 1) {

                                Log.e("D", "" + s);
                                /*for (int i=0;i<paymentModel.getGetchefCalendar().size();i++) {
                                    GetchefCalendarBean.add(paymentModel.getGetchefCalendar().get(i).getDate());
                                    dateAvail.add(paymentModel.getGetchefCalendar().get(i).getAva_status());
                                }*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getApplicationContext() != null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("Error", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", Chef_id);
                map.put("date", selectedDate);
                map.put("lunch_from", lunchStartTimeStr);
                map.put("lunch_to", lunchEndTimeStr);
                map.put("brunch_from", brunchStartTimeStr);
                map.put("brunch_to", brunchEndTimeStr);
                map.put("dinner_from", dinnerStartTimeStr);
                map.put("dinner_to", dinnerEndTimeStr);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    public void getTimeAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.GET_TIME_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Class", "" + s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {

                                Log.e("D", "" + s);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext() != null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getApplicationContext() != null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e("Error", "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("chef_id", Chef_id);
                map.put("date", selectedDate);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    private class ColorDecorator implements DayDecorator {
        @SuppressLint("ResourceType")
        @Override
        public void decorate(DayView dayView) {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            String date = df.format(dayView.getDate());


            if (CalendarUtils.isPastDay(dayView.getDate())) {
                int color = Color.parseColor("#a9afb9");
                dayView.setTextColor(color);
            }
            /*for (int i = 0; i < getchefCalendarBeans.size(); i++) {
                if (date.equalsIgnoreCase(getchefCalendarBeans.get(i).getDate())) {
                    if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("1")) {
                        int color = Color.parseColor("#5ec639");
                        dayView.setBackgroundResource(R.drawable.green_circle);
                        dayView.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        int color = Color.parseColor("#ff0000");
                        dayView.setBackgroundResource(R.drawable.red_circle);
                        dayView.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            }*/
            int color = 0;
            for (int i = 0; i < getchefCalendarBeans.size(); i++) {
                if (date.equalsIgnoreCase(getchefCalendarBeans.get(i).getDate())) {

                    if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("2")) {
                        try {
                            Date d = df.parse(getchefCalendarBeans.get(i).getDate());
                            if (CalendarUtils.isPastDay(d)) {
                                color = getResources().getColor(R.color.filter_color);
                                dayView.setTextColor(color);
                                dayView.setBackgroundResource(R.drawable.ic_substract);
                            } else {
                                dayView.setTextColor(getResources().getColor(R.color.green));
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        // dayView.setBackgroundResource(R.drawable.green_circle);

                    } else if (getchefCalendarBeans.get(i).getAva_status().equalsIgnoreCase("0")) {
                        dayView.setBackgroundResource(R.drawable.red_circle);
                        dayView.setTextColor(getResources().getColor(R.color.filter_color));
                        dayView.setBackgroundResource(R.drawable.ic_substract);
                    }
                }
            }
            /*for (String d:GetchefCalendarBean)
            {
                if (!TextUtils.isEmpty(d))
                {
                    if (date.equalsIgnoreCase(d))
                    {

                        for (String ava:dateAvail)
                        {
                            if (!TextUtils.isEmpty(ava))
                            {
                                if (ava.equalsIgnoreCase("1"))
                                {
                                    Log.e("D1",""+d);
                                    Log.e("AVA","1");
                                    int color = Color.parseColor("#5ec639");
                                    dayView.setTextColor(color);
                                }
                                else *//*if (ava.equalsIgnoreCase("0"))*//*
                                {
                                    Log.e("D0",""+d);
                                    Log.e("AVA","0");
                                    int color = Color.parseColor("#a9afb9");
                                    dayView.setTextColor(color);
                                }
                            }
                        }

                    }
                }

            }*/
            /*if (dayView.getDate().getDate()==1 || dayView.getDate().getDate()==2 || dayView.getDate().getDate()==9)
            {

                int color = Color.parseColor("#9fffc2");
                dayView.setBackgroundColor(color);
            }*/
        }
    }
}
