package com.vervelogic.chefdiner.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;

import java.util.ArrayList;
import java.util.List;

public class BecomeAChefHostGuest extends AppCompatActivity {

    /*---------------------Declarations----------------------------------------------------*/
    Button next7;
    Spinner no_of_guest_min, no_of_guest_max;
    String langaugeList, language, firstName, lastName, phoneNumber, oftenCook, country, city, country_code, latitude, longitude, selectedCurrency;
    String noOfGuestMin, noOfGuestMax;

    DbHelper dbHelper;
    TextView minimum_text, maximum_text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_achef_host_guest);

        /*----------------------------Toolbar-------------------------------------------------*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      /*  // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/

        /*------------------------------------IDS-----------------------------------------------------*/
        next7 = (Button) findViewById(R.id.next7);
        no_of_guest_max = (Spinner) findViewById(R.id.no_of_guest_max);
        no_of_guest_min = (Spinner) findViewById(R.id.no_of_guest_min);
        minimum_text = (TextView) findViewById(R.id.minimum_text);
        maximum_text = (TextView) findViewById(R.id.maximum_text);

        /*-----------------------Populating Min Guest List-----------------------------------------------*/
        List<Integer> no_of_guest_list = new ArrayList<>();
        List<Integer> pos = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            no_of_guest_list.add(i);
        }
       /* no_of_guest_list.add(1);
        no_of_guest_list.add(2);
        no_of_guest_list.add(3);
        no_of_guest_list.add(4);
        no_of_guest_list.add(5);
        no_of_guest_list.add(6);
        no_of_guest_list.add(7);
        no_of_guest_list.add(8);
        no_of_guest_list.add(9);
        no_of_guest_list.add(10);*/

        for (int i = 0; i <= no_of_guest_list.size(); i++) {
            pos.add(i);
        }
        /*----------------------------MIN GUEST------------------------------------------------------------*/
        ArrayAdapter no_of_guest_min_adapter = new ArrayAdapter(getApplicationContext(), R.layout.spinner_text, no_of_guest_list);
        no_of_guest_min.setAdapter(no_of_guest_min_adapter);

        /*------------------------Populating Max Guest List------------------------------------------------*/
        List<Integer> no_of_guest_list_max = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            no_of_guest_list_max.add(i);
        }
       /* no_of_guest_list_max.add(1);
        no_of_guest_list_max.add(2);
        no_of_guest_list_max.add(3);
        no_of_guest_list_max.add(4);
        no_of_guest_list_max.add(5);
        no_of_guest_list_max.add(6);
        no_of_guest_list_max.add(7);
        no_of_guest_list_max.add(8);
        no_of_guest_list_max.add(9);
        no_of_guest_list_max.add(10);*/
        /*----------------------------------Max Guest------------------------------------------------------*/
        ArrayAdapter no_of_guest_max_adapter = new ArrayAdapter(getApplicationContext(), R.layout.spinner_text, no_of_guest_list_max);
        no_of_guest_max.setAdapter(no_of_guest_max_adapter);

        /*----------------------------Getting data from intent----------------------------------------------*/
        Intent intent = getIntent();
        langaugeList = intent.getStringExtra("LanguageList");
        language = intent.getStringExtra("language");
        firstName = intent.getStringExtra("firstName");
        lastName = intent.getStringExtra("lastName");
        phoneNumber = intent.getStringExtra("phoneNumberChef");
        oftenCook = intent.getStringExtra("Often Cook");
        country = intent.getStringExtra("country");
        city = intent.getStringExtra("city");
        country_code = intent.getStringExtra("country_code");
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        selectedCurrency = intent.getStringExtra("selectedCurrency");

        /*--------------------------------Listener on spinners----------------------------------------------*/
        no_of_guest_min.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                noOfGuestMin = adapterView.getItemAtPosition(i).toString();
                minimum_text.setText(getResources().getString(R.string.from_guest) + " " + noOfGuestMin + " " + getResources().getString(R.string.from_guest_text));

                // Showing selected spinner item
                //Toast.makeText(adapterView.getContext(), "Selected: " + noOfGuestMin, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        no_of_guest_max.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                noOfGuestMax = adapterView.getItemAtPosition(i).toString();
                maximum_text.setText(getResources().getString(R.string.upto_guest) + " " + noOfGuestMax + " " + getResources().getString(R.string.from_guest_text));
                // // Showing selected spinner item
                //Toast.makeText(adapterView.getContext(), "Selected: " + noOfGuestMax, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        /*-----------------------Listener---------------------------------------------------------*/
        next7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(noOfGuestMin)) {
                    Toast.makeText(BecomeAChefHostGuest.this, getResources().getString(R.string.please_select_minimum_guest), Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(noOfGuestMax)) {
                    Toast.makeText(BecomeAChefHostGuest.this, getResources().getString(R.string.please_select_maximum_guest), Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(getApplicationContext(), BecomeAChefTitleKitchen.class);
                    intent.putExtra("Often Cook", oftenCook);
                    intent.putExtra("LanguageList", langaugeList);
                    intent.putExtra("language", language);
                    intent.putExtra("firstName", firstName);
                    intent.putExtra("lastName", lastName);
                    intent.putExtra("phoneNumberChef", phoneNumber);
                    intent.putExtra("country", country);
                    intent.putExtra("city", city);
                    intent.putExtra("country_code", country_code);
                    intent.putExtra("minGuest", noOfGuestMin);
                    intent.putExtra("maxGuest", noOfGuestMax);
                    intent.putExtra("latitude", latitude);
                    intent.putExtra("longitude", longitude);
                    intent.putExtra("selectedCurrency", selectedCurrency);
                    startActivity(intent);
                }
            }
        });

        /*----------------------------------DB Access---------------------------------------------*/
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {

            for (int i = 0; i < no_of_guest_min_adapter.getCount(); i++) {
                if (!TextUtils.isEmpty(cn.getMin_guest())) {
                    if (cn.getMin_guest().trim().equals(no_of_guest_min_adapter.getItem(i).toString())) {
                        no_of_guest_min.setSelection(i);
                        minimum_text.setText(getResources().getString(R.string.from_guest) + " " + cn.getMin_guest() + " " + getResources().getString(R.string.from_guest_text));
                        break;
                    } else {
                        minimum_text.setText(getResources().getString(R.string.from_guest) + " " + "x" + " " + getResources().getString(R.string.from_guest_text));
                    }
                }
            }
            for (int i = 0; i < no_of_guest_max_adapter.getCount(); i++) {
                if (!TextUtils.isEmpty(cn.getMax_guest())) {
                    if (cn.getMax_guest().trim().equals(no_of_guest_max_adapter.getItem(i).toString())) {
                        no_of_guest_max.setSelection(i);
                        maximum_text.setText(getResources().getString(R.string.upto_guest) + " " + cn.getMax_guest() + " " + getResources().getString(R.string.from_guest_text));
                        break;
                    } else {
                        minimum_text.setText(getResources().getString(R.string.upto_guest) + " " + "x" + " " + getResources().getString(R.string.from_guest_text));
                    }
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
