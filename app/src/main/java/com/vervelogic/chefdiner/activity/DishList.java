package com.vervelogic.chefdiner.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.sgiosviews.SGPickerView;
import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.app.CustomMultipartRequest;
import com.vervelogic.chefdiner.model.DishListModel;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class DishList extends AppCompatActivity {

    private static final String IMAGE_DIRECTORY = "/chef_diner";
    private static final String TAG = DishList.class.getSimpleName();
    private static final int CAMERA_ENABLE_PERMISSION = 100;
    /*----------------------------------------Declarations------------------------------------------------*/
    Toolbar toolbar;
    Button add_dish;
    RecyclerView addDishRecyclerView;
    TextView empty_text;
    Activity context = this;
    // dialog box data
    Button saveDish;
    EditText dishTitle;
    ImageView add_image_dish;
    CircleImageView add_dish_image;
    SGPickerView pickerView;
    TextView cancel;
    String dishTitleStr;
    String sendImage = "";
    Bitmap bitmap;
    DishListAdapter dishListAdapter;
    List<DishListModel> menulist = new ArrayList<>();
    String dishCourse;
    String id = "0", menu_id, user_id, dish_name, dish_image, dish_category,menuTitle;
    private int TAKE_PICTURE = 2;
    private int SELECT_PICTURE = 1;
    /*----------------------------------------- Runtime permissions-----------------------------------*/
    private SharedPreferences permissionStatus;
    private boolean sentToSettings = false;

    private SharedPreference sharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*------------------------------------------ RUNTIME PERMISSION----------------------------*/
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);

        if (ActivityCompat.checkSelfPermission(DishList.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(DishList.this, Manifest.permission.CAMERA)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DishList.this);
                builder.setTitle(getResources().getString(R.string.need_permission));
                builder.setMessage(getResources().getString(R.string.camera_need_access));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        ActivityCompat.requestPermissions(DishList.this, new String[]{Manifest.permission.CAMERA}, CAMERA_ENABLE_PERMISSION);

                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        finish();
                    }
                });
                builder.show();

            } else {
                ActivityCompat.requestPermissions(DishList.this, new String[]{Manifest.permission.CAMERA}, CAMERA_ENABLE_PERMISSION);
            }
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.CAMERA, true);
            editor.commit();
        } else {
            proceedAfterPermission();

        }

       /*---------------------------------------IDs----------------------------------------------------*/
        add_dish = (Button) findViewById(R.id.add_dish);
        Intent intent = getIntent();
        menu_id = intent.getStringExtra("menu_id");
        menuTitle = intent.getStringExtra("menuName");

        getSupportActionBar().setTitle(menuTitle);
        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            user_id = sharedPreference.getUserId(context);
        } else {
            user_id = "";
        }

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            getSubMenuURL();
        }


        /*---------------------------------Listeners---------------------------------------------------*/
        add_dish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.add_dish_layout);


                saveDish = dialog.findViewById(R.id.save_dish);
                dishTitle = dialog.findViewById(R.id.dish_title);
                add_image_dish = dialog.findViewById(R.id.add_image_dish);
                add_dish_image = dialog.findViewById(R.id.add_dish_image);
                pickerView = dialog.findViewById(R.id.pickerView);

                ArrayList<String> items = new ArrayList<>();
                items.add(getResources().getString(R.string.appetizer));
                items.add(getResources().getString(R.string.starter));
                items.add(getResources().getString(R.string.soup));
                items.add(getResources().getString(R.string.salad));
                items.add(getResources().getString(R.string.drinkM));
                items.add(getResources().getString(R.string.main_course));
                items.add(getResources().getString(R.string.dessert));
                pickerView.setItems(items);
                pickerView.setPickerListener(new SGPickerView.SGPickerViewListener() {
                    @Override
                    public void itemSelected(String item, int index) {
                        pickerView.getCurrentSelectedItemIndex();

                      if (pickerView.getCurrentSelectedItemIndex()==0)
                      {
                          dishCourse = "Appetizer";
                      } if (pickerView.getCurrentSelectedItemIndex()==1)
                      {
                          dishCourse = "Starter";
                      } if (pickerView.getCurrentSelectedItemIndex()==2)
                      {
                          dishCourse = "Soup";
                      } if (pickerView.getCurrentSelectedItemIndex()==3)
                      {
                          dishCourse = "Salad";
                      } if (pickerView.getCurrentSelectedItemIndex()==4)
                      {
                          dishCourse = "Drink";
                      } if (pickerView.getCurrentSelectedItemIndex()==5)
                      {
                          dishCourse = "Main Course";
                      } if (pickerView.getCurrentSelectedItemIndex()==6)
                      {
                          dishCourse = "Dessert";
                      }
                    }
                });

                add_image_dish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showFileChooser();
                    }
                });

                // if button is clicked, close the custom dialog
                saveDish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dishTitleStr = dishTitle.getText().toString();
                        if (TextUtils.isEmpty(dishTitleStr))
                        {
                           dishTitle.setError(getResources().getString(R.string.please_enter_dishName));
                        }
                        else if(TextUtils.isEmpty(dishCourse))
                        {
                            Toast.makeText(DishList.this, getResources().getString(R.string.please_select_category), Toast.LENGTH_SHORT).show();
                        }
                        else {
                            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                            if (netInfo == null) {
                                new AlertDialog.Builder(DishList.this)
                                        .setTitle(getResources().getString(R.string.app_name))
                                        .setMessage(getResources().getString(R.string.internet_error))
                                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
                            } else {
                                addSubMenuURL(sendImage);
                            }
                        }
                        dialog.dismiss();
                    }
                });
                cancel = dialog.findViewById(R.id.cancel);
                // if button is clicked, close the custom dialog
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    @SuppressLint("LongLogTag")
    private void proceedAfterPermission() {
        Log.e("Got The Camera permission", "Got it");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_ENABLE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermission();

            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(DishList.this, Manifest.permission.CAMERA)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(DishList.this);
                    builder.setTitle(getResources().getString(R.string.need_permission));
                    builder.setMessage(getResources().getString(R.string.camera_need_access));
                    builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            ActivityCompat.requestPermissions(DishList.this, new String[]{Manifest.permission.CAMERA}, CAMERA_ENABLE_PERMISSION);

                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            finish();
                        }
                    });
                    builder.show();

                } else {
                    Toast.makeText(this, getResources().getString(R.string.unable_to_get_permission), Toast.LENGTH_SHORT).show();
                }
            }
        }

    }


    private void showFileChooser() {

        final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.choose_photo),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.add_photo);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (options[which].equals(getString(R.string.take_photo))) {
                    Intent cameraIntent = new Intent(
                            android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, TAKE_PICTURE);


                } else if (options[which].equals(getString(R.string.choose_photo))) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, SELECT_PICTURE);
                } else if (options[which].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();


    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(DishList.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                proceedAfterPermission();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == CAMERA_ENABLE_PERMISSION) {
            if (ActivityCompat.checkSelfPermission(DishList.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermission();

            }
        }

        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            Uri filePath = intent.getData();
            if (null != filePath) {
                // Get the path from the Uri
                sendImage = getPathFromURI(filePath);
                // Set the image in ImageView
                add_dish_image.setImageURI(filePath);
                add_image_dish.setImageResource(android.R.color.transparent);

            }

        } else if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            if (intent.getExtras() == null) {
                return;
            }
            bitmap = (Bitmap) intent.getExtras().get("data");
            add_dish_image.setImageBitmap(bitmap);
            add_image_dish.setImageResource(android.R.color.transparent);
            Uri tempUri = getImageUri(this, bitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            sendImage = finalFile.toString();

        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null,
                null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    /*  private void showPictureDialog(){
          AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
          pictureDialog.setTitle("Select Action");
          String[] pictureDialogItems = {
                  "Select photo from gallery",
                  "Capture photo from camera" };
          pictureDialog.setItems(pictureDialogItems,
                  new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                          switch (which) {
                              case 0:
                                  choosePhotoFromGallary();
                                  break;
                              case 1:
                                  takePhotoFromCamera();
                                  break;
                          }
                      }
                  });
          pictureDialog.show();
      }
      public void choosePhotoFromGallary() {
          Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                  android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

          startActivityForResult(galleryIntent, GALLERY_DISH);
      }

      private void takePhotoFromCamera() {
          Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
          startActivityForResult(intent, CAMERA_DISH);
      }

      @Override
      public void onActivityResult(int requestCode, int resultCode, Intent data) {

          super.onActivityResult(requestCode, resultCode, data);
          if (resultCode == this.RESULT_CANCELED) {
              return;
          }
          if (requestCode==GALLERY_DISH)
          {
              if (data != null) {
                  Uri contentURI = data.getData();
                  try {
                      bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                      String path = saveImage(bitmap);
                      Toast.makeText(DishList.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                      add_dish_image.setImageBitmap(bitmap);

                  } catch (IOException e) {
                      e.printStackTrace();
                      Toast.makeText(DishList.this, "Failed!", Toast.LENGTH_SHORT).show();
                  }
              }
          }
          else if (requestCode==CAMERA_DISH)
          {
              Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
              add_dish_image.setImageBitmap(thumbnail);
              saveImage(thumbnail);
              Toast.makeText(DishList.this, "Image Saved!", Toast.LENGTH_SHORT).show();
          }
      }
      public String getStringImage(Bitmap bmp){
          ByteArrayOutputStream baos = new ByteArrayOutputStream();
          bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
          byte[] imageBytes = baos.toByteArray();
          String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
          return encodedImage;
      }
      public String saveImage(Bitmap myBitmap) {
          ByteArrayOutputStream bytes = new ByteArrayOutputStream();
          myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
          File wallpaperDirectory = new File(
                  Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
          // have the object build the directory structure, if needed.
          if (!wallpaperDirectory.exists()) {
              wallpaperDirectory.mkdirs();
          }

          try {
              File f = new File(wallpaperDirectory, Calendar.getInstance()
                      .getTimeInMillis() + ".jpg");
              f.createNewFile();
              FileOutputStream fo = new FileOutputStream(f);
              fo.write(bytes.toByteArray());
              MediaScannerConnection.scanFile(this,
                      new String[]{f.getPath()},
                      new String[]{"image/jpeg"}, null);
              fo.close();
              Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

              return f.getAbsolutePath();
          } catch (IOException e1) {
              e1.printStackTrace();
          }
          return "";
      }*/
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Only if you need to restore open/close state when
        // the orientation is changed
        if (dishListAdapter != null) {
            dishListAdapter.saveStates(outState);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Only if you need to restore open/close state when
        // the orientation is changed
        if (dishListAdapter != null) {
            dishListAdapter.restoreStates(savedInstanceState);
        }
    }

    public void addSubMenuURL(String sendImage) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        if (!sendImage.isEmpty() && sendImage != null && !sendImage.equals("")) {
            //Auth header
            Map<String, String> mHeaderPart = new HashMap<>();
            mHeaderPart.put("Content-type", "multipart/form-data;");

            //File part
            Map<String, File> mFilePartData = new HashMap<>();
            mFilePartData.put("fileToUpload", new File(sendImage));

            //String part
            Map<String, String> mStringPart = new HashMap<>();
            mStringPart.put("user_id", user_id);
            mStringPart.put("id", id);
            mStringPart.put("menu_id", menu_id);
            mStringPart.put("dish_name", dishTitleStr);
            mStringPart.put("dish_category", dishCourse);
            Log.e("map", "" + mStringPart);


            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, getApplicationContext(), AppConfig.ADD_SUBMENU_URL,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            progressDialog.dismiss();

                            try {
                                //JSONObject jsonObject1 = new JSONObject();
                                String status = jsonObject.getString("status");
                                if (status.equalsIgnoreCase("1")) {
                                    Log.e(TAG, "" + jsonObject);
                                    String message = jsonObject.getString("message");
                                    getSubMenuURL();
                                    Log.e(TAG, "" + message);
                                } else {
                                    Log.e(TAG, "" + jsonObject);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                //  Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                if (getApplicationContext()!=null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }if (getApplicationContext()!=null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());
                }

            }, mFilePartData, mStringPart, mHeaderPart);

            RequestQueue rQueue = Volley.newRequestQueue(this);
            rQueue.add(mCustomRequest);
        } else {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.ADD_SUBMENU_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.e(TAG, "" + response);
                            try {
                                JSONObject jsonObject1 = new JSONObject(response);
                                String status = jsonObject1.getString("status");
                                if (status.equalsIgnoreCase("1")) {
                                    Log.e(TAG, "" + jsonObject1);
                                    String message = jsonObject1.getString("message");
                                    getSubMenuURL();
                                    Log.e(TAG, "" + message);
                                } else {
                                    Log.e(TAG, "" + jsonObject1);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                //  Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                if (getApplicationContext()!=null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            String message = "";
                            if (error instanceof NetworkError) {
                                message = "Please check your internet connection.";
                            } else if (error instanceof ServerError) {
                                message = "The server could not be found. Please try again after some time!!";
                            } else if (error instanceof AuthFailureError) {
                                message = "Please check your internet connection.";
                            } else if (error instanceof ParseError) {
                                message = "Parsing error! Please try again after some time!!";
                            } else if (error instanceof NoConnectionError) {
                                message = "Please check your internet connection.";
                            } else if (error instanceof TimeoutError) {
                                message = "Connection TimeOut! Please check your internet connection.";
                            }if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            Log.e(TAG, "onErrorResponse: " + message);
                            Log.e("Error: ", error.toString());
                        }
                    }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();

                    map.put("id", id);
                    map.put("menu_id", menu_id);
                    map.put("user_id", user_id);
                    map.put("dish_name", dishTitleStr);
                    map.put("dish_category", dishCourse);

                    Log.e("map", "" + map);


                    return map;
                }
            };
            int socketTimeout = 30000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(stringRequest);
        }

    }

    public void editSubMenuURl(final String submenuID, String sendImage) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        if (!sendImage.isEmpty() && sendImage != null && !sendImage.equals("")) {
            //Auth header
            Map<String, String> mHeaderPart = new HashMap<>();
            mHeaderPart.put("Content-type", "multipart/form-data;");

            //File part
            Map<String, File> mFilePartData = new HashMap<>();
            mFilePartData.put("fileToUpload", new File(sendImage));

            //String part
            Map<String, String> mStringPart = new HashMap<>();
            mStringPart.put("user_id", user_id);
            mStringPart.put("id", submenuID);
            mStringPart.put("menu_id", menu_id);
            mStringPart.put("dish_name", dishTitleStr);
            mStringPart.put("dish_category", dishCourse);
            Log.e("map", "" + mStringPart);


            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, getApplicationContext(),
                    AppConfig.ADD_SUBMENU_URL, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    progressDialog.dismiss();
                    Log.e(TAG, "" + jsonObject);
                    try {
                        // JSONObject jsonObject1 = new JSONObject();
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("1")) {
                            String message = jsonObject.getString("message");
                            getSubMenuURL();

                            Log.e(TAG, "" + message);
                        } else {
                            Log.e(TAG, "" + jsonObject);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        //Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        if (getApplicationContext()!=null)
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }if (getApplicationContext()!=null)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());
                }

            }, mFilePartData, mStringPart, mHeaderPart);

            RequestQueue rQueue = Volley.newRequestQueue(this);
            rQueue.add(mCustomRequest);
        } else {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.ADD_SUBMENU_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.e(TAG, "" + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                //   id=jsonObject.getString("id");
                                String message = jsonObject.getString("message");

                                getSubMenuURL();

                                Log.e(TAG, "" + message);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                // Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                if (getApplicationContext()!=null)
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            String message = "";
                            if (error instanceof NetworkError) {
                                message = "Please check your internet connection.";
                            } else if (error instanceof ServerError) {
                                message = "The server could not be found. Please try again after some time!!";
                            } else if (error instanceof AuthFailureError) {
                                message = "Please check your internet connection.";
                            } else if (error instanceof ParseError) {
                                message = "Parsing error! Please try again after some time!!";
                            } else if (error instanceof NoConnectionError) {
                                message = "Please check your internet connection.";
                            } else if (error instanceof TimeoutError) {
                                message = "Connection TimeOut! Please check your internet connection.";
                            }if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            Log.e(TAG, "onErrorResponse: " + message);
                            Log.e("Error: ", error.toString());
                        }
                    }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();

                    map.put("id", submenuID);
                    map.put("menu_id", menu_id);
                    map.put("user_id", user_id);
                    map.put("dish_name", dishTitleStr);
                    map.put("dish_category", dishCourse);

                    Log.e("map", "" + map);


                    return map;
                }
            };
            int socketTimeout = 30000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(stringRequest);
        }

    }

    public void getSubMenuURL() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.GET_SUBMENU_URL + "?menu_id=%1$s", menu_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e(TAG, response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                menulist.clear();
                                JSONArray jsonArray = jsonObject.getJSONArray("getSubMenus");
                                for (int i = 0; i <= jsonArray.length(); i++) {
                                    DishListModel menuListModel = new DishListModel();
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String SubMenuId = object.getString("id");
                                    // user_id=object.getString("user_id");
                                    menu_id = object.getString("menu_id");
                                    dish_name = object.getString("dish_name");
                                    dish_image = object.getString("dish_image");
                                    dish_category = object.getString("dish_category");
                                    menuListModel.setDishName(dish_name);
                                    menuListModel.setDishArea(dish_category);
                                    menuListModel.setUrl(dish_image);
                                    menuListModel.setId(SubMenuId);
                                    menulist.add(menuListModel);
                                    addDishRecyclerView = (RecyclerView) findViewById(R.id.add_dish_list);
                                    empty_text = (TextView) findViewById(R.id.empty_text);

                                    //------------------- Menu Recycler View.......--------------------------------------------------------------
                                    LinearLayoutManager horizontalLayoutManagaer
                                            = new LinearLayoutManager(DishList.this, LinearLayoutManager.VERTICAL, false);
                                    addDishRecyclerView.setLayoutManager(horizontalLayoutManagaer);
                                    addDishRecyclerView.setVisibility(View.VISIBLE);
                                    empty_text.setVisibility(View.GONE);
                                    addDishRecyclerView.setNestedScrollingEnabled(false);
                                    dishListAdapter = new DishListAdapter(getApplicationContext(), menulist);
                                    addDishRecyclerView.setAdapter(dishListAdapter);

                                }
                                dishListAdapter.notifyDataSetChanged();
                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                Log.e(TAG, "" + message);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            //Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }if (getApplicationContext()!=null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void deleteSubMenuAPI(String submenu_id) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.DELETE_SUBMENU_URL + "?submenu_id=%1$s", submenu_id);
        Log.e("Menu", "" + submenu_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                String message = jsonObject.getString("message");
                                Toast.makeText(DishList.this, "" + message, Toast.LENGTH_SHORT).show();
                                getSubMenuURL();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            // Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }if (getApplicationContext()!=null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    class DishListAdapter extends RecyclerView.Adapter<DishListAdapter.MyViewHolder> {
        private final ViewBinderHelper binderHelper = new ViewBinderHelper();
        List<DishListModel> profileMenuModels = new ArrayList<>();
        Context mcontext;

        DishListAdapter(Context mcontext, List<DishListModel> profileMenuModels) {
            this.mcontext = mcontext;
            this.profileMenuModels = profileMenuModels;
        }

        @Override
        public DishListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_list, parent, false);
            return new MyViewHolder(view);
        }

        public void saveStates(Bundle outState) {
            binderHelper.saveStates(outState);
        }

        /**
         * Only if you need to restore open/close state when the orientation is changed.
         * Call this method in {@link android.app.Activity#onRestoreInstanceState(Bundle)}
         */
        public void restoreStates(Bundle inState) {
            binderHelper.restoreStates(inState);
        }

        @Override
        public void onBindViewHolder(DishListAdapter.MyViewHolder holder, int position) {
            DishListModel profileMenuModel = profileMenuModels.get(position);

            if (!profileMenuModel.getUrl().equals(""))
                Picasso.with(mcontext)
                        .load(profileMenuModel.getUrl())
                        .error(R.drawable.briyani)
                        .into(holder.dish_image);
            holder.dishName.setText(profileMenuModel.getDishName());
            holder.dishArea.setText(profileMenuModel.getDishArea());
            binderHelper.bind(holder.swipeLayout, String.valueOf(profileMenuModel));
            // Bind your data here
            holder.bind(profileMenuModel);
            holder.bindEdit(profileMenuModel);
            if (!TextUtils.isEmpty(profileMenuModel.getDishArea()))
            {
                if (profileMenuModel.getDishArea().equalsIgnoreCase("Appetizer"))
                {
                    holder.dishtype_image.setImageResource(R.drawable.ic_appetizer);
                }
                if (profileMenuModel.getDishArea().equalsIgnoreCase("Dessert"))
                {
                    holder.dishtype_image.setImageResource(R.drawable.ic_dessert);
                }
                if (profileMenuModel.getDishArea().equalsIgnoreCase("Drink"))
                {
                    holder.dishtype_image.setImageResource(R.drawable.ic_drink);
                }
                if (profileMenuModel.getDishArea().equalsIgnoreCase("Main Course"))
                {
                    holder.dishtype_image.setImageResource(R.drawable.ic_main_course);
                }
                if (profileMenuModel.getDishArea().equalsIgnoreCase("Salad"))
                {
                    holder.dishtype_image.setImageResource(R.drawable.ic_salad);
                }
                if (profileMenuModel.getDishArea().equalsIgnoreCase("Soup"))
                {
                    holder.dishtype_image.setImageResource(R.drawable.ic_soup);
                }
                if (profileMenuModel.getDishArea().equalsIgnoreCase("Starter"))
                {
                    holder.dishtype_image.setImageResource(R.drawable.ic_starter);
                }

            }
            else
            {
                holder.dishtype_image.setVisibility(View.GONE);
            }

        }

        @Override
        public int getItemCount() {
            return profileMenuModels.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView dishName, dishArea;
            ImageView dish_image,dishtype_image;
            private View deleteLayout, edit_layout;
            private SwipeRevealLayout swipeLayout;

            MyViewHolder(View itemView) {
                super(itemView);
                swipeLayout = itemView.findViewById(R.id.swipe_layout);
                deleteLayout = itemView.findViewById(R.id.delete_layout);
                edit_layout = itemView.findViewById(R.id.edit_layout);
                dish_image = itemView.findViewById(R.id.dish_img);
                dishtype_image = itemView.findViewById(R.id.dishtype_image);
                dishName = itemView.findViewById(R.id.dish_name);
                dishArea = itemView.findViewById(R.id.dish_area);
            }

            public void bind(final DishListModel profileMenuModel) {
                deleteLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        profileMenuModels.remove(getAdapterPosition());
                        notifyItemRemoved(getAdapterPosition());
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            new AlertDialog.Builder(DishList.this)
                                    .setTitle(getResources().getString(R.string.app_name))
                                    .setMessage(getResources().getString(R.string.internet_error))
                                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
                        } else {
                            deleteSubMenuAPI(profileMenuModel.getId());
                        }

                    }
                });

                //   textView.setText(data);
            }

            public void bindEdit(final DishListModel profileMenuModel) {
                edit_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // custom dialog
                        final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                        Window window = dialog.getWindow();
                        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.add_dish_layout);


                        saveDish = dialog.findViewById(R.id.save_dish);
                        dishTitle = dialog.findViewById(R.id.dish_title);
                        add_image_dish = dialog.findViewById(R.id.add_image_dish);
                        add_dish_image = dialog.findViewById(R.id.add_dish_image);
                        pickerView = dialog.findViewById(R.id.pickerView);

                        dishTitle.setText(profileMenuModel.getDishName());
                        dishCourse = profileMenuModel.getDishArea();
                        if (!profileMenuModel.getUrl().equalsIgnoreCase("")) {
                            Picasso.with(mcontext)
                                    .load(profileMenuModel.getUrl())
                                    .error(R.drawable.briyani)
                                    .into(add_dish_image);
                        }

                        ArrayList<String> items = new ArrayList<>();
                        items.add(getResources().getString(R.string.appetizer));
                        items.add(getResources().getString(R.string.starter));
                        items.add(getResources().getString(R.string.soup));
                        items.add(getResources().getString(R.string.salad));
                        items.add(getResources().getString(R.string.drinkM));
                        items.add(getResources().getString(R.string.main_course));
                        items.add(getResources().getString(R.string.dessert));

                        pickerView.setItems(items);
                        pickerView.setPickerListener(new SGPickerView.SGPickerViewListener() {
                            @Override
                            public void itemSelected(String item, int index) {
                                pickerView.getCurrentSelectedItemIndex();
                                dishCourse = pickerView.getCurrentSelectedItem();
                               // Toast.makeText(DishList.this, " Item name " + dishCourse, Toast.LENGTH_SHORT).show();
                            }
                        });

                        add_image_dish.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showFileChooser();
                            }
                        });

                        // if button is clicked, close the custom dialog
                        saveDish.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dishTitleStr = dishTitle.getText().toString();
                                editSubMenuURl(profileMenuModel.getId(), sendImage);
                                dialog.dismiss();
                            }
                        });
                        cancel = dialog.findViewById(R.id.cancel);
                        // if button is clicked, close the custom dialog
                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        dialog.show();
                       // Toast.makeText(mcontext, "Edit clicked", Toast.LENGTH_SHORT).show();
                    }
                });

                //   textView.setText(data);
            }

        }
    }


}
