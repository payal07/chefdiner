package com.vervelogic.chefdiner.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.gson.Gson;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.appdatabase.DatabaseMgrApp;
import com.vervelogic.chefdiner.chatModel.DeviceTokenModel;
import com.vervelogic.chefdiner.chatModel.MessageChatModel;
import com.vervelogic.chefdiner.chatModel.NotificationModel;
import com.vervelogic.chefdiner.chatModel.RecentChatModel;
import com.vervelogic.chefdiner.chatModel.Utils;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.extra.Constants;
import com.vervelogic.chefdiner.firebase.ChatListAdapter;
import com.vervelogic.chefdiner.firebase.FirebaseConstant;
import com.vervelogic.chefdiner.firebase.FirebaseFunctions;
import com.vervelogic.chefdiner.model.ChatDetailsModel;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.util.Locale;

public class MessageOpenActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Chat";
    private static DatabaseReference databaseReferenceSender;
    Toolbar toolbar;
    TextView archive, /*book,*/ titleText;
    ListView list_chat_messages;
    RecentChatModel recentChatModel;
    String groupId;
    String chefName="", chefId;
    ChatDetailsModel chatDetailsModel;
    NotificationModel notificationModel;
    DbHelper dbHelper;
    private EditText edit_msg;
    private ImageView img_send;
    private String reciverId = "43";
    private String senderId = "38";
    private ChatListAdapter mChatListAdapter;
    String language="";
    SharedPreference sharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_open);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitleTextColor(0xFFFFFFFF);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DbHelper(getApplicationContext());

        Bundle b = getIntent().getExtras();
        if (b != null && b.containsKey("Model")) {
            chatDetailsModel = (ChatDetailsModel) b.getSerializable("Model");
            senderId = chatDetailsModel.getUserId();
            reciverId = chatDetailsModel.getChefId();
            chefName = chatDetailsModel.getChefName();
            int add = Integer.parseInt(senderId) + Integer.parseInt(reciverId);
            //   getSupportActionBar().setTitle(chefName);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            if (Integer.parseInt(senderId) > Integer.parseInt(reciverId)) {
                groupId = Utils.md5(senderId + reciverId + add);
            } else {
                groupId = Utils.md5(reciverId + senderId + add);
            }
            if (chatDetailsModel.getDefaultMessage() != null && !chatDetailsModel.getDefaultMessage().isEmpty()) {
                sendMessage(chatDetailsModel.getDefaultMessage());
            }
        } else if (b != null && b.containsKey(Constants.ARG_RECEIVER)) {
            notificationModel = (NotificationModel) b.getSerializable(Constants.ARG_RECEIVER);
            groupId = notificationModel.getGroupId();
            senderId = notificationModel.getReciverId();
            reciverId = notificationModel.getSenderId();
        }

        sharedPreference=new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(getApplicationContext()))){
            language = sharedPreference.getLanguage(getApplicationContext());
            Log.e("Lang",""+language);
            if (!TextUtils.isEmpty(language))
            {
                if (language.equalsIgnoreCase("english"))
                {
                    Locale current = Locale.ENGLISH;
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
                if (language.equalsIgnoreCase("traditional chinese"))
                {
                    Locale current = Locale.TRADITIONAL_CHINESE;
                    Log.e("TAG", "" + current);

                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
                if(language.equalsIgnoreCase("simplified chinese"))
                {
                    Locale current = Locale.SIMPLIFIED_CHINESE;
                    Log.e("TAG", "" + current);
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
            }

        }
        else
        {
            language="english";
            Locale current = Locale.ENGLISH;
            Locale.setDefault(current);
            Configuration config = new Configuration();
            config.locale = current;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        }

      /*  book = (TextView) findViewById(R.id.book);*/
        archive = (TextView) findViewById(R.id.archive);
        titleText = (TextView) findViewById(R.id.titleText);
        titleText.setText(chefName);

  /*      book.setOnClickListener(this);*/
        archive.setOnClickListener(this);

        initView();

        if (chatDetailsModel != null && chatDetailsModel.getIsArchive() != null && chatDetailsModel.getIsArchive().equalsIgnoreCase("1")) {
            archive.setText(getResources().getString(R.string.unArchive));
        } else {
            archive.setText(getResources().getString(R.string.archive));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
     /*   //  finish();
        Intent intent = new Intent(getApplicationContext(), HomePage.class);
        startActivity(intent);*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
           /* case R.id.book:
                Cursor cursor = dbHelper.getChefDetails(Integer.valueOf(reciverId));
                if (cursor.moveToFirst()) {
                    String chef_fname = cursor.getString(1);
                    if (!TextUtils.isEmpty(chef_fname)) {
                        String isChefVerify = cursor.getString(17);
                        if (!TextUtils.isEmpty(isChefVerify)) {
                            if (isChefVerify.equalsIgnoreCase("yes")) {
                                Intent intent = new Intent(getApplicationContext(), ChefProfile.class);
                                intent.putExtra("Chef_id", reciverId);
                                startActivity(intent);
                            } else {
                                Toast.makeText(this, "This user is not chef", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Toast.makeText(this, "This user is not in your zone", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "You can not message this user", Toast.LENGTH_SHORT).show();
                }
                break;*/
            case R.id.archive:
                String timeStap = Utils.currentTimeStap();
                RecentChatModel recentChat = new RecentChatModel(
                        "0",
                        timeStap,
                        "",
                        groupId,
                        "0",
                        "",
                        "",
                        FirebaseConstant.CHAT_TYPE_PRIVTE,
                        timeStap,
                        senderId,
                        reciverId,
                        "",
                        "",
                        "",
                        "0");

                if (archive.getText().equals(getResources().getString(R.string.archive))) {
                    FirebaseFunctions.isArchiveChat(recentChat);
                    archive.setText(getResources().getString(R.string.unArchive));
                } else
                {
                    FirebaseFunctions.isUnArchiveChat(recentChat);
                    archive.setText(getResources().getString(R.string.archive));
                }
                break;
        }
    }

    private void initView() {

        list_chat_messages = (ListView) findViewById(R.id.list_chat_messages);

        edit_msg = (EditText) findViewById(R.id.edit_msg);
        img_send = (ImageView) findViewById(R.id.img_send);

        databaseReferenceSender = AppController.ref.child(FirebaseConstant.TABLE_MESSAGE).child(groupId);
        FirebaseFunctions.GroupId=groupId;

        mChatListAdapter = new ChatListAdapter(databaseReferenceSender, this, R.layout.item_chatiing_list, senderId, groupId);
        list_chat_messages.setAdapter(mChatListAdapter);

        img_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = edit_msg.getText().toString();
                if (!message.isEmpty()) {
                    sendMessage(message);
                    edit_msg.setText("");
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseFunctions.GroupId="";
    }

    private void sendMessage(String input) {
        String timeStap = Utils.currentTimeStap();
        DeviceTokenModel senderDetail = DatabaseMgrApp.getDeviceTokenData(null, senderId);
        DeviceTokenModel reciverDetail = DatabaseMgrApp.getDeviceTokenData(null, reciverId);

        RecentChatModel recentChat = new RecentChatModel(
                "0",
                timeStap,
                "",
                groupId,
                "0",
                input,
                "",
                FirebaseConstant.CHAT_TYPE_PRIVTE,
                timeStap,
                senderId,
                reciverId,
                "",
                "",
                "",
                "0");
        MessageChatModel messageChatModel = new MessageChatModel(
                timeStap,
                groupId,
                "",
                senderId,
                "",
                "0",
                input,
                FirebaseConstant.CHAT_TYPE_PRIVTE,
                timeStap,
                "0",
                reciverId);
        FirebaseFunctions.sendMessageBoth(recentChat, messageChatModel);

        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setGroupId(groupId);
        notificationModel.setMessage(input);
        notificationModel.setNotificationType(FirebaseConstant.TYPE_MESSAGE);
        notificationModel.setReciverId(reciverId);
        notificationModel.setSenderId(senderId);

        Gson gson = new Gson();
        String message = gson.toJson(notificationModel);

        FirebaseFunctions.sendPushNotificationToReceiver(
                FirebaseConstant.TYPE_MESSAGE,
                message,
                reciverDetail.getDevicetoken(),
                //"e2D3u4SikAU:APA91bF_KBuSug8mVJDL6VnPvDRYBwmukQnneDBDf6o4rF7Bq2DKGUotWewY4OwHZeOqDeXAIjFz_8t7GuC7-cTrpVVDajm3Gh4AKxYbTmZ-WeynOnA-8toGfFwEgquaulEi8Xd93Z0S",
                reciverDetail.getDeviceType(),
                senderDetail.getUserName(),
                input
        );
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }


}
