package com.vervelogic.chefdiner.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;

import java.util.List;

public class BecomeAChefPhoneNumber extends AppCompatActivity implements View.OnClickListener {

    /*-----------------------Declarations-------------------------------------*/
    EditText phone_number_chef;
    Button next4;
    String languageList, FirstName, LastName, Langugae;
    DbHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_achef_phone_number);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

     /*   // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/

        /*-------------------------------------Ids--------------------------------*/
        phone_number_chef = (EditText) findViewById(R.id.phone_number_chef);
        next4 = (Button) findViewById(R.id.next4);

        /*------------------------Getting data from intent------------------------------*/
        Intent intent = getIntent();
        languageList = intent.getStringExtra("LanguageList");
        Langugae = intent.getStringExtra("language");
        FirstName = intent.getStringExtra("firstName");
        LastName = intent.getStringExtra("lastName");

        /*------------------------Listeners--------------------------------------------*/
        next4.setOnClickListener(this);

        /*------------------------------------DB Access-----------------------------------*/
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            phone_number_chef.setText(cn.getPhoneNumber());
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next4:
                if (!validate()) {
                    return;
                }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validate() {
        String phNStr = phone_number_chef.getText().toString();
        if (phNStr.isEmpty()) {
            phone_number_chef.setError(getString(R.string.please_enter_contact_number));
        } else if (!isValidMobile(phNStr)) {
            phone_number_chef.setError(getResources().getString(R.string.enter_valid_number));
            phone_number_chef.setFocusable(true);
        } else {
            Intent intent = new Intent(getApplicationContext(), BecomeAChefCook.class);
            intent.putExtra("phoneNumberChef", phNStr);
            intent.putExtra("LanguageList", languageList);
            intent.putExtra("language", Langugae);
            intent.putExtra("firstName", FirstName);
            intent.putExtra("lastName", LastName);
            startActivity(intent);
        }
        return true;
    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

}
