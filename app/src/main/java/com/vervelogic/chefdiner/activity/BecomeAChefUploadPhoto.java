package com.vervelogic.chefdiner.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.app.CustomMultipartRequest;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class BecomeAChefUploadPhoto extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = BecomeAChefUploadPhoto.class.getSimpleName();
    private static final int CAMERA_ENABLE_PERMISSION = 100;
    /*------------------------------------Declarations--------------------------------*/
    Button next11;
    Bitmap bitmap;
    ImageView add_image_chef;
    CircleImageView profile_img_chef;
    String langaugeList, language, firstName, lastName, phoneNumber, oftenCook, country, city, noOfGuestMin, noOfGuestMax, kitchenTitle, kitchenDesc, serviceList, country_code, latitude, longitude, brunchStr = "", lunchStr = "", dinnerStr = "", selectedCurrency;
    DbHelper dbHelper;
    Activity context = this;
    String user_id;
    UserDetail userDetail;
    String sendImage = "";
    String profile_pic;
    private int TAKE_PICTURE = 2;
    private int SELECT_PICTURE = 1;
    private SharedPreference sharedPreference;
    // Runtime permissions
    private SharedPreferences permissionStatus;
    private boolean sentToSettings = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_achef_upload_photo);

        // Toolbar id and setting
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

  /*      // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
*/
        /*---------------------------------------- RUNTIME PERMISSION------------------------*/
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);

        if (ActivityCompat.checkSelfPermission(BecomeAChefUploadPhoto.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(BecomeAChefUploadPhoto.this, Manifest.permission.CAMERA)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(BecomeAChefUploadPhoto.this);
                builder.setTitle(getResources().getString(R.string.need_permission));
                builder.setMessage(getResources().getString(R.string.camera_need_access));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        ActivityCompat.requestPermissions(BecomeAChefUploadPhoto.this, new String[]{Manifest.permission.CAMERA}, CAMERA_ENABLE_PERMISSION);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        finish();
                    }
                });
                builder.show();
            } else {
                ActivityCompat.requestPermissions(BecomeAChefUploadPhoto.this, new String[]{Manifest.permission.CAMERA}, CAMERA_ENABLE_PERMISSION);
            }
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.CAMERA, true);
            editor.commit();
        } else {
            proceedAfterPermission();
        }

        /*--------------------------------Shared preference def------------------------*/
        sharedPreference = new SharedPreference();

        /*------------------------------------ Ids-----------------------------------*/
        next11 = (Button) findViewById(R.id.next11);
        add_image_chef = (ImageView) findViewById(R.id.add_image_chef);
        profile_img_chef = (CircleImageView) findViewById(R.id.profile_image_chef);
        /*------------------------------------Getting data from intent-----------------------*/
        Intent intent = getIntent();
        langaugeList = intent.getStringExtra("LanguageList");
        language = intent.getStringExtra("language");
        firstName = intent.getStringExtra("firstName");
        lastName = intent.getStringExtra("lastName");
        phoneNumber = intent.getStringExtra("phoneNumberChef");
        oftenCook = intent.getStringExtra("Often Cook");
        country = intent.getStringExtra("country");
        city = intent.getStringExtra("city");
        noOfGuestMin = intent.getStringExtra("minGuest");
        noOfGuestMax = intent.getStringExtra("maxGuest");
        kitchenTitle = intent.getStringExtra("kitchenTitle");
        kitchenDesc = intent.getStringExtra("kitchenDesc");
        serviceList = intent.getStringExtra("serviceList");
        country_code = intent.getStringExtra("country_code");
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        String Brunch = intent.getStringExtra("brunch");
        String Lunch = intent.getStringExtra("lunch");
        String Dinner = intent.getStringExtra("dinner");
        selectedCurrency = intent.getStringExtra("selectedCurrency");
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            //language = sharedPreference.getLanguage(context);
            user_id = sharedPreference.getUserId(context);
        } else {
            user_id = "";
        }
        if (!TextUtils.isEmpty(Dinner)) {
            dinnerStr = Dinner;
        } else {
            dinnerStr = "";
        }
        if (!TextUtils.isEmpty(Brunch)) {
            brunchStr = Brunch;
        } else {
            brunchStr = "";
        }
        if (!TextUtils.isEmpty(Lunch)) {
            lunchStr = Lunch;
        } else {
            lunchStr = "";
        }


        /*------------------Listener------------------------------------------------------*/
        next11.setOnClickListener(this);
        add_image_chef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* showPictureDialog();*/
                showFileChooser();
            }
        });

      /*------------------------------DB Access-----------------------------------------------------*/
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {

            if (!cn.getProfilePic().isEmpty() && cn.getProfilePic() != null && !cn.getProfilePic().equals("")) {

                profile_pic = cn.getProfilePic();
                Picasso.with(context)
                        .load(cn.getProfilePic())
                       /* .error(R.drawable.profile)*/
                        .into(profile_img_chef);
                add_image_chef.setImageResource(android.R.color.transparent);
            } else {
             /*   profile_img_chef.setImageResource(R.drawable.profile);
                add_image_chef.setImageResource(android.R.color.transparent);*/
            }
        }
    }

    @SuppressLint("LongLogTag")
    private void proceedAfterPermission() {
        Log.e("Got The Camera permission", "Got it");
    }

    // Code to be used if image is to be sent in byte array
   /* private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                     bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(BecomeAChefUploadPhoto.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    profile_img_chef.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(BecomeAChefUploadPhoto.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            bitmap = (Bitmap) data.getExtras().get("data");
            profile_img_chef.setImageBitmap(bitmap);
            saveImage(bitmap);
            Toast.makeText(BecomeAChefUploadPhoto.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }*/
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_ENABLE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermission();

            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(BecomeAChefUploadPhoto.this, Manifest.permission.CAMERA)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(BecomeAChefUploadPhoto.this);
                    builder.setTitle(getResources().getString(R.string.need_permission));
                    builder.setMessage(getResources().getString(R.string.camera_need_access));
                    builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            ActivityCompat.requestPermissions(BecomeAChefUploadPhoto.this, new String[]{Manifest.permission.CAMERA}, CAMERA_ENABLE_PERMISSION);

                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            finish();
                        }
                    });
                    builder.show();

                } else {
                    Toast.makeText(this, getResources().getString(R.string.unable_to_get_permission), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*----------------------------------------- API to send data of chef--------------------------------------------*/
    public void BecomeAChefApi(final String sendImage) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        if (!sendImage.isEmpty() && sendImage != null && !sendImage.equals("")) {
            /*----------------------------------------- API sends data in multipart form-----------------------------------*/
            //Auth header
            Map<String, String> mHeaderPart = new HashMap<>();
            mHeaderPart.put("Content-type", "multipart/form-data;");

            //File part
            Map<String, File> mFilePartData = new HashMap<>();
            mFilePartData.put("fileToUpload", new File(sendImage));

            //String part
            Map<String, String> mStringPart = new HashMap<>();
            mStringPart.put("user_id", user_id);
            mStringPart.put("language", language);
            mStringPart.put("chef_fname", firstName);
            mStringPart.put("chef_lname", lastName);
            mStringPart.put("language_speak", langaugeList);
            mStringPart.put("phone_number", phoneNumber);
            mStringPart.put("chef_country", country);
            mStringPart.put("chef_city", city);
            mStringPart.put("from_guest", noOfGuestMin);
            mStringPart.put("upto_guest", noOfGuestMax);
            mStringPart.put("kitchen_title", kitchenTitle);
            mStringPart.put("kitchen_descrition", kitchenDesc);
            mStringPart.put("service_type", serviceList);
            mStringPart.put("oftencook", oftenCook);
            mStringPart.put("chef_latitude", latitude);
            mStringPart.put("chef_longitude", longitude);
            mStringPart.put("brunch", brunchStr);
            mStringPart.put("lunch", lunchStr);
            mStringPart.put("dinner", dinnerStr);
            mStringPart.put("currency", selectedCurrency);

            Log.e("Data", "" + mStringPart);

            userDetail = new UserDetail();
            userDetail.setUser_id(user_id);
            userDetail.setFirstName(firstName);
            userDetail.setLastName(lastName);
            userDetail.setPhoneNumber(phoneNumber);
            userDetail.setChefCity(city);
            userDetail.setChefCountry(country);
            userDetail.setLanguageList(langaugeList);
            userDetail.setOftenCook(oftenCook);
            userDetail.setLanguage(language);
            userDetail.setMin_guest(noOfGuestMin);
            userDetail.setMax_guest(noOfGuestMax);
            userDetail.setKitchenTitle(kitchenTitle);
            userDetail.setKitchenDescription(kitchenDesc);
            userDetail.setServiceType(serviceList);
            userDetail.setProfilePic(sendImage);
            userDetail.setCountryCode(country_code);
            userDetail.setLatitude(latitude);
            userDetail.setLongitude(longitude);

            dbHelper.updateUser(userDetail);

            sharedPreference.save(context, country_code);

            // Custom multipart class called
            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, getApplicationContext(), AppConfig.BECOME_A_CHEF_URL,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            progressDialog.dismiss();
                            Log.e(TAG, "" + jsonObject);
                            try {

                        /*JSONObject jsonObject1 = new JSONObject();*/
                                int status = jsonObject.getInt("status");
                                if (status == 1) {
                                    String message = jsonObject.getString("message");
                                    Log.e(TAG, "" + message);
                                    userDetail = new UserDetail();
                                    userDetail.setUser_id(user_id);
                                    userDetail.setFirstName(firstName);
                                    userDetail.setLastName(lastName);
                                    userDetail.setPhoneNumber(phoneNumber);
                                    userDetail.setChefCity(city);
                                    userDetail.setCountry(country);
                                    userDetail.setLanguageList(langaugeList);
                                    userDetail.setOftenCook(oftenCook);
                                    userDetail.setLanguage(language);
                                    userDetail.setMin_guest(noOfGuestMin);
                                    userDetail.setMax_guest(noOfGuestMax);
                                    userDetail.setKitchenTitle(kitchenTitle);
                                    userDetail.setKitchenDescription(kitchenDesc);
                                    userDetail.setServiceType(serviceList);
                                    userDetail.setProfilePic(sendImage);
                                    userDetail.setCountryCode(country_code);
                                    /*userDetail.setReferralCode(referal_code);*/
                                    dbHelper.updateUser(userDetail);

                                    sharedPreference.save(context, country_code);

                                    Intent intent = new Intent(getApplicationContext(), BecomeAChef.class);
                                    startActivity(intent);

                                } else {
                                    Log.e(TAG, "" + jsonObject);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (getApplicationContext() != null)
                                    Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                if (getApplicationContext() != null)
                                    Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    if (getApplicationContext() != null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());
                }

            }, mFilePartData, mStringPart, mHeaderPart);

            RequestQueue rQueue = Volley.newRequestQueue(this);
            rQueue.add(mCustomRequest);
        } else {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.BECOME_A_CHEF_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.e(TAG, "" + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                int status = jsonObject.getInt("status");
                                if (status == 1) {
                                    String message = jsonObject.getString("message");
                                    Log.e(TAG, "" + message);
                                    userDetail = new UserDetail();
                                    userDetail.setUser_id(user_id);
                                    userDetail.setFirstName(firstName);
                                    userDetail.setLastName(lastName);
                                    userDetail.setPhoneNumber(phoneNumber);
                                    userDetail.setChefCity(city);
                                    userDetail.setCountry(country);
                                    userDetail.setLanguageList(langaugeList);
                                    userDetail.setOftenCook(oftenCook);
                                    userDetail.setLanguage(language);
                                    userDetail.setMin_guest(noOfGuestMin);
                                    userDetail.setMax_guest(noOfGuestMax);
                                    userDetail.setKitchenTitle(kitchenTitle);
                                    userDetail.setKitchenDescription(kitchenDesc);
                                    userDetail.setServiceType(serviceList);
                                    userDetail.setProfilePic(sendImage);
                                    userDetail.setCountryCode(country_code);

                                    /*userDetail.setReferralCode(referal_code);*/
                                    dbHelper.updateUser(userDetail);

                                    sharedPreference.save(context, country_code);
                                    Intent intent = new Intent(getApplicationContext(), BecomeAChef.class);
                                    startActivity(intent);
                                } else {
                                    //  Log.e("Response Chef",""+response);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                if (getApplicationContext() != null)
                                    Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                if (getApplicationContext() != null)
                                    Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    if (getApplicationContext() != null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();

                    map.put("user_id", user_id);
                    map.put("language", language);
                    map.put("chef_fname", firstName);
                    map.put("chef_lname", lastName);
                    map.put("language_speak", langaugeList);
                    map.put("phone_number", phoneNumber);
                    map.put("chef_country", country);
                    map.put("chef_city", city);
                    map.put("from_guest", noOfGuestMin);
                    map.put("upto_guest", noOfGuestMax);
                    map.put("kitchen_title", kitchenTitle);
                    map.put("kitchen_descrition", kitchenDesc);
                    map.put("service_type", serviceList);
                    map.put("oftencook", oftenCook);
                    map.put("chef_latitude", latitude);
                    map.put("chef_longitude", longitude);
                    map.put("brunch", brunchStr);
                    map.put("lunch", lunchStr);
                    map.put("dinner", dinnerStr);
                    map.put("currency", selectedCurrency);
                    Log.e("map", "" + map);

                    return map;
                }
            };
            int socketTimeout = 30000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);

            AppController.getInstance().addToRequestQueue(stringRequest);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next11:
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    BecomeAChefApi(sendImage);
                }
        }
    }

    // Image sent in file object form
    private void showFileChooser() {

        final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.choose_photo),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.add_photo);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (options[which].equals(getString(R.string.take_photo))) {
                    Intent cameraIntent = new Intent(
                            android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, TAKE_PICTURE);
                } else if (options[which].equals(getString(R.string.choose_photo))) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, SELECT_PICTURE);
                } else if (options[which].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == CAMERA_ENABLE_PERMISSION) {
            if (ActivityCompat.checkSelfPermission(BecomeAChefUploadPhoto.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermission();

            }
        }
        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            Uri filePath = intent.getData();
            if (null != filePath) {
                // Get the path from the Uri
                sendImage = getPathFromURI(filePath);
                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                profile_img_chef.setImageURI(filePath);
                add_image_chef.setImageResource(android.R.color.transparent);

            }

        } else if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            if (intent.getExtras() == null) {
                return;
            }
            bitmap = (Bitmap) intent.getExtras().get("data");
            profile_img_chef.setImageBitmap(bitmap);
            add_image_chef.setImageResource(android.R.color.transparent);
            Uri tempUri = getImageUri(this, bitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            sendImage = finalFile.toString();

        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null,
                null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        final String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(BecomeAChefUploadPhoto.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                proceedAfterPermission();
            }
        }
    }

    private void uploadImage() {
        class SendHttpRequestTask extends AsyncTask<String, Void, Bitmap> {
            @Override
            protected Bitmap doInBackground(String... params) {
                try {
                    URL url = new URL(profile_pic);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    Log.e("Bitmap", "" + myBitmap);
                    return myBitmap;
                } catch (Exception e) {
                    Log.e("Become a chef Upload", e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap result) {

                profile_img_chef.setImageBitmap(result);
                add_image_chef.setImageResource(android.R.color.transparent);
            }
        }
        SendHttpRequestTask ui = new SendHttpRequestTask();
        ui.execute();
    }

}


