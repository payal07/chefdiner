package com.vervelogic.chefdiner.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.fragments.Upcoming;
import com.vervelogic.chefdiner.utils.SharedPreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditBookingDetails extends AppCompatActivity implements View.OnClickListener,DatePickerDialog.OnDateSetListener{

    Toolbar toolbar;
    TextView edit_date_text,edit_time_text,guest_text,cancel;
    LinearLayout date_edit,time_edit,guest_edit;
    ImageView minus,plus;
    int count;
    String maximum_person="0",minimum_person="0",dateText,guestText,timeText,chefId,bookingId,user_id,oldDate;
    DbHelper dbHelper;
    Spinner selectTime;
    Button save;
    private SharedPreference sharedPreference;
    Activity context = this;
    DatePickerDialog tpd;
    private static final String TAG = EditBookingDetails.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_booking_details);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context)))
        {
            user_id = sharedPreference.getUserId(context);
        }
        else
        {
            user_id="";
        }


        edit_date_text=(TextView)findViewById(R.id.edit_date_text);
        edit_time_text=(TextView)findViewById(R.id.edit_time_text);
        guest_text=(TextView)findViewById(R.id.guest_text);
        date_edit=(LinearLayout) findViewById(R.id.date_edit);
        time_edit=(LinearLayout) findViewById(R.id.time_edit);
        guest_edit=(LinearLayout) findViewById(R.id.guest_edit);
        minus=(ImageView) findViewById(R.id.minus);
        plus=(ImageView) findViewById(R.id.plus);

        Intent intent=getIntent();
        dateText=intent.getStringExtra("dateText");
        timeText=intent.getStringExtra("timeText");
        guestText=intent.getStringExtra("guestText");
        chefId=intent.getStringExtra("chefId");
        bookingId=intent.getStringExtra("bookingcode");
        oldDate=intent.getStringExtra("oldDate");

        dbHelper=new DbHelper(getApplicationContext());

        if (!TextUtils.isEmpty(chefId))
        {
            Cursor cursor = dbHelper.getChefDetails(Integer.valueOf(chefId));
            if (cursor.moveToFirst()) {

                if (!TextUtils.isEmpty(cursor.getString(13)))
                {
                    minimum_person = cursor.getString(13);
                }
                else
                {
                    minimum_person="0";
                }
                if (!TextUtils.isEmpty(cursor.getString(14)))
                {
                    Log.e("Cursor",""+cursor.getString(14));
                    maximum_person = cursor.getString(14);
                }
                else
                {
                    maximum_person="0";
                }
            }
        }
        if (!TextUtils.isEmpty(dateText))
        {
            edit_date_text.setText(dateText);
        }
        if (!TextUtils.isEmpty(timeText))
        {
            edit_time_text.setText(timeText);
        }
        if (!TextUtils.isEmpty(guestText))
        {
            guest_text.setText(guestText);
        }

        minus.setOnClickListener(this);
        plus.setOnClickListener(this);
        date_edit.setOnClickListener(this);
        time_edit.setOnClickListener(this);


    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // custom dialog

/*

       */
/* finish();*//*

        Intent intent = new Intent(getApplicationContext(), HomePage.class);
        startActivity(intent);
*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                final Dialog dialog = new Dialog(EditBookingDetails.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.save_popup);

                TextView agree=dialog.findViewById(R.id.btn_agree);
                TextView cancel=dialog.findViewById(R.id.btn_cancel);


                agree.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        ConnectivityManager conMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            new android.support.v7.app.AlertDialog.Builder(EditBookingDetails.this)
                                    .setTitle(getResources().getString(R.string.app_name))
                                    .setMessage(getResources().getString(R.string.internet_error))
                                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
                        } else {
                            changeDetailsAPI();
                            onBackPressed();
                        }
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        onBackPressed();

                    }
                });

                dialog.show();


                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.plus:
                count = Integer.valueOf(guest_text.getText().toString());
                if ((Integer.valueOf(guest_text.getText().toString())) > Integer.valueOf(maximum_person)) {
                   // Toast.makeText(this, "Not Allowed", Toast.LENGTH_LONG).show();
                } else {
                    Log.e("CountPlus", "" + count);
                    count = count + 1;
                    guest_text.setText("" + count);
                }
                break;
            case R.id.minus:
                count = Integer.valueOf(guest_text.getText().toString());
                if (count < Integer.valueOf(minimum_person)) {
                  // Toast.makeText(this, "Not Allowed", Toast.LENGTH_LONG).show();
                } else {
                    count = count - 1;
                    guest_text.setText("" + count);
                }
                break;
            case R.id.time_edit:
                // ---------------------------custom dialog---------------------------------
                final Dialog dialog = new Dialog(this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.select_time);

                selectTime = dialog.findViewById(R.id.select_time);
                save = dialog.findViewById(R.id.save_time);

                // --------------------------------LISTENER ON SPINNER----------------------------
                selectTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        timeText = selectTime.getSelectedItem().toString();

                        //  String second = tokens.nextToken();// this will contain " they taste good"

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                cancel = dialog.findViewById(R.id.cancel);

                //---------------------- if button is clicked, close the custom dialog-----------------
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        edit_time_text.setText(timeText);
                        dialog.dismiss();
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                List<String> strings = new ArrayList<>();
                strings.add("1:00");
                strings.add("1:30");
                strings.add("2:00");
                strings.add("2:30");
                strings.add("3:00");
                strings.add("3:30");
                strings.add("4:00");
                strings.add("4:30");
                strings.add("5:00");
                strings.add("5:30");
                strings.add("6:00");
                strings.add("6:30");
                strings.add("7:00");
                strings.add("7:30");
                strings.add("8:00");
                strings.add("8:30");
                strings.add("9:00");
                strings.add("9:30");
                strings.add("10:00");
                strings.add("10:30");
                strings.add("11:00");
                strings.add("11:30");
                strings.add("12:00");
                strings.add("12:30");

                ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.currency_spinner_text, strings);

                selectTime.setAdapter(stringArrayAdapter);
                dialog.show();
                break;
            case R.id.date_edit:

                Calendar now = Calendar.getInstance();
                tpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                        this, now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                tpd.setMinDate(now);
                tpd.setVersion(com.wdullaer.materialdatetimepicker.date.DatePickerDialog.Version.VERSION_2);
                tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Log.e("TimePicker", "Dialog was cancelled");
                    }
                });
                tpd.show(getFragmentManager(), "Timepickerdialog");
                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        @SuppressLint("SimpleDateFormat") final Format formatter = new SimpleDateFormat("EEE, MMM .dd");
        Calendar newDate = Calendar.getInstance();
        newDate.set(year, monthOfYear, dayOfMonth);
        int month=monthOfYear+1;
        oldDate=year+"-"+month+"-"+dayOfMonth;
        String today = formatter.format(newDate.getTime());
        edit_date_text.setText(today);
    }

    public void changeDetailsAPI()
    {
       /* final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.CHANGE_BOOOKING_DETAILS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                /*        progressDialog.dismiss();*/
                        Log.e(TAG, response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            String message = jsonObject.getString("message");
                            if (status == 1) {
                                Toast.makeText(EditBookingDetails.this, "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            /*progressDialog.dismiss();*/
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();

                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                          /*  progressDialog.dismiss();*/
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      /*  progressDialog.dismiss();*/
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }if (getApplicationContext()!=null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("booking_id",bookingId);
                map.put("user_id",user_id);
                map.put("total_seats",guest_text.getText().toString());
                map.put("date",oldDate);
                map.put("time", edit_time_text.getText().toString());

                Log.e("MapData", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
