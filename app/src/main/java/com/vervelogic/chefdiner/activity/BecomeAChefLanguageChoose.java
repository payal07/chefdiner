package com.vervelogic.chefdiner.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BecomeAChefLanguageChoose extends AppCompatActivity implements View.OnClickListener {

    /*----------------------------------------Declarations-----------------------------------------------------------*/
    CheckBox english_checkbox, cantonese_checkbox, mandarin_checkbox, malay_checkbox, vietnamese_checkbox, thai_checkbox, filipino_checkbox;
    Button nextLanguage;
    String firstName, lastName, language;
    String languageList = "";
    DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_achef_language_choose);
        /*--------------------------------Toolbar--------------------------------------------*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      /*  // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/

        /*------------------------------IDS-------------------------------------------------------*/
        english_checkbox = (CheckBox) findViewById(R.id.english_checkbox);
        cantonese_checkbox = (CheckBox) findViewById(R.id.cantonese_checkbox);
        mandarin_checkbox = (CheckBox) findViewById(R.id.mandarin_checkbox);
        malay_checkbox = (CheckBox) findViewById(R.id.malay_checkbox);
        vietnamese_checkbox = (CheckBox) findViewById(R.id.vietnamese_checkbox);
        thai_checkbox = (CheckBox) findViewById(R.id.thai_checkbox);
        filipino_checkbox = (CheckBox) findViewById(R.id.filipino_checkbox);
        nextLanguage = (Button) findViewById(R.id.next3);

        /*-----------------------------------Getting data from intent---------------------------------*/
        Intent intent = getIntent();
        firstName = intent.getStringExtra("firstName");
        lastName = intent.getStringExtra("lastName");
        language = intent.getStringExtra("language");

        /*------------------------------Listener-----------------------------------------------------*/
        nextLanguage.setOnClickListener(this);

        /*------------------------------DB Access-----------------------------------------------------*/
        dbHelper = new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            languageList = cn.getLanguageList();
            Log.e("List", "" + cn.getLanguageList());

        }
        if (!TextUtils.isEmpty(languageList)) {
            List<String> items = Arrays.asList(languageList.split("\\s*,\\s*"));
            Log.e("ListItem", "" + items);
            for (int i = 0; i < items.size(); i++) {
                String stringList = items.get(i);
                if (stringList.equalsIgnoreCase("english")) {
                    english_checkbox.setChecked(true);
                }
                if (stringList.equalsIgnoreCase("Cantonese")) {
                    cantonese_checkbox.setChecked(true);
                }
                if (stringList.equalsIgnoreCase("Mandarin")) {
                    mandarin_checkbox.setChecked(true);
                }
                if (stringList.equalsIgnoreCase("Malay")) {
                    malay_checkbox.setChecked(true);
                }
                if (stringList.equalsIgnoreCase("Vietnamese")) {
                    vietnamese_checkbox.setChecked(true);
                }
                if (stringList.equalsIgnoreCase("Thai")) {
                    thai_checkbox.setChecked(true);
                }
                if (stringList.equalsIgnoreCase("Filipino")) {
                    filipino_checkbox.setChecked(true);
                }
            }
        }

    }

    @Override
    public void onClick(View view) {
        if (!validate()) {
            return;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validate() {
        String english_checkboxStr = english_checkbox.getText().toString();
        String cantonese_checkboxStr = cantonese_checkbox.getText().toString();
        String mandarin_checkboxStr = mandarin_checkbox.getText().toString();
        String malay_checkboxStr = malay_checkbox.getText().toString();
        String vietnamese_checkboxStr = vietnamese_checkbox.getText().toString();
        String thai_checkboxStr = thai_checkbox.getText().toString();
        String filipino_checkboxStr = filipino_checkbox.getText().toString();

        List<String> languageList = new ArrayList<>();
        if (english_checkbox.isChecked()) {
            languageList.add("english");
        }
        if (cantonese_checkbox.isChecked()) {
            languageList.add("Cantonese");
        }
        if (mandarin_checkbox.isChecked()) {
            languageList.add("Mandarin");
        }
        if (malay_checkbox.isChecked()) {
            languageList.add("Malay");
        }
        if (vietnamese_checkbox.isChecked()) {
            languageList.add("Vietnamese");
        }
        if (thai_checkbox.isChecked()) {
            languageList.add("Thai");
        }
        if (filipino_checkbox.isChecked()) {
            languageList.add("Filipino");
        }
        if (languageList.isEmpty()) {
            Toast.makeText(this, getString(R.string.please_select_a_lang), Toast.LENGTH_SHORT).show();
        } else {
            String list = TextUtils.join(",", languageList);
            Intent intent = new Intent(getApplicationContext(), BecomeAChefPhoneNumber.class);
            intent.putExtra("LanguageList", list);
            intent.putExtra("firstName", firstName);
            intent.putExtra("lastName", lastName);
            intent.putExtra("language", language);
            startActivity(intent);
        }

        return true;
    }
}
