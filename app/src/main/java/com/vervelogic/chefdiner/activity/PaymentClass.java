package com.vervelogic.chefdiner.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.bottomFragment.Message;
import com.vervelogic.chefdiner.chatModel.Utils;
import com.vervelogic.chefdiner.model.ChatDetailsModel;
import com.vervelogic.chefdiner.model.PaymentModel;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


public class PaymentClass extends AppCompatActivity {

    private static final String TAG = "PaymentClass";
    TextView data;
    Button exit;
    WebView webView;
    String user_id, bookingNowId, androidS = "android", UID, AndR, BookId;
    SharedPreference sharedPreference;
    Activity context = this;
    Toolbar toolbar;
    ChatDetailsModel chatDetailsModel;

    @SuppressLint({"SetJavaScriptEnabled", "JavascriptInterface"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_class);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.payment);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            user_id = sharedPreference.getUserId(context);
        } else {
            user_id = "";
        }

        Intent intent = getIntent();
        int b = intent.getIntExtra("booknow_id", 0);
        bookingNowId = String.valueOf(b);
        Log.e("Book", "" + bookingNowId);

        byte[] userId = new byte[0];
        byte[] Android = new byte[0];
        byte[] BookingId = new byte[0];
        try {
            userId = user_id.getBytes("UTF-8");
            Android = androidS.getBytes("UTF-8");
            BookingId = bookingNowId.getBytes("UTF-8");
            UID = Base64.encodeToString(userId, Base64.DEFAULT);
            AndR = Base64.encodeToString(Android, Base64.DEFAULT);
            BookId = Base64.encodeToString(BookingId, Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.e("UID", "" + UID);
        Log.e("AndR", "" + AndR);
        Log.e("BookId", "" + BookId);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("Model")) {
            chatDetailsModel = (ChatDetailsModel) bundle.getSerializable("Model");

        }

        data = (TextView) findViewById(R.id.data_received);
        exit = (Button) findViewById(R.id.exit);
        webView = (WebView) findViewById(R.id.webView);

        // String url = "http://192.168.1.63/chefdiner/index.php/admin/paypal/"+UID+"/"+BookId+"/"+AndR;
        String url = "http://vervedemos.in/chefdiner/index.php/admin/paypal/" + UID + "/" + BookId + "/" + AndR;

        Log.e("Url", "" + url);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        //  webView.addJavascriptInterface(new javascriptInterface(), "javascriptInterface");
        webView.setWebChromeClient(new WebChromeClient());
        webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        webView.setWebViewClient(new WebViewClient() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Log.e("Url", "shouldOverrideUrlLoading: " + url);
                final Intent intent;
                if (url.contains("http://vervedemos.in/chefdiner/index.php/admin/successPaypal")) {
                 /*   if (url.contains("http://192.168.1.63/chefdiner/index.php/admin/successPaypal")) {*/
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(PaymentClass.this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {

                        getPaymentDetails();
                        //  Toast.makeText(PaymentClass.this, "Url", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                else if(url.contains("http://vervedemos.in/chefdiner/admin/successPaypal"))
                {
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(PaymentClass.this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    }
                    else
                    {

                        getPaymentDetails();
                        //  Toast.makeText(PaymentClass.this, "Url", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                else
                    {
                    //Toast.makeText(PaymentClass.this, "WebView", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }

        });
        webView.loadUrl(url);
    }


    public void getPaymentDetails() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.GET_PAYMENT_DETAILS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e(TAG, "" + s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            PaymentModel paymentModel = new Gson().fromJson(s, PaymentModel.class);
                            if (status == 1) {
                                Log.e(TAG, "" + s);
                                String s1 = paymentModel.getGetDetails().get(0).getPayment_status();
                                final String chef_id = paymentModel.getGetDetails().get(0).getChef_id();
                                final String bookingId = paymentModel.getGetDetails().get(0).getBooknow_id();
                                final String mainUserId = paymentModel.getGetDetails().get(0).getMain_user_id();
                                /*String s1 = paymentModel.getGetDetails().get(0).getPayment_status();*/
                                webView.setVisibility(View.GONE);
                                // ---------------------------custom dialog---------------------------------
                                final Dialog dialog = new Dialog(PaymentClass.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                                Window window = dialog.getWindow();
                                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog.setContentView(R.layout.payment_ok_screen);

                                data = dialog.findViewById(R.id.data_received);
                                exit = dialog.findViewById(R.id.exit);

                                data.setText(s1);

                                exit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                        Intent cal = new Intent(getApplicationContext(), MessageOpenActivity.class);
                                        cal.putExtra("Model",chatDetailsModel);
                                        startActivity(cal);
                                    }
                                });
                                dialog.show();
                            }
                            if (status == 0) {
                                String s1 = jsonObject.getString("message");
                                webView.setVisibility(View.GONE);
                                // ---------------------------custom dialog---------------------------------
                                final Dialog dialog = new Dialog(PaymentClass.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                                Window window = dialog.getWindow();
                                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog.setContentView(R.layout.payment_ok_screen);

                                data = dialog.findViewById(R.id.data_received);
                                exit = dialog.findViewById(R.id.exit);

                                data.setText(s1);

                                exit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                        Intent intent1 = new Intent(getApplicationContext(), HomePage.class);
                                        startActivity(intent1);

                                    }
                                });
                                dialog.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }if (getApplicationContext()!=null)
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                Log.e(TAG, "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("user_id", user_id);
                map.put("booknow_id", bookingNowId);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
