package com.vervelogic.chefdiner.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.adapter.HowItWorksAdapter;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.HowItWorksModel;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HowItWorks extends AppCompatActivity {

    private static final String TAG = HowItWorks.class.getSimpleName();
    RecyclerView how_it_works_recyclerview;
    HowItWorksAdapter howItWorksAdapter;
    List<HowItWorksModel> howItWorksList = new ArrayList<>();
    Toolbar toolbar;
    String language;
    DbHelper dbHelper;
    Activity context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_it_works);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle(R.string.how_it_works);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        //upArrow.setColorFilter(getResources().getColor(R.color.whit), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*dbHelper=new DbHelper(getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            language=cn.getLanguage();
        }*/

        SharedPreference sharedPreference = new SharedPreference();
        Intent intent = getIntent();
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(context))) {
            language = sharedPreference.getLanguage(context);
        } else {
            language = "english";
        }

        Log.e("Lang", "" + language);
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new android.support.v7.app.AlertDialog.Builder(HowItWorks.this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton("OK", null).show();
        } else {
            howItWorkAPI();
        }


        how_it_works_recyclerview = (RecyclerView) findViewById(R.id.how_it_works_recyclerview);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*-------------------------------------------How It Work API-------------------------------------*/
    public void howItWorkAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = "";
        if (!TextUtils.isEmpty(language)) {
            if (language.equalsIgnoreCase("english")) {
                uri = String.format(AppConfig.HOW_IT_WORK_URL + "?language=%1$s", "english");
            }
            if (language.equalsIgnoreCase("simplified chinese")) {
                uri = String.format(AppConfig.HOW_IT_WORK_URL + "?language=%1$s", "simplified");
            }
            if (language.equalsIgnoreCase("traditional chinese")) {
                uri = String.format(AppConfig.HOW_IT_WORK_URL + "?language=%1$s", "traditional");
            }
        }

        Log.e("Response", "" + uri);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.e("Response", "" + s);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                Log.e("Response", "" + s);
                                JSONArray jsonArray = jsonObject.getJSONArray("howitworks");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    HowItWorksModel howItWorksModel = new HowItWorksModel();
                                    String title = jsonObject2.getString("title");
                                    String titleDesc = jsonObject2.getString("description");
                                    String url = jsonObject2.getString("image");

                                    /*-----------------Replace " from string url-----------------------------*/
                                    //  url = url.replaceAll("^\"|\"$", "");

                                    howItWorksModel.setTitle(title);
                                    howItWorksModel.setTitleDesc(titleDesc);
                                    howItWorksModel.setUrl(url);
                                    howItWorksList.add(howItWorksModel);
                                }

                                howItWorksAdapter = new HowItWorksAdapter(getApplicationContext(), howItWorksList);
                                LinearLayoutManager horizontalLayoutManager
                                        = new LinearLayoutManager(HowItWorks.this, LinearLayoutManager.VERTICAL, false);
                                how_it_works_recyclerview.setLayoutManager(horizontalLayoutManager);

                                how_it_works_recyclerview.setItemAnimator(new DefaultItemAnimator());

                                how_it_works_recyclerview.setAdapter(howItWorksAdapter);

                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                Toast.makeText(HowItWorks.this, "" + message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if (getApplicationContext()!=null)
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }if (getApplicationContext()!=null)
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);

    }
}
