package com.vervelogic.chefdiner.firebase;

/**
 * Created by ubuntu on 4/10/16.
 */
public class FirebaseConstant {

    public static final String RECENT_CHAT_DATA = "recentChatData";
    public static String TABLE_MESSAGE = "message";
    public static String TABLE_RECENT = "Recent";
    public static String TABLE_USER = "User";
    public static String APPFOLDER = "concierge";
    public static String CHAT_TYPE_PRIVTE = "private";
    public static String CHAT_TYPE_GROUP = "group";
    public static String TYPE_MESSAGE = "message";
    public static String TYPE_ALERT = "alert";
    public static String CHAT_TYPE = "chatType";
}
