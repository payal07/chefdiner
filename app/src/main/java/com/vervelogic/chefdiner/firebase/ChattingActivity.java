package com.vervelogic.chefdiner.firebase;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.database.DatabaseReference;
import com.google.gson.Gson;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.appdatabase.DatabaseMgrApp;
import com.vervelogic.chefdiner.chatModel.DeviceTokenModel;
import com.vervelogic.chefdiner.chatModel.MessageChatModel;
import com.vervelogic.chefdiner.chatModel.NotificationModel;
import com.vervelogic.chefdiner.chatModel.RecentChatModel;
import com.vervelogic.chefdiner.chatModel.Utils;
import com.vervelogic.chefdiner.extra.Constants;
import com.vervelogic.chefdiner.model.ChatDetailsModel;

public class ChattingActivity extends AppCompatActivity {

    private EditText edit_msg;
    private ImageView img_send;

    private String reciverId = "43";
    private String senderId = "38";

    private static final String TAG = "Chat";
    private static DatabaseReference databaseReferenceSender;
    ListView list_chat_messages;
    private ChatListAdapter mChatListAdapter;
    RecentChatModel recentChatModel;
    String groupId;

    ChatDetailsModel chatDetailsModel;
    NotificationModel notificationModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);

        Bundle b = getIntent().getExtras();
        if (b != null && b.containsKey("Model")) {
            chatDetailsModel = (ChatDetailsModel) b.getSerializable("Model");
            senderId = chatDetailsModel.getUserId();
            reciverId = chatDetailsModel.getChefId();

            if (Integer.parseInt(senderId) > Integer.parseInt(reciverId)) {
                groupId = Utils.md5(senderId + reciverId);
            } else {
                groupId = Utils.md5(reciverId + senderId);
            }
            if (chatDetailsModel.getDefaultMessage()!=null && !chatDetailsModel.getDefaultMessage().isEmpty()) {
                sendMessage(chatDetailsModel.getDefaultMessage());
            }
        }
        else if (b!=null && b.containsKey(Constants.ARG_RECEIVER))
        {
            notificationModel = (NotificationModel) b.getSerializable(Constants.ARG_RECEIVER);
            groupId=notificationModel.getGroupId();
            senderId=notificationModel.getReciverId();
            reciverId=notificationModel.getSenderId();
        }

        initView();
    }

    private void initView() {

        list_chat_messages = (ListView) findViewById(R.id.list_chat_messages);

        edit_msg = (EditText) findViewById(R.id.edit_msg);
        img_send = (ImageView) findViewById(R.id.img_send);

        databaseReferenceSender = AppController.ref.child(FirebaseConstant.TABLE_MESSAGE).child(groupId);

        mChatListAdapter = new ChatListAdapter(databaseReferenceSender, this, R.layout.item_chatiing_list, senderId,groupId);
        list_chat_messages.setAdapter(mChatListAdapter);

        img_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = edit_msg.getText().toString();
                if (!message.isEmpty()) {
                    sendMessage(message);
                    edit_msg.setText("");
                }
            }
        });
    }

    private void sendMessage(String input) {
        String timeStap = Utils.currentTimeStap();
        DeviceTokenModel senderDetail = DatabaseMgrApp.getDeviceTokenData(null, senderId);
        DeviceTokenModel reciverDetail = DatabaseMgrApp.getDeviceTokenData(null, reciverId);

        RecentChatModel recentChat = new RecentChatModel(
                "0",
                timeStap,
                "",
                groupId,
                "0",
                input,
                "",
                FirebaseConstant.CHAT_TYPE_PRIVTE,
                timeStap,
                senderId,
                reciverId,
                "",
                "",
                "",
                "0");
        MessageChatModel messageChatModel = new MessageChatModel(
                timeStap,
                groupId,
                "",
                senderId,
                "",
                "0",
                input,
                FirebaseConstant.CHAT_TYPE_PRIVTE,
                timeStap,
                "0",
                reciverId);
        FirebaseFunctions.sendMessageBoth(recentChat, messageChatModel);

        NotificationModel notificationModel=new NotificationModel();
        notificationModel.setGroupId(groupId);
        notificationModel.setMessage(input);
        notificationModel.setNotificationType(FirebaseConstant.TYPE_MESSAGE);
        notificationModel.setReciverId(reciverId);
        notificationModel.setSenderId(senderId);

        Gson gson = new Gson();
        String message = gson.toJson(notificationModel);

        FirebaseFunctions.sendPushNotificationToReceiver(
                FirebaseConstant.TYPE_MESSAGE,
                message,
                reciverDetail.getDevicetoken(),
               //"e2D3u4SikAU:APA91bF_KBuSug8mVJDL6VnPvDRYBwmukQnneDBDf6o4rF7Bq2DKGUotWewY4OwHZeOqDeXAIjFz_8t7GuC7-cTrpVVDajm3Gh4AKxYbTmZ-WeynOnA-8toGfFwEgquaulEi8Xd93Z0S",
                reciverDetail.getDeviceType(),
                senderDetail.getUserName(),
                input
        );
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
