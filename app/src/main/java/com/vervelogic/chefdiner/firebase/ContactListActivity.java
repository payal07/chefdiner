package com.vervelogic.chefdiner.firebase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.appdatabase.DatabaseMgrApp;
import com.vervelogic.chefdiner.chatModel.DeviceTokenModel;
import com.vervelogic.chefdiner.chatModel.RecentChatModel;
import com.vervelogic.chefdiner.model.ChatDetailsModel;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

public class ContactListActivity extends AppCompatActivity implements View.OnClickListener, ChildEventListener {
    private ChatingContactAdapter chatingContactAdapter;
    private RecentActiveAdapter recentActiveAdapter;
    private RecyclerView recyclerView_ChattingContactList;
    private RecyclerView recyclerView_ChattingRecentList;

    private static final String TAG = "Chat";
    ArrayList<String> chatlist;
    ArrayList<RecentChatModel> listRecentChatModel;
    ArrayList<RecentChatModel> recentActivityList;
    private String senderId = "38";
    private String user_id="";
    SharedPreference sharedPreference;
    Activity context=this;

    private ImageView img_contact1;
    private ImageView img_contact2;
    private ImageView img_contact3;
    private ImageView img_contact4;

    private TextView tv_nameContact1;
    private TextView tv_nameContact2;
    private TextView tv_nameContact3;
    private TextView tv_nameContact4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context)))
        {
            user_id = sharedPreference.getUserId(context);
        }
        else
        {
            user_id="";
        }


    }

    @Override
    protected void onStart() {
        super.onStart();

        inItView();
    }

    private void inItView() {

        setViewDinamicaly();

        chatlist = new ArrayList<>();
        listRecentChatModel = new ArrayList<>();
        recentActivityList = new ArrayList<>();

        AppController.ref.child(FirebaseConstant.TABLE_RECENT).child(user_id).addChildEventListener(this);

        recyclerView_ChattingContactList = (RecyclerView) findViewById(R.id.recyclerView_ChattingContactList);
        recyclerView_ChattingRecentList = (RecyclerView) findViewById(R.id.recyclerView_ChattingRecentList);

        chatingContactAdapter = new ChatingContactAdapter(this, this, listRecentChatModel);
        recyclerView_ChattingContactList.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView_ChattingContactList.setItemAnimator(new DefaultItemAnimator());
        recyclerView_ChattingContactList.setAdapter(chatingContactAdapter);

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.img_wallpaperItem:
                int possion= (int) view.getTag();
                ChatDetailsModel chatDetailsModel=new ChatDetailsModel();
                chatDetailsModel.setChefId(listRecentChatModel.get(possion).getObjectId());
                DeviceTokenModel deviceTokenModel= DatabaseMgrApp.getDeviceTokenData(null,listRecentChatModel.get(possion).getObjectId());
                chatDetailsModel.setUserId(user_id);
                chatDetailsModel.setDefaultMessage("");
                chatDetailsModel.setChefName(deviceTokenModel.getUserName());
                Intent zoom = new Intent(ContactListActivity.this, ChattingActivity.class);
                zoom.putExtra("Model",chatDetailsModel);
                startActivity(zoom);
                break;

            default:
                break;
        }

    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        chatlist.add(dataSnapshot.getKey().toString());

        try {
            // HashMap<String,Chat> hashMap=dataSnapshot.getValue(HashMap.class);
            Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
            RecentChatModel recentChatModel = new RecentChatModel();

            recentChatModel.setObjectId(dataSnapshot.getKey());
            recentChatModel.setCounter((String) map.get("counter"));
            recentChatModel.setCreatedAt((String) map.get("createdAt"));
            recentChatModel.setDescription((String) map.get("description"));
            recentChatModel.setGroupId((String) map.get("groupId"));
            recentChatModel.setIsDeleted((String) map.get("isDeleted"));
            recentChatModel.setLastMessage((String) map.get("lastMessage"));
            recentChatModel.setPicture((String) map.get("picture"));
            recentChatModel.setType((String) map.get("type"));
            recentChatModel.setUpdatedAt((String) map.get("updatedAt"));
            recentChatModel.setSenderId((String) map.get("senderId"));
            recentChatModel.setReciverId((String) map.get("reciverId"));
            recentChatModel.setGroupName((String) map.get("groupName"));
            recentChatModel.setData((String) map.get("data"));

            // Utils.dbHelper.updateRemoteJid(recentChatModel.getObjectId(), recentChatModel.getGroupId());
            listRecentChatModel.add(recentChatModel);

            notifyTOAdapter();

            Log.d(TAG, "Value is: " + map.toString());
        } catch (Exception e) {
            Log.d(TAG, "Value is: " + e.toString());
        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        for (int i = 0; i < listRecentChatModel.size(); i++) {
            if (listRecentChatModel.get(i).getObjectId().contains(dataSnapshot.getKey().toString())) {

                try {
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();

                    RecentChatModel recentChatModel = new RecentChatModel();
                    recentChatModel.setObjectId(dataSnapshot.getKey());
                    recentChatModel.setCounter((String) map.get("counter"));
                    recentChatModel.setCreatedAt((String) map.get("createdAt"));
                    recentChatModel.setDescription((String) map.get("description"));
                    recentChatModel.setGroupId((String) map.get("groupId"));
                    recentChatModel.setIsDeleted((String) map.get("isDeleted"));
                    recentChatModel.setLastMessage((String) map.get("lastMessage"));
                    recentChatModel.setPicture((String) map.get("picture"));
                    recentChatModel.setType((String) map.get("type"));
                    recentChatModel.setUpdatedAt((String) map.get("updatedAt"));
                    recentChatModel.setSenderId((String) map.get("senderId"));
                    recentChatModel.setReciverId((String) map.get("reciverId"));
                    recentChatModel.setGroupName((String) map.get("groupName"));
                    recentChatModel.setData((String) map.get("data"));

                    if (recentChatModel.getType().equals(FirebaseConstant.CHAT_TYPE_PRIVTE)) {
                        //Utils.dbHelper.updateRemoteJid(recentChatModel.getReciverId(), recentChatModel.getGroupId());
                    }
                    listRecentChatModel.remove(i);
                    listRecentChatModel.add(0, recentChatModel);
                    notifyTOAdapter();

                    Log.d(TAG, "Value is: " + map.toString());
                } catch (Exception e) {
                    Log.d(TAG, "Value is: " + e.toString());
                }
                break;

            }
        }
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        chatlist.add(dataSnapshot.getKey().toString());
        notifyTOAdapter();

        try {
            // HashMap<String,Chat> hashMap=dataSnapshot.getValue(HashMap.class);
            Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
            String value = dataSnapshot.getValue(String.class);

            Log.d(TAG, "Value is: " + map.toString());
        } catch (Exception e) {
            Log.d(TAG, "Value is: " + e.toString());
        }
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        notifyTOAdapter();

        try {
            // HashMap<String,Chat> hashMap=dataSnapshot.getValue(HashMap.class);
            Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
            String value = dataSnapshot.getValue(String.class);

            Log.d(TAG, "Value is: " + map.toString());
        } catch (Exception e) {
            Log.d(TAG, "Value is: " + e.toString());
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    public void notifyTOAdapter() {
        sort();
        recentActivityList=new ArrayList<>();
        if (listRecentChatModel.size()>4)
        {
            for (int i=0;i<4;i++)
            {
                recentActivityList.add(listRecentChatModel.get(i));
            }
        }
        else
        {
            for (int i=0;i<listRecentChatModel.size();i++)
            {
                recentActivityList.add(listRecentChatModel.get(i));
            }
        }
        chatingContactAdapter.notifyDataSetChanged();

        recentActiveAdapter = new RecentActiveAdapter(this, this, recentActivityList);
        recyclerView_ChattingRecentList.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView_ChattingRecentList.setItemAnimator(new DefaultItemAnimator());
        recyclerView_ChattingRecentList.setAdapter(recentActiveAdapter);
    }

    public void sort() {
        Collections.sort(listRecentChatModel, new Comparator<RecentChatModel>() {
            @Override
            public int compare(RecentChatModel item1, RecentChatModel item2) {
                return item2.getUpdatedAt().compareTo(item1.getUpdatedAt());
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void setViewDinamicaly()
    {
        img_contact1= (ImageView) findViewById(R.id.img_contact1);
        img_contact2= (ImageView) findViewById(R.id.img_contact2);
        img_contact3= (ImageView) findViewById(R.id.img_contact3);
        img_contact4= (ImageView) findViewById(R.id.img_contact4);

        tv_nameContact1= (TextView) findViewById(R.id.tv_nameContact1);
        tv_nameContact2= (TextView) findViewById(R.id.tv_nameContact2);
        tv_nameContact3= (TextView) findViewById(R.id.tv_nameContact3);
        tv_nameContact4= (TextView) findViewById(R.id.tv_nameContact4);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        int finalHeight=(height)/4;
        int finalwidth=(width)/4;
        img_contact1.setLayoutParams(new LinearLayout.LayoutParams(finalwidth, finalwidth));
        img_contact2.setLayoutParams(new LinearLayout.LayoutParams(finalwidth, finalwidth));
        img_contact3.setLayoutParams(new LinearLayout.LayoutParams(finalwidth, finalwidth));
        img_contact4.setLayoutParams(new LinearLayout.LayoutParams(finalwidth, finalwidth));

    }
}
