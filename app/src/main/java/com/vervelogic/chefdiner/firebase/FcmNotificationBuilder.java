package com.vervelogic.chefdiner.firebase;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Author: Vishnu Saini
 * Created on: 19/01/2018 , 1:53 PM
 * Project: FirebaseChat
 */

public class FcmNotificationBuilder {
    public static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    private static final String TAG = "FcmNotificationBuilder";
    private static final String SERVER_API_KEY = "AIzaSyBNZ8HRgv002jAFSQLrgLS9I7OTk2WVoBE";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APPLICATION_JSON = "application/json";
    private static final String AUTHORIZATION = "Authorization";
    private static final String AUTH_KEY = "key=" + SERVER_API_KEY;
    private static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";
    // json related keys
    private static final String KEY_TO = "to";
    private static final String KEY_NOTIFICATION = "notification";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_DATA = "data";
    //private static final String KEY_TYPE = "msgType";
    private static final String KEY_DEVICE_TYPE = "msgType";

    private static final String KEY_TITLE = "title";
    private static final String KEY_TEXT = "text";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_UID = "uid";
    private static final String KEY_FCM_TOKEN = "fcm_token";
    private static final String KEY_GROUUP_ID = "group_id";
    private static final String KEY_TYPE = "notificationType";


    private String mMessage;
    private String mReceiverFirebaseToken;
    private String mMsgType;
    private String mDeviceType;
    private String mTitle;
    private String mText;

    private FcmNotificationBuilder() {

    }

    public static FcmNotificationBuilder initialize() {
        return new FcmNotificationBuilder();
    }

    public FcmNotificationBuilder deviceType(String title) {
        mDeviceType = title;
        return this;
    }

    public FcmNotificationBuilder title(String title) {
        mTitle = title;
        return this;
    }

    public FcmNotificationBuilder text(String title) {
        mText = title;
        return this;
    }

    public FcmNotificationBuilder messageType(String msgType) {
        mMsgType = msgType;
        return this;
    }

    public FcmNotificationBuilder message(String message) {
        mMessage = message;
        return this;
    }

    public FcmNotificationBuilder receiverFirebaseToken(String receiverFirebaseToken) {
        mReceiverFirebaseToken = receiverFirebaseToken;
        return this;
    }

    public void send() {
        RequestBody requestBody = null;
        try {
            requestBody = RequestBody.create(MEDIA_TYPE_JSON, getValidJsonBody().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        /*requestBody = RequestBody.create(MEDIA_TYPE_JSON, "{   \n" +
                "  \"to\": \"cP1A1iItzqU:APA91bFL3QMfPcoBAfdTj868wdbLrHm_LZ0ksQAfXwS2v0WLFQLweCGsMRoMDW8p49fvlcHzBnxGuNdXuBcLtxo_w3U9ty9RXMAu9D4-FIdwHh3bofpzi2sBiHEEM5zKc5DGHodexwFV\",\n" +
                "  \"priority\": \"high\",\n" +
                "  \"notification\" : {\n" +
                "    \"body\" : \"hello!\",\n" +
                "    \"title\": \"afruz\",\n" +
                "    \"sound\": \"default\"\n" +
                "  }\n" +
                "} ");*/

        Request request = new Request.Builder()
                .addHeader(CONTENT_TYPE, APPLICATION_JSON)
                .addHeader(AUTHORIZATION, AUTH_KEY)
                .url(FCM_URL)
                .post(requestBody)
                .build();

        Call call = new OkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "onGetAllUsersFailure: " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.e(TAG, "onResponse: " + response.body().string());
            }
        });
    }

    private JSONObject getValidJsonBody() throws JSONException {

        JSONObject jsonObjectData = new JSONObject();
        jsonObjectData.put(KEY_MESSAGE, mMessage);
        jsonObjectData.put(KEY_TYPE, mMsgType);

        jsonObjectData.put(KEY_TITLE, mTitle);
        jsonObjectData.put(KEY_TEXT, mText);

        JSONObject jsonObjectBody = new JSONObject();
        jsonObjectBody.put(KEY_TO, mReceiverFirebaseToken);
        jsonObjectBody.put("priority", "high");

        if (mDeviceType!=null && mDeviceType.equalsIgnoreCase("A")) {
            jsonObjectBody.put(KEY_DATA, jsonObjectData);
        } else {
            jsonObjectBody.put(KEY_DATA, jsonObjectData);
            jsonObjectBody.put(KEY_NOTIFICATION, jsonObjectData);
        }

        return jsonObjectBody;
    }
}