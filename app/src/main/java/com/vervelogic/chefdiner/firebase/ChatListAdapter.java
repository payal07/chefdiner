package com.vervelogic.chefdiner.firebase;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.appdatabase.DatabaseMgrApp;
import com.vervelogic.chefdiner.chatModel.DeviceTokenModel;
import com.vervelogic.chefdiner.chatModel.MessageChatModel;
import com.vervelogic.chefdiner.chatModel.Utils;


/**
 * @author Vishnu Saini
 * @since 18/12018
 * <p/>
 * This class is an example of how to use FirebaseListAdapter. It uses the <code>Chat</code> class to encapsulate the
 * data for each individual chat message
 */
public class ChatListAdapter extends FirebaseListAdapter<MessageChatModel> {

    // The mUsername for this client. We use this to indicate which messages originated from this user
    private String mUsername;
    Activity activity;
    String groupId;

    public ChatListAdapter(DatabaseReference ref, Activity activity, int layout, String mUsername, String groupId) {
        super(ref, MessageChatModel.class, layout, activity);
        this.mUsername = mUsername;
        this.activity = activity;
        this.groupId = groupId;
    }

    /**
     * Bind an instance of the <code>Chat</code> class to our view. This method is called by <code>FirebaseListAdapter</code>
     * when there is a data change, and we are given an instance of a View that corresponds to the layout that we passed
     * to the constructor, as well as a single <code>Chat</code> instance that represents the current data to bind.
     *
     * @param view A view instance corresponding to the layout we passed to the constructor.
     * @param chat An instance representing the current state of a chat message
     */
    @Override
    protected void populateView(View view, MessageChatModel chat, String key) {
        // Map a Chat object to an entry in our listview
        String author = chat.getSenderId();
        //TextView authorText = (TextView) view.findViewById(R.id.author);
        //  authorText.setText(author + ": ");
        // If the message was sent by this user, color it differently

        LinearLayout llReceiver = view.findViewById(R.id.llReceiver);
        LinearLayout llSender = view.findViewById(R.id.llSender);
        TextView tv_msg_user = view.findViewById(R.id.tv_msg_user);
        TextView tv_msg_sender = view.findViewById(R.id.tv_msg_sender);

        TextView tv_chat_time_sender = view.findViewById(R.id.tv_chat_time_sender);
        TextView tv_chattime_user = view.findViewById(R.id.tv_chattime_user);
        ImageView img_reademsg_success= view.findViewById(R.id.img_reademsg_success);

        String time = Utils.parsedlastseen(chat.getCreatedAt());
        String date = Utils.getmsgDate(chat.getCreatedAt());
        if (author != null && author.equals(mUsername)) {

            llSender.setVisibility(ViewGroup.VISIBLE);
            llReceiver.setVisibility(ViewGroup.GONE);

            tv_msg_user.setText(chat.getText());
            tv_chattime_user.setText(time);

            if (chat.getIsRead().equalsIgnoreCase("0")) {
                img_reademsg_success.setBackground(activity.getResources().getDrawable(R.mipmap.checkgrey));

            }else
            {
                img_reademsg_success.setBackground(activity.getResources().getDrawable(R.mipmap.checkred));
            }

        } else {
            llSender.setVisibility(ViewGroup.GONE);
            llReceiver.setVisibility(ViewGroup.VISIBLE);

            if (chat.getIsRead().equalsIgnoreCase("0")) {
                chat.setIsRead("1");
                FirebaseFunctions.updateMessageStatus(chat, key);
            }
            DeviceTokenModel deviceTokenModel = DatabaseMgrApp.getDeviceTokenData(null, chat.getSenderId());
            if (deviceTokenModel.getMobileNo() == null) {
                String senderId = chat.getSenderId();
                String intValue = senderId.replaceAll("[^0-9]", "");
                deviceTokenModel = DatabaseMgrApp.getDeviceTokenData(null, intValue);
            }
            tv_msg_sender.setText(chat.getText());
            tv_chat_time_sender.setText(time);
        }
    }
}
