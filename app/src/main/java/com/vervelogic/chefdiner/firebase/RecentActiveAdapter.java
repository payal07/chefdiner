package com.vervelogic.chefdiner.firebase;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.appdatabase.DatabaseMgrApp;
import com.vervelogic.chefdiner.chatModel.DeviceTokenModel;
import com.vervelogic.chefdiner.chatModel.RecentChatModel;
import com.vervelogic.chefdiner.chatModel.Utils;

import java.util.ArrayList;

/**
 * Created by Vishnu Saini on 1/17/2018.
 * vishnusainideveloper27@gmail.com
 */

public class RecentActiveAdapter extends RecyclerView.Adapter<RecentActiveAdapter.MyViewHolder> {

    Activity context;
    private boolean onBind;
    View.OnClickListener onClickListener;
    ArrayList<RecentChatModel> listRecentChatModel;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_wallpaperItem;
        TextView tv_contactName;

        public MyViewHolder(View convertView) {
            super(convertView);
            tv_contactName = convertView.findViewById(R.id.tv_contactName);
            img_wallpaperItem = convertView.findViewById(R.id.img_wallpaperItem);
        }
    }


    public RecentActiveAdapter(Activity context, View.OnClickListener onClickListener, ArrayList<RecentChatModel> listRecentChatModel) {
        super();
        this.context = context;
        this.onClickListener = onClickListener;
        this.listRecentChatModel = listRecentChatModel;
    }

    @Override
    public RecentActiveAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat_contact, parent, false);
        return new RecentActiveAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecentActiveAdapter.MyViewHolder holder, final int position) {
        onBind = true;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        int finalHeight=(height)/4;
        int finalwidth=(width)/4;
        holder.img_wallpaperItem.setLayoutParams(new LinearLayout.LayoutParams(finalwidth, finalwidth));
        holder.img_wallpaperItem.setOnClickListener(onClickListener);
        holder.img_wallpaperItem.setTag(position);
        RecentChatModel recentChatModel = listRecentChatModel.get(position);

        DeviceTokenModel deviceTokenModel= DatabaseMgrApp.getDeviceTokenData(null,recentChatModel.getObjectId());
        holder.tv_contactName.setText(deviceTokenModel.getUserName());

        if (deviceTokenModel.getProfilepic()!=null && !deviceTokenModel.getProfilepic().isEmpty())
        {
            Utils.loadImageUsingUrl(context,holder.img_wallpaperItem,deviceTokenModel.getProfilepic());
        }


        onBind = false;
    }

    @Override
    public int getItemCount() {
        return listRecentChatModel.size();
        // return 20;

    }
}