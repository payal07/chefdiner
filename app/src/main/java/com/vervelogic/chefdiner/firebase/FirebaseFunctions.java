package com.vervelogic.chefdiner.firebase;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.activity.MessageOpenActivity;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.appdatabase.DatabaseMgrApp;
import com.vervelogic.chefdiner.chatModel.DeviceTokenModel;
import com.vervelogic.chefdiner.chatModel.MessageChatModel;
import com.vervelogic.chefdiner.chatModel.NotificationModel;
import com.vervelogic.chefdiner.chatModel.NotificationReciveModel;
import com.vervelogic.chefdiner.chatModel.RecentChatModel;
import com.vervelogic.chefdiner.extra.Constants;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by ubuntu on 4/10/16.
 */
public class FirebaseFunctions {

    private static DatabaseReference databaseReferenceSender;
    private static DatabaseReference databaseReferenceReciver;
    private static DatabaseReference databaseReferenceSendMessage;
    public static String GroupId="";

    public static DatabaseReference getDBreferance(String tableName) {
        databaseReferenceSender = AppController.database.getReference(tableName).child("");
        databaseReferenceReciver = AppController.database.getReference(tableName).child("vishnu");
        return databaseReferenceSender;
    }

    public static void saveDeviceTokenIntoServer(DeviceTokenModel deviceTokenModel) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FirebaseDatabase.getInstance()
                    .getReference()
                    .child(Constants.ARG_USERS)
                    .child(deviceTokenModel.getUserId())
                    .setValue(deviceTokenModel);
        }
    }


    public static void sendMessageBoth(RecentChatModel recentChat, MessageChatModel messageChatModel) {
        databaseReferenceSender = AppController.database.getReference(FirebaseConstant.TABLE_RECENT).child(recentChat.getSenderId()).child(recentChat.getReciverId());
        databaseReferenceReciver = AppController.database.getReference(FirebaseConstant.TABLE_RECENT).child(recentChat.getReciverId()).child(recentChat.getSenderId());
        databaseReferenceSendMessage = AppController.database.getReference(FirebaseConstant.TABLE_MESSAGE).child(recentChat.getGroupId());

        databaseReferenceSender.setValue(recentChat);
        databaseReferenceReciver.setValue(recentChat);
        databaseReferenceSendMessage.push().setValue(messageChatModel);
        //AppController.database.getReference(FirebaseConstant.TABLE_RECENT).child(recentChat.getSenderId()).child(recentChat.getReciverId()).updateChildren(recentChats);
    }

    public static void isArchiveChat(RecentChatModel recentChat)
    {
        HashMap<String, Object> result = new HashMap<>();
        result.put("isArchive", "1");

        AppController.database.getReference(FirebaseConstant.TABLE_RECENT).
                child(recentChat.getSenderId()).child(recentChat.getReciverId()).updateChildren(result);
        //AppController.database.getReference(FirebaseConstant.TABLE_RECENT).child(recentChat.getReciverId()).child(recentChat.getSenderId()).updateChildren(result);

    }

    public static void isUnArchiveChat(RecentChatModel recentChat)
    {
        HashMap<String, Object> result = new HashMap<>();
        result.put("isArchive", "0");

        AppController.database.getReference(FirebaseConstant.TABLE_RECENT).
                child(recentChat.getSenderId()).child(recentChat.getReciverId()).updateChildren(result);
        //AppController.database.getReference(FirebaseConstant.TABLE_RECENT).child(recentChat.getReciverId()).child(recentChat.getSenderId()).updateChildren(result);

    }


    public static void updateMessageStatus(MessageChatModel messageChatModel, String key) {

        databaseReferenceSendMessage = AppController.database.getReference(FirebaseConstant.TABLE_MESSAGE).child(messageChatModel.getGroupId()).child(key);
        databaseReferenceSendMessage.setValue(messageChatModel);
    }

    public static void sendPushNotificationToReceiver(String type,
                                                      String message,
                                                      String receiverFirebaseToken,
                                                      String deviceType,
                                                      String title,
                                                      String text
    ) {
        FcmNotificationBuilder.initialize()
                .message(message)
                .receiverFirebaseToken(receiverFirebaseToken)
                .messageType(type)
                .deviceType(deviceType)
                .title(title)
                .text(text)
                .send();

    }


    public static void creatMessageNotification(Context context, JSONObject json) {
        NotificationReciveModel notificationReciveModel;
        Gson gson = new Gson();
        notificationReciveModel = gson.fromJson(json.toString(), NotificationReciveModel.class);

        NotificationModel notificationModel = new NotificationModel();
        notificationModel = gson.fromJson(notificationReciveModel.getMessage(), NotificationModel.class);
        DeviceTokenModel senderDetail = DatabaseMgrApp.getDeviceTokenData(null, notificationModel.getSenderId());

        Intent intent = new Intent(context, MessageOpenActivity.class);
        intent.putExtra(Constants.ARG_RECEIVER, notificationModel);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int notifyID = 1;
            String CHANNEL_ID = "my_channel_01";// The id of the channel.
            CharSequence name = context.getString(R.string.channel_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            @SuppressLint("WrongConstant") NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mNotificationManager.createNotificationChannel(mChannel);
// Create a notification and set the notification channel.
            Notification notification = new Notification.Builder(context)
                    .setContentTitle(senderDetail.getUserName())
                    .setContentText(notificationModel.getMessage())
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.logo))
                    .setChannelId(CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setSound(defaultSoundUri)
                    .build();
            if (!GroupId.equalsIgnoreCase(notificationModel.getGroupId()))
                mNotificationManager.notify(0, notification);
        } else {

            Notification notification = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.logo)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.logo))
                    .setContentTitle(senderDetail.getUserName())
                    .setContentText(notificationModel.getMessage())
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .build();

            if (!GroupId.equalsIgnoreCase(notificationModel.getGroupId()))
                mNotificationManager.notify(0, notification);

//            Notification notification =
//                    new NotificationCompat.Builder(context)
//                            .setSmallIcon(R.drawable.logo)
//                            .setContentTitle("My notification")
//                            .setContentText("Hello World!")
//                            .build();
        }
    }
    public static void updateUserName(String userId,String userName)
    {
        HashMap<String, Object> result = new HashMap<>();
        result.put("userName", userName);

        AppController.database.getReference(Constants.ARG_USERS).child(userId).updateChildren(result);

    }

    public static void updateUserImage(String userId,String imageUrl)
    {
        HashMap<String, Object> result = new HashMap<>();
        result.put("profilepic", imageUrl);

        AppController.database.getReference(Constants.ARG_USERS).child(userId).updateChildren(result);

    }

    public static void updateUserMobileNo(String userId,String mobileNo)
    {
        HashMap<String, Object> result = new HashMap<>();
        result.put("mobileNo", mobileNo);

        AppController.database.getReference(Constants.ARG_USERS).child(userId).updateChildren(result);

    }
}



