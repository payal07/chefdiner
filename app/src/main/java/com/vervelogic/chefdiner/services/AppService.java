package com.vervelogic.chefdiner.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.vervelogic.chefdiner.appdatabase.DatabaseMgrApp;
import com.vervelogic.chefdiner.chatModel.DeviceTokenModel;
import com.vervelogic.chefdiner.extra.Constants;

import java.util.Map;

/**
 * Created by ubuntu on 29/11/16.
 */
public class AppService extends Service {

    public static DatabaseReference ref;
    public static FirebaseAuth mAuth;
    public static FirebaseDatabase database;
    private static final String TAG = "Contact Token";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e("App oncreat called", "Service start");

        mAuth = FirebaseAuth.getInstance();
        ref = FirebaseDatabase.getInstance().getReference();
        database = FirebaseDatabase.getInstance();

        // printLog();



    }

    public void printLog() {
        Log.e("service", "service is running");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        printLog();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        getContactFromFireBase();


        return Service.START_STICKY;
    }

    private void getContactFromFireBase() {

        ref.child(Constants.ARG_USERS).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();

                String mobileNo= (String) map.get("mobileNo");
                String devicetoken= (String) map.get("devicetoken");
                String profilepic= (String) map.get("profilepic");
                String email= (String) map.get("email");
                String groupid= (String) map.get("deviceType");
                String username= (String) map.get("userName");
                String userId= (String) map.get("userId");


                DeviceTokenModel deviceTokenModel=new DeviceTokenModel();

                deviceTokenModel.setMobileNo(mobileNo);
                deviceTokenModel.setProfilepic(profilepic);
                deviceTokenModel.setDevicetoken(devicetoken);
                deviceTokenModel.setUserId(userId);
                deviceTokenModel.setEmail(email);
                deviceTokenModel.setDeviceType(groupid);
                deviceTokenModel.setUserName(username);

                DatabaseMgrApp.insertDataToDeviceTokenTable(deviceTokenModel);

                Log.d(TAG, "Value is new " + map.toString());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                String mobileNo= (String) map.get("mobileNo");
                String devicetoken= (String) map.get("devicetoken");
                String profilepic= (String) map.get("profilepic");
                String email= (String) map.get("email");
                String groupid= (String) map.get("deviceType");
                String username= (String) map.get("userName");
                String userId= (String) map.get("userId");

                DeviceTokenModel deviceTokenModel=new DeviceTokenModel();

                deviceTokenModel.setMobileNo(mobileNo);
                deviceTokenModel.setProfilepic(profilepic);
                deviceTokenModel.setDevicetoken(devicetoken);
                deviceTokenModel.setUserId(userId);
                deviceTokenModel.setEmail(email);
                deviceTokenModel.setDeviceType(groupid);
                deviceTokenModel.setUserName(username);

                DatabaseMgrApp.insertDataToDeviceTokenTable(deviceTokenModel);
                Log.d(TAG, "Value is updated " + map.toString());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                String mobileNo= (String) map.get("mobileNo");
                String devicetoken= (String) map.get("devicetoken");
                String profilepic= (String) map.get("profilepic");
                String email= (String) map.get("email");
                String groupid= (String) map.get("deviceType");
                String username= (String) map.get("userName");
                String userId= (String) map.get("userId");


                DeviceTokenModel deviceTokenModel=new DeviceTokenModel();

                deviceTokenModel.setMobileNo(mobileNo);
                deviceTokenModel.setProfilepic(profilepic);
                deviceTokenModel.setDevicetoken(devicetoken);
                deviceTokenModel.setUserId(userId);
                deviceTokenModel.setEmail(email);
                deviceTokenModel.setDeviceType(groupid);
                deviceTokenModel.setUserName(username);

                DatabaseMgrApp.deleteDataToDeviceTokenTable(deviceTokenModel);
                Log.d(TAG, "Value is: " + map.toString());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                Log.d(TAG, "Value is: " + map.toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("App oncreat called", "Service destroy");
    }


}
