package com.vervelogic.chefdiner.services;


import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.vervelogic.chefdiner.activity.Splash;
import com.vervelogic.chefdiner.firebase.FirebaseConstant;
import com.vervelogic.chefdiner.firebase.FirebaseFunctions;
import com.vervelogic.chefdiner.utils.NotificationUtils;
import com.vervelogic.chefdiner.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;
    SessionManager session;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage);
        session = new SessionManager(getApplicationContext());
        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            // int questionId = Integer.parseInt(remoteMessage.getData().get("bookingid").toString());
           /* String questionTitle = remoteMessage.getData().get("notification").toString();
            String userDisplayName = remoteMessage.getData().get("bookingid").toString();*/
            // String commentText = remoteMessage.getData().get("latestComment").toString();
            Log.e(TAG, "Notification Body: " + remoteMessage.getData());
            /*Log.e(TAG, "questionTitle Body: " + questionTitle);*/
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
                JSONObject json = new JSONObject(remoteMessage.getData());
                String notificationType = json.getString("notificationType");

                //this is the code for fire base chatting notification by vishnu saini.
                if (notificationType.equalsIgnoreCase(FirebaseConstant.TYPE_MESSAGE) && session.isLoggedIn() )
                {
                    FirebaseFunctions.creatMessageNotification(this,json);
                }
                else
                {
                    Log.e("LOG", "" + json);
                    handleDataMessage(json);
                }
                //handleNotification(remoteMessage.getNotification().getBody());
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent("pushNotification");
            pushNotification.putExtra("message", message);
            Log.e("Handle No", "" + message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json);

        try {
            // JSONObject data = json.getJSONObject("data");

            String chefId = json.getString("chefid");
            String notificationType = json.getString("notificationType");
            String bookingStatus = json.getString("booking_status");
            String bookingId = json.getString("bookingid");
       /*     String smallIcon=json.getString("smallIcon");
            String largeIcon=json.getString("largeIcon");*/
           /* boolean isBackground = json.getBoolean("is_background");
            String imageUrl = json.getString("image");
            String timestamp = json.getString("timestamp");
           JSONObject payload = json.getJSONObject("payload");*/

            Log.e(TAG, "title: " + chefId);
            Log.e(TAG, "message: " + notificationType);
           /* Log.e(TAG, "isBackground: " + isBackground);
            Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);*/


            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent("pushNotification");
                pushNotification.putExtra("notificationType", notificationType);
                pushNotification.putExtra("chefid", chefId);
                pushNotification.putExtra("booking_status", bookingStatus);
                pushNotification.putExtra("bookingid", bookingId);
                Log.e("Handle Mes", "" + pushNotification);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();

            } else {
                Log.e("Help", "" + chefId);
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), Splash.class);
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                resultIntent.putExtra("notificationType", notificationType);
                resultIntent.putExtra("chefid", chefId);
                resultIntent.putExtra("booking_status", bookingStatus);
                resultIntent.putExtra("bookingid", bookingId);
                Log.e("Handle Mes Back", "" + resultIntent);
                showNotificationMessage(getApplicationContext(), bookingStatus, bookingStatus, resultIntent);

              /*  // check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                }*/
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, Intent intent) {
        Log.e("Show Notification", "" + message);
        notificationUtils = new NotificationUtils(context);
        Log.e("Show1", "" + message);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Log.e("Show2", "" + message);
        notificationUtils.showNotificationMessage(title, message, intent);
        Log.e("Show3", "" + message);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, intent, imageUrl);
    }
}
