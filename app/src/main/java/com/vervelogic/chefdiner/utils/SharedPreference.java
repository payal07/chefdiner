package com.vervelogic.chefdiner.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
//import android.preference.PreferenceManager;

public class SharedPreference {

    private static final String PREFS_NAME = "CHEF_DINER";
    private static final String PREF_USER_ID = "USER_ID";
    private static final String PREF_AUTH_TOKEN = "AUTH_TOKEN";
    private static final String PREF_COUNTRY_CODE = "COUNTRY_CODE";
    private static final String PREF_LANGUAGE = "LANGUAGE";
    private static final String PREF_selectedCountry = "selectedCountry";
    private static final String PREF_minimumPriceStr = "minimumPriceStr";
    private static final String PREF_maximumPriceStr = "maximumPriceStr";
    private static final String PREF_selectedDate = "selectedDate";
    private static final String PREF_selectedNoOfGuest = "selectedNoOfGuest";
    private static final String PREF_formattedDate = "formattedDate";

    public SharedPreference() {
        super();
    }

    public void save(Context context, String userId, String authToken) {
        SharedPreferences settings;
        Editor editor;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2

        editor.putString(PREF_USER_ID, userId); //3
        editor.putString(PREF_AUTH_TOKEN, authToken); //3

        editor.commit(); //4
    }

    public void saveData(Context context, String selectedCountry, String minimumPriceStr, String maximumPriceStr
            , String selectedDate, String selectedNoOfGuest, String formattedDate) {
        SharedPreferences settings;
        Editor editor;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2

        editor.putString(PREF_selectedCountry, selectedCountry); //3
        editor.putString(PREF_minimumPriceStr, minimumPriceStr); //3
        editor.putString(PREF_maximumPriceStr, maximumPriceStr); //3
        editor.putString(PREF_selectedDate, selectedDate); //3
        editor.putString(PREF_selectedNoOfGuest, selectedNoOfGuest); //3
        editor.putString(PREF_formattedDate, formattedDate); //3


        editor.commit(); //4
    }

    public void save(Context context, String countryCode) {
        SharedPreferences settings;
        Editor editor;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2

        editor.putString(PREF_COUNTRY_CODE, countryCode); //3

        editor.commit(); //4
    }

    public void saveLanguage(Context context, String language) {
        SharedPreferences settings;
        Editor editor;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2

        editor.putString(PREF_LANGUAGE, language); //3

        editor.commit(); //4
    }

    public String getCountryCode(Context context) {
        SharedPreferences settings;
        String text;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREF_COUNTRY_CODE, null);
        return text;
    }

    public String getselectedCountry(Context context) {
        SharedPreferences settings;
        String text;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREF_selectedCountry, null);
        return text;
    }

    public String getminimumPriceStr(Context context) {
        SharedPreferences settings;
        String text;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREF_minimumPriceStr, null);
        return text;
    }

    public String getmaximumPriceStr(Context context) {
        SharedPreferences settings;
        String text;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREF_maximumPriceStr, null);
        return text;
    }

    public String getselectedDate(Context context) {
        SharedPreferences settings;
        String text;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREF_selectedDate, null);
        return text;
    }

    public String getselectedNoOfGuest(Context context) {
        SharedPreferences settings;
        String text;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREF_selectedNoOfGuest, null);
        return text;
    }

    public String getformattedDate(Context context) {
        SharedPreferences settings;
        String text;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREF_formattedDate, null);
        return text;
    }

    public void removeData(Context context) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.remove(PREF_selectedCountry);
        editor.remove(PREF_minimumPriceStr);
        editor.remove(PREF_maximumPriceStr);
        editor.remove(PREF_selectedDate);
        editor.remove(PREF_selectedNoOfGuest);
        editor.remove(PREF_formattedDate);

        editor.commit();
    }

    public String getLanguage(Context context) {
        SharedPreferences settings;
        String text;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREF_LANGUAGE, null);
        return text;
    }

    public String getUserId(Context context) {
        SharedPreferences settings;
        String text;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREF_USER_ID, null);
        return text;
    }

    public String getAuthToken(Context context) {
        SharedPreferences settings;
        String text;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREF_AUTH_TOKEN, null);
        return text;
    }

    public void clearSharedPreference(Context context) {
        SharedPreferences settings;
        Editor editor;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();
    }

    public void removeValue(Context context) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.remove(PREF_USER_ID);
        editor.remove(PREF_AUTH_TOKEN);
        editor.remove(PREF_COUNTRY_CODE);
        editor.commit();
    }
}
