package com.vervelogic.chefdiner.app;

/**
 * Created by Verve on 09-10-2017.
 */

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.facebook.stetho.Stetho;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.vervelogic.chefdiner.appdatabase.DBHelperApp;
import com.vervelogic.chefdiner.appdatabase.DatabaseMgrApp;
import com.vervelogic.chefdiner.logger.Env;
import com.vervelogic.chefdiner.services.AppService;
import com.vervelogic.chefdiner.utils.SessionManager;

public class AppController extends MultiDexApplication {

    public static final String TAG = AppController.class
            .getSimpleName();
    public static final String CLIENT_ID = "b5a059d71e6f4aaebab2851cafdbb202";
    public static final String CLIENT_SECRET = "8ad9323baa6f4707b6efeb86404a4101";
    public static final String CALLBACK_URL = "http://www.chefdiner.com";
    private static AppController mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static DatabaseReference ref;
    public static FirebaseAuth mAuth;
    public static FirebaseDatabase database;
    boolean previouslyStarted;
    SharedPreferences prefs;
    SessionManager session;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    //Initiate Image Loader Configuration
    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(
                context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        com.nostra13.universalimageloader.core.ImageLoader.getInstance().init(config.build());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        initImageLoader(getApplicationContext());

        MultiDex.install(this);
        Stetho.initializeWithDefaults(this);
        session = new SessionManager(getApplicationContext());
        mAuth = FirebaseAuth.getInstance();
        ref = FirebaseDatabase.getInstance().getReference();
        database = FirebaseDatabase.getInstance();

        if (mAuth.getCurrentUser() != null) {
            Log.e("fire Base status", "online");
        } else {
            Log.e("fire Base status", "offline");
        }

        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        previouslyStarted= prefs.getBoolean("First_Time", false);

        if (session.isLoggedIn()) {
            Intent service = new Intent(this, AppService.class);
            startService(service);
        }

        try {
            Env.init(this, new DBHelperApp(), null, true);
            DatabaseMgrApp.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

}