package com.vervelogic.chefdiner.app;

/**
 * Created by Verve on 09-10-2017.
 */

public class AppConfig {
    // Referral Code
    public static final String REFERRAL_CODE_URL = "http://vervedemos.in/chefdiner/index.php/welcome/SignUpR/";
    // URL
    private static final String URL = "http://vervedemos.in/chefdiner/index.php/apicontroller/";
    //  private static final String URL = "http://192.168.1.63/chefdiner/index.php/apicontroller/";
    // REGISTER API
    public static final String REGISTER_URL = URL + "userRegister";
    // LOGIN API
    public static final String LOGIN_URL = URL + "userLogin";
    //BECOME A CHEF API
    public static final String BECOME_A_CHEF_URL = URL + "becomeChef ";
    //GET CHEF API
    public static final String GET_CHEF_URL = URL + "getChef";
    //ADD MENU API
    public static final String ADD_MENU_URL = URL + "addMenu";
    // GET MENU API
    public static final String GET_MENU_URL = URL + "getMenus";
    // DELETE MENU API
    public static final String DELETE_MENU_URL = URL + "deleteMenus";
    // ADD DISH MENU API
    public static final String ADD_SUBMENU_URL = URL + "addSubMenu";
    //GET SUBMENU DISHED
    public static final String GET_SUBMENU_URL = URL + "getSubMenus";
    // DELETE SUBMENU DISHED API
    public static final String DELETE_SUBMENU_URL = URL + "deleteSubmenus";
    // GET CHEF INFO
    public static final String GET_CHEF_INFO_URL = URL + "getchefinfo";
    //GET USER INFO
    public static final String GET_USER_INFO_URL = URL + "getuserinfo";
    // UPDATE USER INFO
    public static final String UPDATE_USER_INFO_URL = URL + "UpdateuserProfile";
    // ADD PERMISSION API
    public static final String ADD_PERMISSION_URL = URL + "addPermission";
    // GET PERMISSION API
    public static final String GET_PERMISSION_URL = URL + "getPermission";
    //KITCHEN GALLERY UPLOAD API
    public static final String KITCHEN_GALLERY_UPLOAD_URL = URL + "KitchenGallery";
    // GET KITCHEN GALLERY API
    public static final String GET_KITCHEN_GALLERY_URL = URL + "getKitchengallery";
    //DELETE IMAGE FROM GALLERY API
    public static final String DELETE_IMAGE_GALLERY_URL = URL + "DeleteKitchenGallery";
    // FAVORITE API
    public static final String FAVORITE_URL = URL + "favouriteChef";
    //WISH-LIST COUNT API
    public static final String WISHLIST_URL = URL + "wishlistChef";
    // GET CUISINE API
    public static final String GET_CUISINE_URL = URL + "getCuisine";
    //GET SUB CUISINE API
    public static final String GET_SUB_CUISINE_URL = URL + "getsubcuisine";
    // GET MENU SUBMENU API
    public static final String GET_MENU_SUBMENU_URL = URL + "getMenusSubmenus";
    // GET MENU CHEF PROFILE
    public static final String GET_MENU_CHEFPROFILE_URL = URL + "ChefmenuList";
    // GET ON-SITE COOKING API
    public static final String GET_ONSITE_COOKING_URL = URL + "onsiteCooking";
    // CHEF FAV LIST API
    public static final String GET_CHEF_FAV_URL = URL + "getfavroiteChef";
    // HOW IT WORK API
    public static final String HOW_IT_WORK_URL = URL + "howitworks";
    // BOOKING API
    public static final String BOOKING_URL = URL + "bookNow";
    // LOGIN WITH FACEBOOK
    public static final String FBLOGIN_URL = URL + "FBlogin";
    // INSTA LOGIN
    public static final String INSTA_LOGIN_URL = URL + "InstagramLogin";
    //  UPCOMING MEALS
    public static final String UPCOMING_MEAL_URL = URL + "UpcomingMeal";
    // HISTORY MEALS
    public static final String HISTORY_MEAL_URL = URL + "HistoryMeal";
    // SHARED BOOKING
    public static final String SHARED_BOOKING_MEAL_URL = URL + "sharedBooking";
    // CANCEL BOOKING
    public static final String CANCEL_BOOKING_URL = URL + "cancelBooking";
    // CHANGE DETAILS API
    public static final String CHANGE_BOOOKING_DETAILS_URL = URL + "changeDetails";
    // GET PAYMENT DETAILS
    public static final String GET_PAYMENT_DETAILS_URL = URL + "getPaypalPayment";
    // CALENDAR API
    public static final String CALENDAR_API = URL + "chefCalendar";
    // GET CALENDAR DETAILS
    public static final String CALENDAR_DETAILS_API = URL + "getchefCalendar";
    // GET BOOKING LIST DETAILS
    public static final String GET_BOOKING_DETAILS_URL = URL + "getBookingList";
    // ACCEPT REJECT GUEST API
    public static final String ACCEPT_REJECT_GUEST_DETAILS_URL = URL + "acceptRejectbooking";
    //  SAVE TIME API
    public static final String SAVE_TIME_URL = URL + "saveTimeforDate";
    //  SAVE TIME API
    public static final String GET_TIME_URL = URL + "getTimeforDate";
    // save review
    public static final String SAVE_REVIEW_URL = URL + "SaveReviewRating";
    // GET REVIEW
    public static final String GET_REVIEW_URL = URL + "getReviewRating";
    // GET REVIEW COMMENT
    public static final String GET_REVIEW_COMMENT_URL = URL + "getRRlist";
    // Completed Dinner API
    public static final String COMPLETED_DINNER_URL = URL + "bookingComplete";
    // Available Credit API
    public static final String AVAIL_CREDIT_URL = URL + "getReferralAmount";
    // Like Review
    public static final String LIKE_REVIEW_URL = URL + "likeReview";
    // Cuisine Filter List
    public static final String CUISINE_FILTER_URL = URL + "filterCuisine";
    // FILTERED CUISINE CHEF LIST
    public static final String FILTERED_CUISINE_LIST_URL = URL + "getChefCuisines";
    // Filtered SubCuisine
    public static final String FILTERED_SUBCUISINE_LIST_URL = URL + "filterSubcuisine";
    // Draft a menu
    public static final String DRAFT_MENU = URL + "draftMenus";
}
