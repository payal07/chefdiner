package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 28-12-2017.
 */

public class GetDateModel {

    /**
     * status : 1
     * message : OK
     * getchefCalendar : [{"calendar_id":"19","chef_id":"3","date":"2018-01-05","ava_status":"1","is_book":"yes"},{"calendar_id":"20","chef_id":"3","date":"2018-01-06","ava_status":"2","is_book":"no"},{"calendar_id":"21","chef_id":"3","date":"2018-01-07","ava_status":"0","is_book":"no"},{"calendar_id":"22","chef_id":"3","date":"2018-01-08","ava_status":"1","is_book":"yes"},{"calendar_id":"23","chef_id":"3","date":"2018-01-09","ava_status":"0","is_book":"no"},{"calendar_id":"24","chef_id":"3","date":"2018-01-10","ava_status":"0","is_book":"no"},{"calendar_id":"47","chef_id":"3","date":"2018-01-12","ava_status":"1","is_book":"yes"},{"calendar_id":"48","chef_id":"3","date":"2018-01-19","ava_status":"2","is_book":"no"},{"calendar_id":"49","chef_id":"3","date":"2018-01-13","ava_status":"2","is_book":"no"},{"calendar_id":"50","chef_id":"3","date":"2018-01-20","ava_status":"0","is_book":"no"},{"calendar_id":"51","chef_id":"3","date":"2018-01-21","ava_status":"0","is_book":"no"},{"calendar_id":"52","chef_id":"3","date":"2018-01-17","ava_status":"0","is_book":"no"},{"calendar_id":"53","chef_id":"3","date":"2018-01-24","ava_status":"0","is_book":"no"}]
     */

    private String status;
    private String message;
    private List<GetchefCalendarBean> getchefCalendar;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetchefCalendarBean> getGetchefCalendar() {
        return getchefCalendar;
    }

    public void setGetchefCalendar(List<GetchefCalendarBean> getchefCalendar) {
        this.getchefCalendar = getchefCalendar;
    }

    public static class GetchefCalendarBean {
        /**
         * calendar_id : 19
         * chef_id : 3
         * date : 2018-01-05
         * ava_status : 1
         * is_book : yes
         */

        private String calendar_id;
        private String chef_id;
        private String date;
        private String ava_status;
        private String is_book;

        public String getCalendar_id() {
            return calendar_id;
        }

        public void setCalendar_id(String calendar_id) {
            this.calendar_id = calendar_id;
        }

        public String getChef_id() {
            return chef_id;
        }

        public void setChef_id(String chef_id) {
            this.chef_id = chef_id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getAva_status() {
            return ava_status;
        }

        public void setAva_status(String ava_status) {
            this.ava_status = ava_status;
        }

        public String getIs_book() {
            return is_book;
        }

        public void setIs_book(String is_book) {
            this.is_book = is_book;
        }
    }
}
