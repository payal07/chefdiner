package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 07-12-2017.
 */

public class ChooseSubMenuModel {


    /**
     * status : 1
     * message : OK
     * MenusSubmenus : [[{"id":"20","menu_id":"20","user_id":"3","dish_name":"Dal","dish_image":"","dish_category":"Drink","created_at":"2017-12-12 13:26:18","updated_at":"2017-12-12 13:26:18","status":"1","menu_price":55.05,"menucurrency":"USD","menu_title":"Italian Indian","whatshot":"yes","menu_counter":"7","menu_image":"http://vervedemos.in/chefdiner/assets/menuImgae/IMG_20171102_0900511.jpg","currency":"HKD"}],[{"id":"80","menu_id":"80","user_id":"3","dish_name":"Red Fish","dish_image":"","dish_category":"Main Course","created_at":"2017-12-07 07:30:36","updated_at":"2017-12-07 07:30:36","status":"1","menu_price":47,"menucurrency":"USD","menu_title":"Mexican menu","whatshot":"no","menu_counter":"2","menu_image":"http://vervedemos.in/chefdiner/assets/menuImgae/1509095175821.jpg","currency":"HKD"}],[{"id":"125","menu_id":"125","user_id":"3","dish_name":"Baked Lasagne","dish_image":"","dish_category":"Main Course","created_at":"2018-01-22 14:55:45","updated_at":"2018-01-22 14:55:45","status":"1","menu_price":414.14,"menucurrency":"USD","menu_title":"Lasagne","whatshot":"no","menu_counter":"0","menu_image":"","currency":"HKD"}]]
     */

    private String status;
    private String message;
    private List<List<MenusSubmenusBean>> MenusSubmenus;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<List<MenusSubmenusBean>> getMenusSubmenus() {
        return MenusSubmenus;
    }

    public void setMenusSubmenus(List<List<MenusSubmenusBean>> MenusSubmenus) {
        this.MenusSubmenus = MenusSubmenus;
    }

    public static class MenusSubmenusBean {
        /**
         * id : 20
         * menu_id : 20
         * user_id : 3
         * dish_name : Dal
         * dish_image :
         * dish_category : Drink
         * created_at : 2017-12-12 13:26:18
         * updated_at : 2017-12-12 13:26:18
         * status : 1
         * menu_price : 55.05
         * menucurrency : USD
         * menu_title : Italian Indian
         * whatshot : yes
         * menu_counter : 7
         * menu_image : http://vervedemos.in/chefdiner/assets/menuImgae/IMG_20171102_0900511.jpg
         * currency : HKD
         */

        private String id;
        private String menu_id;
        private String user_id;
        private String dish_name;
        private String dish_image;
        private String dish_category;
        private String created_at;
        private String updated_at;
        private String status;
        private double menu_price;
        private String menucurrency;
        private String menu_title;
        private String whatshot;
        private String menu_counter;
        private String menu_image;
        private String currency;

        public String getWhatsHotKey() {
            return whatsHotKey;
        }

        public void setWhatsHotKey(String whatsHotKey) {
            this.whatsHotKey = whatsHotKey;
        }

        private String whatsHotKey;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMenu_id() {
            return menu_id;
        }

        public void setMenu_id(String menu_id) {
            this.menu_id = menu_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getDish_name() {
            return dish_name;
        }

        public void setDish_name(String dish_name) {
            this.dish_name = dish_name;
        }

        public String getDish_image() {
            return dish_image;
        }

        public void setDish_image(String dish_image) {
            this.dish_image = dish_image;
        }

        public String getDish_category() {
            return dish_category;
        }

        public void setDish_category(String dish_category) {
            this.dish_category = dish_category;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public double getMenu_price() {
            return menu_price;
        }

        public void setMenu_price(double menu_price) {
            this.menu_price = menu_price;
        }

        public String getMenucurrency() {
            return menucurrency;
        }

        public void setMenucurrency(String menucurrency) {
            this.menucurrency = menucurrency;
        }

        public String getMenu_title() {
            return menu_title;
        }

        public void setMenu_title(String menu_title) {
            this.menu_title = menu_title;
        }

        public String getWhatshot() {
            return whatshot;
        }

        public void setWhatshot(String whatshot) {
            this.whatshot = whatshot;
        }

        public String getMenu_counter() {
            return menu_counter;
        }

        public void setMenu_counter(String menu_counter) {
            this.menu_counter = menu_counter;
        }

        public String getMenu_image() {
            return menu_image;
        }

        public void setMenu_image(String menu_image) {
            this.menu_image = menu_image;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }
}
