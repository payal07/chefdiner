package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 09-01-2018.
 */

public class SubCuisineModel {
    /**
     * status : 1
     * message : OK
     * filtersubCuisine : [{"subcuisinename":"All Chinese","counter":0},{"subcuisinename":"Guangdong","counter":0},{"subcuisinename":"Chiu Chow","counter":0},{"subcuisinename":"Hakka","counter":0},{"subcuisinename":"Sichuan","counter":0},{"subcuisinename":"Shanghai","counter":0},{"subcuisinename":"Yunnan","counter":0},{"subcuisinename":"Hunan","counter":0},{"subcuisinename":"Taiwan","counter":0},{"subcuisinename":"Shunde","counter":0},{"subcuisinename":"Beijing","counter":0},{"subcuisinename":"Jingchuanhu","counter":0},{"subcuisinename":"Guangxi","counter":0},{"subcuisinename":"Northeastern","counter":0},{"subcuisinename":"Shandong","counter":0},{"subcuisinename":"Xinjiang","counter":0},{"subcuisinename":"Village Food","counter":0},{"subcuisinename":"Fujian","counter":0},{"subcuisinename":"Jiang-Zhe","counter":0},{"subcuisinename":"Guizhou","counter":0},{"subcuisinename":"Shanxi(Shan)","counter":0},{"subcuisinename":"Mongolia","counter":0},{"subcuisinename":"Shanxi(Jin)","counter":0},{"subcuisinename":"Hubei","counter":0},{"subcuisinename":"Huaiyang","counter":0}]
     */

    private String status;
    private String message;
    private List<FiltersubCuisineBean> filtersubCuisine;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<FiltersubCuisineBean> getFiltersubCuisine() {
        return filtersubCuisine;
    }

    public void setFiltersubCuisine(List<FiltersubCuisineBean> filtersubCuisine) {
        this.filtersubCuisine = filtersubCuisine;
    }

    public static class FiltersubCuisineBean {
        /**
         * subcuisinename : All Chinese
         * counter : 0
         */

        private String subcuisinename;
        private int counter;

        public String getSubcuisinename() {
            return subcuisinename;
        }

        public void setSubcuisinename(String subcuisinename) {
            this.subcuisinename = subcuisinename;
        }

        public int getCounter() {
            return counter;
        }

        public void setCounter(int counter) {
            this.counter = counter;
        }
    }
}
