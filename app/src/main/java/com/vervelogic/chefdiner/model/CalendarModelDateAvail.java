package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 28-12-2017.
 */

public class CalendarModelDateAvail {

    /**
     * status : 1
     * message : OK
     * chefCalendar : [{"calendar_id":"6","chef_id":"0","date":"0000-00-00","ava_status":"","lunch_from":"","lunch_to":"","brunch_from":"","brunch_to":"","dinner_from":"","dinner_to":"","created_at":"2017-12-28 17:32:16","updated_at":"2017-12-28 17:32:16"}]
     */

    private String status;
    private String message;
    private List<ChefCalendarBean> chefCalendar;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ChefCalendarBean> getChefCalendar() {
        return chefCalendar;
    }

    public void setChefCalendar(List<ChefCalendarBean> chefCalendar) {
        this.chefCalendar = chefCalendar;
    }

    public static class ChefCalendarBean {
        /**
         * calendar_id : 6
         * chef_id : 0
         * date : 0000-00-00
         * ava_status :
         * lunch_from :
         * lunch_to :
         * brunch_from :
         * brunch_to :
         * dinner_from :
         * dinner_to :
         * created_at : 2017-12-28 17:32:16
         * updated_at : 2017-12-28 17:32:16
         */

        private String calendar_id;
        private String chef_id;
        private String date;
        private String ava_status;
        private String lunch_from;
        private String lunch_to;
        private String brunch_from;
        private String brunch_to;
        private String dinner_from;
        private String dinner_to;
        private String created_at;
        private String updated_at;

        public String getCalendar_id() {
            return calendar_id;
        }

        public void setCalendar_id(String calendar_id) {
            this.calendar_id = calendar_id;
        }

        public String getChef_id() {
            return chef_id;
        }

        public void setChef_id(String chef_id) {
            this.chef_id = chef_id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getAva_status() {
            return ava_status;
        }

        public void setAva_status(String ava_status) {
            this.ava_status = ava_status;
        }

        public String getLunch_from() {
            return lunch_from;
        }

        public void setLunch_from(String lunch_from) {
            this.lunch_from = lunch_from;
        }

        public String getLunch_to() {
            return lunch_to;
        }

        public void setLunch_to(String lunch_to) {
            this.lunch_to = lunch_to;
        }

        public String getBrunch_from() {
            return brunch_from;
        }

        public void setBrunch_from(String brunch_from) {
            this.brunch_from = brunch_from;
        }

        public String getBrunch_to() {
            return brunch_to;
        }

        public void setBrunch_to(String brunch_to) {
            this.brunch_to = brunch_to;
        }

        public String getDinner_from() {
            return dinner_from;
        }

        public void setDinner_from(String dinner_from) {
            this.dinner_from = dinner_from;
        }

        public String getDinner_to() {
            return dinner_to;
        }

        public void setDinner_to(String dinner_to) {
            this.dinner_to = dinner_to;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
