package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 19-12-2017.
 */

public class HistoryMeals {

    /**
     * status : 1
     * message : OK
     * gethistory : [{"booknow_id":"40","booking_id":"CHEF791069","chef_id":"3","main_user_id":"4","date":"Tue, Dec.12th","time":"10:30","total_seats":"4","menu_id":"20","menu_price":"58","menu_currency":"","booking_status":"Waiting for approval","kitchen_title":"Payal kitchen","kitchen_descrition":"Hey there","user_id":"3","id":"20","menuuserid":"3","menu_title":"Italian Indian","shared_id":"4,3","hours":"0 hours","days":"9 days","kitchenimage":"http://192.168.1.63/chefdiner/assets/kitchengallery/-8843558554.jpg"},{"booknow_id":"41","booking_id":"CHEF45657","chef_id":"3","main_user_id":"4","date":"Tue, Dec.19th","time":"10:30","total_seats":"5","menu_id":"20","menu_price":"58","menu_currency":"","booking_status":"Waiting for approval","kitchen_title":"Payal kitchen","kitchen_descrition":"Hey there","user_id":"3","id":"20","menuuserid":"3","menu_title":"Italian Indian","shared_id":"4","hours":"0 hours","days":"2 days","kitchenimage":"http://192.168.1.63/chefdiner/assets/kitchengallery/-8843558554.jpg"}]
     */

    private String status;
    private String message;
    private List<GethistoryBean> gethistory;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GethistoryBean> getGethistory() {
        return gethistory;
    }

    public void setGethistory(List<GethistoryBean> gethistory) {
        this.gethistory = gethistory;
    }

    public static class GethistoryBean {
        /**
         * booknow_id : 40
         * booking_id : CHEF791069
         * chef_id : 3
         * main_user_id : 4
         * date : Tue, Dec.12th
         * time : 10:30
         * total_seats : 4
         * menu_id : 20
         * menu_price : 58
         * menu_currency :
         * booking_status : Waiting for approval
         * kitchen_title : Payal kitchen
         * kitchen_descrition : Hey there
         * user_id : 3
         * id : 20
         * menuuserid : 3
         * menu_title : Italian Indian
         * shared_id : 4,3
         * hours : 0 hours
         * days : 9 days
         * kitchenimage : http://192.168.1.63/chefdiner/assets/kitchengallery/-8843558554.jpg
         */

        private String booknow_id;
        private String booking_id;
        private String chef_id;
        private String main_user_id;
        private String date;
        private String time;
        private String total_seats;
        private String menu_id;
        private String menu_price;
        private String menu_currency;
        private String booking_status;
        private String kitchen_title;
        private String kitchen_descrition;
        private String user_id;
        private String id;
        private String menuuserid;
        private String menu_title;
        private String shared_id;
        private String hours;
        private String days;
        private String kitchenimage;

        public String getBooknow_id() {
            return booknow_id;
        }

        public void setBooknow_id(String booknow_id) {
            this.booknow_id = booknow_id;
        }

        public String getBooking_id() {
            return booking_id;
        }

        public void setBooking_id(String booking_id) {
            this.booking_id = booking_id;
        }

        public String getChef_id() {
            return chef_id;
        }

        public void setChef_id(String chef_id) {
            this.chef_id = chef_id;
        }

        public String getMain_user_id() {
            return main_user_id;
        }

        public void setMain_user_id(String main_user_id) {
            this.main_user_id = main_user_id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTotal_seats() {
            return total_seats;
        }

        public void setTotal_seats(String total_seats) {
            this.total_seats = total_seats;
        }

        public String getMenu_id() {
            return menu_id;
        }

        public void setMenu_id(String menu_id) {
            this.menu_id = menu_id;
        }

        public String getMenu_price() {
            return menu_price;
        }

        public void setMenu_price(String menu_price) {
            this.menu_price = menu_price;
        }

        public String getMenu_currency() {
            return menu_currency;
        }

        public void setMenu_currency(String menu_currency) {
            this.menu_currency = menu_currency;
        }

        public String getBooking_status() {
            return booking_status;
        }

        public void setBooking_status(String booking_status) {
            this.booking_status = booking_status;
        }

        public String getKitchen_title() {
            return kitchen_title;
        }

        public void setKitchen_title(String kitchen_title) {
            this.kitchen_title = kitchen_title;
        }

        public String getKitchen_descrition() {
            return kitchen_descrition;
        }

        public void setKitchen_descrition(String kitchen_descrition) {
            this.kitchen_descrition = kitchen_descrition;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMenuuserid() {
            return menuuserid;
        }

        public void setMenuuserid(String menuuserid) {
            this.menuuserid = menuuserid;
        }

        public String getMenu_title() {
            return menu_title;
        }

        public void setMenu_title(String menu_title) {
            this.menu_title = menu_title;
        }

        public String getShared_id() {
            return shared_id;
        }

        public void setShared_id(String shared_id) {
            this.shared_id = shared_id;
        }

        public String getHours() {
            return hours;
        }

        public void setHours(String hours) {
            this.hours = hours;
        }

        public String getDays() {
            return days;
        }

        public void setDays(String days) {
            this.days = days;
        }

        public String getKitchenimage() {
            return kitchenimage;
        }

        public void setKitchenimage(String kitchenimage) {
            this.kitchenimage = kitchenimage;
        }
    }
}
