package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 29-12-2017.
 */

public class GuestListModel
{

    /**
     * status : 1
     * message : OK
     * getBookingList : [{"booknow_id":"58","booking_id":"CHEF46487","chef_id":"3","main_user_id":"4","date":"2017-12-29","time":"10:30","total_seats":"5","menu_id":"20","menu_price":"58","menu_currency":"","service_type":"Cooking workshop","food_delivery":"","kitchen_tool":"","first_name":"","last_name":"","email":"","contact_number":"","delivery_address":"","user_msg":"","created_at":"2017-12-27 18:39:55","updated_at":"2017-12-27 18:39:55","status":"1","booking_status":"Waiting for approval","shared_id":"4","payment_type":"","payment_status":"","payment_device":"","id":"20","menu_title":"Italian Indian"},{"booknow_id":"59","booking_id":"CHEF11855","chef_id":"3","main_user_id":"4","date":"2017-12-29","time":"10:30","total_seats":"5","menu_id":"20","menu_price":"58","menu_currency":"","service_type":"Cooking workshop","food_delivery":"","kitchen_tool":"","first_name":"","last_name":"","email":"","contact_number":"","delivery_address":"","user_msg":"","created_at":"2017-12-27 18:39:57","updated_at":"2017-12-27 18:39:57","status":"1","booking_status":"Waiting for approval","shared_id":"4","payment_type":"","payment_status":"","payment_device":"","id":"20","menu_title":"Italian Indian"},{"booknow_id":"61","booking_id":"CHEF53381","chef_id":"3","main_user_id":"4","date":"2017-12-29","time":"10:30","total_seats":"4","menu_id":"20","menu_price":"58","menu_currency":"","service_type":"Cooking workshop","food_delivery":"","kitchen_tool":"","first_name":"","last_name":"","email":"","contact_number":"","delivery_address":"","user_msg":"","created_at":"2017-12-27 18:55:20","updated_at":"2017-12-27 18:55:20","status":"1","booking_status":"Waiting for approval","shared_id":"4","payment_type":"Paypal","payment_status":"Pending","payment_device":"","id":"20","menu_title":"Italian Indian"},{"booknow_id":"63","booking_id":"CHEF25484","chef_id":"3","main_user_id":"4","date":"2017-12-29","time":"10:30","total_seats":"4","menu_id":"20","menu_price":"58","menu_currency":"","service_type":"Cooking workshop","food_delivery":"","kitchen_tool":"","first_name":"","last_name":"","email":"","contact_number":"","delivery_address":"","user_msg":"","created_at":"2017-12-27 18:58:19","updated_at":"2017-12-27 18:58:19","status":"1","booking_status":"Waiting for approval","shared_id":"4","payment_type":"Paypal","payment_status":"Pending","payment_device":"","id":"20","menu_title":"Italian Indian"},{"booknow_id":"66","booking_id":"CHEF40151","chef_id":"3","main_user_id":"4","date":"2017-12-29","time":"10:30","total_seats":"5","menu_id":"80","menu_price":"79","menu_currency":"","service_type":"Restaurent private meal","food_delivery":"","kitchen_tool":"","first_name":"","last_name":"","email":"","contact_number":"","delivery_address":"","user_msg":"","created_at":"2017-12-28 11:18:36","updated_at":"2017-12-28 11:18:36","status":"1","booking_status":"Waiting for approval","shared_id":"4","payment_type":"","payment_status":"","payment_device":"","id":"80","menu_title":"Mexican menu"},{"booknow_id":"67","booking_id":"CHEF967108","chef_id":"3","main_user_id":"4","date":"2017-12-29","time":"10:30","total_seats":"5","menu_id":"80","menu_price":"79","menu_currency":"","service_type":"Restaurent private meal","food_delivery":"","kitchen_tool":"","first_name":"","last_name":"","email":"","contact_number":"","delivery_address":"","user_msg":"","created_at":"2017-12-28 11:26:22","updated_at":"2017-12-28 11:26:22","status":"1","booking_status":"Waiting for approval","shared_id":"4","payment_type":"Stripe","payment_status":"Success","payment_device":"ios","id":"80","menu_title":"Mexican menu"},{"booknow_id":"68","booking_id":"CHEF110824","chef_id":"3","main_user_id":"4","date":"2017-12-29","time":"10:30","total_seats":"5","menu_id":"80","menu_price":"79","menu_currency":"","service_type":"Restaurent private meal","food_delivery":"","kitchen_tool":"","first_name":"","last_name":"","email":"","contact_number":"","delivery_address":"","user_msg":"","created_at":"2017-12-28 11:29:07","updated_at":"2017-12-28 11:29:07","status":"1","booking_status":"Waiting for approval","shared_id":"4","payment_type":"Stripe","payment_status":"Success","payment_device":"ios","id":"80","menu_title":"Mexican menu"},{"booknow_id":"119","booking_id":"CHEF71557","chef_id":"3","main_user_id":"4","date":"2017-12-29","time":"10:30","total_seats":"5","menu_id":"20","menu_price":"58","menu_currency":"","service_type":"Cooking workshop","food_delivery":"","kitchen_tool":"","first_name":"","last_name":"","email":"","contact_number":"","delivery_address":"","user_msg":"","created_at":"2017-12-28 14:36:56","updated_at":"2017-12-28 14:36:56","status":"1","booking_status":"Waiting for approval","shared_id":"4","payment_type":"","payment_status":"","payment_device":"","id":"20","menu_title":"Italian Indian"},{"booknow_id":"120","booking_id":"CHEF033101","chef_id":"3","main_user_id":"4","date":"2017-12-29","time":"10:30","total_seats":"5","menu_id":"20","menu_price":"58","menu_currency":"","service_type":"Cooking workshop","food_delivery":"","kitchen_tool":"","first_name":"","last_name":"","email":"","contact_number":"","delivery_address":"","user_msg":"","created_at":"2017-12-28 14:38:19","updated_at":"2017-12-28 14:38:19","status":"1","booking_status":"Waiting for approval","shared_id":"4","payment_type":"","payment_status":"","payment_device":"","id":"20","menu_title":"Italian Indian"},{"booknow_id":"124","booking_id":"CHEF10289","chef_id":"3","main_user_id":"4","date":"2017-12-29","time":"10:30","total_seats":"5","menu_id":"20","menu_price":"58","menu_currency":"","service_type":"Cooking workshop","food_delivery":"","kitchen_tool":"","first_name":"","last_name":"","email":"","contact_number":"","delivery_address":"","user_msg":"","created_at":"2017-12-28 14:58:40","updated_at":"2017-12-28 14:58:40","status":"1","booking_status":"Waiting for approval","shared_id":"4","payment_type":"","payment_status":"","payment_device":"","id":"20","menu_title":"Italian Indian"},{"booknow_id":"125","booking_id":"CHEF58582","chef_id":"3","main_user_id":"4","date":"2017-12-29","time":"10:30","total_seats":"5","menu_id":"20","menu_price":"58","menu_currency":"","service_type":"Cooking workshop","food_delivery":"","kitchen_tool":"","first_name":"","last_name":"","email":"","contact_number":"","delivery_address":"","user_msg":"","created_at":"2017-12-28 15:00:22","updated_at":"2017-12-28 15:00:22","status":"1","booking_status":"Waiting for approval","shared_id":"4","payment_type":"Paypal","payment_status":"Pending","payment_device":"ios","id":"20","menu_title":"Italian Indian"}]
     */

    private String status;
    private String message;
    private List<GetBookingListBean> getBookingList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetBookingListBean> getGetBookingList() {
        return getBookingList;
    }

    public void setGetBookingList(List<GetBookingListBean> getBookingList) {
        this.getBookingList = getBookingList;
    }

    public static class GetBookingListBean {
        /**
         * booknow_id : 58
         * booking_id : CHEF46487
         * chef_id : 3
         * main_user_id : 4
         * date : 2017-12-29
         * time : 10:30
         * total_seats : 5
         * menu_id : 20
         * menu_price : 58
         * menu_currency :
         * service_type : Cooking workshop
         * food_delivery :
         * kitchen_tool :
         * first_name :
         * last_name :
         * email :
         * contact_number :
         * delivery_address :
         * user_msg :
         * created_at : 2017-12-27 18:39:55
         * updated_at : 2017-12-27 18:39:55
         * status : 1
         * booking_status : Waiting for approval
         * shared_id : 4
         * payment_type :
         * payment_status :
         * payment_device :
         * id : 20
         * menu_title : Italian Indian
         */

        private String booknow_id;
        private String booking_id;
        private String chef_id;
        private String main_user_id;
        private String date;
        private String time;
        private String total_seats;
        private String menu_id;
        private String menu_price;
        private String menu_currency;
        private String service_type;
        private String food_delivery;
        private String kitchen_tool;
        private String first_name;
        private String last_name;
        private String email;
        private String contact_number;
        private String delivery_address;
        private String user_msg;
        private String created_at;
        private String updated_at;
        private String status;
        private String booking_status;
        private String shared_id;
        private String payment_type;
        private String payment_status;
        private String payment_device;
        private String id;
        private String menu_title;

        public String getBooknow_id() {
            return booknow_id;
        }

        public void setBooknow_id(String booknow_id) {
            this.booknow_id = booknow_id;
        }

        public String getBooking_id() {
            return booking_id;
        }

        public void setBooking_id(String booking_id) {
            this.booking_id = booking_id;
        }

        public String getChef_id() {
            return chef_id;
        }

        public void setChef_id(String chef_id) {
            this.chef_id = chef_id;
        }

        public String getMain_user_id() {
            return main_user_id;
        }

        public void setMain_user_id(String main_user_id) {
            this.main_user_id = main_user_id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTotal_seats() {
            return total_seats;
        }

        public void setTotal_seats(String total_seats) {
            this.total_seats = total_seats;
        }

        public String getMenu_id() {
            return menu_id;
        }

        public void setMenu_id(String menu_id) {
            this.menu_id = menu_id;
        }

        public String getMenu_price() {
            return menu_price;
        }

        public void setMenu_price(String menu_price) {
            this.menu_price = menu_price;
        }

        public String getMenu_currency() {
            return menu_currency;
        }

        public void setMenu_currency(String menu_currency) {
            this.menu_currency = menu_currency;
        }

        public String getService_type() {
            return service_type;
        }

        public void setService_type(String service_type) {
            this.service_type = service_type;
        }

        public String getFood_delivery() {
            return food_delivery;
        }

        public void setFood_delivery(String food_delivery) {
            this.food_delivery = food_delivery;
        }

        public String getKitchen_tool() {
            return kitchen_tool;
        }

        public void setKitchen_tool(String kitchen_tool) {
            this.kitchen_tool = kitchen_tool;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContact_number() {
            return contact_number;
        }

        public void setContact_number(String contact_number) {
            this.contact_number = contact_number;
        }

        public String getDelivery_address() {
            return delivery_address;
        }

        public void setDelivery_address(String delivery_address) {
            this.delivery_address = delivery_address;
        }

        public String getUser_msg() {
            return user_msg;
        }

        public void setUser_msg(String user_msg) {
            this.user_msg = user_msg;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getBooking_status() {
            return booking_status;
        }

        public void setBooking_status(String booking_status) {
            this.booking_status = booking_status;
        }

        public String getShared_id() {
            return shared_id;
        }

        public void setShared_id(String shared_id) {
            this.shared_id = shared_id;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getPayment_device() {
            return payment_device;
        }

        public void setPayment_device(String payment_device) {
            this.payment_device = payment_device;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMenu_title() {
            return menu_title;
        }

        public void setMenu_title(String menu_title) {
            this.menu_title = menu_title;
        }
    }
}
