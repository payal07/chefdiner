package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 05-10-2017.
 */

/*---------------------------------------- HOME FRAGMENT FOR LISTING ALL THE CHEF------------------------------------------------*/
public class HomeFragmentModel {
    public String getWhatsHotKey() {
        return whatsHotKey;
    }

    public void setWhatsHotKey(String whatsHotKey) {
        this.whatsHotKey = whatsHotKey;
    }

    private String HKD, chef_id,whatsHotKey,maxprice;
    private String chefRate, bookmarkedCount, chefLName;
    private String chefName, chefAddress, reviewCount, rateCount, happyCount, sadCount, currency,
            foodType1, foodType2, kitchen_title, kitchen_description, language, languageSpeak, serviceType, phoneNumber,
            oftenCook, chefCountry, fromGuest, uptoGuest, chef_latitude, chef_longiude, userFname, userLname, email, is_verify, is_user, is_chef_verify,
            is_chef, deviceId, userLat, userLong,price;
    private String profileUrl = "";
    private String allImages;
    private String cuisine;
    private String access_token = "";

    public String getMaxprice() {
        return maxprice;
    }

    public void setMaxprice(String maxprice) {
        this.maxprice = maxprice;
    }

    public String getDailyMeal() {
        return dailyMeal;
    }

    public void setDailyMeal(String dailyMeal) {
        this.dailyMeal = dailyMeal;
    }

    private String dailyMeal;
    private String imageUrl = "";

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    private String lunch, brunch, dinner, cid;
    private int imageLength;
    private String image2Url = "";
    private String image3Url = "";
    private float rating;

    public String getLunch() {
        return lunch;
    }

    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

    public String getBrunch() {
        return brunch;
    }

    public void setBrunch(String brunch) {
        this.brunch = brunch;
    }

    public String getDinner() {
        return dinner;
    }

    public void setDinner(String dinner) {
        this.dinner = dinner;
    }

    public int getImageLength() {
        return imageLength;
    }

    public void setImageLength(int imageLength) {
        this.imageLength = imageLength;
    }

    public String getIs_chef_verify() {
        return is_chef_verify;
    }

    public void setIs_chef_verify(String is_chef_verify) {
        this.is_chef_verify = is_chef_verify;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguageSpeak() {
        return languageSpeak;
    }

    public void setLanguageSpeak(String languageSpeak) {
        this.languageSpeak = languageSpeak;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOftenCook() {
        return oftenCook;
    }

    public void setOftenCook(String oftenCook) {
        this.oftenCook = oftenCook;
    }

    public String getChefCountry() {
        return chefCountry;
    }

    public void setChefCountry(String chefCountry) {
        this.chefCountry = chefCountry;
    }

    public String getFromGuest() {
        return fromGuest;
    }

    public void setFromGuest(String fromGuest) {
        this.fromGuest = fromGuest;
    }

    public String getUptoGuest() {
        return uptoGuest;
    }

    public void setUptoGuest(String uptoGuest) {
        this.uptoGuest = uptoGuest;
    }

    public String getChef_latitude() {
        return chef_latitude;
    }

    public void setChef_latitude(String chef_latitude) {
        this.chef_latitude = chef_latitude;
    }

    public String getChef_longiude() {
        return chef_longiude;
    }

    public void setChef_longiude(String chef_longiude) {
        this.chef_longiude = chef_longiude;
    }

    public String getUserFname() {
        return userFname;
    }

    public void setUserFname(String userFname) {
        this.userFname = userFname;
    }

    public String getUserLname() {
        return userLname;
    }

    public void setUserLname(String userLname) {
        this.userLname = userLname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIs_verify() {
        return is_verify;
    }

    public void setIs_verify(String is_verify) {
        this.is_verify = is_verify;
    }

    public String getIs_user() {
        return is_user;
    }

    public void setIs_user(String is_user) {
        this.is_user = is_user;
    }

    public String getIs_chef() {
        return is_chef;
    }

    public void setIs_chef(String is_chef) {
        this.is_chef = is_chef;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserLat() {
        return userLat;
    }

    public void setUserLat(String userLat) {
        this.userLat = userLat;
    }

    public String getUserLong() {
        return userLong;
    }

    public void setUserLong(String userLong) {
        this.userLong = userLong;
    }


    public String getChef_id() {
        return chef_id;
    }

    public void setChef_id(String chef_id) {
        this.chef_id = chef_id;
    }

    public String getAllImages() {
        return allImages;
    }

    public void setAllImages(String allImages) {
        this.allImages = allImages;
    }

    public String getChefLName() {
        return chefLName;

    }

    public void setChefLName(String chefLName) {
        this.chefLName = chefLName;
    }

    public String getKitchen_title() {
        return kitchen_title;
    }

    public void setKitchen_title(String kitchen_title) {
        this.kitchen_title = kitchen_title;
    }

    public String getKitchen_description() {
        return kitchen_description;
    }

    public void setKitchen_description(String kitchen_description) {
        this.kitchen_description = kitchen_description;
    }

    public HomeFragmentModel() {
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImage2Url() {
        return image2Url;
    }

    public void setImage2Url(String image2Url) {
        this.image2Url = image2Url;
    }

    public String getImage3Url() {
        return image3Url;
    }

    public void setImage3Url(String image3Url) {
        this.image3Url = image3Url;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public HomeFragmentModel(String HKD, String chefRate, String chefName, String chefAddress, String reviewCount, String rateCount, String happyCount, String sadCount, String foodType1, String foodType2, String bookmarkedCount) {
        this.HKD = HKD;
        this.chefRate = chefRate;
        this.chefName = chefName;
        this.chefAddress = chefAddress;
        this.reviewCount = reviewCount;
        this.rateCount = rateCount;
        this.happyCount = happyCount;
        this.sadCount = sadCount;
        this.foodType1 = foodType1;
        this.foodType2 = foodType2;
        this.bookmarkedCount = bookmarkedCount;
    }

    public String getHKD() {
        return HKD;
    }

    public void setHKD(String HKD) {
        this.HKD = HKD;
    }

    public String getChefRate() {
        return chefRate;
    }

    public void setChefRate(String chefRate) {
        this.chefRate = chefRate;
    }

    public String getChefName() {
        return chefName;
    }

    public void setChefName(String chefName) {
        this.chefName = chefName;
    }

    public String getChefAddress() {
        return chefAddress;
    }

    public void setChefAddress(String chefAddress) {
        this.chefAddress = chefAddress;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getRateCount() {
        return rateCount;
    }

    public void setRateCount(String rateCount) {
        this.rateCount = rateCount;
    }

    public String getHappyCount() {
        return happyCount;
    }

    public void setHappyCount(String happyCount) {
        this.happyCount = happyCount;
    }

    public String getSadCount() {
        return sadCount;
    }

    public void setSadCount(String sadCount) {
        this.sadCount = sadCount;
    }

    public String getFoodType1() {
        return foodType1;
    }

    public void setFoodType1(String foodType1) {
        this.foodType1 = foodType1;
    }

    public String getFoodType2() {
        return foodType2;
    }

    public void setFoodType2(String foodType2) {
        this.foodType2 = foodType2;
    }

    public String getBookmarkedCount() {
        return bookmarkedCount;
    }

    public void setBookmarkedCount(String bookmarkedCount) {
        this.bookmarkedCount = bookmarkedCount;
    }

}
