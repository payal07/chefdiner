package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 06-10-2017.
 */

public class TimeModel {
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public TimeModel(String time) {

        this.time = time;
    }
}
