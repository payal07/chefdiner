package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 17-11-2017.
 */

public class GridItem {
    private String image;


    public GridItem() {
        super();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}