package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 05-01-2018.
 */

public class AvailCreditModel {


    /**
     * status : 1
     * message : OK
     * getReferralAmount : [{"id":"3","credited_amount":0,"currency":"LYD"}]
     */

    private String status;
    private String message;
    private List<GetReferralAmountBean> getReferralAmount;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetReferralAmountBean> getGetReferralAmount() {
        return getReferralAmount;
    }

    public void setGetReferralAmount(List<GetReferralAmountBean> getReferralAmount) {
        this.getReferralAmount = getReferralAmount;
    }

    public static class GetReferralAmountBean {
        /**
         * id : 3
         * credited_amount : 0
         * currency : LYD
         */

        private String id;
        private int credited_amount;
        private String currency;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getCredited_amount() {
            return credited_amount;
        }

        public void setCredited_amount(int credited_amount) {
            this.credited_amount = credited_amount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }
}
