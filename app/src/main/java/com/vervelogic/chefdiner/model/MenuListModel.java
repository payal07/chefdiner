package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 25-10-2017.
 */

/*----------------------------MENU LIST TO SHOW MENU OF THE CHEF------------------------------------------*/
public class MenuListModel {
    private String menuTitle, dishesCount, price, serviceList, menuId, cuisineList, subCuisineList,actualPrice,money;

    public String getStatus1() {
        return status1;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(String actualPrice) {
        this.actualPrice = actualPrice;
    }

    public void setStatus1(String status1) {
        this.status1 = status1;
    }

    private String imageUrl,status1;

    public String getCuisineList() {
        return cuisineList;
    }

    public void setCuisineList(String cuisineList) {
        this.cuisineList = cuisineList;
    }

    public String getSubCuisineList() {
        return subCuisineList;
    }

    public void setSubCuisineList(String subCuisineList) {
        this.subCuisineList = subCuisineList;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getServiceList() {
        return serviceList;
    }

    public void setServiceList(String serviceList) {
        this.serviceList = serviceList;
    }

    public MenuListModel(String menuTitle, String dishesCount, String price, String imageUrl) {
        this.menuTitle = menuTitle;
        this.dishesCount = dishesCount;

        this.price = price;
        this.imageUrl = imageUrl;
    }

    public MenuListModel(String menuTitle, String dishesCount, String price) {
        this.menuTitle = menuTitle;
        this.dishesCount = dishesCount;
        this.price = price;
    }

    public MenuListModel() {
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public String getDishesCount() {
        return dishesCount;
    }

    public void setDishesCount(String dishesCount) {
        this.dishesCount = dishesCount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
