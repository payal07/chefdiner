package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 25-10-2017.
 */

/*------------------------------------DISH LIST CLASS--------------------------------------------------------*/
public class DishListModel {
    private String dishName, dishArea, url, id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DishListModel() {
    }

    public DishListModel(String dishName, String dishArea, String url) {
        this.dishName = dishName;
        this.dishArea = dishArea;

        this.url = url;
    }

    public DishListModel(String dishName, String dishArea) {
        this.dishName = dishName;
        this.dishArea = dishArea;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public String getDishArea() {
        return dishArea;
    }

    public void setDishArea(String dishArea) {
        this.dishArea = dishArea;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
