package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 04-12-2017.
 */

public class OnsiteCookingModel {
    String checkBox;
    boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public OnsiteCookingModel() {
    }

    public OnsiteCookingModel(String checkBox) {
        this.checkBox = checkBox;
    }

    public String getCheckBox() {
        return checkBox;
    }

    public void setCheckBox(String checkBox) {
        this.checkBox = checkBox;
    }
}
