package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 07-12-2017.
 */

public class BookingOutput {

    /**
     * status : 1
     * booknow_id : 1
     * booking_id : 90521
     * message : OK
     */

    private String status;
    private int booknow_id;
    private String booking_id;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getBooknow_id() {
        return booknow_id;
    }

    public void setBooknow_id(int booknow_id) {
        this.booknow_id = booknow_id;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
