package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 16-10-2017.
 */

/*-------------------------------------------MAIN MODEL  CLASS FOR DATABASE------------------------------------------*/
public class UserDetail {
    private String id;
    private String user_id;
    private String fbId;
    private String isVerifyUser,maxPrice;

    public String getCreditedAmount() {
        return creditedAmount;
    }

    public void setCreditedAmount(String creditedAmount) {
        this.creditedAmount = creditedAmount;
    }

    private String isUser;
    private String accessToken;
    private String chefName,creditedAmount;

    public String getOk() {
        return ok;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getRecommended() {
        return recommended;
    }

    public void setRecommended(String recommended) {
        this.recommended = recommended;
    }

    public String getNotRecommended() {
        return notRecommended;
    }

    public void setNotRecommended(String notRecommended) {
        this.notRecommended = notRecommended;
    }

    public String getTotalReview() {
        return totalReview;
    }

    public void setTotalReview(String totalReview) {
        this.totalReview = totalReview;
    }

    public String getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(String avgRating) {
        this.avgRating = avgRating;
    }

    private String chefLastName;
    private String chefCountry,allImages,ok,recommended,notRecommended,totalReview,avgRating;

    public String getAllImages() {
        return allImages;
    }

    public void setAllImages(String allImages) {
        this.allImages = allImages;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getFavCount() {
        return favCount;
    }

    public void setFavCount(String favCount) {
        this.favCount = favCount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    private String chefCity;
    private String cheflat;
    private String chefLong;
    private String cuisine;
    private String fav,distance,referralCode,favCount,price;
    private String FirstName, LastName, PhoneNumber, EmailId, language, authToken, deviceId, oftenCook, country,
            city, max_guest, min_guest, kitchenTitle, kitchenDescription, lunch, brunch, dinner;
    private String languageList, serviceType, password, isChef, latitude, longitude, countryCode, isChefVerify, currency;
    private String profilePic = "";

    private String chef_id, date, time, noOfSeats, menuTitle, menuId, menuPrice, foodDelivery, kitchenTool, deliveryAddress, userMsg,
            bookingId, booNowId;


    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getIsVerifyUser() {
        return isVerifyUser;
    }

    public void setIsVerifyUser(String isVerifyUser) {
        this.isVerifyUser = isVerifyUser;
    }

    public String getIsUser() {
        return isUser;
    }

    public void setIsUser(String isUser) {
        this.isUser = isUser;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getChefName() {
        return chefName;
    }

    public void setChefName(String chefName) {
        this.chefName = chefName;
    }

    public String getChefLastName() {
        return chefLastName;
    }

    public void setChefLastName(String chefLastName) {
        this.chefLastName = chefLastName;
    }

    public String getChefCountry() {
        return chefCountry;
    }

    public void setChefCountry(String chefCountry) {
        this.chefCountry = chefCountry;
    }

    public String getChefCity() {
        return chefCity;
    }

    public void setChefCity(String chefCity) {
        this.chefCity = chefCity;
    }

    public String getCheflat() {
        return cheflat;
    }

    public void setCheflat(String cheflat) {
        this.cheflat = cheflat;
    }

    public String getChefLong() {
        return chefLong;
    }

    public void setChefLong(String chefLong) {
        this.chefLong = chefLong;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public String getFav() {
        return fav;
    }

    public void setFav(String fav) {
        this.fav = fav;
    }


    public UserDetail(String user_id, String date, String time, String noOfSeats, String menuTitle, String menuId,
                      String menuPrice, String currency, String serviceType, String foodDelivery, String kitchenTool,
                      String firstName, String lastName, String emailId, String phoneNumber, String deliveryAddress,
                      String userMsg, String bookingId, String booNowId) {
        this.user_id = user_id;
        FirstName = firstName;
        LastName = lastName;
        PhoneNumber = phoneNumber;
        EmailId = emailId;
        this.serviceType = serviceType;
        this.currency = currency;
        this.date = date;
        this.time = time;
        this.noOfSeats = noOfSeats;
        this.menuTitle = menuTitle;
        this.menuId = menuId;
        this.menuPrice = menuPrice;
        this.foodDelivery = foodDelivery;
        this.kitchenTool = kitchenTool;
        this.deliveryAddress = deliveryAddress;
        this.userMsg = userMsg;
        this.bookingId = bookingId;
        this.booNowId = booNowId;
    }

    public String getChef_id() {
        return chef_id;
    }

    public void setChef_id(String chef_id) {
        this.chef_id = chef_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(String noOfSeats) {
        this.noOfSeats = noOfSeats;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(String menuPrice) {
        this.menuPrice = menuPrice;
    }

    public String getFoodDelivery() {
        return foodDelivery;
    }

    public void setFoodDelivery(String foodDelivery) {
        this.foodDelivery = foodDelivery;
    }

    public String getKitchenTool() {
        return kitchenTool;
    }

    public void setKitchenTool(String kitchenTool) {
        this.kitchenTool = kitchenTool;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getUserMsg() {
        return userMsg;
    }

    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getBooNowId() {
        return booNowId;
    }

    public void setBooNowId(String booNowId) {
        this.booNowId = booNowId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBrunch() {
        return brunch;
    }

    public void setBrunch(String brunch) {
        this.brunch = brunch;
    }

    public String getDinner() {
        return dinner;
    }

    public void setDinner(String dinner) {
        this.dinner = dinner;
    }

    public String getLunch() {
        return lunch;
    }

    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    public String getIsChefVerify() {
        return isChefVerify;
    }

    public void setIsChefVerify(String isChefVerify) {
        this.isChefVerify = isChefVerify;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserDetail(String id, String firstName, String lastName, String language, String languageList, String phoneNumber,
                      String country, String city, String min_guest, String max_guest,
                      String kitchenTitle, String kitchenDescription, String serviceType, String oftenCook) {
        this.id = id;
        FirstName = firstName;
        LastName = lastName;
        PhoneNumber = phoneNumber;
        this.language = language;
        this.oftenCook = oftenCook;
        this.country = country;
        this.city = city;
        this.max_guest = max_guest;
        this.min_guest = min_guest;
        this.kitchenTitle = kitchenTitle;
        this.kitchenDescription = kitchenDescription;
        this.languageList = languageList;
        this.serviceType = serviceType;
    }

    public UserDetail() {
    }

    public UserDetail(String firstName, String lastName, String phoneNumber, String emailId, String password) {

        FirstName = firstName;
        LastName = lastName;
        EmailId = emailId;
        this.password = password;
        PhoneNumber = phoneNumber;
    }

    public UserDetail(String firstName, String lastName, String emailId, String phoneNumber, String password,
                      String authToken, String profilePic, String language, String languageList, String oftenCook,
                      String country, String city, String min_guest, String max_guest, String serviceType, String isChef,
                      String isChefVerify, String countryCode, String kitchenTitle, String kitchenDescription,
                      String latitude, String longitude, String lunch, String brunch, String dinner) {
        FirstName = firstName;
        LastName = lastName;
        PhoneNumber = phoneNumber;
        EmailId = emailId;
        this.language = language;
        this.authToken = authToken;
        this.oftenCook = oftenCook;
        this.country = country;
        this.city = city;
        this.max_guest = max_guest;
        this.min_guest = min_guest;
        this.kitchenTitle = kitchenTitle;
        this.kitchenDescription = kitchenDescription;
        this.lunch = lunch;
        this.brunch = brunch;
        this.dinner = dinner;
        this.languageList = languageList;
        this.serviceType = serviceType;
        this.password = password;
        this.isChef = isChef;
        this.latitude = latitude;
        this.longitude = longitude;
        this.countryCode = countryCode;
        this.isChefVerify = isChefVerify;
        this.profilePic = profilePic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return EmailId;
    }

    public String getIsChef() {
        return isChef;
    }

    public void setIsChef(String isChef) {
        this.isChef = isChef;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getOftenCook() {
        return oftenCook;
    }

    public void setOftenCook(String oftenCook) {
        this.oftenCook = oftenCook;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMax_guest() {
        return max_guest;
    }

    public void setMax_guest(String max_guest) {
        this.max_guest = max_guest;
    }

    public String getMin_guest() {
        return min_guest;
    }

    public void setMin_guest(String min_guest) {
        this.min_guest = min_guest;
    }

    public String getKitchenTitle() {
        return kitchenTitle;
    }

    public void setKitchenTitle(String kitchenTitle) {
        this.kitchenTitle = kitchenTitle;
    }

    public String getKitchenDescription() {
        return kitchenDescription;
    }

    public void setKitchenDescription(String kitchenDescription) {
        this.kitchenDescription = kitchenDescription;
    }

    public String getLanguageList() {
        return languageList;
    }

    public void setLanguageList(String languageList) {
        this.languageList = languageList;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
