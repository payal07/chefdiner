package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 21-11-2017.
 */

public class CuisinesModel {


    /**
     * status : 1
     * message : OK
     * filterCuisine : [{"cuisinename":"Hong Kong Style","counter":2},{"cuisinename":"Chinese","counter":2},{"cuisinename":"Cantonese","counter":2},{"cuisinename":"Taiwanese","counter":2},{"cuisinename":"Japanese","counter":1},{"cuisinename":"Other Asians","counter":2},{"cuisinename":"Italian","counter":2},{"cuisinename":"Middle East/Mediterranean","counter":1}]
     */

    private String status;
    private String message;
    private List<FilterCuisineBean> filterCuisine;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<FilterCuisineBean> getFilterCuisine() {
        return filterCuisine;
    }

    public void setFilterCuisine(List<FilterCuisineBean> filterCuisine) {
        this.filterCuisine = filterCuisine;
    }

    public static class FilterCuisineBean {
        /**
         * cuisinename : Hong Kong Style
         * counter : 2
         */

        private String cuisinename;
        private int counter;

        public String getCuisinename() {
            return cuisinename;
        }

        public void setCuisinename(String cuisinename) {
            this.cuisinename = cuisinename;
        }

        public int getCounter() {
            return counter;
        }

        public void setCounter(int counter) {
            this.counter = counter;
        }
    }
}


