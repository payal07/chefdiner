package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 06-10-2017.
 */

/*------------------------------------MENU LIST (HORIZONTAL)---------------------------------------------*/
public class HorizontalScrollModel {
    private String menu_title, hkd, dish_name, dish_desc, food_type;
    private boolean isSelected;

    public void setSelected(boolean selection) {
        this.isSelected = selection;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public String getDish_name() {
        return dish_name;
    }

    public void setDish_name(String dish_name) {
        this.dish_name = dish_name;
    }

    public String getDish_desc() {
        return dish_desc;
    }

    public void setDish_desc(String dish_desc) {
        this.dish_desc = dish_desc;
    }

    public String getFood_type() {
        return food_type;
    }

    public void setFood_type(String food_type) {
        this.food_type = food_type;
    }

    public HorizontalScrollModel() {
    }

    public HorizontalScrollModel(String menu_title, String hkd) {
        this.menu_title = menu_title;
        this.hkd = hkd;
    }

    public HorizontalScrollModel(String menu_title, String hkd, String dish_name, String dish_desc, String food_type) {
        this.menu_title = menu_title;
        this.hkd = hkd;
        this.dish_name = dish_name;
        this.dish_desc = dish_desc;
        this.food_type = food_type;
    }

    public HorizontalScrollModel(String dish_name, String dish_desc, String food_type) {
        this.dish_name = dish_name;
        this.dish_desc = dish_desc;

        this.food_type = food_type;
    }

    public String getMenu_title() {
        return menu_title;
    }

    public void setMenu_title(String menu_title) {
        this.menu_title = menu_title;
    }

    public String getHkd() {
        return hkd;
    }

    public void setHkd(String hkd) {
        this.hkd = hkd;
    }


}
