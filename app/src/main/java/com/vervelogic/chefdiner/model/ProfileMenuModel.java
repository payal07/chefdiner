package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 07-10-2017.
 */

/*-------------------------------------------CHEF PROFILE MENU LIST---------------------------------------*/
public class ProfileMenuModel {
    private String url;

    public String getWhatsHotKey() {
        return whatsHotKey;
    }

    public void setWhatsHotKey(String whatsHotKey) {
        this.whatsHotKey = whatsHotKey;
    }

    private String dishName,perGuest,whatshot,whatsHotKey;

    public String getWhatshot() {
        return whatshot;
    }

    public void setWhatshot(String whatshot) {
        this.whatshot = whatshot;
    }

    public ProfileMenuModel(String dishName, String perGuest) {
        this.dishName = dishName;
        this.perGuest = perGuest;

    }

    public ProfileMenuModel() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public String getPerGuest() {
        return perGuest;
    }

    public void setPerGuest(String perGuest) {
        this.perGuest = perGuest;
    }
}
