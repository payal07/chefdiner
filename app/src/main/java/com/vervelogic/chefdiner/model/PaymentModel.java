package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 27-12-2017.
 */

public class PaymentModel {


    /**
     * status : 1
     * message : OK
     * getDetails : [{"booknow_id":"60","booking_id":"60","chef_id":"4","main_user_id":"3","date":"2017-12-28","time":"1:20","total_seats":"4","menu_id":"13","menu_price":"78","menu_currency":"CHF","service_type":"Cooking workshop","food_delivery":"","kitchen_tool":"","first_name":"Payal","last_name":"Tak","email":"payalverve@gmail.com","contact_number":"1234567890","delivery_address":"","user_msg":"bd","created_at":"2017-12-27 18:50:07","updated_at":"2017-12-27 18:50:07","status":"1","booking_status":"Waiting for approval","shared_id":"3","payment_type":"Paypal","payment_status":"Pending","payement_id":"10","txn_id":"9BA27518BR5622211","payment_gross":"78.00","currency_code":"USD","device":"android"}]
     */

    private String status;
    private String message;
    private List<GetDetailsBean> getDetails;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetDetailsBean> getGetDetails() {
        return getDetails;
    }

    public void setGetDetails(List<GetDetailsBean> getDetails) {
        this.getDetails = getDetails;
    }

    public static class GetDetailsBean {
        /**
         * booknow_id : 60
         * booking_id : 60
         * chef_id : 4
         * main_user_id : 3
         * date : 2017-12-28
         * time : 1:20
         * total_seats : 4
         * menu_id : 13
         * menu_price : 78
         * menu_currency : CHF
         * service_type : Cooking workshop
         * food_delivery :
         * kitchen_tool :
         * first_name : Payal
         * last_name : Tak
         * email : payalverve@gmail.com
         * contact_number : 1234567890
         * delivery_address :
         * user_msg : bd
         * created_at : 2017-12-27 18:50:07
         * updated_at : 2017-12-27 18:50:07
         * status : 1
         * booking_status : Waiting for approval
         * shared_id : 3
         * payment_type : Paypal
         * payment_status : Pending
         * payement_id : 10
         * txn_id : 9BA27518BR5622211
         * payment_gross : 78.00
         * currency_code : USD
         * device : android
         */

        private String booknow_id;
        private String booking_id;
        private String chef_id;
        private String main_user_id;
        private String date;
        private String time;
        private String total_seats;
        private String menu_id;
        private String menu_price;
        private String menu_currency;
        private String service_type;
        private String food_delivery;
        private String kitchen_tool;
        private String first_name;
        private String last_name;
        private String email;
        private String contact_number;
        private String delivery_address;
        private String user_msg;
        private String created_at;
        private String updated_at;
        private String status;
        private String booking_status;
        private String shared_id;
        private String payment_type;
        private String payment_status;
        private String payement_id;
        private String txn_id;
        private String payment_gross;
        private String currency_code;
        private String device;

        public String getBooknow_id() {
            return booknow_id;
        }

        public void setBooknow_id(String booknow_id) {
            this.booknow_id = booknow_id;
        }

        public String getBooking_id() {
            return booking_id;
        }

        public void setBooking_id(String booking_id) {
            this.booking_id = booking_id;
        }

        public String getChef_id() {
            return chef_id;
        }

        public void setChef_id(String chef_id) {
            this.chef_id = chef_id;
        }

        public String getMain_user_id() {
            return main_user_id;
        }

        public void setMain_user_id(String main_user_id) {
            this.main_user_id = main_user_id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTotal_seats() {
            return total_seats;
        }

        public void setTotal_seats(String total_seats) {
            this.total_seats = total_seats;
        }

        public String getMenu_id() {
            return menu_id;
        }

        public void setMenu_id(String menu_id) {
            this.menu_id = menu_id;
        }

        public String getMenu_price() {
            return menu_price;
        }

        public void setMenu_price(String menu_price) {
            this.menu_price = menu_price;
        }

        public String getMenu_currency() {
            return menu_currency;
        }

        public void setMenu_currency(String menu_currency) {
            this.menu_currency = menu_currency;
        }

        public String getService_type() {
            return service_type;
        }

        public void setService_type(String service_type) {
            this.service_type = service_type;
        }

        public String getFood_delivery() {
            return food_delivery;
        }

        public void setFood_delivery(String food_delivery) {
            this.food_delivery = food_delivery;
        }

        public String getKitchen_tool() {
            return kitchen_tool;
        }

        public void setKitchen_tool(String kitchen_tool) {
            this.kitchen_tool = kitchen_tool;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContact_number() {
            return contact_number;
        }

        public void setContact_number(String contact_number) {
            this.contact_number = contact_number;
        }

        public String getDelivery_address() {
            return delivery_address;
        }

        public void setDelivery_address(String delivery_address) {
            this.delivery_address = delivery_address;
        }

        public String getUser_msg() {
            return user_msg;
        }

        public void setUser_msg(String user_msg) {
            this.user_msg = user_msg;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getBooking_status() {
            return booking_status;
        }

        public void setBooking_status(String booking_status) {
            this.booking_status = booking_status;
        }

        public String getShared_id() {
            return shared_id;
        }

        public void setShared_id(String shared_id) {
            this.shared_id = shared_id;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getPayement_id() {
            return payement_id;
        }

        public void setPayement_id(String payement_id) {
            this.payement_id = payement_id;
        }

        public String getTxn_id() {
            return txn_id;
        }

        public void setTxn_id(String txn_id) {
            this.txn_id = txn_id;
        }

        public String getPayment_gross() {
            return payment_gross;
        }

        public void setPayment_gross(String payment_gross) {
            this.payment_gross = payment_gross;
        }

        public String getCurrency_code() {
            return currency_code;
        }

        public void setCurrency_code(String currency_code) {
            this.currency_code = currency_code;
        }

        public String getDevice() {
            return device;
        }

        public void setDevice(String device) {
            this.device = device;
        }
    }
}
