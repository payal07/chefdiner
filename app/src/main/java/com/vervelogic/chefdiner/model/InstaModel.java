package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 07-10-2017.
 */

/*------------------------------INSTAGRAM IMAGES------------------------------------------------*/
public class InstaModel {
     private String uri;

    public InstaModel() {
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
