package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 07-12-2017.
 */

public class CuisineSubCuisineModel {

    /**
     * status : 1
     * message : OK
     * cuisine : [{"cuisine_id":"1","cuisine_name":"Hong Kong Style","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"Hong Kong Style"}]},{"cuisine_id":"2","cuisine_name":"Chinese","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"All Chinese"},{"subcuisine_id":"2","subcuisine_name":"Guangdong"},{"subcuisine_id":"3","subcuisine_name":"Chiu Chow"},{"subcuisine_id":"4","subcuisine_name":"Hakka"},{"subcuisine_id":"5","subcuisine_name":"Sichuan"},{"subcuisine_id":"6","subcuisine_name":"Shanghai"},{"subcuisine_id":"7","subcuisine_name":"Yunnan"},{"subcuisine_id":"8","subcuisine_name":"Hunan"},{"subcuisine_id":"9","subcuisine_name":"Taiwan"},{"subcuisine_id":"10","subcuisine_name":"Shunde"},{"subcuisine_id":"11","subcuisine_name":"Beijing"},{"subcuisine_id":"12","subcuisine_name":"Jingchuanhu"},{"subcuisine_id":"13","subcuisine_name":"Guangxi"},{"subcuisine_id":"14","subcuisine_name":"Northeastern"},{"subcuisine_id":"15","subcuisine_name":"Shandong"},{"subcuisine_id":"16","subcuisine_name":"Xinjiang"},{"subcuisine_id":"17","subcuisine_name":"Village Food"},{"subcuisine_id":"18","subcuisine_name":"Fujian"},{"subcuisine_id":"19","subcuisine_name":"Jiang-Zhe"},{"subcuisine_id":"20","subcuisine_name":"Guizhou"},{"subcuisine_id":"21","subcuisine_name":"Shanxi(Shan)"},{"subcuisine_id":"22","subcuisine_name":"Mongolia"},{"subcuisine_id":"23","subcuisine_name":"Shanxi(Jin)"},{"subcuisine_id":"24","subcuisine_name":"Hubei"},{"subcuisine_id":"25","subcuisine_name":"Huaiyang"}]},{"cuisine_id":"3","cuisine_name":"Cantonese","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"All Cantonese"},{"subcuisine_id":"2","subcuisine_name":"Guangdong"},{"subcuisine_id":"3","subcuisine_name":"Chiu Chow"},{"subcuisine_id":"4","subcuisine_name":"Hakka"},{"subcuisine_id":"5","subcuisine_name":"Shunde"}]},{"cuisine_id":"4","cuisine_name":"Taiwanese","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"Taiwanese"}]},{"cuisine_id":"5","cuisine_name":"Japanese","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"Japanese"}]},{"cuisine_id":"6","cuisine_name":"Korean","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"Korean"}]},{"cuisine_id":"7","cuisine_name":"Thai","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"Thai"}]},{"cuisine_id":"8","cuisine_name":"Other Asians","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"All Asian"},{"subcuisine_id":"2","subcuisine_name":"Vietnamese"},{"subcuisine_id":"3","subcuisine_name":"Indonesian"},{"subcuisine_id":"4","subcuisine_name":"Singaporean"},{"subcuisine_id":"5","subcuisine_name":"Malaysian"},{"subcuisine_id":"6","subcuisine_name":"Philippines"},{"subcuisine_id":"7","subcuisine_name":"Indian"},{"subcuisine_id":"8","subcuisine_name":"Nepalese"},{"subcuisine_id":"9","subcuisine_name":"Sri Lanka"}]},{"cuisine_id":"9","cuisine_name":"Italian","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"Italian"}]},{"cuisine_id":"10","cuisine_name":"French","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"French"}]},{"cuisine_id":"11","cuisine_name":"Western","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"All Western"},{"subcuisine_id":"2","subcuisine_name":"Italian"},{"subcuisine_id":"3","subcuisine_name":"French"},{"subcuisine_id":"4","subcuisine_name":"American"},{"subcuisine_id":"5","subcuisine_name":"British"},{"subcuisine_id":"6","subcuisine_name":"Spanish"},{"subcuisine_id":"7","subcuisine_name":"German"},{"subcuisine_id":"8","subcuisine_name":"Belgian"},{"subcuisine_id":"9","subcuisine_name":"Australian"},{"subcuisine_id":"10","subcuisine_name":"Portuguese"},{"subcuisine_id":"11","subcuisine_name":"Swiss"},{"subcuisine_id":"12","subcuisine_name":"Irish"},{"subcuisine_id":"13","subcuisine_name":"Russian"},{"subcuisine_id":"14","subcuisine_name":"Dutch"},{"subcuisine_id":"15","subcuisine_name":"Austrian"},{"subcuisine_id":"16","subcuisine_name":"Western"}]},{"cuisine_id":"12","cuisine_name":"Middle East/Mediterranean","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"All Middle Eastern / Mediterranean"},{"subcuisine_id":"2","subcuisine_name":"Middle Eastern"},{"subcuisine_id":"3","subcuisine_name":"Mediterranean"},{"subcuisine_id":"4","subcuisine_name":"Turkish"},{"subcuisine_id":"5","subcuisine_name":"Lebanon"},{"subcuisine_id":"6","subcuisine_name":"Moroccan"},{"subcuisine_id":"7","subcuisine_name":"Egyptian"},{"subcuisine_id":"8","subcuisine_name":"Afrian"},{"subcuisine_id":"9","subcuisine_name":"Jewish"},{"subcuisine_id":"10","subcuisine_name":"Greek"}]},{"cuisine_id":"13","cuisine_name":"Latin American","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"All Latin American"},{"subcuisine_id":"2","subcuisine_name":"Mexican"},{"subcuisine_id":"3","subcuisine_name":"Cuba"},{"subcuisine_id":"4","subcuisine_name":"Aargentinian"},{"subcuisine_id":"5","subcuisine_name":"Peruvian"},{"subcuisine_id":"6","subcuisine_name":"Brazillian"}]},{"cuisine_id":"14","cuisine_name":"Multinational","subCuisne":[{"subcuisine_id":"1","subcuisine_name":"Multinational"}]}]
     */

    private String status;
    private String message;
    private List<CuisineBean> cuisine;
    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CuisineBean> getCuisine() {
        return cuisine;
    }

    public void setCuisine(List<CuisineBean> cuisine) {
        this.cuisine = cuisine;
    }

    public static class CuisineBean {
        /**
         * cuisine_id : 1
         * cuisine_name : Hong Kong Style
         * subCuisne : [{"subcuisine_id":"1","subcuisine_name":"Hong Kong Style"}]
         */

        private String cuisine_id;
        private String cuisine_name;
        private boolean isSelectedCuisine = false;
        private List<SubCuisneBean> subCuisne;
        private int c;

        public int getC() {
            return c;
        }

        public void setC(int c) {
            this.c = c;
        }

        public boolean isSelectedCuisine() {
            return isSelectedCuisine;
        }

        public void setSelectedCuisine(boolean selectedCuisine) {
            isSelectedCuisine = selectedCuisine;
        }

        public String getCuisine_id() {
            return cuisine_id;
        }

        public void setCuisine_id(String cuisine_id) {
            this.cuisine_id = cuisine_id;
        }

        public String getCuisine_name() {
            return cuisine_name;
        }

        public void setCuisine_name(String cuisine_name) {
            this.cuisine_name = cuisine_name;
        }

        public List<SubCuisneBean> getSubCuisne() {
            return subCuisne;
        }

        public void setSubCuisne(List<SubCuisneBean> subCuisne) {
            this.subCuisne = subCuisne;
        }

        public static class SubCuisneBean {
            /**
             * subcuisine_id : 1
             * subcuisine_name : Hong Kong Style
             */

            private String subcuisine_id;
            private String subcuisine_name;
            private boolean isChecked = false;
            private int subCuisineCount;

            public boolean isChecked() {
                return isChecked;
            }

            public int getSubCuisineCount() {
                return subCuisineCount;
            }

            public void setSubCuisineCount(int subCuisineCount) {
                this.subCuisineCount = subCuisineCount;
            }

            public void setChecked(boolean checked) {
                isChecked = checked;
            }

            public String getSubcuisine_id() {
                return subcuisine_id;
            }

            public void setSubcuisine_id(String subcuisine_id) {
                this.subcuisine_id = subcuisine_id;
            }

            public String getSubcuisine_name() {
                return subcuisine_name;
            }

            public void setSubcuisine_name(String subcuisine_name) {
                this.subcuisine_name = subcuisine_name;
            }
        }
    }
}
