package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 03-01-2018.
 */

public class ReviewModelClass {


    /**
     * status : 1
     * message : OK
     * getRRlist : [{"rr_id":"67","chef_id":"3","main_user_id":"4","avr_rating":"3.2","review":"Hhg","images":"image06.jpeg","created_at":"2018-01-04 15:50:27","updated_at":"2018-01-04 15:50:27","id":"4","first_name":"Mukesh","last_name":"Sharma","profile_pic":"http://192.168.1.63/chefdiner/assets/chefprofile/image36.jpeg","newimages":["http://192.168.1.63/chefdiner/assets/reviewrating/image06.jpeg"],"newdate":"04 January 2018 | 15:50","likecount":2,"likeKey":"no"}]
     */

    private String status;
    private String message;
    private List<GetRRlistBean> getRRlist;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetRRlistBean> getGetRRlist() {
        return getRRlist;
    }

    public void setGetRRlist(List<GetRRlistBean> getRRlist) {
        this.getRRlist = getRRlist;
    }

    public static class GetRRlistBean {
        /**
         * rr_id : 67
         * chef_id : 3
         * main_user_id : 4
         * avr_rating : 3.2
         * review : Hhg
         * images : image06.jpeg
         * created_at : 2018-01-04 15:50:27
         * updated_at : 2018-01-04 15:50:27
         * id : 4
         * first_name : Mukesh
         * last_name : Sharma
         * profile_pic : http://192.168.1.63/chefdiner/assets/chefprofile/image36.jpeg
         * newimages : ["http://192.168.1.63/chefdiner/assets/reviewrating/image06.jpeg"]
         * newdate : 04 January 2018 | 15:50
         * likecount : 2
         * likeKey : no
         */

        private String rr_id;
        private String chef_id;
        private String main_user_id;
        private String avr_rating;
        private String review;
        private String images;
        private String created_at;
        private String updated_at;
        private String id;
        private String first_name;
        private String last_name;
        private String profile_pic;
        private String newdate;
        private int likecount;
        private String likeKey;
        private List<String> newimages;

        public String getRr_id() {
            return rr_id;
        }

        public void setRr_id(String rr_id) {
            this.rr_id = rr_id;
        }

        public String getChef_id() {
            return chef_id;
        }

        public void setChef_id(String chef_id) {
            this.chef_id = chef_id;
        }

        public String getMain_user_id() {
            return main_user_id;
        }

        public void setMain_user_id(String main_user_id) {
            this.main_user_id = main_user_id;
        }

        public String getAvr_rating() {
            return avr_rating;
        }

        public void setAvr_rating(String avr_rating) {
            this.avr_rating = avr_rating;
        }

        public String getReview() {
            return review;
        }

        public void setReview(String review) {
            this.review = review;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }

        public String getNewdate() {
            return newdate;
        }

        public void setNewdate(String newdate) {
            this.newdate = newdate;
        }

        public int getLikecount() {
            return likecount;
        }

        public void setLikecount(int likecount) {
            this.likecount = likecount;
        }

        public String getLikeKey() {
            return likeKey;
        }

        public void setLikeKey(String likeKey) {
            this.likeKey = likeKey;
        }

        public List<String> getNewimages() {
            return newimages;
        }

        public void setNewimages(List<String> newimages) {
            this.newimages = newimages;
        }
    }
}
