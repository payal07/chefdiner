package com.vervelogic.chefdiner.model;

import java.util.List;

/**
 * Created by Verve on 19-12-2017.
 */

public class UpcomingData {


    /**
     * status : 1
     * message : OK
     * getupcoming : [{"booknow_id":"44","booking_id":"CHEF87746","chef_id":"4","main_user_id":"3","date":"Fri, Dec.22nd","time":"1:20","total_seats":"5","menu_id":"13","menu_price":"78","menu_currency":"CHF","booking_status":"Waiting for approval","kitchen_title":"verve kitchen","kitchen_descrition":"Hey there","user_id":"4","id":"13","menuuserid":"4","menu_title":"thai","shared_id":"3","olddate":"2017-12-22","hours":"14 hours","days":"0 days","kitchenimage":"http://192.168.1.63/chefdiner/assets/kitchengallery/image113.jpeg"},
     * {"booknow_id":"43","booking_id":"CHEF06867","chef_id":"4","main_user_id":"3","date":"Sat, Dec.23rd","time":"1:20","total_seats":"4","menu_id":"13","menu_price":"78","menu_currency":"CHF","booking_status":"Waiting for approval","kitchen_title":"verve kitchen","kitchen_descrition":"Hey there","user_id":"4","id":"13","menuuserid":"4","menu_title":"thai","shared_id":"3","olddate":"2017-12-23","hours":"9 hours","days":"0 days","kitchenimage":"http://192.168.1.63/chefdiner/assets/kitchengallery/image113.jpeg"},{"booknow_id":"45","booking_id":"CHEF810789","chef_id":"4","main_user_id":"3","date":"Fri, Dec.29th","time":"4:00","total_seats":"6","menu_id":"13","menu_price":"78","menu_currency":"CHF","booking_status":"Waiting for approval","kitchen_title":"verve kitchen","kitchen_descrition":"Hey there","user_id":"4","id":"13","menuuserid":"4","menu_title":"thai","shared_id":"3","olddate":"2017-12-29","hours":"12 hours","days":"6 days","kitchenimage":"http://192.168.1.63/chefdiner/assets/kitchengallery/image113.jpeg"}]
     */

    private String status;
    private String message;
    private List<GetupcomingBean> getupcoming;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetupcomingBean> getGetupcoming() {
        return getupcoming;
    }

    public void setGetupcoming(List<GetupcomingBean> getupcoming) {
        this.getupcoming = getupcoming;
    }

    public static class GetupcomingBean {
        /**
         * booknow_id : 44
         * booking_id : CHEF87746
         * chef_id : 4
         * main_user_id : 3
         * date : Fri, Dec.22nd
         * time : 1:20
         * total_seats : 5
         * menu_id : 13
         * menu_price : 78
         * menu_currency : CHF
         * booking_status : Waiting for approval
         * kitchen_title : verve kitchen
         * kitchen_descrition : Hey there
         * user_id : 4
         * id : 13
         * menuuserid : 4
         * menu_title : thai
         * shared_id : 3
         * olddate : 2017-12-22
         * hours : 14 hours
         * days : 0 days
         * kitchenimage : http://192.168.1.63/chefdiner/assets/kitchengallery/image113.jpeg
         */

        private String booknow_id;
        private String booking_id;
        private String chef_id;
        private String main_user_id;
        private String date;
        private String time;
        private String total_seats;
        private String menu_id;
        private String menu_price;
        private String menu_currency;
        private String booking_status;
        private String kitchen_title;
        private String kitchen_descrition;
        private String user_id;
        private String id;
        private String menuuserid;
        private String menu_title;
        private String shared_id;
        private String olddate;
        private String hours;
        private String days;
        private String kitchenimage;

        public String getBooknow_id() {
            return booknow_id;
        }

        public void setBooknow_id(String booknow_id) {
            this.booknow_id = booknow_id;
        }

        public String getBooking_id() {
            return booking_id;
        }

        public void setBooking_id(String booking_id) {
            this.booking_id = booking_id;
        }

        public String getChef_id() {
            return chef_id;
        }

        public void setChef_id(String chef_id) {
            this.chef_id = chef_id;
        }

        public String getMain_user_id() {
            return main_user_id;
        }

        public void setMain_user_id(String main_user_id) {
            this.main_user_id = main_user_id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTotal_seats() {
            return total_seats;
        }

        public void setTotal_seats(String total_seats) {
            this.total_seats = total_seats;
        }

        public String getMenu_id() {
            return menu_id;
        }

        public void setMenu_id(String menu_id) {
            this.menu_id = menu_id;
        }

        public String getMenu_price() {
            return menu_price;
        }

        public void setMenu_price(String menu_price) {
            this.menu_price = menu_price;
        }

        public String getMenu_currency() {
            return menu_currency;
        }

        public void setMenu_currency(String menu_currency) {
            this.menu_currency = menu_currency;
        }

        public String getBooking_status() {
            return booking_status;
        }

        public void setBooking_status(String booking_status) {
            this.booking_status = booking_status;
        }

        public String getKitchen_title() {
            return kitchen_title;
        }

        public void setKitchen_title(String kitchen_title) {
            this.kitchen_title = kitchen_title;
        }

        public String getKitchen_descrition() {
            return kitchen_descrition;
        }

        public void setKitchen_descrition(String kitchen_descrition) {
            this.kitchen_descrition = kitchen_descrition;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMenuuserid() {
            return menuuserid;
        }

        public void setMenuuserid(String menuuserid) {
            this.menuuserid = menuuserid;
        }

        public String getMenu_title() {
            return menu_title;
        }

        public void setMenu_title(String menu_title) {
            this.menu_title = menu_title;
        }

        public String getShared_id() {
            return shared_id;
        }

        public void setShared_id(String shared_id) {
            this.shared_id = shared_id;
        }

        public String getOlddate() {
            return olddate;
        }

        public void setOlddate(String olddate) {
            this.olddate = olddate;
        }

        public String getHours() {
            return hours;
        }

        public void setHours(String hours) {
            this.hours = hours;
        }

        public String getDays() {
            return days;
        }

        public void setDays(String days) {
            this.days = days;
        }

        public String getKitchenimage() {
            return kitchenimage;
        }

        public void setKitchenimage(String kitchenimage) {
            this.kitchenimage = kitchenimage;
        }
    }
}
