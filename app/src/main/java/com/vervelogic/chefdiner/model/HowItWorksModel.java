package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 05-12-2017.
 */

public class HowItWorksModel {
    private String title, titleDesc;
    private String url = "";
    public static final int EVEN = 0;
    public static final int ODD = 1;


    public HowItWorksModel(String title, String titleDesc) {
        this.title = title;
        this.titleDesc = titleDesc;
    }

    public HowItWorksModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleDesc() {
        return titleDesc;
    }

    public void setTitleDesc(String titleDesc) {
        this.titleDesc = titleDesc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
