package com.vervelogic.chefdiner.model;

/**
 * Created by Verve on 13-10-2017.
 */

public class SpinnerModel {

    private String text;
    private Integer imageId;
    public SpinnerModel(String text, Integer imageId){
        this.text=text;
        this.imageId=imageId;
    }

    public String getText(){
        return text;
    }

    public Integer getImageId(){
        return imageId;
    }
}
