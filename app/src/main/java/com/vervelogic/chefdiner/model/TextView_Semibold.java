package com.vervelogic.chefdiner.model;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Verve on 02-10-2017.
 */

public class TextView_Semibold extends TextView {
    public TextView_Semibold(Context context) {
        super(context);
    }

    public TextView_Semibold(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TextView_Semibold(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TextView_Semibold(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
    private void init(AttributeSet attrs) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/SF-UI-Display-Semibold.otf");
        setTypeface(tf ,1);
    }

}
