package com.vervelogic.chefdiner.bottomFragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.activity.Cuisines;
import com.vervelogic.chefdiner.activity.MapClass;
import com.vervelogic.chefdiner.activity.SelectOptions;
import com.vervelogic.chefdiner.adapter.HomeFragmentAdapter;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.HomeFragmentModel;
import com.vervelogic.chefdiner.model.RecyclerTouchListener;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.GPSTracker;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class Home extends Fragment implements View.OnClickListener {

    private static final String TAG = "Home";
    // --------------------------Declarations----------------------------------
    RecyclerView recyclerView;
    TextView whatsHot, nearBy, cuisine, district, map, chefService, dailyMeal, chefRanking, newChef;
    HomeFragmentAdapter homeFragmentAdapter;
    List<HomeFragmentModel> homeFragmentModelList = new ArrayList<>();
    ImageView filter, sortOptions;
    LinearLayout filter_ly;
    TextView no_image_available, seatsBooked, priceRange;
    String language, filterStr = "";
    DbHelper dbHelper;
    String whatsHotKey = "0";
    RadioButton highestRecommended, highestBookmarked, priceLowToHigh, priceHighToLow;
    List<HomeFragmentModel> homeFragmentModels = new ArrayList<>();
    List<HomeFragmentModel> serviceTypeModel = new ArrayList<>();
    ProgressDialog progressDialog;

    ImageView search;
    LinearLayout search_container;
    EditText searchedit;

    Activity context = getActivity();
    ArrayList<HomeFragmentModel> listFile;
    ArrayList<HomeFragmentModel> filterlistFile;
    HomeFragmentModel obj;
    android.support.v4.widget.SwipeRefreshLayout SwipeRefreshLayout;
    boolean flagToggleButton = false;
    CheckBox food_delivery_chef, home_private_meal_chef, restaurant_private_meal_chef,
            chef_onsite_cooking_chef, cooking_workshop_chef, daily_meal;
    Button ok;
    TextView cancelServicePopup;
    //----------------------------- GPS-----------------------------
    GPSTracker gps;
    String latitude = "", longitude = "", userId = "", list = "", formattedDate = "",
            selectedCountry = "", minimumPriceStr = "", maximumPriceStr = "", selectedNoOfGuest = "", selectedDate = "", currencyUser = "";
    private SharedPreference sharedPreference;

    TextWatcher searchtext = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            String name = "" + s;

            if (listFile.size() > 0) {

                if (!name.equals("")) {


                    filterlistFile = new ArrayList<>();
                    for (int i = 0; i < listFile.size(); i++) {

                        String namelist = listFile.get(i).getKitchen_title();

                        // String[] parts = datelist.split(" ");
                        //  String dateformat=parts[0];

                        String selectewddate = "" + name;
                        if (containsIgnoreCase(namelist, s.toString())) {
                            filterlistFile.clear();
                            obj = new HomeFragmentModel();

                            obj.setKitchen_title(listFile.get(i).getKitchen_title());
                            obj.setChef_id(listFile.get(i).getChef_id());
                            obj.setProfileUrl(listFile.get(i).getProfileUrl());

                            obj.setLanguage(language);
                            obj.setMaxprice(listFile.get(i).getMaxprice());
                            obj.setWhatsHotKey(whatsHotKey);
                            obj.setPrice(listFile.get(i).getPrice());
                            obj.setAccess_token(listFile.get(i).getAccess_token());
                            obj.setLanguageSpeak(listFile.get(i).getLanguageSpeak());
                            obj.setServiceType(listFile.get(i).getServiceType());
                            obj.setPhoneNumber(listFile.get(i).getPhoneNumber());
                            obj.setOftenCook(listFile.get(i).getOftenCook());
                            obj.setChefCountry(listFile.get(i).getChefCountry());
                            obj.setFromGuest(listFile.get(i).getFromGuest());
                            obj.setUptoGuest(listFile.get(i).getUptoGuest());
                            obj.setKitchen_description(listFile.get(i).getKitchen_description());
                            obj.setChef_latitude(listFile.get(i).getChef_latitude());
                            obj.setChef_longiude(listFile.get(i).getChef_longiude());
                            obj.setLunch(listFile.get(i).getLunch());
                            obj.setDinner(listFile.get(i).getDinner());
                            obj.setBrunch(listFile.get(i).getBrunch());
                            obj.setCuisine(listFile.get(i).getCuisine());
                            obj.setEmail(listFile.get(i).getEmail());
                            obj.setIs_verify(listFile.get(i).getIs_verify());
                            obj.setIs_chef_verify(listFile.get(i).getIs_chef_verify());
                            obj.setIs_chef(listFile.get(i).getIs_chef());
                            obj.setUserLat(listFile.get(i).getUserLat());
                            obj.setUserLong(listFile.get(i).getUserLong());
                            obj.setChefName(listFile.get(i).getChefName());
                            obj.setChefLName(listFile.get(i).getChefLName());
                            obj.setChefAddress(listFile.get(i).getChefAddress());
                            obj.setKitchen_title(listFile.get(i).getKitchen_title());
                            obj.setProfileUrl(listFile.get(i).getProfileUrl());
                            obj.setHKD(listFile.get(i).getHKD());
                            obj.setAllImages(listFile.get(i).getAllImages());
                            obj.setChefRate(listFile.get(i).getChefRate());
                            obj.setReviewCount(listFile.get(i).getReviewCount());
                            obj.setRateCount(listFile.get(i).getRateCount());
                            obj.setHappyCount(listFile.get(i).getHappyCount());
                            obj.setSadCount(listFile.get(i).getSadCount());
                            obj.setFoodType1("Indian");
                            obj.setFoodType2("Italian");
                            obj.setBookmarkedCount(listFile.get(i).getBookmarkedCount());
                            obj.setChef_id(listFile.get(i).getChef_id());
                            obj.setCurrency(listFile.get(i).getCurrency());

                            //  homeFragmentModel.setCid(cid);
/*
                            List<String> items = Arrays.asList(all_Images.split("\\|"));

                            ArrayList<String> listView = new ArrayList<>();
                            for (int j = 0; j < items.size(); j++) {
                                if (!items.get(j).equals("")) {
                                    listView.add(items.get(j));
                                }
                            }
                            int imagesLength = listView.size();
                            homeFragmentModel.setImageLength(imagesLength);
                            if (!listView.isEmpty()) {
                                homeFragmentModel.setImageUrl(listView.get(0));
                                homeFragmentModel.setImage2Url(listView.get(1));
                                homeFragmentModel.setImage3Url(listView.get(2));
                            }*/

                        /*    obj.setFileUploadDate(listFile.get(i).getFileUploadDate());
                            obj.setUploadedPicURL(listFile.get(i).getUploadedPicURL());
                            obj.setFileuploadedBy(listFile.get(i).getFileuploadedBy());
                            obj.setImagesArray(listFile.get(i).getImagesArray());
                            obj.setProductNumber(listFile.get(i).getProductNumber());
                            obj.setRefernceNumber(listFile.get(i).getRefernceNumber());
                            obj.setUserprfileImageURL(listFile.get(i).getUserprfileImageURL());*/
                            String fileType = listFile.get(i).getKitchen_title();

                            /*if (valuefromActivity.equals(fileType)) {
                                filterlistFile.add(obj);
                            }*/

                            filterlistFile.add(obj);
                        }
                    }

                    if (filterlistFile.size() > 0) {
                        recyclerView.setVisibility(View.VISIBLE);
                        homeFragmentAdapter = new HomeFragmentAdapter(getActivity(), filterlistFile);
                        recyclerView.setAdapter(homeFragmentAdapter);

                    } else {
                        recyclerView.setVisibility(View.GONE);
                        homeFragmentAdapter = new HomeFragmentAdapter(getActivity(), listFile);
                        recyclerView.setAdapter(homeFragmentAdapter);
                        Toast.makeText(getActivity(), "No data found for this name", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    homeFragmentAdapter = new HomeFragmentAdapter(getActivity(), listFile);
                    recyclerView.setAdapter(homeFragmentAdapter);
                }
            }

        }

        @Override
        public void afterTextChanged(Editable s) {


        }
    };


    public static Home newInstance() {
        return new Home();
    }

    public static boolean containsIgnoreCase(String src, String what) {
        final int length = what.length();
        if (length == 0)
            return true; // Empty string is contained

        final char firstLo = Character.toLowerCase(what.charAt(0));
        final char firstUp = Character.toUpperCase(what.charAt(0));

        for (int i = src.length() - length; i >= 0; i--) {
            // Quick check before calling the more expensive regionMatches() method:
            final char ch = src.charAt(i);
            if (ch != firstLo && ch != firstUp)
                continue;

            if (src.regionMatches(true, i, what, 0, length))
                return true;
        }

        return false;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);


        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
       /* setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/


        Intent intent = getActivity().getIntent();
      /*  language = intent.getStringExtra("language");
        Log.e("Lang",""+language);*/

        selectedCountry = intent.getStringExtra("selectedCountry");
        minimumPriceStr = intent.getStringExtra("minimumPriceStr");
        maximumPriceStr = intent.getStringExtra("maximumPriceStr");
        selectedDate = intent.getStringExtra("selectedDate");
        selectedNoOfGuest = intent.getStringExtra("selectedNoOfGuest");
        formattedDate = intent.getStringExtra("formattedDate");

        filterlistFile = new ArrayList<>();

        dbHelper = new DbHelper(getActivity().getApplicationContext());

        //---------------------------------ID----------------------------------------------------
        recyclerView = view.findViewById(R.id.home_recycler_view);
        whatsHot = view.findViewById(R.id.whatsHot);
        nearBy = view.findViewById(R.id.nearBy);
        cuisine = view.findViewById(R.id.cuisine);
        district = view.findViewById(R.id.district);
        map = view.findViewById(R.id.map);
        chefService = view.findViewById(R.id.chefService);
        dailyMeal = view.findViewById(R.id.dailyMeal);
        chefRanking = view.findViewById(R.id.chefRanking);
        newChef = view.findViewById(R.id.newChef);
        search = view.findViewById(R.id.search);
        search_container = view.findViewById(R.id.search_container);
        searchedit = view.findViewById(R.id.name_enter);
        no_image_available = view.findViewById(R.id.no_data);
        SwipeRefreshLayout = view.findViewById(R.id.swipeContainer);
        seatsBooked = view.findViewById(R.id.seatsBooked);
        priceRange = view.findViewById(R.id.priceRange);

        progressDialog = new ProgressDialog(getActivity());

        if (!TextUtils.isEmpty(selectedCountry) && !TextUtils.isEmpty(selectedDate) && !TextUtils.isEmpty(selectedNoOfGuest) &&
                !TextUtils.isEmpty(minimumPriceStr) && !TextUtils.isEmpty(maximumPriceStr)) {
            seatsBooked.setText(selectedNoOfGuest + " " + getResources().getString(R.string.seats) + ", " + selectedDate);
            dbHelper = new DbHelper(getActivity());
            List<UserDetail> contacts = dbHelper.getAllDetails();
            for (UserDetail cn : contacts) {

                currencyUser = cn.getCurrency();
            }
            if (!TextUtils.isEmpty(currencyUser)) {
                priceRange.setText(getResources().getString(R.string.price_range) + " : " + currencyUser + " " + minimumPriceStr + "-" + maximumPriceStr);
            } else {
                priceRange.setText(getResources().getString(R.string.price_range) + " : HKD " + minimumPriceStr + "-" + maximumPriceStr);
            }

        } else {
            seatsBooked.setText("");
            priceRange.setText("");
        }


        listFile = new ArrayList<>();

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!flagToggleButton) {
                    search_container.setVisibility(View.GONE);
                    flagToggleButton = true;
                } else {
                    search_container.setVisibility(View.VISIBLE);
                    flagToggleButton = false;
                }
            }
        });
        searchedit.addTextChangedListener(searchtext);

        //--------------------------- create class object--------------------------------
        gps = new GPSTracker(getActivity());

        // check if GPS enabled
        if (gps.canGetLocation()) {

            double lat = gps.getLatitude();
            double lon = gps.getLongitude();
            latitude = String.valueOf(lat);
            longitude = String.valueOf(lon);

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(getActivity().getApplicationContext()))) {
            userId = sharedPreference.getUserId(getActivity().getApplicationContext());
        } else {
            userId = "";
        }
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(getActivity().getApplicationContext()))) {
            language = sharedPreference.getLanguage(getActivity().getApplicationContext());
            Log.e("Lang", "" + language);
            if (!TextUtils.isEmpty(language)) {
                if (language.equalsIgnoreCase("english")) {
                    Locale current = Locale.ENGLISH;
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
                if (language.equalsIgnoreCase("traditional chinese")) {
                    Locale current = Locale.TRADITIONAL_CHINESE;
                    Log.e("TAG", "" + current);

                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
                if (language.equalsIgnoreCase("simplified chinese")) {
                    Locale current = Locale.SIMPLIFIED_CHINESE;
                    Log.e("TAG", "" + current);
                    Locale.setDefault(current);
                    Configuration config = new Configuration();
                    config.locale = current;
                    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                }
            }

        } else {
            language = "english";
            Locale current = Locale.ENGLISH;
            Locale.setDefault(current);
            Configuration config = new Configuration();
            config.locale = current;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        }
        ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.internet_error))
                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
        } else {
            whatsHot.setBackgroundColor(getResources().getColor(R.color.button_color));
            whatsHot.setTextColor(getResources().getColor(R.color.white));
            nearBy.setBackgroundColor(getResources().getColor(R.color.white));
            cuisine.setBackgroundColor(getResources().getColor(R.color.white));
            district.setBackgroundColor(getResources().getColor(R.color.white));
            map.setBackgroundColor(getResources().getColor(R.color.white));
            chefService.setBackgroundColor(getResources().getColor(R.color.white));
            chefRanking.setBackgroundColor(getResources().getColor(R.color.white));
            newChef.setBackgroundColor(getResources().getColor(R.color.white));
            dailyMeal.setBackgroundColor(getResources().getColor(R.color.white));

            whatsHot.setTextColor(getResources().getColor(R.color.white));
            nearBy.setTextColor(getResources().getColor(R.color.black));
            cuisine.setTextColor(getResources().getColor(R.color.black));
            district.setTextColor(getResources().getColor(R.color.black));
            map.setTextColor(getResources().getColor(R.color.black));
            chefService.setTextColor(getResources().getColor(R.color.black));
            chefRanking.setTextColor(getResources().getColor(R.color.black));
            newChef.setTextColor(getResources().getColor(R.color.black));
            dailyMeal.setTextColor(getResources().getColor(R.color.black));

            if (!TextUtils.isEmpty(selectedCountry) && !TextUtils.isEmpty(selectedDate) && !TextUtils.isEmpty(selectedNoOfGuest) &&
                    !TextUtils.isEmpty(minimumPriceStr) && !TextUtils.isEmpty(maximumPriceStr) && !TextUtils.isEmpty(formattedDate)) {
                filterStr = "selectoption";
                whatsHotKey = "0";
                GetChefAPI(filterStr);
            } else {
                selectedDate = "";
                maximumPriceStr = "";
                minimumPriceStr = "";
                selectedNoOfGuest = "";
                selectedCountry = "";
                filterStr = "whatshot";
                whatsHotKey = "1";
                seatsBooked.setText("");
                priceRange.setText("");
                formattedDate = "";
                GetChefAPI("");
            }

        }
        SwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle(getResources().getString(R.string.app_name))
                                    .setMessage(getResources().getString(R.string.internet_error))
                                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
                        } else {
                            filterStr = "whatshot";
                            whatsHot.setBackgroundColor(getResources().getColor(R.color.button_color));
                            nearBy.setBackgroundColor(getResources().getColor(R.color.white));
                            cuisine.setBackgroundColor(getResources().getColor(R.color.white));
                            district.setBackgroundColor(getResources().getColor(R.color.white));
                            map.setBackgroundColor(getResources().getColor(R.color.white));
                            chefService.setBackgroundColor(getResources().getColor(R.color.white));
                            chefRanking.setBackgroundColor(getResources().getColor(R.color.white));
                            newChef.setBackgroundColor(getResources().getColor(R.color.white));
                            dailyMeal.setBackgroundColor(getResources().getColor(R.color.white));

                            whatsHot.setTextColor(getResources().getColor(R.color.white));
                            nearBy.setTextColor(getResources().getColor(R.color.black));
                            cuisine.setTextColor(getResources().getColor(R.color.black));
                            district.setTextColor(getResources().getColor(R.color.black));
                            map.setTextColor(getResources().getColor(R.color.black));
                            chefService.setTextColor(getResources().getColor(R.color.black));
                            chefRanking.setTextColor(getResources().getColor(R.color.black));
                            newChef.setTextColor(getResources().getColor(R.color.black));
                            dailyMeal.setTextColor(getResources().getColor(R.color.black));
                            whatsHotKey = "1";
                            no_image_available.setVisibility(View.GONE);
                            seatsBooked.setText("");
                            priceRange.setText("");

                            GetChefAPI("");
                        }
                    }
                }
        );


        // ------------------------setting listener on recycler view------------------------------
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getApplicationContext(), recyclerView,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }

                }));


        //--------------------------------Listeners--------------------------------------------
        whatsHot.setOnClickListener(this);
        nearBy.setOnClickListener(this);
        cuisine.setOnClickListener(this);
        district.setOnClickListener(this);
        map.setOnClickListener(this);
        chefService.setOnClickListener(this);
        dailyMeal.setOnClickListener(this);
        chefRanking.setOnClickListener(this);
        newChef.setOnClickListener(this);

        filter = view.findViewById(R.id.filter);
        filter_ly = view.findViewById(R.id.filter_ly);
        filter_ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // custom dialog
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.custom_filter_dialog);
                Button dialogButton = dialog.findViewById(R.id.applyfilter);
                highestRecommended = dialog.findViewById(R.id.highestRecommended);
                highestBookmarked = dialog.findViewById(R.id.highestBookmarked);
                priceHighToLow = dialog.findViewById(R.id.priceHighToLow);
                priceLowToHigh = dialog.findViewById(R.id.priceLowToHigh);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (highestBookmarked.isChecked()) {
                            Collections.sort(homeFragmentModelList, new Comparator<HomeFragmentModel>() {
                                @Override
                                public int compare(HomeFragmentModel userDetail, HomeFragmentModel t1) {
                                    String stringName1 = userDetail.getBookmarkedCount();
                                    String stringName2 = t1.getBookmarkedCount();

                                    return stringName2.compareToIgnoreCase(stringName1);//Desc Order
                                }
                            });
                            if (!homeFragmentModelList.isEmpty()) {
                                // ---------------Adapter---------------------
                                homeFragmentAdapter = new HomeFragmentAdapter(getActivity().getApplicationContext(), homeFragmentModelList);
                                // setting adapter on recycler view
                                recyclerView.setAdapter(homeFragmentAdapter);

                                homeFragmentAdapter.notifyDataSetChanged();
                            } else {
                                no_image_available.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                        /*Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();*/
                            }

                        }
                        if (highestRecommended.isChecked()) {
                            Collections.sort(homeFragmentModelList, new Comparator<HomeFragmentModel>() {
                                @Override
                                public int compare(HomeFragmentModel userDetail, HomeFragmentModel t1) {
                                    String stringName1 = userDetail.getHappyCount();
                                    String stringName2 = t1.getHappyCount();

                                    return stringName2.compareToIgnoreCase(stringName1);//Desc Order
                                }
                            });
                            if (!homeFragmentModelList.isEmpty()) {
                                // ---------------Adapter---------------------
                                homeFragmentAdapter = new HomeFragmentAdapter(getActivity().getApplicationContext(), homeFragmentModelList);
                                // setting adapter on recycler view
                                recyclerView.setAdapter(homeFragmentAdapter);

                                homeFragmentAdapter.notifyDataSetChanged();
                            } else {
                                no_image_available.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                        /*Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();*/
                            }


                        }
                        if (priceHighToLow.isChecked()) {
                            Collections.sort(homeFragmentModelList, new Comparator<HomeFragmentModel>() {
                                @Override
                                public int compare(HomeFragmentModel userDetail, HomeFragmentModel t1) {
                                    float stringName1 = Float.parseFloat(userDetail.getPrice());
                                    float stringName2 = Float.parseFloat(t1.getPrice());
                                    if (stringName1 < stringName2) {
                                        return 1;
                                    } else {
                                        return -1;
                                    }
                                    // return stringName2.compareToIgnoreCase(stringName1);//Desc Order
                                }
                            });
                            if (!homeFragmentModelList.isEmpty()) {
                                // ---------------Adapter---------------------
                                homeFragmentAdapter = new HomeFragmentAdapter(getActivity().getApplicationContext(), homeFragmentModelList);
                                // setting adapter on recycler view
                                recyclerView.setAdapter(homeFragmentAdapter);

                                homeFragmentAdapter.notifyDataSetChanged();
                            } else {
                                no_image_available.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                        /*Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();*/
                            }
                        }
                        if (priceLowToHigh.isChecked()) {
                            Collections.sort(homeFragmentModelList, new Comparator<HomeFragmentModel>() {
                                @Override
                                public int compare(HomeFragmentModel userDetail, HomeFragmentModel t1) {
                                    float stringName1 = Float.parseFloat(userDetail.getPrice());
                                    float stringName2 = Float.parseFloat(t1.getPrice());
                                    if (stringName2 < stringName1) {
                                        return 1;
                                    } else {
                                        return -1;
                                    }
                                }
                            });
                            if (!homeFragmentModelList.isEmpty()) {
                                // ---------------Adapter---------------------
                                homeFragmentAdapter = new HomeFragmentAdapter(getActivity().getApplicationContext(), homeFragmentModelList);
                                // setting adapter on recycler view
                                recyclerView.setAdapter(homeFragmentAdapter);

                                homeFragmentAdapter.notifyDataSetChanged();
                            } else {
                                no_image_available.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                        /*Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();*/
                            }
                        }
                    }
                });
                TextView cancel = dialog.findViewById(R.id.cancel);
                // if button is clicked, close the custom dialog
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
        sortOptions = view.findViewById(R.id.sortOptions);
        sortOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity().getApplicationContext(), SelectOptions.class);
                startActivity(intent);

            }
        });


        return view;
    }

    /*------------------------------API for chef details-----------------------------------------------*/
    public void GetChefAPI(final String filterStr) {
        if (getActivity() != null) {
            progressDialog.setMessage(getResources().getString(R.string.please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        String uri = String.format(AppConfig.GET_CHEF_URL + "?latitude=%1$s", latitude + "&longitude=" + longitude);
        //String uri = String.format(AppConfig.GET_CHEF_URL+"?latitude=26.911974&longitude=75.789882");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.GET_CHEF_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDismiss();
                        try {
                            /*nearBy.setBackgroundColor(getResources().getColor(R.color.button_color));
                            nearBy.setTextColor(getResources().getColor(R.color.white));*/
                            Log.e(TAG, response);
                            dbHelper.deleteChef();
                            SwipeRefreshLayout.setRefreshing(false);
                            UserDetail userDetail = new UserDetail();
                            //Log.e("Response",response);
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                homeFragmentModelList.clear();
                                homeFragmentModels.clear();
                                serviceTypeModel.clear();
                                filterlistFile.clear();
                                no_image_available.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                JSONArray jsonArray = jsonObject.getJSONArray("getChef");
                                for (int i = 0; i <= jsonArray.length(); i++) {
                                    HomeFragmentModel homeFragmentModel = new HomeFragmentModel();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    String id = jsonObject1.getString("id");
                                    String language = jsonObject1.getString("language");
                                    String fName = jsonObject1.getString("chef_fname");
                                    String last_name = jsonObject1.getString("chef_lname");
                                    String language_speak = jsonObject1.getString("language_speak");
                                    String serviceType = jsonObject1.getString("service_type");
                                    String phoneNumber = jsonObject1.getString("phone_number");
                                    String oftenCook = jsonObject1.getString("oftencook");
                                    String chefCountry = jsonObject1.getString("chef_country");
                                    String fromGuest = jsonObject1.getString("from_guest");
                                    String uptoGuest = jsonObject1.getString("upto_guest");
                                    String kitchen_title = jsonObject1.getString("kitchen_title");
                                    String kitchen_desc = jsonObject1.getString("kitchen_descrition");
                                    String profile_pic = jsonObject1.getString("profile_pic");
                                    String chef_latitude = jsonObject1.getString("chef_latitude");
                                    String chef_longitude = jsonObject1.getString("chef_longitude");
                                    String favorite = jsonObject1.getString("favouritecount");
                                    String userF_name = jsonObject1.getString("first_name");
                                    String userL_name = jsonObject1.getString("last_name");
                                    String email = jsonObject1.getString("email");
                                    String is_verify = jsonObject1.getString("is_verify");
                                    String is_chef_verify = jsonObject1.getString("is_verify_chef");
                                    String is_user = jsonObject1.getString("is_user");
                                    String device_ID = jsonObject1.getString("deviceId");
                                    String user_latitude = jsonObject1.getString("latitude");
                                    String user_longitude = jsonObject1.getString("longitude");
                                    String chef_city = jsonObject1.getString("chef_city");
                                    String all_Images = jsonObject1.getString("allimage");
                                    String price = jsonObject1.getString("price");
                                    String is_chef = jsonObject1.getString("is_chef");
                                    String lunch = jsonObject1.getString("lunch");
                                    String brunch = jsonObject1.getString("brunch");
                                    String dinner = jsonObject1.getString("dinner");
                                    String currency = jsonObject1.getString("currency");
                                    String cuisine = jsonObject1.getString("cuisine");
                                    String access_token = jsonObject1.getString("access_token");
                                    String totalreview = jsonObject1.getString("totalreview");
                                    String ok = jsonObject1.getString("ok");
                                    String no_recommended = jsonObject1.getString("no_recommended");
                                    String recommended = jsonObject1.getString("recommended");
                                    String average_rating = jsonObject1.getString("average_rating");
                                    String maxprice = jsonObject1.getString("maxprice");

                                    homeFragmentModel.setLanguage(language);
                                    homeFragmentModel.setMaxprice(maxprice);
                                    homeFragmentModel.setWhatsHotKey(whatsHotKey);
                                    homeFragmentModel.setPrice(price);
                                    homeFragmentModel.setAccess_token(access_token);
                                    homeFragmentModel.setLanguageSpeak(language_speak);
                                    homeFragmentModel.setServiceType(serviceType);
                                    homeFragmentModel.setPhoneNumber(phoneNumber);
                                    homeFragmentModel.setOftenCook(oftenCook);
                                    homeFragmentModel.setChefCountry(chefCountry);
                                    homeFragmentModel.setFromGuest(fromGuest);
                                    homeFragmentModel.setUptoGuest(uptoGuest);
                                    homeFragmentModel.setKitchen_description(kitchen_desc);
                                    homeFragmentModel.setChef_latitude(chef_latitude);
                                    homeFragmentModel.setChef_longiude(chef_longitude);
                                    homeFragmentModel.setLunch(lunch);
                                    homeFragmentModel.setDinner(dinner);
                                    homeFragmentModel.setBrunch(brunch);
                                    homeFragmentModel.setCuisine(cuisine);
                                    homeFragmentModel.setEmail(email);
                                    homeFragmentModel.setIs_verify(is_verify);
                                    homeFragmentModel.setIs_chef_verify(is_chef_verify);
                                    homeFragmentModel.setIs_chef(is_chef);
                                    homeFragmentModel.setUserLat(user_latitude);
                                    homeFragmentModel.setUserLong(user_longitude);
                                    homeFragmentModel.setChefName(fName);
                                    homeFragmentModel.setChefLName(last_name);
                                    homeFragmentModel.setChefAddress(chef_city);
                                    homeFragmentModel.setKitchen_title(kitchen_title);
                                    homeFragmentModel.setProfileUrl(profile_pic);
                                    homeFragmentModel.setHKD(currency + " " + price);
                                    homeFragmentModel.setAllImages(all_Images);
                                    homeFragmentModel.setChefRate(average_rating);
                                    homeFragmentModel.setReviewCount(totalreview + " "+getResources().getString(R.string.review));
                                    homeFragmentModel.setRateCount(average_rating);
                                    homeFragmentModel.setHappyCount(recommended);
                                    homeFragmentModel.setSadCount(no_recommended);
                                    homeFragmentModel.setFoodType1("Indian");
                                    homeFragmentModel.setFoodType2("Italian");
                                    homeFragmentModel.setBookmarkedCount(favorite);
                                    homeFragmentModel.setChef_id(id);
                                    homeFragmentModel.setCurrency(currency);

                                    //  homeFragmentModel.setCid(cid);

                                    List<String> items = Arrays.asList(all_Images.split("\\|"));

                                    ArrayList<String> listView = new ArrayList<>();
                                    for (int j = 0; j < items.size(); j++) {
                                        if (!items.get(j).equals("")) {
                                            listView.add(items.get(j));
                                        }
                                    }
                                    int imagesLength = listView.size();
                                    homeFragmentModel.setImageLength(imagesLength);
                                    if (!listView.isEmpty()) {
                                        homeFragmentModel.setImageUrl(listView.get(0));
                                        homeFragmentModel.setImage2Url(listView.get(1));
                                        homeFragmentModel.setImage3Url(listView.get(2));
                                    }
                                    userDetail.setLanguage(language);
                                    userDetail.setAccessToken(access_token);
                                    userDetail.setLanguageList(language_speak);
                                    userDetail.setServiceType(serviceType);
                                    userDetail.setPhoneNumber(phoneNumber);
                                    userDetail.setOftenCook(oftenCook);
                                    userDetail.setChefCountry(chefCountry);
                                    userDetail.setMin_guest(fromGuest);
                                    userDetail.setMax_guest(uptoGuest);
                                    userDetail.setKitchenDescription(kitchen_desc);
                                    userDetail.setLatitude(chef_latitude);
                                    userDetail.setLongitude(chef_longitude);
                                    userDetail.setLunch(lunch);
                                    userDetail.setDinner(dinner);
                                    userDetail.setBrunch(brunch);
                                    userDetail.setCuisine(cuisine);
                                    userDetail.setEmailId(email);
                                    userDetail.setIsVerifyUser(is_verify);
                                    userDetail.setIsChefVerify(is_chef_verify);
                                    userDetail.setIsChef(is_chef);
                                   /* userDetail.setLatitude(user_latitude);
                                    userDetail.setLongitude(user_longitude);*/
                                    userDetail.setFirstName(fName);
                                    userDetail.setLastName(last_name);
                                    userDetail.setCity(chef_city);
                                    userDetail.setKitchenTitle(kitchen_title);
                                    userDetail.setProfilePic(profile_pic);
                                    userDetail.setPrice(price);
                                    userDetail.setAllImages(all_Images);
                                    /*userDetail.setChefRate("4.5");*/
                                    userDetail.setTotalReview(totalreview + " " + getResources().getString(R.string.review));
                                    userDetail.setAvgRating(average_rating);
                                    userDetail.setRecommended(recommended);
                                    userDetail.setNotRecommended(no_recommended);
                                    userDetail.setOk(ok);
                                   /* userDetail.setFoodType2("Italian");*/
                                    userDetail.setFavCount(favorite);
                                    userDetail.setUser_id(id);
                                    userDetail.setCurrency(currency);
                                    userDetail.setCountry(chefCountry);
                                    userDetail.setMaxPrice(maxprice);

                                    dbHelper.addChefDetails(userDetail);
                                    homeFragmentModelList.add(homeFragmentModel);
                                    listFile.add(homeFragmentModel);

                                    List<String> item = Arrays.asList(serviceType.split("\\s*,\\s*"));
                                    for (int j = 0; j < item.size(); j++) {
                                        if (item.get(j).equalsIgnoreCase("Daily meal")) {
                                            homeFragmentModels.add(homeFragmentModel);
                                        }

                                    }


                                    // -------------Layout setting on recycler view---------------
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                                    recyclerView.setLayoutManager(mLayoutManager);
                                    recyclerView.setNestedScrollingEnabled(false);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                        @Override
                                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                            int topRowVerticalPosition =
                                                    (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                                            SwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
                                        }
                                    });

                                    if (!homeFragmentModelList.isEmpty()) {
                                        no_image_available.setVisibility(View.GONE);
                                        // ---------------Adapter---------------------
                                        homeFragmentAdapter = new HomeFragmentAdapter(getActivity().getApplicationContext(), homeFragmentModelList);
                                        // setting adapter on recycler view
                                        recyclerView.setAdapter(homeFragmentAdapter);
                                        homeFragmentAdapter.notifyDataSetChanged();
                                    } else {
                                        no_image_available.setVisibility(View.VISIBLE);
                                        recyclerView.setVisibility(View.GONE);
                                        /*Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();*/
                                    }
                                }

                            } else if (status == 0) {
                                String message = jsonObject.getString("message");
                                no_image_available.setVisibility(View.VISIBLE);
                                no_image_available.setText(message);
                                recyclerView.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), getResources().getString(R.string.no_chef_available_around_you), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDismiss();
                            // Toast.makeText(getActivity(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDismiss();
                            Activity activity = getActivity();
                            if (activity != null) {

                                Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();

                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                } else if (error instanceof Exception) {

                }
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                Log.e(TAG, "onErrorResponse: " + message);
                Log.d("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //  String image = getStringImage(bitmap);
                map.put("latitude", latitude);
                map.put("longitude", longitude);
                map.put("filter", filterStr);
                map.put("userid", userId);
                map.put("seats", selectedNoOfGuest);
                map.put("date", formattedDate);
                map.put("minprice", minimumPriceStr);
                map.put("maxprice", maximumPriceStr);
                map.put("country", selectedCountry);

                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.whatsHot:
                whatsHot.setBackgroundColor(getResources().getColor(R.color.button_color));
                whatsHot.setTextColor(getResources().getColor(R.color.white));
                nearBy.setBackgroundColor(getResources().getColor(R.color.white));
                cuisine.setBackgroundColor(getResources().getColor(R.color.white));
                district.setBackgroundColor(getResources().getColor(R.color.white));
                map.setBackgroundColor(getResources().getColor(R.color.white));
                chefService.setBackgroundColor(getResources().getColor(R.color.white));
                chefRanking.setBackgroundColor(getResources().getColor(R.color.white));
                newChef.setBackgroundColor(getResources().getColor(R.color.white));
                dailyMeal.setBackgroundColor(getResources().getColor(R.color.white));

                whatsHot.setTextColor(getResources().getColor(R.color.white));
                nearBy.setTextColor(getResources().getColor(R.color.black));
                cuisine.setTextColor(getResources().getColor(R.color.black));
                district.setTextColor(getResources().getColor(R.color.black));
                map.setTextColor(getResources().getColor(R.color.black));
                chefService.setTextColor(getResources().getColor(R.color.black));
                chefRanking.setTextColor(getResources().getColor(R.color.black));
                newChef.setTextColor(getResources().getColor(R.color.black));
                dailyMeal.setTextColor(getResources().getColor(R.color.black));

                filterStr = "whatshot";
                whatsHotKey = "1";

                selectedDate = "";
                maximumPriceStr = "";
                minimumPriceStr = "";
                selectedNoOfGuest = "";
                selectedCountry = "";
                filterStr = "";

                seatsBooked.setText("");
                priceRange.setText("");
                formattedDate = "";
                ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton("OK", null).show();
                } else {
                    GetChefAPI(filterStr);
                }
                break;
            case R.id.nearBy:
                whatsHot.setBackgroundColor(getResources().getColor(R.color.white));
                nearBy.setBackgroundColor(getResources().getColor(R.color.button_color));
                cuisine.setBackgroundColor(getResources().getColor(R.color.white));
                district.setBackgroundColor(getResources().getColor(R.color.white));
                map.setBackgroundColor(getResources().getColor(R.color.white));
                chefService.setBackgroundColor(getResources().getColor(R.color.white));
                chefRanking.setBackgroundColor(getResources().getColor(R.color.white));
                newChef.setBackgroundColor(getResources().getColor(R.color.white));
                dailyMeal.setBackgroundColor(getResources().getColor(R.color.white));

                whatsHot.setTextColor(getResources().getColor(R.color.black));
                nearBy.setTextColor(getResources().getColor(R.color.white));
                cuisine.setTextColor(getResources().getColor(R.color.black));
                district.setTextColor(getResources().getColor(R.color.black));
                map.setTextColor(getResources().getColor(R.color.black));
                chefService.setTextColor(getResources().getColor(R.color.black));
                chefRanking.setTextColor(getResources().getColor(R.color.black));
                newChef.setTextColor(getResources().getColor(R.color.black));
                dailyMeal.setTextColor(getResources().getColor(R.color.black));
                whatsHotKey = "0";

                selectedDate = "";
                maximumPriceStr = "";
                minimumPriceStr = "";
                selectedNoOfGuest = "";
                selectedCountry = "";
                filterStr = "";

                seatsBooked.setText("");
                priceRange.setText("");
                formattedDate = "";

                if (!latitude.equalsIgnoreCase("0.0000") && !longitude.equalsIgnoreCase("0.0000")) {
                    GetChefAPI("");
                }
                break;
            case R.id.cuisine:
                whatsHot.setBackgroundColor(getResources().getColor(R.color.white));
                nearBy.setBackgroundColor(getResources().getColor(R.color.white));
                cuisine.setBackgroundColor(getResources().getColor(R.color.button_color));
                district.setBackgroundColor(getResources().getColor(R.color.white));
                map.setBackgroundColor(getResources().getColor(R.color.white));
                chefService.setBackgroundColor(getResources().getColor(R.color.white));
                chefRanking.setBackgroundColor(getResources().getColor(R.color.white));
                newChef.setBackgroundColor(getResources().getColor(R.color.white));
                dailyMeal.setBackgroundColor(getResources().getColor(R.color.white));

                whatsHot.setTextColor(getResources().getColor(R.color.black));
                nearBy.setTextColor(getResources().getColor(R.color.black));
                cuisine.setTextColor(getResources().getColor(R.color.white));
                district.setTextColor(getResources().getColor(R.color.black));
                map.setTextColor(getResources().getColor(R.color.black));
                chefService.setTextColor(getResources().getColor(R.color.black));
                chefRanking.setTextColor(getResources().getColor(R.color.black));
                newChef.setTextColor(getResources().getColor(R.color.black));
                dailyMeal.setTextColor(getResources().getColor(R.color.black));
                whatsHotKey = "0";

                selectedDate = "";
                maximumPriceStr = "";
                minimumPriceStr = "";
                selectedNoOfGuest = "";
                selectedCountry = "";
                filterStr = "";

                seatsBooked.setText("");
                priceRange.setText("");
                formattedDate = "";

                Intent intent = new Intent(getContext(), Cuisines.class);
                startActivity(intent);
                break;
            case R.id.district:
                whatsHot.setBackgroundColor(getResources().getColor(R.color.white));
                nearBy.setBackgroundColor(getResources().getColor(R.color.white));
                cuisine.setBackgroundColor(getResources().getColor(R.color.white));
                district.setBackgroundColor(getResources().getColor(R.color.button_color));
                map.setBackgroundColor(getResources().getColor(R.color.white));
                chefService.setBackgroundColor(getResources().getColor(R.color.white));
                chefRanking.setBackgroundColor(getResources().getColor(R.color.white));
                newChef.setBackgroundColor(getResources().getColor(R.color.white));
                dailyMeal.setBackgroundColor(getResources().getColor(R.color.white));

                whatsHot.setTextColor(getResources().getColor(R.color.black));
                nearBy.setTextColor(getResources().getColor(R.color.black));
                cuisine.setTextColor(getResources().getColor(R.color.black));
                district.setTextColor(getResources().getColor(R.color.white));
                map.setTextColor(getResources().getColor(R.color.black));
                chefService.setTextColor(getResources().getColor(R.color.black));
                chefRanking.setTextColor(getResources().getColor(R.color.black));
                newChef.setTextColor(getResources().getColor(R.color.black));
                dailyMeal.setTextColor(getResources().getColor(R.color.black));
                whatsHotKey = "0";
                selectedDate = "";
                maximumPriceStr = "";
                minimumPriceStr = "";
                selectedNoOfGuest = "";
                selectedCountry = "";
                filterStr = "";

                seatsBooked.setText("");
                priceRange.setText("");
                formattedDate = "";


                break;
            case R.id.map:
                whatsHot.setBackgroundColor(getResources().getColor(R.color.white));
                nearBy.setBackgroundColor(getResources().getColor(R.color.white));
                cuisine.setBackgroundColor(getResources().getColor(R.color.white));
                district.setBackgroundColor(getResources().getColor(R.color.white));
                map.setBackgroundColor(getResources().getColor(R.color.button_color));
                chefService.setBackgroundColor(getResources().getColor(R.color.white));
                chefRanking.setBackgroundColor(getResources().getColor(R.color.white));
                newChef.setBackgroundColor(getResources().getColor(R.color.white));
                dailyMeal.setBackgroundColor(getResources().getColor(R.color.white));
                whatsHotKey = "0";
                whatsHot.setTextColor(getResources().getColor(R.color.black));
                nearBy.setTextColor(getResources().getColor(R.color.black));
                cuisine.setTextColor(getResources().getColor(R.color.black));
                district.setTextColor(getResources().getColor(R.color.black));
                map.setTextColor(getResources().getColor(R.color.white));
                chefService.setTextColor(getResources().getColor(R.color.black));
                chefRanking.setTextColor(getResources().getColor(R.color.black));
                newChef.setTextColor(getResources().getColor(R.color.black));
                dailyMeal.setTextColor(getResources().getColor(R.color.black));

                whatsHotKey = "0";
                selectedDate = "";
                maximumPriceStr = "";
                minimumPriceStr = "";
                selectedNoOfGuest = "";
                selectedCountry = "";
                filterStr = "";

                seatsBooked.setText("");
                priceRange.setText("");
                formattedDate = "";


                Intent mapIntent = new Intent(getContext(), MapClass.class);
                startActivity(mapIntent);
                break;
            case R.id.chefService:
                whatsHot.setBackgroundColor(getResources().getColor(R.color.white));
                nearBy.setBackgroundColor(getResources().getColor(R.color.white));
                cuisine.setBackgroundColor(getResources().getColor(R.color.white));
                district.setBackgroundColor(getResources().getColor(R.color.white));
                map.setBackgroundColor(getResources().getColor(R.color.white));
                chefService.setBackgroundColor(getResources().getColor(R.color.button_color));
                chefRanking.setBackgroundColor(getResources().getColor(R.color.white));
                newChef.setBackgroundColor(getResources().getColor(R.color.white));
                dailyMeal.setBackgroundColor(getResources().getColor(R.color.white));

                whatsHot.setTextColor(getResources().getColor(R.color.black));
                nearBy.setTextColor(getResources().getColor(R.color.black));
                cuisine.setTextColor(getResources().getColor(R.color.black));
                district.setTextColor(getResources().getColor(R.color.black));
                map.setTextColor(getResources().getColor(R.color.black));
                chefService.setTextColor(getResources().getColor(R.color.white));
                chefRanking.setTextColor(getResources().getColor(R.color.black));
                newChef.setTextColor(getResources().getColor(R.color.black));
                dailyMeal.setTextColor(getResources().getColor(R.color.black));
                whatsHotKey = "0";

                selectedDate = "";
                maximumPriceStr = "";
                minimumPriceStr = "";
                selectedNoOfGuest = "";
                selectedCountry = "";
                filterStr = "";

                seatsBooked.setText("");
                priceRange.setText("");
                formattedDate = "";


                // custom dialog
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.service_type_popup);

                 /*--------------------IDS------------------------------------------*/
                food_delivery_chef = dialog.findViewById(R.id.food_delivery_chef);
                home_private_meal_chef = dialog.findViewById(R.id.home_private_meal_chef);
                restaurant_private_meal_chef = dialog.findViewById(R.id.restaurant_private_meal_chef);
                chef_onsite_cooking_chef = dialog.findViewById(R.id.chef_onsite_cooking_chef);
                cooking_workshop_chef = dialog.findViewById(R.id.cooking_workshop_chef);
                daily_meal = dialog.findViewById(R.id.daily_meal);
                ok = dialog.findViewById(R.id.ok);
                cancelServicePopup = dialog.findViewById(R.id.cancelServices);

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();

                        if (!validate()) {

                        }
                    }
                });
                cancelServicePopup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

                break;
            case R.id.dailyMeal:
                whatsHot.setBackgroundColor(getResources().getColor(R.color.white));
                nearBy.setBackgroundColor(getResources().getColor(R.color.white));
                cuisine.setBackgroundColor(getResources().getColor(R.color.white));
                district.setBackgroundColor(getResources().getColor(R.color.white));
                map.setBackgroundColor(getResources().getColor(R.color.white));
                chefService.setBackgroundColor(getResources().getColor(R.color.white));
                chefRanking.setBackgroundColor(getResources().getColor(R.color.white));
                newChef.setBackgroundColor(getResources().getColor(R.color.white));
                dailyMeal.setBackgroundColor(getResources().getColor(R.color.button_color));

                whatsHot.setTextColor(getResources().getColor(R.color.black));
                nearBy.setTextColor(getResources().getColor(R.color.black));
                cuisine.setTextColor(getResources().getColor(R.color.black));
                district.setTextColor(getResources().getColor(R.color.black));
                map.setTextColor(getResources().getColor(R.color.black));
                chefService.setTextColor(getResources().getColor(R.color.black));
                chefRanking.setTextColor(getResources().getColor(R.color.black));
                newChef.setTextColor(getResources().getColor(R.color.black));
                dailyMeal.setTextColor(getResources().getColor(R.color.white));
                whatsHotKey = "0";
                selectedDate = "";
                maximumPriceStr = "";
                minimumPriceStr = "";
                selectedNoOfGuest = "";
                selectedCountry = "";
                filterStr = "";

                seatsBooked.setText("");
                priceRange.setText("");
                formattedDate = "";


                if (!homeFragmentModels.isEmpty()) {
                    // ---------------Adapter---------------------
                    homeFragmentAdapter = new HomeFragmentAdapter(getActivity().getApplicationContext(), homeFragmentModels);
                    // setting adapter on recycler view
                    recyclerView.setAdapter(homeFragmentAdapter);
                    homeFragmentAdapter.notifyDataSetChanged();
                } else {
                    no_image_available.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
                break;
            case R.id.chefRanking:
                whatsHot.setBackgroundColor(getResources().getColor(R.color.white));
                nearBy.setBackgroundColor(getResources().getColor(R.color.white));
                cuisine.setBackgroundColor(getResources().getColor(R.color.white));
                district.setBackgroundColor(getResources().getColor(R.color.white));
                map.setBackgroundColor(getResources().getColor(R.color.white));
                chefService.setBackgroundColor(getResources().getColor(R.color.white));
                chefRanking.setBackgroundColor(getResources().getColor(R.color.button_color));
                newChef.setBackgroundColor(getResources().getColor(R.color.white));
                dailyMeal.setBackgroundColor(getResources().getColor(R.color.white));

                whatsHot.setTextColor(getResources().getColor(R.color.black));
                nearBy.setTextColor(getResources().getColor(R.color.black));
                cuisine.setTextColor(getResources().getColor(R.color.black));
                district.setTextColor(getResources().getColor(R.color.black));
                map.setTextColor(getResources().getColor(R.color.black));
                chefService.setTextColor(getResources().getColor(R.color.black));
                chefRanking.setTextColor(getResources().getColor(R.color.white));
                newChef.setTextColor(getResources().getColor(R.color.black));
                dailyMeal.setTextColor(getResources().getColor(R.color.black));
                whatsHotKey = "0";
                selectedDate = "";
                maximumPriceStr = "";
                minimumPriceStr = "";
                selectedNoOfGuest = "";
                selectedCountry = "";
                filterStr = "";

                seatsBooked.setText("");
                priceRange.setText("");
                formattedDate = "";

                break;
            case R.id.newChef:
                filterStr = "newchef";
                whatsHot.setBackgroundColor(getResources().getColor(R.color.white));
                nearBy.setBackgroundColor(getResources().getColor(R.color.white));
                cuisine.setBackgroundColor(getResources().getColor(R.color.white));
                district.setBackgroundColor(getResources().getColor(R.color.white));
                map.setBackgroundColor(getResources().getColor(R.color.white));
                chefService.setBackgroundColor(getResources().getColor(R.color.white));
                chefRanking.setBackgroundColor(getResources().getColor(R.color.white));
                newChef.setBackgroundColor(getResources().getColor(R.color.button_color));
                dailyMeal.setBackgroundColor(getResources().getColor(R.color.white));

                whatsHot.setTextColor(getResources().getColor(R.color.black));
                nearBy.setTextColor(getResources().getColor(R.color.black));
                cuisine.setTextColor(getResources().getColor(R.color.black));
                district.setTextColor(getResources().getColor(R.color.black));
                map.setTextColor(getResources().getColor(R.color.black));
                chefService.setTextColor(getResources().getColor(R.color.black));
                chefRanking.setTextColor(getResources().getColor(R.color.black));
                newChef.setTextColor(getResources().getColor(R.color.white));
                dailyMeal.setTextColor(getResources().getColor(R.color.black));

                whatsHotKey = "0";
                selectedDate = "";
                maximumPriceStr = "";
                minimumPriceStr = "";
                selectedNoOfGuest = "";
                selectedCountry = "";


                seatsBooked.setText("");
                priceRange.setText("");
                formattedDate = "";


                ConnectivityManager conMgr1 = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo1 = conMgr1.getActiveNetworkInfo();
                if (netInfo1 == null) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getResources().getString(R.string.app_name))
                            .setMessage(getResources().getString(R.string.internet_error))
                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                } else {
                    whatsHotKey = "0";
                    GetChefAPI(filterStr);
                }

                break;
        }

    }

    public boolean validate() {
        String food_delivery_chefStr = food_delivery_chef.getText().toString();
        String home_private_meal_chefStr = home_private_meal_chef.getText().toString();
        String restaurant_private_meal_chefStr = restaurant_private_meal_chef.getText().toString();
        String chef_onsite_cooking_chefStr = chef_onsite_cooking_chef.getText().toString();
        String cooking_workshop_chefStr = cooking_workshop_chef.getText().toString();
        String daily_mealStr = daily_meal.getText().toString();
        serviceTypeModel.clear();
        List<String> serviceList = new ArrayList<>();
        if (food_delivery_chef.isChecked()) {
            serviceList.add(food_delivery_chefStr);
            for (int j = 0; j < homeFragmentModelList.size(); j++) {
                List<String> item = Arrays.asList(homeFragmentModelList.get(j).getServiceType().split("\\s*,\\s*"));
                Log.e("food_delivery_chef", "" + homeFragmentModelList.get(j).getServiceType());
                for (int i = 0; i < item.size(); i++) {
                    if (item.get(i).equalsIgnoreCase("Food delivery")) {
                        serviceTypeModel.add(homeFragmentModelList.get(j));
                        Log.e("food_delivery_chef", "" + serviceTypeModel);
                    }

                }

            }
        }
        if (home_private_meal_chef.isChecked()) {
            serviceList.add(home_private_meal_chefStr);
            for (int j = 0; j < homeFragmentModelList.size(); j++) {
                List<String> item = Arrays.asList(homeFragmentModelList.get(j).getServiceType().split("\\s*,\\s*"));
                Log.e("food_delivery_chef", "" + homeFragmentModelList.get(j).getServiceType());
                for (int i = 0; i < item.size(); i++) {
                    if (item.get(i).equalsIgnoreCase("Home private meal")) {
                        serviceTypeModel.add(homeFragmentModelList.get(j));
                        Log.e("food_delivery_chef", "" + serviceTypeModel);
                    }

                }
            }
        }
        if (restaurant_private_meal_chef.isChecked()) {
            serviceList.add(restaurant_private_meal_chefStr);
            for (int j = 0; j < homeFragmentModelList.size(); j++) {
                List<String> item = Arrays.asList(homeFragmentModelList.get(j).getServiceType().split("\\s*,\\s*"));
                Log.e("food_delivery_chef", "" + homeFragmentModelList.get(j).getServiceType());
                for (int i = 0; i < item.size(); i++) {
                    if (item.get(i).equalsIgnoreCase("Restaurant private meal")) {
                        serviceTypeModel.add(homeFragmentModelList.get(j));
                        Log.e("food_delivery_chef", "" + serviceTypeModel);
                    }

                }
            }
        }
        if (chef_onsite_cooking_chef.isChecked()) {
            serviceList.add(chef_onsite_cooking_chefStr);
            for (int j = 0; j < homeFragmentModelList.size(); j++) {
                List<String> item = Arrays.asList(homeFragmentModelList.get(j).getServiceType().split("\\s*,\\s*"));
                Log.e("food_delivery_chef", "" + homeFragmentModelList.get(j).getServiceType());
                for (int i = 0; i < item.size(); i++) {
                    if (item.get(i).equalsIgnoreCase("Chef onsite cooking")) {
                        serviceTypeModel.add(homeFragmentModelList.get(j));
                        Log.e("food_delivery_chef", "" + serviceTypeModel);
                    }

                }
            }
        }
        if (cooking_workshop_chef.isChecked()) {
            serviceList.add(cooking_workshop_chefStr);
            for (int j = 0; j < homeFragmentModelList.size(); j++) {
                List<String> item = Arrays.asList(homeFragmentModelList.get(j).getServiceType().split("\\s*,\\s*"));
                Log.e("food_delivery_chef", "" + homeFragmentModelList.get(j).getServiceType());
                for (int i = 0; i < item.size(); i++) {
                    if (item.get(i).equalsIgnoreCase("Cooking workshop")) {
                        serviceTypeModel.add(homeFragmentModelList.get(j));
                        Log.e("food_delivery_chef", "" + serviceTypeModel);
                    }

                }

            }

        }
        if (daily_meal.isChecked()) {
            serviceList.add(daily_mealStr);
            for (int j = 0; j < homeFragmentModelList.size(); j++) {
                List<String> item = Arrays.asList(homeFragmentModelList.get(j).getServiceType().split("\\s*,\\s*"));
                Log.e("food_delivery_chef", "" + homeFragmentModelList.get(j).getServiceType());
                for (int i = 0; i < item.size(); i++) {
                    if (item.get(i).equalsIgnoreCase("Daily meal")) {
                        serviceTypeModel.add(homeFragmentModelList.get(j));
                        Log.e("food_delivery_chef", "" + serviceTypeModel);
                    }

                }

            }
        }
        if (serviceList.isEmpty()) {
            Toast.makeText(getActivity(), R.string.please_select, Toast.LENGTH_SHORT).show();
        } else {
            list = TextUtils.join(",", serviceList);
            Log.e("list", "" + list);

            if (!serviceTypeModel.isEmpty()) {
                // ---------------Adapter---------------------
                Log.e("Data", "" + serviceTypeModel);
                HashSet<HomeFragmentModel> hashSet = new HashSet<HomeFragmentModel>();
                hashSet.addAll(serviceTypeModel);
                serviceTypeModel.clear();
                serviceTypeModel.addAll(hashSet);

                homeFragmentAdapter = new HomeFragmentAdapter(getActivity().getApplicationContext(), serviceTypeModel);
                // setting adapter on recycler view
                recyclerView.setAdapter(homeFragmentAdapter);

                homeFragmentAdapter.notifyDataSetChanged();
            } else {
                no_image_available.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        }
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void progressDismiss() {
        if (progressDialog.isShowing() && progressDialog != null && getActivity() != null) {
            progressDialog.dismiss();
        }
    }
}
