package com.vervelogic.chefdiner.bottomFragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.appdatabase.DatabaseMgrApp;
import com.vervelogic.chefdiner.chatModel.DeviceTokenModel;
import com.vervelogic.chefdiner.chatModel.RecentChatModel;
import com.vervelogic.chefdiner.firebase.ChatingContactAdapter;
import com.vervelogic.chefdiner.firebase.ChattingActivity;
import com.vervelogic.chefdiner.firebase.ContactListActivity;
import com.vervelogic.chefdiner.firebase.FirebaseConstant;
import com.vervelogic.chefdiner.firebase.RecentActiveAdapter;
import com.vervelogic.chefdiner.fragments.About;
import com.vervelogic.chefdiner.fragments.Account;
import com.vervelogic.chefdiner.fragments.ArchiveFrag;
import com.vervelogic.chefdiner.fragments.ChefProfileFragment;
import com.vervelogic.chefdiner.fragments.InboxFrag;
import com.vervelogic.chefdiner.model.ChatDetailsModel;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


public class Message extends Fragment  {


    public View view;
    String languageSpeak;



    public static Message newInstance() {
        Message fragment = new Message();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view=inflater.inflate(R.layout.fragment_message, container, false);


            //Toolbar setting in fragment
            Toolbar toolbar = view.findViewById(R.id.toolbar);
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
            toolbar.setTitleTextColor(0xFFFFFFFF);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.message);


            ViewPager viewPager = view.findViewById(R.id.sub_fragment_message);
            setupViewPager(viewPager);

            TabLayout tabLayout = view.findViewById(R.id.tabs_messages);
            tabLayout.setupWithViewPager(viewPager);

            SharedPreference sharedPreference=new SharedPreference();
            if (!TextUtils.isEmpty(sharedPreference.getLanguage(getActivity().getApplicationContext())))
            {
                languageSpeak=sharedPreference.getLanguage(getActivity().getApplicationContext());
            }
            else
            {
                Intent intent = getActivity().getIntent();
                languageSpeak = intent.getStringExtra("language");
            }


        }
        // Inflate the layout for this fragment
        return view;
    }



    /*---------------------------Set Fragments on tabs-----------------------------------*/
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new InboxFrag(), getString(R.string.inbox));
        adapter.addFragment(new ArchiveFrag(), getString(R.string.archive));
        viewPager.setAdapter(adapter);
    }
    /*------------------Adapter for Fragments---------------------------------------------*/
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



}
