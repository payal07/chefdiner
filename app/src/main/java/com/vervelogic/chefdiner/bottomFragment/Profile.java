package com.vervelogic.chefdiner.bottomFragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.activity.BecomeAChef;
import com.vervelogic.chefdiner.activity.BecomeAChefSelectLanguage;
import com.vervelogic.chefdiner.adapter.ItemResponse;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.app.CustomMultipartRequest;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.fragments.About;
import com.vervelogic.chefdiner.fragments.Account;
import com.vervelogic.chefdiner.fragments.ChefProfileFragment;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;


public class Profile extends Fragment implements ItemResponse {

    private static final String TAG = "Profile";
    public View view;
    LinearLayout user_name_ly;
    ImageView edit_user_name;
    EditText user_name;
    Button become_a_chef_profile;
    CircleImageView profileImageChef;
    TextView saveImage;
    ImageView add_image_chef;
    DbHelper dbHelper;
    String user_id;
    String f_name, l_name="", credited_amount;
    /*------------------------- for user string json------------------*/
    String first_name, last_name, email, phone_number, profile_pic, is_verify, is_verify_chef, is_user,
            is_chef, language, authtoken, id, fb_auth_id, access_token, referal_code, facebook_login;
    /*--------------------------for chef string json-------------------*/
    String id_chef, first_name_chef, last_name_chef, email_chef, phone_number_chef, profile_pic_chef, is_verify_chef_chef, is_user_chef,
            is_chef_chef, language_chef, authtoken_chef, chef_fname, chef_lname, language_speak, service_type, oftencook, chef_country,
            chef_city, from_guest, upto_guest, kitchen_title, kitchen_descrition, favorite, brunchStr, lunchStr, dinnerStr, currency,
            selectedCur = "";
    String sendImage;
    Bitmap bitmap;
    String languageSpeak;
    ProgressDialog progressDialog;
    private int TAKE_PICTURE = 2;
    private int SELECT_PICTURE = 1;
    private SharedPreference sharedPreference;

    public static Profile newInstance() {
        return new Profile();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            // Inflate the layout for this fragment
            view = inflater.inflate(R.layout.fragment_profile, container, false);

            //Toolbar setting in fragment
            Toolbar toolbar = view.findViewById(R.id.toolbar);
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
            toolbar.setTitleTextColor(0xFFFFFFFF);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.profile);


            ViewPager viewPager = view.findViewById(R.id.sub_fragment);
            setupViewPager(viewPager);

            TabLayout tabLayout = view.findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
            viewPager.setCurrentItem(1);

            progressDialog = new ProgressDialog(getActivity());

            SharedPreference sharedPreference = new SharedPreference();
            if (!TextUtils.isEmpty(sharedPreference.getLanguage(getActivity().getApplicationContext()))) {
                languageSpeak = sharedPreference.getLanguage(getActivity().getApplicationContext());
            } else {
                Intent intent = getActivity().getIntent();
                languageSpeak = intent.getStringExtra("language");
            }

        /* --------------------------------IDs----------------------------------*/
            user_name = view.findViewById(R.id.user_name);
            edit_user_name = view.findViewById(R.id.edit_name);
            user_name_ly = view.findViewById(R.id.user_name_ly);
            profileImageChef = view.findViewById(R.id.profileImageChef);
            become_a_chef_profile = view.findViewById(R.id.become_a_chef_profile);
            add_image_chef = view.findViewById(R.id.add_image_chef);
            saveImage = view.findViewById(R.id.saveImage);

       /*---------------------------Listener-------------------------------------*/
            edit_user_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    user_name.setEnabled(true);
                }
            });

            add_image_chef.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showFileChooser();
                }
            });
            become_a_chef_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String name = user_name.getText().toString();
                    String[] result = name.split("\\s");
                    for (int x = 0; x < result.length; x++) {
                       /* System.out.println(result[x]);*/
                        f_name = result[0];
                        if (x > 0) {
                            l_name = result[1];
                        }
                    }

                    dbHelper = new DbHelper(getActivity().getApplicationContext());
                    List<UserDetail> contacts = dbHelper.getAllDetails();
                    for (UserDetail cn : contacts) {
                        is_chef_chef = cn.getIsChef();
                        Log.e(TAG, "is_chef " + cn.getIsChef());
                    }
                    if (!TextUtils.isEmpty(is_chef_chef)) {
                        if (is_chef_chef.equals("1")) {
                            Intent intent = new Intent(getActivity(), BecomeAChef.class);
                            startActivity(intent);

                        } else {
                            Intent intent = new Intent(getActivity(), BecomeAChefSelectLanguage.class);
                            intent.putExtra("language", languageSpeak);
                            startActivity(intent);
                        }
                    }
                }
            });

            saveImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String name = user_name.getText().toString();
                    StringTokenizer tokens = new StringTokenizer(name, " ");
                    f_name = tokens.nextToken();// this will contain "Fruit"
                    while (tokens.hasMoreTokens()) {
                        l_name = tokens.nextToken();// this will contain " they taste good"
                    }
                    ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        if (!TextUtils.isEmpty(last_name))
                        {
                            updateUserAPI(sendImage);
                        }
                        else
                        {
                            last_name="";
                            updateUserAPI(sendImage);
                        }

                    }

                }
            });
            //Database
            dbHelper = new DbHelper(getActivity().getApplicationContext());
            List<UserDetail> contacts = dbHelper.getAllDetails();
            for (UserDetail cn : contacts) {
                //sendImage = cn.getProfilePic();
                user_name.setText(cn.getFirstName() + " " + cn.getLastName());

                if (!TextUtils.isEmpty(cn.getProfilePic())) {
                    Log.e("Image", "" + cn.getProfilePic());
                    Picasso.with(getActivity())
                            .load(cn.getProfilePic())
                            .fit()
                            .into(profileImageChef);
                    add_image_chef.setImageResource(android.R.color.transparent);
                } else {
                    //profileImageChef.setImageResource(R.drawable.profile);
                    /*add_image_chef.setImageResource(android.R.color.transparent);*/
                }
            }
            sharedPreference = new SharedPreference();
            user_id = sharedPreference.getUserId(getActivity());
            ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            if (netInfo == null) {
                new AlertDialog.Builder(getActivity())
                        .setTitle(getResources().getString(R.string.app_name))
                        .setMessage(getResources().getString(R.string.internet_error))
                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
            } else {
                getUserInfoAPI();
            }

        }
        return view;
    }


    /*---------------------------Set Fragments on tabs-----------------------------------*/
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new About(), getString(R.string.about));
        adapter.addFragment(new Account(this), getString(R.string.account));
        adapter.addFragment(new ChefProfileFragment(), getString(R.string.chef_profile));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void itemClickResponse(Object status, Object response, String pos) {
        boolean s = (boolean) status;
        if (s) {
            selectedCur = response.toString();
            Log.e(TAG, "Selected Currency " + selectedCur);
        } else {
            selectedCur = "";
        }
    }

    // Image sent in file object form
    private void showFileChooser() {

        final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.choose_photo),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.add_photo);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (options[which].equals(getString(R.string.take_photo))) {
                    Intent cameraIntent = new Intent(
                            android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, TAKE_PICTURE);


                } else if (options[which].equals(getString(R.string.choose_photo))) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, SELECT_PICTURE);
                } else if (options[which].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);


        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            Uri filePath = intent.getData();
            if (null != filePath) {
                // Get the path from the Uri
                sendImage = getPathFromURI(filePath);
                Log.e("Upload", "Image Path->Gallery :" + sendImage);
                // Set the image in ImageView
                profileImageChef.setImageURI(filePath);
                add_image_chef.setImageResource(android.R.color.transparent);

            }
          /* try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                img_profile.setImageBitmap(bitmap);
                sendImage = getStringImage(bitmap);
                Log.e(TAG, "onActivityResult: "+ sendImage );

               updateProfile(sendImage);
            } catch (IOException e) {
                e.printStackTrace();
            }*/
        } else if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            if (intent.getExtras() == null) {
                return;
            }
            bitmap = (Bitmap) intent.getExtras().get("data");
            Log.e("Bitmap", "" + bitmap);
            profileImageChef.setImageBitmap(bitmap);
            add_image_chef.setImageResource(android.R.color.transparent);
            Uri tempUri = getImageUri(getActivity(), bitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            sendImage = finalFile.toString();
            Log.e("Image", "Image path->Camera :" + sendImage);
        }
    }

    public String getRealPathFromURI(Uri uri) {
        @SuppressLint("Recycle") Cursor cursor = getActivity().getContentResolver().query(uri, null,
                null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    /*-----------------------UPDATE USER API-----------------------------------------*/
    public void updateUserAPI(String sendImage) {

        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        if (!TextUtils.isEmpty(sendImage)) {
            //Auth header
            Map<String, String> mHeaderPart = new HashMap<>();
            mHeaderPart.put("Content-type", "multipart/form-data;");

            //File part
            Map<String, File> mFilePartData = new HashMap<>();
            mFilePartData.put("fileToUpload", new File(sendImage));
            Log.e("Image File", "" + new File(sendImage));


            //String part
            Map<String, String> mStringPart = new HashMap<>();
            mStringPart.put("id", user_id);
            mStringPart.put("first_name", f_name);
            mStringPart.put("last_name", l_name);
            mStringPart.put("currency", selectedCur);
            Log.e("Map", "" + mStringPart);

            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, getContext(), AppConfig.UPDATE_USER_INFO_URL, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    progressDismiss();

                    try {
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            Log.e(TAG, "" + jsonObject);
                            int id = jsonObject.getInt("id");
                            ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                            if (netInfo == null) {
                                new AlertDialog.Builder(getActivity())
                                        .setTitle(getResources().getString(R.string.app_name))
                                        .setMessage(getResources().getString(R.string.internet_error))
                                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
                            } else {
                                getUserInfoAPI();
                            }
                            String message = jsonObject.getString("message");
                            Log.e(TAG, "" + message);


                        } else if (status == 0) {
                            int id = jsonObject.getInt("id");
                            String message = jsonObject.getString("message");
                            Toast.makeText(getContext(), "" + message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDismiss();
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDismiss();
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());
                }

            }, mFilePartData, mStringPart, mHeaderPart);

            RequestQueue rQueue = Volley.newRequestQueue(getContext());
            rQueue.add(mCustomRequest);
        } else {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.UPDATE_USER_INFO_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDismiss();
                            Log.e(TAG, "" + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                int status = jsonObject.getInt("status");
                                if (status == 1) {
                                    Log.e(TAG, "" + jsonObject);
                                    int id = jsonObject.getInt("id");
                                    ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                    if (netInfo == null) {
                                        new AlertDialog.Builder(getActivity())
                                                .setTitle(getResources().getString(R.string.app_name))
                                                .setMessage(getResources().getString(R.string.internet_error))
                                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                    } else {
                                        getUserInfoAPI();
                                    }
                                    String message = jsonObject.getString("message");
                                    Log.e(TAG, "" + message);

                                } else if (status == 0) {
                                    int id = jsonObject.getInt("id");
                                    String message = jsonObject.getString("message");
                                    Toast.makeText(getContext(), "" + message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDismiss();
                                if (getActivity() != null) {
                                    Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDismiss();
                                if (getActivity() != null) {
                                    Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDismiss();
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Please check your internet connection.";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onErrorResponse: " + message);
                    Log.e("Error: ", error.toString());

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    //  String image = getStringImage(bitmap);
                    map.put("id", user_id);
                    map.put("first_name", f_name);
                    map.put("last_name", l_name);
                    map.put("currency", selectedCur);
                    Log.e("Map", "" + map);
                    return map;
                }
            };
            int socketTimeout = 30000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(stringRequest);
        }
    }

    /*------------------USER INFO API-----------------------------------------*/
    public void getUserInfoAPI() {

        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        String uri = String.format(AppConfig.GET_USER_INFO_URL + "?user_id=%1$s", user_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (getActivity() != null) {
                           progressDismiss();
                        }
                        Log.e(TAG, response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            String message = jsonObject.getString("message");
                            if (status == 1) {
                                UserDetail userDetail = new UserDetail();
                                Log.e(TAG, response);
                                dbHelper.deleteUser();
                                JSONArray jsonArray = jsonObject.getJSONArray("getuserinfo");
                                for (int i = 0; i <= jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    first_name = object.getString("first_name");
                                    last_name = object.getString("last_name");
                                    email = object.getString("email");
                                    phone_number = object.getString("phone_number");
                                    profile_pic = object.getString("profile_pic");
                                    is_verify = object.getString("is_verify");
                                    is_verify_chef = object.getString("is_verify_chef");
                                    is_user = object.getString("is_user");
                                    is_chef = object.getString("is_chef");
                                    language = object.getString("language");
                                    authtoken = object.getString("authtoken");
                                    id = object.getString("id");
                                    fb_auth_id = object.getString("fb_auth_id");
                                    facebook_login = object.getString("facebook_login");
                                    currency = object.getString("currency");
                                    access_token = object.getString("access_token");
                                    referal_code = object.getString("referal_code");
                                    credited_amount = object.getString("credited_amount");
                                    currency = object.getString("currency");

                                    userDetail.setUser_id(user_id);
                                    userDetail.setFirstName(first_name);
                                    userDetail.setLastName(last_name);
                                    userDetail.setEmailId(email);
                                    userDetail.setPhoneNumber(phone_number);
                                    userDetail.setIsChef(is_chef);
                                    userDetail.setLanguage(language);
                                    userDetail.setAuthToken(authtoken);
                                    userDetail.setProfilePic(profile_pic);
                                    userDetail.setIsVerifyUser(is_verify);
                                    userDetail.setIsChefVerify(is_verify_chef);
                                    userDetail.setIsUser(is_user);
                                    userDetail.setFbId(fb_auth_id);
                                    userDetail.setCurrency(currency);
                                    userDetail.setAccessToken(access_token);
                                    userDetail.setCreditedAmount(credited_amount);
                                    userDetail.setReferralCode(referal_code);
                                    /*userDetail.setAccessToken(referal_code);*/
                                    dbHelper.addUser(userDetail);
                                    getChefInfoAPI();

                                }
                            } else if (status == 0) {
                                Toast.makeText(getContext(), "" + message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDismiss();
                            //  Toast.makeText(getActivity(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDismiss();
                            if (getActivity() != null) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());

                    }
                });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    // --------------IS CHEF VALIDATION API FOR BECOME A CHEF BUTTON------------
    public void getChefInfoAPI() {

        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();

        String uri = String.format(AppConfig.GET_CHEF_INFO_URL + "?user_id=%1$s", user_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDismiss();

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            String message = jsonObject.getString("message");
                            if (status == 1) {
                                Log.e(TAG, response);
                                dbHelper.deleteUser();
                                UserDetail userDetail = new UserDetail();
                                JSONArray jsonArray = jsonObject.getJSONArray("getchefinfo");
                                for (int i = 0; i <= jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    id_chef = object.getString("id");
                                    first_name_chef = object.getString("first_name");
                                    last_name_chef = object.getString("last_name");
                                    email_chef = object.getString("email");
                                    phone_number_chef = object.getString("phone_number");
                                    profile_pic_chef = object.getString("profile_pic");
                                    is_verify_chef_chef = object.getString("is_verify_chef");
                                    is_user_chef = object.getString("is_user");
                                    is_chef_chef = object.getString("is_chef");
                                    language_chef = object.getString("language");
                                    authtoken_chef = object.getString("authtoken");
                                    chef_fname = object.getString("chef_fname");
                                    chef_lname = object.getString("chef_lname");
                                    language_speak = object.getString("language_speak");
                                    service_type = object.getString("service_type");
                                    oftencook = object.getString("oftencook");
                                    chef_country = object.getString("chef_country");
                                    chef_city = object.getString("chef_city");
                                    from_guest = object.getString("from_guest");
                                    upto_guest = object.getString("upto_guest");
                                    kitchen_title = object.getString("kitchen_title");
                                    kitchen_descrition = object.getString("kitchen_descrition");
                                    favorite = object.getString("favorite");
                                    brunchStr = object.getString("brunch");
                                    lunchStr = object.getString("lunch");
                                    dinnerStr = object.getString("dinner");
                                    currency = object.getString("currency");

                                    userDetail.setUser_id(user_id);
                                    userDetail.setFirstName(first_name_chef);
                                    userDetail.setLastName(last_name_chef);
                                    userDetail.setEmailId(email_chef);
                                    userDetail.setPhoneNumber(phone_number_chef);
                                    userDetail.setIsChefVerify(is_verify_chef_chef);
                                    userDetail.setLanguage(language_chef);
                                    userDetail.setIsChef(is_chef_chef);
                                    userDetail.setAuthToken(authtoken_chef);
                                    userDetail.setProfilePic(profile_pic_chef);
                                    userDetail.setLanguageList(language_speak);
                                    userDetail.setServiceType(service_type);
                                    userDetail.setOftenCook(oftencook);
                                    userDetail.setCountry(chef_country);
                                    userDetail.setChefCity(chef_city);
                                    userDetail.setMax_guest(upto_guest);
                                    userDetail.setMin_guest(from_guest);
                                    userDetail.setKitchenTitle(kitchen_title);
                                    userDetail.setKitchenDescription(kitchen_descrition);
                                    userDetail.setLunch(lunchStr);
                                    userDetail.setBrunch(brunchStr);
                                    userDetail.setDinner(dinnerStr);
                                    userDetail.setCurrency(currency);
                                    userDetail.setReferralCode(referal_code);
                                    dbHelper.addUser(userDetail);
/*
                                    Intent intent = new Intent(getActivity(), BecomeAChef.class);
                                    startActivity(intent);*/
                                }
                            } else if (status == 0) {
                                Toast.makeText(getContext(), "" + message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDismiss();
                            //Toast.makeText(getActivity(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDismiss();
                            if (getActivity() != null)
                                Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        Log.e("Path", "" + Uri.parse(path));
        return Uri.parse(path);
    }

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public void progressDismiss() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /*------------------Adapter for Fragments---------------------------------------------*/
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
