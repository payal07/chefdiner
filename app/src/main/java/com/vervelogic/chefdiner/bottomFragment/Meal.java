package com.vervelogic.chefdiner.bottomFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.fragments.CalendarFrag;
import com.vervelogic.chefdiner.fragments.History;
import com.vervelogic.chefdiner.fragments.Upcoming;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.util.ArrayList;
import java.util.List;


public class Meal extends Fragment {

    View view;
    String languageSpeak;
    SharedPreference sharedPreference;

    public static Meal newInstance() {
        return new Meal();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_meal, container, false);


            //Toolbar setting in fragment
            Toolbar toolbar = view.findViewById(R.id.toolbar);
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
            toolbar.setTitleTextColor(0xFFFFFFFF);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.meal);


            ViewPager viewPager = view.findViewById(R.id.meals_sub_fragment);
            setupViewPager(viewPager);

            TabLayout tabLayout = view.findViewById(R.id.meals_tabs);
            tabLayout.setupWithViewPager(viewPager);

            sharedPreference=new SharedPreference();
            if (!TextUtils.isEmpty(sharedPreference.getLanguage(getActivity().getApplicationContext())))
            {
                languageSpeak=sharedPreference.getLanguage(getActivity().getApplicationContext());
            }
            else
            {
                Intent intent = getActivity().getIntent();
                languageSpeak = intent.getStringExtra("language");
            }



        }
        return view;
    }


    /*---------------------------Set Fragments on tabs-----------------------------------*/
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new Upcoming(), getString(R.string.upcoming));
        adapter.addFragment(new History(), getString(R.string.history));
        adapter.addFragment(new CalendarFrag(), getString(R.string.calendar));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
