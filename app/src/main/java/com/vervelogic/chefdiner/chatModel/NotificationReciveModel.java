package com.vervelogic.chefdiner.chatModel;

/**
 * Created by Vishnu Saini on 1/19/2018.
 * vishnusainideveloper27@gmail.com
 */

public class NotificationReciveModel {


    /**
     * message : {"groupId":"beed13602b9b0e6ecb5b568ff5058f07","message":"helloo","notificationType":"message","reciverId":"38","senderId":"3"}
     * notificationType : message
     * text : helloo
     */

    private String message;
    private String notificationType;
    private String text;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


}
