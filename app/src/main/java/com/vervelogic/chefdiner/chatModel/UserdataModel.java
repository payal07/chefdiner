package com.vervelogic.chefdiner.chatModel;

/**
 * Created by Vishnu Saini on 1/15/2018.
 * vishnusainideveloper27@gmail.com
 */

public class UserdataModel {


    /**
     * userId : 43
     * first_name : abcd
     * last_name : efg
     * is_chef : 0
     * is_verify_chef : no
     * phone_number : 1234567896
     * email : abcefg@gmail.com
     * referal_code : O6YOXNEXHK
     * profile_pic :
     */

    private String userId;
    private String first_name;
    private String last_name;
    private String is_chef;
    private String is_verify_chef;
    private String phone_number;
    private String email;
    private String referal_code;
    private String profile_pic;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getIs_chef() {
        return is_chef;
    }

    public void setIs_chef(String is_chef) {
        this.is_chef = is_chef;
    }

    public String getIs_verify_chef() {
        return is_verify_chef;
    }

    public void setIs_verify_chef(String is_verify_chef) {
        this.is_verify_chef = is_verify_chef;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReferal_code() {
        return referal_code;
    }

    public void setReferal_code(String referal_code) {
        this.referal_code = referal_code;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
