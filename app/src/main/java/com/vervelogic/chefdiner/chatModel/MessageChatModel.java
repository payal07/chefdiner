package com.vervelogic.chefdiner.chatModel;

import java.io.Serializable;

/**
 * Created by ubuntu on 24/10/16.
 */
public class MessageChatModel implements Serializable {

    public String createdAt;
    public String groupId;
    public String objectId;
    public String senderId;
    public String senderName;
    public String status;
    public String text;
    public String type;
    public String updatedAt;
    public String isRead;
    public String reciverId;


    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    public MessageChatModel() {
    }

    public MessageChatModel(String createdAt, String groupId, String objectId, String senderId, String senderName,
                            String status, String text, String type, String updatedAt, String isRead, String reciverId) {
        this.createdAt = createdAt;
        this.groupId = groupId;
        this.objectId = objectId;
        this.senderId = senderId;
        this.senderName = senderName;
        this.status = status;
        this.text = text;
        this.type = type;
        this.updatedAt = updatedAt;
        this.isRead = isRead;
        this.reciverId = reciverId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getReciverId() {
        return reciverId;
    }

    public void setReciverId(String reciverId) {
        this.reciverId = reciverId;
    }
}
