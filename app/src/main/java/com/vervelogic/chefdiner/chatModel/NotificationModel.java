package com.vervelogic.chefdiner.chatModel;

import java.io.Serializable;

/**
 * Created by Vishnu Saini on 1/19/2018.
 * vishnusainideveloper27@gmail.com
 */

public class NotificationModel implements Serializable{

    private String  notificationType;
    private String message;
    private String groupId;
    private String senderId;
    private String reciverId;

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReciverId() {
        return reciverId;
    }

    public void setReciverId(String reciverId) {
        this.reciverId = reciverId;
    }
}
