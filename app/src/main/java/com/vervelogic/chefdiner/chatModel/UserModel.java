package com.vervelogic.chefdiner.chatModel;

/**
 * Created by ubuntu on 6/10/16.
 */
public class UserModel {

    private String userId;
    private String userName;
    private String userPhoneNo;
    private String userEmail;
    private String userQuickblockEmail;
    private String userAddress;
    private String userImage;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhoneNo() {
        return userPhoneNo;
    }

    public void setUserPhoneNo(String userPhoneNo) {
        this.userPhoneNo = userPhoneNo;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserQuickblockEmail() {
        return userQuickblockEmail;
    }

    public void setUserQuickblockEmail(String userQuickblockEmail) {
        this.userQuickblockEmail = userQuickblockEmail;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }
}
