package com.vervelogic.chefdiner.chatModel;

import java.io.Serializable;

/**
 * Created by ubuntu on 24/10/16.
 */
public class RecentChatModel implements Serializable {

    public String counter;
    public String createdAt;
    public String description;
    public String groupId;
    public String isDeleted;
    public String lastMessage;
    public String picture;
    public String type;
    public String updatedAt;
    public String senderId;
    public String reciverId;
    public String objectId;
    public String groupName;
    public String isArchive;

    private String data;

    public RecentChatModel(String counter, String createdAt, String description, String groupId, String isDeleted,
                           String lastMessage, String picture, String type, String updatedAt,
                           String senderId, String reciverId, String objectId, String data, String groupName,String isArchive) {
        this.counter = counter;
        this.createdAt = createdAt;
        this.description = description;
        this.groupId = groupId;
        this.isDeleted = isDeleted;
        this.lastMessage = lastMessage;
        this.picture = picture;
        this.type = type;
        this.updatedAt = updatedAt;
        this.senderId = senderId;
        this.reciverId = reciverId;
        this.objectId = objectId;
        this.data=data;
        this.groupName=groupName;
        this.isArchive=isArchive;
    }

    public RecentChatModel() {

    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReciverId() {
        return reciverId;
    }

    public void setReciverId(String reciverId) {
        this.reciverId = reciverId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public static class MemberList implements Serializable {
        private String id;
        private String number;
        private String isSender;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getIsSender() {
            return isSender;
        }

        public void setIsSender(String isSender) {
            this.isSender = isSender;
        }
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getIsArchive() {
        return isArchive;
    }

    public void setIsArchive(String isArchive) {
        this.isArchive = isArchive;
    }
}
