package com.vervelogic.chefdiner.chatModel;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Vishnu Saini on 1/12/2018.
 * vishnusainideveloper27@gmail.com
 */

public class Utils {

    public static long SERVER_TIME = 00;
    public static long CURRENT_TIME = 00;

    public static String currentTimeStap() {

        String currentTime = "";
        try {
            long diff = System.currentTimeMillis() - CURRENT_TIME;
            long cirrentTmStap = SERVER_TIME + diff;
            //	String newsfsf= Utils.getmsgTime(String.valueOf(cirrentTmStap));
            currentTime = (String.valueOf(cirrentTmStap));

        } catch (Exception e) {
            currentTime = (String.valueOf(System.currentTimeMillis()));
            e.printStackTrace();
        }

        return currentTime;

    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String parsedlastseen(String timemillisec) {
        String lastseen = "";
        if (timemillisec != null && timemillisec.trim().length() != 0) {
            long currentmili = Long.valueOf(Utils.currentTimeStap()); //System.currentTimeMillis();
            long msgmili = Long.parseLong(timemillisec);
            long diffmili = currentmili - msgmili;
            long hours = TimeUnit.MILLISECONDS.toHours(diffmili);
            long min = TimeUnit.MILLISECONDS.toMinutes(diffmili);
            long sec = TimeUnit.MILLISECONDS.toSeconds(diffmili);
            long day = TimeUnit.MILLISECONDS.toDays(diffmili);
            if (day != 0) {
                lastseen = String.valueOf(day) + " days ago";

                lastseen = getmsgDate(timemillisec);
                lastseen = getTimeStringfrommiliSec(timemillisec);

            } else if (hours != 0) {
                lastseen = String.valueOf(hours) + " hours ago";
                lastseen = getTimeStringfrommiliSec(timemillisec);

            } else if (min != 0) {
                lastseen = String.valueOf(min) + " mins ago";
                lastseen = getTimeStringfrommiliSec(timemillisec);
            } else if (sec != 0) {
                lastseen = String.valueOf(sec) + " secs ago";
                lastseen = lastseen.replace("-", "");
                lastseen = getTimeStringfrommiliSec(timemillisec);

            } else {
                lastseen = "just now";
                lastseen = getTimeStringfrommiliSec(timemillisec);
            }

        }
        return lastseen;
    }

    public static String getmsgDate(String timemillisec) {
        String msgDate = "";
        if (timemillisec != null && timemillisec.trim().length() != 0) {

            Date msgD = new Date(Long.parseLong(timemillisec));
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            msgDate = dateFormat.format(msgD);
        }
        return msgDate;
    }

    public static String getTimeStringfrommiliSec(String milisec) {
        Date date = new Date(Long.parseLong(milisec));
        SimpleDateFormat formate = new SimpleDateFormat("hh:mm a");
        return formate.format(date).toUpperCase();
    }

    public static void loadImageUsingUrl(Context context, ImageView imageView, String url) {

        if (url!=null && !url.isEmpty()) {
            Picasso.with(context)
                    .load(url)
                    .placeholder(context.getResources().getDrawable(R.drawable.profile))
                    .into(imageView);
        }
        else
        {
            Picasso.with(context)
                    .load(R.drawable.profile)
                    .placeholder(context.getResources().getDrawable(R.drawable.profile))
                    .into(imageView);
        }
    }

}
