package com.vervelogic.chefdiner.chatModel;

/**
 * Created by Vishnu Saini on 1/19/2018.
 * vishnusainideveloper27@gmail.com
 */

public class NotificationMessageModel {


    /**
     * groupId : beed13602b9b0e6ecb5b568ff5058f07
     * message : helloo
     * notificationType : message
     * reciverId : 38
     * senderId : 3
     */

    private String groupId;
    private String message;
    private String notificationType;
    private String reciverId;
    private String senderId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getReciverId() {
        return reciverId;
    }

    public void setReciverId(String reciverId) {
        this.reciverId = reciverId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
