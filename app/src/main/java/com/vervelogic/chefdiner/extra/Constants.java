package com.vervelogic.chefdiner.extra;

/**
 * Created by ubuntu on 28/9/16.
 */
public class Constants {

    public static final String COMINGDATEFORMATFROMSERVER = "yyyy-MM-dd HH:mm:ssz";
    public static final String SHOW_DATEFORMATE = "yyyy-MM-dd";
    public static final String SHOW_TIMEFORMATE = "HH:mm";
    public static final String SHOW_TIMEFORMATES = "HH:mm:ssz";
    public static final String PHONENUMBER = "phonenumber";
    public static final String UTC_TIMEZONE = "UTC";
    public static final String SMS = "sms";
    public static final String PHONE = "phone";
    public static final String WEB = "web";
    public static final String OPTION = "option";
    public static final String STATUS = "status";

    public static final String FLD_Alert = "alert";
    public static final String FLD_PHONE = "phone";
    public static final String FLD_SMS = "sms";
    public static final String FLD_CONTACT_DATA = "contactData";
    public static final String ARG_USERS = "users";
    public static final String ARG_RECEIVER = "receiver";
    public static final String ARG_RECEIVER_UID = "receiver_uid";
    public static final String ARG_CHAT_ROOMS = "chat_rooms";
    public static final String ARG_FIREBASE_TOKEN = "firebaseToken";
    public static final String ARG_FRIENDS = "friends";
    public static final String FLD_NOTY_CONTACT_DATA = "notyContactData";
    public static final String FLD_GROUP_ID = "group_Id";
    public static final String ARG_UID = "uid";

    public static final String FLD_ALERT_DATA = "alertData";
    public static final String FLD_HOME_ALERT = "homeAlert";



    /*************DB Constantsa************************/

    public static final String DB_NAME = "ChefDiners";
    public static final String TYPE = "chatType";
}
