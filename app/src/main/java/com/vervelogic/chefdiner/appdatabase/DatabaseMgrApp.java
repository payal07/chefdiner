package com.vervelogic.chefdiner.appdatabase;


import android.content.ContentValues;
import android.database.Cursor;

import com.vervelogic.chefdiner.chatModel.DeviceTokenModel;
import com.vervelogic.chefdiner.db.DatabaseMgr;


/**
 * Created by Ramnivas Singh on 9/3/2016.
 */
public class DatabaseMgrApp {
    public static DatabaseMgr dbMgr;
    private static Object allGroupRoomJid;

    public static void init() {
        try {
            dbMgr = DatabaseMgr.getInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

//    public static int insertDataToAlertMessageTable(AlertMessage alertMessage) {
//        int insertedRow = 0;
//        if (alertMessage != null)
//            try {
//                ContentValues[] contentValues = new ContentValues[1];
//                ContentValues values = new ContentValues();
//                values.put(DatabaseFields.FLD_ALERT_ID, alertMessage.getAlertId());
//                values.put(DatabaseFields.FLD_ALERTMESSAGE, alertMessage.getAlertMessage());
//                values.put(DatabaseFields.FLD_ALERTSENDINGTIME, alertMessage.getAlertSendingTime());
//                values.put(DatabaseFields.FLD_ALERTTYPE, alertMessage.getAlertType());
//                values.put(DatabaseFields.FLD_USERCURRENTLOCATION, alertMessage.getUserCurrentLocation());
//                values.put(DatabaseFields.FLD_USERHOMEADDRESS, alertMessage.getUserHomeAddress());
//                values.put(DatabaseFields.FLD_USERIMAGE, alertMessage.getUserImage());
//                values.put(DatabaseFields.FLD_USERNAME, alertMessage.getUserName());
//                values.put(DatabaseFields.FLD_USERNO, alertMessage.getUserNo());
//
//                contentValues[0] = values;
//                insertedRow = DatabaseMgr.insertRows(DatabaseFields.TABLE_ALERTMESSAGES, contentValues);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        return insertedRow;
//    }

    public static int insertDataToDeviceTokenTable(DeviceTokenModel deviceTokenModel) {
        int insertedRow = 0;
        if (deviceTokenModel != null)
            try {
                ContentValues[] contentValues = new ContentValues[1];
                ContentValues values = new ContentValues();
                values.put(DatabaseFields.FLD_MOBILE_NO, deviceTokenModel.getMobileNo());
                values.put(DatabaseFields.FLD_DEVICE_TOKEN, deviceTokenModel.getDevicetoken());
                values.put(DatabaseFields.FLD_PROFILE_PIC, deviceTokenModel.getProfilepic());
                values.put(DatabaseFields.FLD_DEVICE_TYPE, deviceTokenModel.getDeviceType());
                values.put(DatabaseFields.FLD_EMAIL, deviceTokenModel.getEmail());
                values.put(DatabaseFields.FLD_USER_ID, deviceTokenModel.getUserId());
                values.put(DatabaseFields.FLD_USERNAME, deviceTokenModel.getUserName());
                contentValues[0] = values;
                insertedRow = DatabaseMgr.insertRows(DatabaseFields.TABLE_DEVICE_TOKEN, contentValues);

            } catch (Exception e) {
                e.printStackTrace();
            }
        return insertedRow;
    }

    public static int deleteDataToDeviceTokenTable(DeviceTokenModel deviceTokenModel) {
        int deletedRow = 0;
        if (deviceTokenModel != null)
            try {
                ContentValues[] contentValues = new ContentValues[1];
                ContentValues values = new ContentValues();
                values.put(DatabaseFields.FLD_MOBILE_NO, deviceTokenModel.getMobileNo());
                values.put(DatabaseFields.FLD_DEVICE_TOKEN, deviceTokenModel.getDevicetoken());
                values.put(DatabaseFields.FLD_PROFILE_PIC, deviceTokenModel.getProfilepic());
                values.put(DatabaseFields.FLD_DEVICE_TYPE, deviceTokenModel.getDeviceType());
                values.put(DatabaseFields.FLD_EMAIL, deviceTokenModel.getEmail());
                values.put(DatabaseFields.FLD_USER_ID, deviceTokenModel.getUserId());
                values.put(DatabaseFields.FLD_USERNAME, deviceTokenModel.getUserName());
                contentValues[0] = values;
                deletedRow = DatabaseMgr.deleteRow(DatabaseFields.TABLE_DEVICE_TOKEN, DatabaseFields.FLD_USER_ID+" =? ",new String[]{deviceTokenModel.getMobileNo()});

            } catch (Exception e) {
                e.printStackTrace();
            }
        return deletedRow;
    }



    public static DeviceTokenModel getDeviceTokenData(String query, String phoneNo) {
        DeviceTokenModel deviceTokenModel=new DeviceTokenModel();
        Cursor cursor = null;
        try {
            if (query == null)
                query = "Select * from " + TableType.DeviceTokenTable.getTableName() +" WHERE "+ DatabaseFields.FLD_USER_ID+" = '"+phoneNo+ "'";
            cursor = DatabaseMgr.sqLiteDb.rawQuery(query, null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {

                deviceTokenModel.setMobileNo(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_MOBILE_NO)));
                deviceTokenModel.setDevicetoken(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_DEVICE_TOKEN)));
                deviceTokenModel.setProfilepic(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_PROFILE_PIC)));
                deviceTokenModel.setDeviceType(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_DEVICE_TYPE)));
                deviceTokenModel.setEmail(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_EMAIL)));
                deviceTokenModel.setUserId(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_USER_ID)));
                deviceTokenModel.setUserName(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_USERNAME)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return deviceTokenModel;
    }

//    public static ArrayList<AlertMessage> getAlertMessageData(String query, Context context) {
//        ArrayList<AlertMessage> callHistoryList = new ArrayList<AlertMessage>();
//        Cursor cursor = null;
//        try {
//            if (query == null)
//                query = "Select * from " + TableType.AlertTable.getTableName() +" ORDER BY "+DatabaseFields.FLD_ALERTSENDINGTIME+ " DESC ";
//            cursor = dbMgr.sqLiteDb.rawQuery(query, null);
//            cursor.moveToFirst();
//            if (cursor.getCount() > 0) {
//                do {
//                    AlertMessage alertMessage = new AlertMessage();
//                    alertMessage.setAlertId(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_ALERT_ID)));
//                    alertMessage.setUserNo(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_USERNO)));
//                    alertMessage.setUserName(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_USERNAME)));
//                    alertMessage.setUserImage(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_USERIMAGE)));
//                    alertMessage.setUserCurrentLocation(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_USERCURRENTLOCATION)));
//                    alertMessage.setUserHomeAddress(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_USERHOMEADDRESS)));
//                    alertMessage.setAlertType(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_ALERTTYPE)));
//                    alertMessage.setAlertMessage(cursor.getString(cursor.getColumnIndex(DatabaseFields.FLD_ALERTMESSAGE)));
//                    alertMessage.setAlertSendingTime(""+cursor.getLong(cursor.getColumnIndex(DatabaseFields.FLD_ALERTSENDINGTIME)));
//                    callHistoryList.add(alertMessage);
//                } while (cursor.moveToNext());
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (cursor != null) {
//                cursor.close();
//                cursor = null;
//            }
//        }
//        return callHistoryList;
//    }

    private static void clearDbTable(String tableName) {
        try {
            DatabaseMgr.sqLiteDb.execSQL("DELETE FROM " + tableName);
            DatabaseMgr.sqLiteDb.execSQL("DELETE FROM sqlite_sequence WHERE  name='" + tableName + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int deleteRowAlertMessageTable(String alertId) {
        int effectedRows=0;
        try {
            effectedRows = DatabaseMgr.getInstance().deleteRow(TableType.AlertTable.getTableName(), DatabaseFields.FLD_ALERT_ID + " =? ", new String[]{alertId});
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return effectedRows;
    }



   /* public static String getCallLogRecentMessageId(ActLandingScreen actLandingScreen, String orderDescAsc) {
        String callLogId = "0";
        Cursor cursor = null;
        try {
            String query = "Select * from " + TableType.CallLogTable.getTableName() + " ORDER BY " + CallLogModel.FLD_ID + orderDescAsc + " LIMIT 1";
            cursor = DatabaseMgr.sqLiteDb.rawQuery(query, null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                callLogId = cursor.getString(cursor.getColumnIndex(CallLogModel.FLD_ID));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return callLogId;
    }


    public static Long getRecentDateFromMessageTable(ActLandingScreen actLandingScreen, String dialogId, String orderDescAsc) {
        Long messageDateSendInMill = 0L;
        if (dialogId != null) {
            Cursor cursor = null;
            try {
                String query = "Select * from " + TableType.ChatMessageTable.getTableName() + " Where " + ChatMessageModel.FLD_DIALOGID + " = '" + dialogId + "'" + " ORDER BY " + ChatMessageModel.FLD_MESSAGE_SENT_DATE + orderDescAsc + " LIMIT 1";
                cursor = DatabaseMgr.sqLiteDb.rawQuery(query, null);
                cursor.moveToFirst();
                if (cursor.getCount() > 0) {
                    messageDateSendInMill = cursor.getLong(cursor.getColumnIndex(ChatMessageModel.FLD_MESSAGE_SENT_DATE));
                    Log.d("tag", "Date sent value is  : " + messageDateSendInMill);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null) {
                    cursor.close();
                    cursor = null;
                }
            }
        }
        return messageDateSendInMill;
    }


    public static int updateUnreadCountToFriendsTable(String dialog_id, String lastSeenAt, Long lastSeenAtInTime,boolean increaseNotiCounter) {
        int unreadMessageCount = 0;
        Cursor cursor = null;
        int effectedRows = 0;
        try {
            String query = "Select * from " + TableType.FriendsTable.getTableName() + " where " + ChatFriendsModel.FLD_DIALOG_ID + " = '" + dialog_id + "'";
            cursor = dbMgr.sqLiteDb.rawQuery(query, null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                unreadMessageCount = cursor.getInt(cursor.getColumnIndex(ChatFriendsModel.FLD_UNREAD_MSG_COUNT));
                Long lastSentDateTime = cursor.getLong(cursor.getColumnIndex(ChatFriendsModel.FLD_LAST_MESSAGE_DATE_SENT_TIME));
                ContentValues contentValues = new ContentValues();
                if(increaseNotiCounter) {
                    unreadMessageCount += 1;
                    contentValues.put(ChatFriendsModel.FLD_UNREAD_MSG_COUNT, unreadMessageCount);
                }
                contentValues.put(ChatFriendsModel.FLD_LAST_MESSAGE_DATE_SENT, lastSeenAt);
                if (lastSeenAtInTime > lastSentDateTime)
                    contentValues.put(ChatFriendsModel.FLD_LAST_MESSAGE_DATE_SENT_TIME, lastSeenAtInTime);
                effectedRows = DatabaseMgr.getInstance().updateRow(TableType.FriendsTable.getTableName(), contentValues, ChatFriendsModel.FLD_DIALOG_ID + "=?", new String[]{"" + dialog_id});
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return effectedRows;
    }


    public static int updateMessageMarkableStatusChatTable(QBChatMessage chatMessage) {
        int unreadMessageCount = 0;
        Cursor cursor = null;
        int effectedRows = 0;
        try {
//            String query="Select * from " + TableType.ChatMessageTable.getTableName()+" where "+ChatMessageModel.FLD_MARKABLE+" = '"+chatMessage.getId()+"'";
//            cursor = dbMgr.sqLiteDb.rawQuery(query, null);
//            cursor.moveToFirst();
//            if (cursor.getCount() > 0) {

            ContentValues contentValues = new ContentValues();
            contentValues.put(ChatMessageModel.FLD_MARKABLE, "" + 0);

            effectedRows = DatabaseMgr.getInstance().updateRow(TableType.ChatMessageTable.getTableName(), contentValues, ChatMessageModel.FLD_MESSAGE_ID + "=?", new String[]{"" + chatMessage.getId()});
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return effectedRows;
    }


    public static int updateUnreadCountToFriendsTable(long quickblox_id, int unreadMessageCount) {
        Cursor cursor = null;
        int effectedRows = 0;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ChatFriendsModel.FLD_UNREAD_MSG_COUNT, unreadMessageCount);
            effectedRows = DatabaseMgr.getInstance().updateRow(TableType.FriendsTable.getTableName(), contentValues, ChatFriendsModel.FLD_QUICKBLOX_ID + "=?", new String[]{"" + quickblox_id});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return effectedRows;
    }
    public static int updateUnreadCountToFriendsTableGroup(String dialog_id, int unreadMessageCount) {
        Cursor cursor = null;
        int effectedRows = 0;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ChatFriendsModel.FLD_UNREAD_MSG_COUNT, unreadMessageCount);
            effectedRows = DatabaseMgr.getInstance().updateRow(TableType.FriendsTable.getTableName(), contentValues, ChatFriendsModel.FLD_DIALOG_ID + "=?", new String[]{"" + dialog_id});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return effectedRows;
    }



    public static int updateUnreadCountToFriendsTableUsingQuickBID(String dialog_id, int unreadMessageCount) {
        Cursor cursor = null;
        int effectedRows = 0;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ChatFriendsModel.FLD_UNREAD_MSG_COUNT, unreadMessageCount);
            effectedRows = DatabaseMgr.getInstance().updateRow(TableType.FriendsTable.getTableName(), contentValues, ChatFriendsModel.FLD_DIALOG_ID + "=?", new String[]{"" + dialog_id});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return effectedRows;
    }


    public static int insertDataToFriendsTable(ChatFriendsModel chatFriendsModel) {
        int insertedRow = 0;

        long lastSentDateTimeTemp = 0;
        if (dbMgr == null)
            init();
        if (dbMgr != null) {
            if (chatFriendsModel != null) {
                String query = "Select * from " + TableType.FriendsTable.getTableName();
                if (chatFriendsModel.dialog_id != null) {
                    query = "Select * from " + TableType.FriendsTable.getTableName() + " Where " + ChatFriendsModel.FLD_DIALOG_ID + " = '" + chatFriendsModel.dialog_id + "'";
                }
                Cursor cursor = dbMgr.sqLiteDb.rawQuery(query, null);
                cursor.moveToFirst();
                if (cursor.getCount() > 0) {
                    lastSentDateTimeTemp = cursor.getLong(cursor.getColumnIndex(ChatFriendsModel.FLD_LAST_MESSAGE_DATE_SENT_TIME));
                }
                try {
                    ContentValues[] contentValues = new ContentValues[1];
                    ContentValues values = new ContentValues();
                    if (chatFriendsModel.last_name != null)
                        values.put(ChatFriendsModel.FLD_LAST_NAME, chatFriendsModel.last_name);
                    if (chatFriendsModel.first_name != null)
                        values.put(ChatFriendsModel.FLD_FIRST_NAME, chatFriendsModel.first_name);
                    if (chatFriendsModel.name != null)
                        values.put(ChatFriendsModel.FLD_NAME, chatFriendsModel.name);
                    if (chatFriendsModel.id != null)
                        values.put(ChatFriendsModel.FLD_ID, chatFriendsModel.id);
                    if (chatFriendsModel.image != null)
                        values.put(ChatFriendsModel.FLD_IMAGE, chatFriendsModel.image);
                    if (chatFriendsModel.quickblox_id != null)
                        values.put(ChatFriendsModel.FLD_QUICKBLOX_ID, chatFriendsModel.quickblox_id);
                    if (chatFriendsModel.quickblox_username != null)
                        values.put(ChatFriendsModel.FLD_QUICKBLOX_USERNAME, chatFriendsModel.quickblox_username);
                    if (chatFriendsModel.unread_msg_count >= 0)
                        values.put(ChatFriendsModel.FLD_UNREAD_MSG_COUNT, chatFriendsModel.unread_msg_count);
                    if (chatFriendsModel.last_message_date_sent == null) {
                        chatFriendsModel.last_message_date_sent = 0L;
                    }
                    if (chatFriendsModel.user_details_ != null)
                        values.put(ChatFriendsModel.FLD_USER_DETAILS, chatFriendsModel.user_details_.toString());
                    if (chatFriendsModel.last_message_date_sent > lastSentDateTimeTemp) {
                        lastSentDateTimeTemp = chatFriendsModel.last_message_date_sent;
                    }

                    values.put(ChatFriendsModel.FLD_LAST_MESSAGE_DATE_SENT, Utils.getDate((lastSentDateTimeTemp * 1000), Constants.COMINGDATEFORMATFROMSERVER));
                    values.put(ChatFriendsModel.FLD_LAST_MESSAGE_DATE_SENT_TIME, lastSentDateTimeTemp);
                    if (chatFriendsModel.request_id >= 0)
                        values.put(ChatFriendsModel.FLD_REQUEST_ID, chatFriendsModel.request_id);

                    if (chatFriendsModel.occupants_ids != null) {
                        JSONArray jsonArray = new JSONArray();
                        for (int i = 0; i < chatFriendsModel.occupants_ids.size(); i++) {
                            jsonArray.put(chatFriendsModel.occupants_ids.get(i));
                        }
                        values.put(ChatFriendsModel.FLD_OCCUPANTS_IDS, jsonArray.toString());
                    }
                    if (chatFriendsModel.name != null)
                        values.put(ChatFriendsModel.FLD_NAME, chatFriendsModel.name);
                    if (chatFriendsModel._id != null)
                        values.put(ChatFriendsModel.FLD__ID, chatFriendsModel._id);
                    if (chatFriendsModel.xmpp_room_jid != null)
                        values.put(ChatFriendsModel.FLD_XMPP_ROOM_JID, chatFriendsModel.xmpp_room_jid);
                    if (chatFriendsModel.cover_image != null)
                        values.put(ChatFriendsModel.FLD_COVER_IMAGE, chatFriendsModel.cover_image);
                    if (chatFriendsModel.dob != null)
                        values.put(ChatFriendsModel.FLD_DOB, chatFriendsModel.dob);
                    if (chatFriendsModel.gender != null)
                        values.put(ChatFriendsModel.FLD_GENDER, chatFriendsModel.gender);
                    if (chatFriendsModel.live_in != null)
                        values.put(ChatFriendsModel.FLD_LIVE_IN, chatFriendsModel.live_in);
                    if (chatFriendsModel.dialog_id != null)
                        values.put(ChatFriendsModel.FLD_DIALOG_ID, chatFriendsModel.dialog_id);
                    if (chatFriendsModel.modified != null)
                        values.put(ChatFriendsModel.FLD_MODIFIED, chatFriendsModel.modified);
                    if (chatFriendsModel.is_logged != null)
                        values.put(ChatFriendsModel.FLD_IS_LOGGED, chatFriendsModel.is_logged);
                    contentValues[0] = values;
                    insertedRow = DatabaseMgr.insertRows(Constants.TABLE_FRIENDS, contentValues);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return insertedRow;
    }

    public static ArrayList<Object> getFriendsData(String query, Context context) {
        ArrayList<ChatFriendsModel> chatFriendList = new ArrayList<ChatFriendsModel>();
        ArrayList<Object> objectList = new ArrayList<Object>();
        HashMap<String, ChatFriendsModel> friendsModelHashMap = new HashMap<>();
        Cursor cursor = null;
        try {
            if (query == null)
                query = "Select * from " + TableType.FriendsTable.getTableName() + " ORDER BY " + ChatFriendsModel.FLD_LAST_MESSAGE_DATE_SENT_TIME + " DESC";
            cursor = dbMgr.sqLiteDb.rawQuery(query, null);
            cursor.moveToFirst();
            int count = cursor.getCount();
            if (count > 0) {
                do {
                    ChatFriendsModel chatFriendsModel = new ChatFriendsModel();
                    chatFriendsModel.last_name = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_LAST_NAME));
                    chatFriendsModel.first_name = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_FIRST_NAME));
                    chatFriendsModel.name = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_NAME));
                    chatFriendsModel.id = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_ID));
                    chatFriendsModel.image = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_IMAGE));
                    chatFriendsModel.quickblox_id = cursor.getInt(cursor.getColumnIndex(ChatFriendsModel.FLD_QUICKBLOX_ID));
                    chatFriendsModel.quickblox_username = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_QUICKBLOX_USERNAME));
                    chatFriendsModel.unread_msg_count = cursor.getInt(cursor.getColumnIndex(ChatFriendsModel.FLD_UNREAD_MSG_COUNT));
                    chatFriendsModel.last_message_date_sent_date = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_LAST_MESSAGE_DATE_SENT));
//                    chatFriendsModel.last_message_date_sent_date =cursor.getString(cursor.getColumnIndex(chatFriendsModel.FLD_LAST_MESSAGE_DATE_SENT));
//                  if(lastSeenDateTime!=null)
//                    chatFriendsModel.last_message_date_sent_date = new Date(lastSeenDateTime);
                    chatFriendsModel.request_id = cursor.getInt(cursor.getColumnIndex(ChatFriendsModel.FLD_REQUEST_ID));
                    chatFriendsModel.dialog_id = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_DIALOG_ID));
                    String occupants_ = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_OCCUPANTS_IDS));
                    if (occupants_ != null && !occupants_.isEmpty()) {
                        JSONArray jsonArray = new JSONArray(occupants_);
                        chatFriendsModel.occupants_ids = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            int value = (Integer) jsonArray.get(i);
                            chatFriendsModel.occupants_ids.add(value);
                        }
                    }


                    String userDetails_ = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_USER_DETAILS));
                    if (occupants_ != null && !occupants_.isEmpty()) {
                        JSONArray jsonArray = new JSONArray(userDetails_);
                        chatFriendsModel.user_details = new ArrayList<>();
                        chatFriendsModel.userDetailsHashMap = new HashMap<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            UserDetails userDetailsVa = Utils.getGsonBuilder(Constants.COMINGDATEFORMATFROMSERVER).fromJson(jsonObject.toString(), UserDetails.class);
                            chatFriendsModel.user_details.add(userDetailsVa);
                            chatFriendsModel.userDetailsHashMap.put(userDetailsVa.quickblox_id, userDetailsVa);
                        }
                    }
                    chatFriendsModel._id = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD__ID));
                    chatFriendsModel.last_message = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_LAST_MESSAGE));
                    chatFriendsModel.name = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_NAME));
                    chatFriendsModel.xmpp_room_jid = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_XMPP_ROOM_JID));
                    chatFriendsModel.cover_image = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_COVER_IMAGE));
                    chatFriendsModel.live_in = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_LIVE_IN));
                    chatFriendsModel.dob = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_DOB));
                    chatFriendsModel.gender = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_GENDER));
                    chatFriendsModel.is_logged = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_IS_LOGGED));
                    friendsModelHashMap.put(chatFriendsModel.dialog_id, chatFriendsModel);
                    chatFriendList.add(chatFriendsModel);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        objectList.add(chatFriendList);
        objectList.add(friendsModelHashMap);
        return objectList;
    }

    public static String getModifiedDateFromChatFriends(String query,ActLandingScreen actLandingScreen, String orderDescAsc) {
        String modifiedDate = null;
        Cursor cursor = null;
        try {
            if(query==null)
             query = "Select * from " + TableType.FriendsTable.getTableName() + " ORDER BY datetime(" + ChatFriendsModel.FLD_MODIFIED + ")" + orderDescAsc + " LIMIT 1";
            cursor = dbMgr.sqLiteDb.rawQuery(query, null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                modifiedDate = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_MODIFIED));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return modifiedDate;
    }

    public static int insertDataToMessageTable(QBChatMessage chatMessage) {
        int insertedRow = 0;
        if (chatMessage != null)
            try {
                if (chatMessage.getSenderId().equals(CustomSharedPref.getQuickbloxId())) {
                    chatMessage.setMarkable(false);
                }
                ContentValues[] contentValues = new ContentValues[1];
                ContentValues values = new ContentValues();
                String messageId = chatMessage.getId();
                values.put(ChatMessageModel.FLD_MESSAGE_ID, messageId);
                values.put(ChatMessageModel.FLD_SENDER_ID, chatMessage.getSenderId());
                values.put(ChatMessageModel.FLD_RECIEVER_ID, chatMessage.getRecipientId());
                if (chatMessage.getAttachments() != null) {
                    ArrayList<QBAttachment> qbAttachments = new ArrayList<QBAttachment>(chatMessage.getAttachments());
                    if (qbAttachments != null && qbAttachments.size() > 0) {
                        QBAttachment qbAttachment = qbAttachments.get(0);
                        if (qbAttachment.getUrl() != null && !qbAttachment.getUrl().isEmpty()) {
                            values.put(ChatMessageModel.FLD_QB_ATTACHMENT_URL, qbAttachment.getUrl());
                            values.put(ChatMessageModel.FLD_QB_ATTACHMENT_TYPE, qbAttachment.getType());
                            Map<String, String> propertiesMap = chatMessage.getProperties();
                            if (propertiesMap.containsKey(Constants.TOTAL_AUDIO_LENGTH)) {
                                String totalTimeDuration = "00:00";
                                totalTimeDuration = propertiesMap.get(Constants.TOTAL_AUDIO_LENGTH);
                                values.put(ChatMessageModel.FLD_TOTAL_AUDIO_LENGTH, totalTimeDuration);
                            }
                        }
                    }
                }
                values.put(ChatMessageModel.FLD_BODY, chatMessage.getBody());
                values.put(ChatMessageModel.FLD_DIALOGID, chatMessage.getDialogId());

                int isMarkableValue = 0;
                isMarkableValue = 1;
                if (chatMessage.isMarkable())
                    values.put(ChatMessageModel.FLD_MARKABLE, isMarkableValue);
                long sentDate = chatMessage.getDateSent() * 1000;
                values.put(ChatMessageModel.FLD_MESSAGE_SENT_DATE, "" + sentDate);
                contentValues[0] = values;
                insertedRow = DatabaseMgr.insertRows(Constants.TABLE_CHAT_MESSAGE, contentValues);
            } catch (Exception e) {
                e.printStackTrace();
            }
        return insertedRow;
    }

    public static ArrayList<QBChatMessage> getMessagesList(Context ctx, String dialogIdValue) {
        ArrayList<QBChatMessage> chatMessages = new ArrayList<QBChatMessage>();
        HashMap<String, String> chatMessageDate = new HashMap<>();
        Cursor cursor = null;
        try {
            String query = "Select * from " + TableType.ChatMessageTable.getTableName() + " where " + ChatMessageModel.FLD_DIALOGID + " = '" + dialogIdValue + "'" + " ORDER BY " + ChatMessageModel.FLD_MESSAGE_SENT_DATE + " ASC ";
            cursor = dbMgr.sqLiteDb.rawQuery(query, null);
            cursor.moveToFirst();
            int chatCount = cursor.getCount();
            if (chatCount > 0) {
                do {
                    QBChatMessage chatMessage = new QBChatMessage();
                    int senderId = cursor.getInt(cursor.getColumnIndex(ChatMessageModel.FLD_SENDER_ID));
                    int reciever_id = cursor.getInt(cursor.getColumnIndex(ChatMessageModel.FLD_RECIEVER_ID));
                    String qb_attachment_url = cursor.getString(cursor.getColumnIndex(ChatMessageModel.FLD_QB_ATTACHMENT_URL));
                    String qb_attachment_type = cursor.getString(cursor.getColumnIndex(ChatMessageModel.FLD_QB_ATTACHMENT_TYPE));
                    String total_audio_length = cursor.getString(cursor.getColumnIndex(ChatMessageModel.FLD_TOTAL_AUDIO_LENGTH));
                    String body = cursor.getString(cursor.getColumnIndex(ChatMessageModel.FLD_BODY));
                    String dialogId = cursor.getString(cursor.getColumnIndex(ChatMessageModel.FLD_DIALOGID));
                    String message_id = cursor.getString(cursor.getColumnIndex(ChatMessageModel.FLD_MESSAGE_ID));
                    int isMarka = cursor.getInt(cursor.getColumnIndex(ChatMessageModel.FLD_MARKABLE));
                    boolean isMarkable = false;
                    if (isMarka == 1)
                        isMarkable = true;
                    chatMessage.setMarkable(isMarkable);
                    long message_sent_date_long = cursor.getLong(cursor.getColumnIndex(ChatMessageModel.FLD_MESSAGE_SENT_DATE));
                    chatMessage.setSenderId(senderId);
                    chatMessage.setRecipientId(reciever_id);
                    if (qb_attachment_url != null) {
                        ArrayList<QBAttachment> qbAttachments = new ArrayList<QBAttachment>();
                        QBAttachment qbAttachment = new QBAttachment(qb_attachment_type);
                        qbAttachment.setUrl(qb_attachment_url);
                        qbAttachment.setType(qb_attachment_type);
                        qbAttachments.add(qbAttachment);
                        chatMessage.setAttachments(qbAttachments);
                        if (QBAttachment.AUDIO_TYPE.equals(qb_attachment_type))
                            chatMessage.setProperty(ChatMessageModel.FLD_TOTAL_AUDIO_LENGTH, total_audio_length);
                    }

                    Date date = new Date(message_sent_date_long);
                    String messageSentDate = Utils.convertDateToGivenFormat(date, Constants.SHOW_DATE_FORMAT);
                    if (!chatMessageDate.containsKey(messageSentDate)) {
                        QBChatMessage chatMessage1 = new QBChatMessage();
                        String sentDate = messageSentDate;
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat(Constants.SHOW_DATE_FORMAT);
                        String formattedDate = df.format(c.getTime());
                        long timeCurrent = df.parse(formattedDate).getTime();
                        long timeCreated = df.parse(messageSentDate).getTime();
                        if (timeCreated == timeCurrent) {
                            sentDate = ctx.getResources().getString(R.string.Today);
                        }
                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.DATE, -1);
                        String previousDate = df.format(cal.getTime());
                        long previousTime = df.parse(previousDate).getTime();
                        if (previousTime == timeCreated) {
                            sentDate = ctx.getResources().getString(R.string.Yesterday);
                        }
                        chatMessage1.setProperty(ChatMessageModel.FLD_CHAT_DATE, sentDate);
                        chatMessage1.setDateSent(message_sent_date_long);
                        chatMessages.add(chatMessage1);
                        chatMessageDate.put(messageSentDate, messageSentDate);
                    }
                    chatMessage.setBody(body);
                    chatMessage.setDialogId(dialogId);
                    chatMessage.setId(message_id);
                    chatMessage.setDateSent(message_sent_date_long);
                    chatMessages.add(chatMessage);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }

        return chatMessages;
    }

    public static void clearUserData() {
        try {
            String tableName = TableType.ChatMessageTable.getTableName();
            clearDbTable(tableName);
            tableName = TableType.CallLogTable.getTableName();
            clearDbTable(tableName);
            tableName = TableType.FriendsTable.getTableName();
            clearDbTable(tableName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/




    /*public static int updateFriendOnlineOffLineStatus(JSONArray friendArray) {
        int effectedRows = 0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatFriendsModel.FLD_IS_LOGGED, Constants.IS_LOGGED_N);
        try {
            effectedRows = DatabaseMgr.getInstance().updateRow(TableType.FriendsTable.getTableName(), contentValues, null, null);
            if (friendArray != null && friendArray.length() > 0) {
                for (int i = 0; i < friendArray.length(); i++) {
                    try {
                        JSONObject jsonObject = friendArray.getJSONObject(i);
                        ContentValues contentValues1 = new ContentValues();
                        String quickblox_id = jsonObject.getString(ChatFriendsModel.FLD_QUICKBLOX_ID);
                        contentValues1.put(ChatFriendsModel.FLD_IS_LOGGED, Constants.IS_LOGGED_Y);
                        effectedRows = DatabaseMgr.getInstance().updateRow(TableType.FriendsTable.getTableName(), contentValues1, ChatFriendsModel.FLD_QUICKBLOX_ID + " =? ", new String[]{quickblox_id});
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return effectedRows;
    }



    public static int updateGroupUsersDetail(JSONArray users,String dialogId) {
        int effectedRows = 0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatFriendsModel.FLD_USER_DETAILS, users.toString());
        try {
            effectedRows = DatabaseMgr.getInstance().updateRow(TableType.FriendsTable.getTableName(), contentValues, UserDataFields.FLD_DIALOG_ID+"=?", new String[]{dialogId});
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return effectedRows;
    }


    public static int updateFriendOffLineStatus() {
        int effectedRows = 0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatFriendsModel.FLD_IS_LOGGED, Constants.IS_LOGGED_N);
        try {
            effectedRows = DatabaseMgr.getInstance().updateRow(TableType.FriendsTable.getTableName(), contentValues, null, null);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return effectedRows;
    }

    public static int deleteRowFromFriendsTable(String dialog_id) {
        int effectedRows=0;
        try {
            effectedRows = DatabaseMgr.getInstance().deleteRow(TableType.FriendsTable.getTableName(), ChatFriendsModel.FLD_DIALOG_ID + " =? ", new String[]{dialog_id});
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return effectedRows;
    }

    public static ArrayList<ChatFriendsModel> getAllGroupRoomJid(String query) {

        ArrayList<ChatFriendsModel> chatRoomIdList = new ArrayList<ChatFriendsModel>();
        Cursor cursor = null;
        try {
              cursor = dbMgr.sqLiteDb.rawQuery(query, null);
            cursor.moveToFirst();
            int count = cursor.getCount();
            if (count > 0) {
                do {
                    ChatFriendsModel chatFriendsModel = new ChatFriendsModel();
                    chatFriendsModel.xmpp_room_jid = cursor.getString(cursor.getColumnIndex(ChatFriendsModel.FLD_XMPP_ROOM_JID));
                    chatRoomIdList.add(chatFriendsModel);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return chatRoomIdList;
    }*/
}
