package com.vervelogic.chefdiner.appdatabase;

/**
 * Created by ubuntu on 13/12/16.
 */
public class DatabaseFields {


    /*********************All Table Fields*************/

    public static String TABLE_ALERTMESSAGES="AlertMessages";
    public static String TABLE_DEVICE_TOKEN="deviceToken";
    public static String FLD_ID_DATABASE="Id";


    /****************** Alert Data Fields***********/
    public static String FLD_ALERT_ID="AlertId";
    public static String FLD_USERNO="UserNo";
    public static String FLD_USERNAME="UserName";
    public static String FLD_USERIMAGE="UserImage";
    public static String FLD_USERCURRENTLOCATION="UserCurrentLocation";
    public static String FLD_USERHOMEADDRESS="UserHomeAddress";
    public static String FLD_ALERTTYPE="alertType";
    public static String FLD_ALERTMESSAGE="alertMessage";
    public static String FLD_ALERTSENDINGTIME="alertSendingTime";



    /****************** Contact Token Data Fields***********/
    public static String FLD_MOBILE_NO="mobileNo";
    public static String FLD_DEVICE_TOKEN="deviceToken";
    public static String FLD_PROFILE_PIC="profilePic";
    public static String FLD_DEVICE_TYPE="deviceType";
    public static  String FLD_EMAIL = "email";
    public static  String FLD_USER_ID = "userId";

    
}
