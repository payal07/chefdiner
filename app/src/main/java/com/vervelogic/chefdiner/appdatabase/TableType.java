package com.vervelogic.chefdiner.appdatabase;


public enum TableType {
    AlertTable(1, DatabaseFields.TABLE_ALERTMESSAGES),
    DeviceTokenTable(2, DatabaseFields.TABLE_DEVICE_TOKEN);
    private int index;
    private String tableName;

    TableType(int index, String tableName) {
        this.index = index;
        this.tableName = tableName;
    }

    public int getIndex() {
        return index;
    }

    public String getTableName() {
        return tableName;
    }
}