package com.vervelogic.chefdiner.appdatabase;

import android.database.sqlite.SQLiteDatabase;

import com.vervelogic.chefdiner.db.DBHelper;
import com.vervelogic.chefdiner.extra.Constants;


/**

 */
public class DBHelperApp implements DBHelper {
    @Override
    public int getDBVersion() {
        return DatabaseVersoning.DATABASEVERSION_1;
    }

    @Override
    public String getDBName() {
        return Constants.DB_NAME;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createAlertMessageTable(db);
        createDeviceTokenTable(db);
       // createCallLogTable(db);
      //  createFriendsTable(db);
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       /* if (newVersion == DatabaseVersoning.DATABASEVERSION_1) {
            dropTable(db, TableType.ChatMessageTable.getTableName());
            dropTable(db, TableType.CallLogTable.getTableName());
            dropTable(db, TableType.FriendsTable.getTableName());
            createCallLogTable(db);
            createFriendsTable(db);
            createCMessageTable(db);
        }*/
//        else if(newVersion==DatabaseVersoning.DATABASEVERSION_29_Nov_16_4)
//{
//
//}

    }


    private void createDeviceTokenTable(SQLiteDatabase db) {
        try {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + TableType.DeviceTokenTable.getTableName() + "("
                    + DatabaseFields.FLD_ID_DATABASE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + DatabaseFields.FLD_MOBILE_NO + " TEXT,"
                    + DatabaseFields.FLD_DEVICE_TOKEN + " TEXT,"
                    + DatabaseFields.FLD_PROFILE_PIC + " TEXT,"
                    + DatabaseFields.FLD_EMAIL + " TEXT,"
                    + DatabaseFields.FLD_USER_ID + " TEXT NOT NULL UNIQUE ON CONFLICT REPLACE ,"
                    + DatabaseFields.FLD_USERNAME + " TEXT,"
                    + DatabaseFields.FLD_DEVICE_TYPE + " TEXT);");
            db.execSQL("CREATE INDEX device_token_index on " +
                    TableType.DeviceTokenTable.getTableName() + "(" + DatabaseFields.FLD_USER_ID + ");");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void createAlertMessageTable(SQLiteDatabase db) {
        try {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + TableType.AlertTable.getTableName() + "("
                    + DatabaseFields.FLD_ID_DATABASE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + DatabaseFields.FLD_ALERT_ID + " TEXT NOT NULL UNIQUE ON CONFLICT REPLACE ,"
                    + DatabaseFields.FLD_USERNO + " TEXT,"
                    + DatabaseFields.FLD_USERNAME + " TEXT,"
                    + DatabaseFields.FLD_USERIMAGE + " TEXT,"
                    + DatabaseFields.FLD_USERCURRENTLOCATION + " TEXT,"
                    + DatabaseFields.FLD_USERHOMEADDRESS + " TEXT,"
                    + DatabaseFields.FLD_ALERTTYPE + " TEXT,"
                    + DatabaseFields.FLD_ALERTMESSAGE + " TEXT,"
                    + DatabaseFields.FLD_ALERTSENDINGTIME + " LONG);");
            db.execSQL("CREATE INDEX alert_msg_index on " + TableType.AlertTable.getTableName() + "(" + DatabaseFields.FLD_ALERT_ID + ");");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* private void createCallLogTable(SQLiteDatabase db) {
        Log.e("tag", "createCallLogTable function called ");
        try {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + TableType.CallLogTable.getTableName() + "("
                    + UserDataFields.FLD_ID_DATABASE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + CallLogModel.FLD_ID + " INTEGER NOT NULL UNIQUE ON CONFLICT REPLACE ,"
                    + CallLogModel.FLD_DIALOG_ID + " TEXT ,"
                    + CallLogModel.FLD_CALLER_ID + " TEXT ,"
                    + CallLogModel.FLD_RECEVIER_ID + " TEXT ,"
                    + CallLogModel.FLD_DURATION + " TEXT ,"
                    + CallLogModel.FLD_CREATED + " DATETIME ,"
                    + CallLogModel.FLD_CALL_TYPE + " TEXT ,"
                    + CallLogModel.FLD_MODIFIED + " DATETIME);");
            db.execSQL("CREATE INDEX call_history_index on " + TableType.CallLogTable.getTableName() + "(" + CallLogModel.FLD_ID + ");");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createFriendsTable(SQLiteDatabase db) {
        Log.e("tag", "createCallLogTable function called ");
        try {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + TableType.FriendsTable.getTableName() + "("
                    + UserDataFields.FLD_ID_DATABASE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + ChatFriendsModel.FLD_ID + " INTEGER NOT NULL UNIQUE ON CONFLICT REPLACE ,"
                    + ChatFriendsModel.FLD_LAST_NAME + " TEXT ,"
                    + ChatFriendsModel.FLD_FIRST_NAME + " TEXT ,"
                    + ChatFriendsModel.FLD_IMAGE + " TEXT ,"
                    + ChatFriendsModel.FLD_IS_LOGGED + " TEXT ,"
                    + ChatFriendsModel.FLD_QUICKBLOX_ID + " INTEGER ,"
                    + ChatFriendsModel.FLD_QUICKBLOX_USERNAME + " TEXT ,"
                    + ChatFriendsModel.FLD_UNREAD_MSG_COUNT + " INTEGER ,"
                    + ChatFriendsModel.FLD_LAST_MESSAGE_DATE_SENT + " DATETIME ,"
                    + ChatFriendsModel.FLD_LAST_MESSAGE_DATE_SENT_TIME + " LONG ,"
                    + ChatFriendsModel.FLD_MODIFIED + " DATETIME ,"
                    + ChatFriendsModel.FLD_REQUEST_ID + " INTEGER ,"
                    + ChatFriendsModel.FLD_OCCUPANTS_IDS + " TEXT ,"
                    + ChatFriendsModel.FLD_USER_DETAILS + " TEXT ,"
                    + ChatFriendsModel.FLD_NAME + " TEXT ,"
                    + ChatFriendsModel.FLD_LAST_MESSAGE + " TEXT ,"
                    + ChatFriendsModel.FLD__ID + " TEXT ,"
                    + ChatFriendsModel.FLD_DIALOG_ID + " TEXT ,"
                    + ChatFriendsModel.FLD_XMPP_ROOM_JID + " TEXT ,"
                    + ChatFriendsModel.FLD_COVER_IMAGE + " TEXT ,"
                    + ChatFriendsModel.FLD_DOB + " TEXT ,"
                    + ChatFriendsModel.FLD_GENDER + " TEXT ,"
                    + ChatFriendsModel.FLD_LIVE_IN + " TEXT);");
            db.execSQL("CREATE INDEX friends_index on " + TableType.FriendsTable.getTableName() + "(" + ChatFriendsModel.FLD_ID + ");");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void dropTable(SQLiteDatabase db, String tableName) {
        try {
            db.execSQL("DROP TABLE IF EXISTS " + tableName + ";");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
