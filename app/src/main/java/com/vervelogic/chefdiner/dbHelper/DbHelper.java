package com.vervelogic.chefdiner.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.vervelogic.chefdiner.model.UserDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Verve on 16-10-2017.
 */

public class DbHelper extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "ChefDiner";

    /*------------------------------------------Chef Diner table name--------------------------------------------------*/
    private static final String TABLE_USER = "user";
    private static final String TABLE_CHEF = "chef";
    private static final String TABLE_BOOKING = "booking";


    /*---------------------------------------User table column names-------------------------------------------------------*/
    private static final String KEY_ID_USER = "id";
    private static final String KEY_FIRST_NAME_USER = "first_name";
    private static final String KEY_LAST_NAME_USER = "last_name";
    private static final String KEY_EMAIL_USER = "email_id";
    private static final String KEY_CONTACT_USER = "contact_no";
    private static final String KEY_PASSWORD_USER = "password";
    private static final String KEY_PROFILE_USER = "profile_photo";
    private static final String KEY_AUTH_TOKEN_USER = "auth_token";
    private static final String KEY_LANGUAGE_USER = "language";
    private static final String KEY_IS_CHEF_USER = "is_chef";
    private static final String KEY_FB_AUTH_ID_USER = "fb_auth_id";
    private static final String KEY_IS_VERIFY_USER = "is_verify";
    private static final String KEY_IS_USER = "is_user";
    private static final String KEY_VERIFY_CHEF_USER = "is_vreify_chef";
    private static final String KEY_CURRENCY_USER = "currency";
    private static final String KEY_ACCESS_TOKEN_USER = "access_token";
    private static final String KEY_CHEF_NAME_USER = "chef_name";
    private static final String KEY_CHEF_LAST_NAME_USER = "chef_lname";
    private static final String KEY_LANGUAGE_SPEAK_USER = "lang_list";
    private static final String KEY_SERVICE_TYPE_USER = "service_type";
    private static final String KEY_OFTEN_COOK_USER = "often_cook";
    private static final String KEY_CHEF_COUNTRY_USER = "chef_country";
    private static final String KEY_CHEF_CITY_USER = "chef_city";
    private static final String KEY_FROMGUEST_USER = "from_guest";
    private static final String KEY_UPTOGUEST_USER = "upto_guest";
    private static final String KEY_KITCHEN_TITLE_USER = "kit_title";
    private static final String KEY_KITCHEN_DESC_USER = "kit_desc";
    private static final String KEY_CHEF_LAT_USER = "chef_lat";
    private static final String KEY_CHEF_LONG_USER = "chef_long";
    private static final String KEY_LUNCH_USER = "lunch";
    private static final String KEY_BRUNCH_USER = "brunch";
    private static final String KEY_DINNER_USER = "dinner";
    private static final String KEY_FAV_USER = "fav";
    private static final String KEY_CUISINE_USER = "cuisine";
    private static final String KEY_REFERRAL_CODE = "referal_code";


    /*---------------------------------------------Chef table column names----------------------------------------------*/
    private static final String KEY_ID_CHEF = "id";
    private static final String KEY_FIRST_NAME_CHEF = "first_name";
    private static final String KEY_LAST_NAME_CHEF = "last_name";
    private static final String KEY_EMAIL_CHEF = "email_id";
    private static final String KEY_CONTACT_CHEF = "contact_no";
    private static final String KEY_PASSWORD_CHEF = "password";
    private static final String KEY_PROFILE_CHEF = "profile_photo";
    private static final String KEY_AUTH_TOKEN_CHEF = "auth_token";
    private static final String KEY_LANGUAGE_CHEF = "language_known";
    private static final String KEY_LANGUAGE_LIST_CHEF = "language_list";
    private static final String KEY_OFTEN_COOK_CHEF = "often_cook";
    private static final String KEY_COUNTRY_CHEF = "country";
    private static final String KEY_CITY_CHEF = "city";
    private static final String KEY_MIN_GUEST_CHEF = "min_guest";
    private static final String KEY_MAX_GUEST_CHEF = "max_guest";
    private static final String KEY_KITCHEN_TITLE_CHEF = "kitchen_title";
    private static final String KEY_KITCHEN_DESC_CHEF = "kitchen_desc";
    private static final String KEY_SERVICE_LIST_CHEF = "service_list";
    private static final String KEY_IS_CHEF_CHEF = "is_chef";
    private static final String KEY_IS_CHEF_VERIFY_CHEF = "is_verify_chef";
    private static final String KEY_COUNTRY_CODE = "country_code";
    private static final String KEY_LATITUDE = "latitude_chef";
    private static final String KEY_LONGITUDE = "longitude_chef";
    private static final String KEY_LUNCH = "lunch_time";
    private static final String KEY_BRUNCH = "brunch_time";
    private static final String KEY_DINNER = "dinner_time";
    private static final String KEY_CURRENCY_CHEF = "currency_chef";
    private static final String KEY_CUISIN_CHEF = "cuisine";
    private static final String KEY_FAV_CHEF = "fav";
    private static final String KEY_IS_VERIFY_USER_CHEF = "is_verify_user";
    private static final String KEY_IS_USER_CHEF = "is_user";
    private static final String KEY_REFERELCODE_CHEF = "referral_code";
    private static final String KEY_ACCESSTOKEN_CHEF = "access_token";
    private static final String KEY_DISTANCE_CHEF = "distance";
    private static final String KEY_FAVCOUNT_CHEF = "fav_count";
    private static final String KEY_PRICE_CHEF = "price";
    private static final String KEY_ALLIMAGES_CHEF = "all_images";
    private static final String KEY_OK_CHEF = "ok";
    private static final String KEY_RECOMMENDED_CHEF = "recommended";
    private static final String KEY_NOT_RECOMMENDED_CHEF = "not_recommended";
    private static final String KEY_AVG_RATING_CHEF = "avg_rating";
    private static final String KEY_TOTAL_REVIEW_CHEF = "total_review";
    private static final String KEY_MAX_PRICE = "max_price";

    /*------------------------------------BOOKING TABLE DATA------------------------------------------------*/
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_CHEF_ID = "chef_id";
    private static final String KEY_DATE = "booking_date";
    private static final String KEY_TIME = "booking_time";
    private static final String KEY_SEATS = "no_of_seats";
    private static final String KEY_MENU_TITLE = "menu_title";
    private static final String KEY_MENU_ID = "menu_id";
    private static final String KEY_MENU_PRICE = "menu_price";
    private static final String KEY_MENU_CURRENCY = "menu_currrency";
    private static final String KEY_SERVICE_TYPE = "service_type";
    private static final String KEY_FOOD_DELIVERY = "food_delivery";
    private static final String KEY_KITCHEN_TOOL = "kitchen_tool";
    private static final String KEY_F_NAME = "f_name";
    private static final String KEY_L_NAME = "l_name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_CONTACT_NO = "contact_number";
    private static final String KEY_DELIVERY_ADD = "delivery_address";
    private static final String KEY_USER_MSG = "user_msg";
    private static final String KEY_BOOKING_ID = "booking_id";
    private static final String KEY_BOOK_NOW_ID = "booking_now_id";


    /*---------------------------CREATE TABLE USER-------------------------------------------------------*/
    private static final String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_USER + " ( " + KEY_ID_USER + " INTEGER PRIMARY KEY, "
            + KEY_FIRST_NAME_USER + " TEXT, " + KEY_LAST_NAME_USER + " TEXT, " + KEY_EMAIL_USER + " TEXT, "
            + KEY_CONTACT_USER + " TEXT, " + KEY_PASSWORD_USER + " TEXT, " + KEY_AUTH_TOKEN_USER + " TEXT, " + KEY_LANGUAGE_USER + " TEXT, "
            + KEY_IS_CHEF_USER + " TEXT, "
            + KEY_PROFILE_USER + " TEXT, " + KEY_FB_AUTH_ID_USER + " TEXT, " + KEY_IS_VERIFY_USER + " TEXT, "
            + KEY_IS_USER + " TEXT, " + KEY_VERIFY_CHEF_USER + " TEXT, " + KEY_CURRENCY_USER + " TEXT, " + KEY_ACCESS_TOKEN_USER + " TEXT, "
            + KEY_CHEF_NAME_USER + " TEXT, " + KEY_CHEF_LAST_NAME_USER + " TEXT, " + KEY_LANGUAGE_SPEAK_USER + " TEXT, " + KEY_SERVICE_TYPE_USER + " TEXT, "
            + KEY_OFTEN_COOK_USER + " TEXT, " + KEY_CHEF_COUNTRY_USER + " TEXT, " + KEY_CHEF_CITY_USER + " TEXT, " + KEY_FROMGUEST_USER + " TEXT, "
            + KEY_UPTOGUEST_USER + " TEXT, " + KEY_KITCHEN_TITLE_USER + " TEXT," + KEY_KITCHEN_DESC_USER + " TEXT, " + KEY_CHEF_LAT_USER + " TEXT, "
            + KEY_FAV_USER + " TEXT, " + KEY_CUISINE_USER + " TEXT, "
            + KEY_CHEF_LONG_USER + " TEXT, " + KEY_LUNCH_USER + " TEXT," + KEY_BRUNCH_USER + " TEXT, " + KEY_DINNER_USER + " TEXT, "
            + KEY_REFERRAL_CODE + " TEXT " + " );";


    /*--------------------------------CREATE TABLE CHEF-----------------------------------------------------*/
    private static final String CREATE_TABLE_CHEF = "CREATE TABLE " + TABLE_CHEF + " ( " + KEY_ID_CHEF + " INTEGER , "
            + KEY_FIRST_NAME_CHEF + " TEXT, " + KEY_LAST_NAME_CHEF + " TEXT, " + KEY_EMAIL_CHEF + " TEXT, "
            + KEY_CONTACT_CHEF + " TEXT, " + KEY_PASSWORD_CHEF + " TEXT, " + KEY_AUTH_TOKEN_CHEF + " TEXT, "
            + KEY_PROFILE_CHEF + " TEXT, " + KEY_LANGUAGE_CHEF + " TEXT, " + KEY_LANGUAGE_LIST_CHEF + " TEXT, "
            + KEY_OFTEN_COOK_CHEF + " TEXT, " + KEY_COUNTRY_CHEF + " TEXT, " + KEY_CITY_CHEF + " TEXT, "
            + KEY_MIN_GUEST_CHEF + " TEXT, " + KEY_MAX_GUEST_CHEF + " TEXT, " + KEY_SERVICE_LIST_CHEF + " TEXT, "
            + KEY_IS_CHEF_CHEF + " TEXT, " + KEY_IS_CHEF_VERIFY_CHEF + " TEXT, " + KEY_COUNTRY_CODE + " TEXT, "
            + KEY_KITCHEN_TITLE_CHEF + " TEXT, " + KEY_KITCHEN_DESC_CHEF + " TEXT, " + KEY_LATITUDE + " TEXT, " + KEY_LONGITUDE + " TEXT, "
            + KEY_LUNCH + " TEXT, " + KEY_BRUNCH + " TEXT, " + KEY_DINNER + " TEXT, " + KEY_CURRENCY_CHEF + " TEXT, "
            + KEY_CUISIN_CHEF + " TEXT, " + KEY_FAV_CHEF + " TEXT, " + KEY_IS_VERIFY_USER_CHEF + " TEXT, " + KEY_IS_USER_CHEF + " TEXT, "
            + KEY_REFERELCODE_CHEF + " TEXT, " + KEY_ACCESSTOKEN_CHEF + " TEXT, " + KEY_DISTANCE_CHEF + " TEXT, " + KEY_FAVCOUNT_CHEF + " TEXT, "
            + KEY_PRICE_CHEF + " TEXT, " + KEY_ALLIMAGES_CHEF + " TEXT, " + KEY_OK_CHEF + " TEXT, " + KEY_RECOMMENDED_CHEF + " TEXT, "
            + KEY_NOT_RECOMMENDED_CHEF + " TEXT, " + KEY_AVG_RATING_CHEF + " TEXT, " + KEY_TOTAL_REVIEW_CHEF + " TEXT, " + KEY_MAX_PRICE +
            " TEXT "
            + " );";

    /*---------------------------------CREATE TABLE BOOKING--------------------------------------------------*/
    private static final String CREATE_TABLE_BOOKING = "CREATE TABLE " + TABLE_BOOKING + " ( " + KEY_USER_ID + " INTEGER, " + KEY_CHEF_ID + " INTEGER, "
            + KEY_DATE + " TEXT, " + KEY_TIME + " TEXT, " + KEY_SEATS + " TEXT, " + KEY_MENU_TITLE + " TEXT, " + KEY_MENU_ID + " TEXT, " + KEY_MENU_PRICE + " TEXT, "
            + KEY_MENU_CURRENCY + " TEXT, " + KEY_SERVICE_TYPE + " TEXT, " + KEY_FOOD_DELIVERY + " TEXT, " + KEY_KITCHEN_TOOL + " TEXT, " + KEY_F_NAME + " TEXT, "
            + KEY_L_NAME + " TEXT, " + KEY_EMAIL + " TEXT, " + KEY_CONTACT_NO + " TEXT, "
            + KEY_DELIVERY_ADD + " TEXT, " + KEY_USER_MSG + " TEXT, "
            + KEY_BOOKING_ID + " TEXT, " + KEY_BOOK_NOW_ID + " TEXT " + " );";


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_TABLE_CHEF);
        db.execSQL(CREATE_TABLE_BOOKING);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHEF);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKING);
        onCreate(db);
    }

    /*--------------------------------------- ADDING NEW USER REGISTER-----------------------------------------------*/
    public void addUser(UserDetail userDetail) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_USER, userDetail.getUser_id());
        values.put(KEY_FIRST_NAME_USER, userDetail.getFirstName());
        values.put(KEY_LAST_NAME_USER, userDetail.getLastName());
        values.put(KEY_EMAIL_USER, userDetail.getEmailId());
        values.put(KEY_CONTACT_USER, userDetail.getPhoneNumber());
        values.put(KEY_PASSWORD_USER, userDetail.getPassword());
        values.put(KEY_AUTH_TOKEN_USER, userDetail.getAuthToken());
        values.put(KEY_PROFILE_USER, userDetail.getProfilePic());
        values.put(KEY_LANGUAGE_USER, userDetail.getLanguage());
        values.put(KEY_IS_CHEF_USER, userDetail.getIsChef());
        values.put(KEY_FB_AUTH_ID_USER, userDetail.getFbId());
        values.put(KEY_IS_VERIFY_USER, userDetail.getIsVerifyUser());
        values.put(KEY_IS_USER, userDetail.getIsUser());
        values.put(KEY_VERIFY_CHEF_USER, userDetail.getIsChefVerify());
        values.put(KEY_CURRENCY_USER, userDetail.getCurrency());
        values.put(KEY_ACCESS_TOKEN_USER, userDetail.getAccessToken());
        values.put(KEY_CHEF_NAME_USER, userDetail.getChefName());
        values.put(KEY_CHEF_LAST_NAME_USER, userDetail.getChefLastName());
        values.put(KEY_LANGUAGE_SPEAK_USER, userDetail.getLanguageList());
        values.put(KEY_SERVICE_TYPE_USER, userDetail.getServiceType());
        values.put(KEY_OFTEN_COOK_USER, userDetail.getOftenCook());
        values.put(KEY_CHEF_COUNTRY_USER, userDetail.getChefCountry());
        values.put(KEY_CHEF_CITY_USER, userDetail.getChefCity());
        values.put(KEY_FROMGUEST_USER, userDetail.getMin_guest());
        values.put(KEY_UPTOGUEST_USER, userDetail.getMax_guest());
        values.put(KEY_KITCHEN_TITLE_USER, userDetail.getKitchenTitle());
        values.put(KEY_KITCHEN_DESC_USER, userDetail.getKitchenDescription());
        values.put(KEY_CHEF_LAT_USER, userDetail.getCheflat());
        values.put(KEY_CHEF_LONG_USER, userDetail.getChefLong());
        values.put(KEY_LUNCH_USER, userDetail.getLunch());
        values.put(KEY_BRUNCH_USER, userDetail.getBrunch());
        values.put(KEY_DINNER_USER, userDetail.getDinner());
        values.put(KEY_CUISINE_USER, userDetail.getCuisine());
        values.put(KEY_FAV_USER, userDetail.getFav());
        values.put(KEY_REFERRAL_CODE, userDetail.getReferralCode());

        //INSERT DB
        db.insert(TABLE_USER, null, values);
        db.close();
        Log.e("Db", "Inserted");
    }

    /*--------------------------------------------ADDING CHEF DETAILS-----------------------------------------------------------*/
    public void addChefDetails(UserDetail userDetail) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_ID_CHEF, userDetail.getUser_id());
        contentValues.put(KEY_FIRST_NAME_CHEF, userDetail.getFirstName());
        contentValues.put(KEY_LAST_NAME_CHEF, userDetail.getLastName());
        contentValues.put(KEY_EMAIL_CHEF, userDetail.getEmailId());
        contentValues.put(KEY_CONTACT_CHEF, userDetail.getPhoneNumber());
        contentValues.put(KEY_PASSWORD_CHEF, userDetail.getPassword());
        contentValues.put(KEY_AUTH_TOKEN_CHEF, userDetail.getAuthToken());
        contentValues.put(KEY_PROFILE_CHEF, userDetail.getProfilePic());
        contentValues.put(KEY_LANGUAGE_CHEF, userDetail.getLanguage());
        contentValues.put(KEY_LANGUAGE_LIST_CHEF, userDetail.getLanguageList());
        contentValues.put(KEY_OFTEN_COOK_CHEF, userDetail.getOftenCook());
        contentValues.put(KEY_COUNTRY_CHEF, userDetail.getCountry());
        contentValues.put(KEY_CITY_CHEF, userDetail.getCity());
        contentValues.put(KEY_MIN_GUEST_CHEF, userDetail.getMin_guest());
        contentValues.put(KEY_MAX_GUEST_CHEF, userDetail.getMax_guest());
        contentValues.put(KEY_KITCHEN_TITLE_CHEF, userDetail.getKitchenTitle());
        contentValues.put(KEY_KITCHEN_DESC_CHEF, userDetail.getKitchenDescription());
        contentValues.put(KEY_SERVICE_LIST_CHEF, userDetail.getServiceType());
        contentValues.put(KEY_IS_CHEF_CHEF, userDetail.getIsChef());
        contentValues.put(KEY_IS_CHEF_VERIFY_CHEF, userDetail.getIsChefVerify());
        contentValues.put(KEY_COUNTRY_CODE, userDetail.getCountryCode());
        contentValues.put(KEY_LATITUDE, userDetail.getLatitude());
        contentValues.put(KEY_LONGITUDE, userDetail.getLongitude());
        contentValues.put(KEY_LUNCH, userDetail.getLunch());
        contentValues.put(KEY_BRUNCH, userDetail.getBrunch());
        contentValues.put(KEY_DINNER, userDetail.getDinner());
        contentValues.put(KEY_CURRENCY_CHEF, userDetail.getCurrency());
        contentValues.put(KEY_CUISIN_CHEF, userDetail.getCuisine());
        contentValues.put(KEY_FAV_CHEF, userDetail.getFav());
        contentValues.put(KEY_IS_VERIFY_USER_CHEF, userDetail.getIsVerifyUser());
        contentValues.put(KEY_IS_USER_CHEF, userDetail.getIsUser());
        contentValues.put(KEY_REFERELCODE_CHEF, userDetail.getReferralCode());
        contentValues.put(KEY_ACCESSTOKEN_CHEF, userDetail.getAccessToken());
        contentValues.put(KEY_DISTANCE_CHEF, userDetail.getDistance());
        contentValues.put(KEY_FAVCOUNT_CHEF, userDetail.getFavCount());
        contentValues.put(KEY_PRICE_CHEF, userDetail.getPrice());
        contentValues.put(KEY_ALLIMAGES_CHEF, userDetail.getAllImages());
        contentValues.put(KEY_OK_CHEF, userDetail.getOk());
        contentValues.put(KEY_RECOMMENDED_CHEF, userDetail.getRecommended());
        contentValues.put(KEY_NOT_RECOMMENDED_CHEF, userDetail.getNotRecommended());
        contentValues.put(KEY_TOTAL_REVIEW_CHEF, userDetail.getTotalReview());
        contentValues.put(KEY_AVG_RATING_CHEF, userDetail.getAvgRating());
        contentValues.put(KEY_MAX_PRICE, userDetail.getMaxPrice());


        //INSERT DB
        db.insert(TABLE_CHEF, null, contentValues);
        db.close();
        Log.e("Db", "Updated chef");

    }

    /*------------------------------------------ADDING BOOKING DETAILS----------------------------------------------*/
    public void addBookingDetails(UserDetail userDetail) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userDetail.getUser_id());
        values.put(KEY_CHEF_ID, userDetail.getChef_id());
        values.put(KEY_DATE, userDetail.getDate());
        values.put(KEY_TIME, userDetail.getTime());
        values.put(KEY_SEATS, userDetail.getNoOfSeats());
        values.put(KEY_MENU_TITLE, userDetail.getMenuTitle());
        values.put(KEY_MENU_ID, userDetail.getMenuId());
        values.put(KEY_MENU_CURRENCY, userDetail.getCurrency());
        values.put(KEY_MENU_PRICE, userDetail.getMenuPrice());
        values.put(KEY_SERVICE_TYPE, userDetail.getServiceType());
        values.put(KEY_FOOD_DELIVERY, userDetail.getFoodDelivery());
        values.put(KEY_KITCHEN_TOOL, userDetail.getKitchenTool());
        values.put(KEY_F_NAME, userDetail.getFirstName());
        values.put(KEY_L_NAME, userDetail.getLastName());
        values.put(KEY_EMAIL, userDetail.getEmailId());
        values.put(KEY_CONTACT_NO, userDetail.getPhoneNumber());
        values.put(KEY_DELIVERY_ADD, userDetail.getDeliveryAddress());
        values.put(KEY_USER_MSG, userDetail.getUserMsg());
        values.put(KEY_BOOKING_ID, userDetail.getBookingId());
        values.put(KEY_BOOK_NOW_ID, userDetail.getBooNowId());

        //INSERT DB
        db.insert(TABLE_BOOKING, null, values);
        db.close();
        Log.e("Db", "Inserted");
    }

    /*---------------------------------------------------------FETCHING USER DETAILS-------------------------------------------------*/
    public UserDetail getUser(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_USER, new String[]{
                        KEY_FIRST_NAME_USER, KEY_LAST_NAME_USER, KEY_EMAIL_USER, KEY_PASSWORD_USER, KEY_CONTACT_USER,
                        KEY_AUTH_TOKEN_USER, KEY_LANGUAGE_USER, KEY_IS_CHEF_USER}, KEY_ID_USER + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        UserDetail contact = new UserDetail((cursor.getString(1)),
                cursor.getString(2), cursor.getString(7), cursor.getString(4), cursor.getString(5));
        Log.e("Db", "Got user");
        // return contact
        return contact;
    }


    /*--------------------------------------FETCHING CHEF DETAILS---------------------------------------------------------*/
    public Cursor getChefDetails(int id) {

        Log.e("Id", "" + id);
        SQLiteDatabase db = this.getReadableDatabase();
       /* db.rawQuery("SELECT * FROM"+TABLE_CHEF+"WHERE"+KEY_ID_CHEF+"=?"+id, new String[]{String.valueOf(id)});*/
        Cursor cursor = db.query(TABLE_CHEF, new String[]{
                        KEY_ID_CHEF, KEY_FIRST_NAME_CHEF, KEY_LAST_NAME_CHEF, KEY_EMAIL_CHEF, KEY_CONTACT_CHEF, KEY_PASSWORD_CHEF,
                        KEY_AUTH_TOKEN_CHEF, KEY_PROFILE_CHEF, KEY_LANGUAGE_CHEF, KEY_LANGUAGE_LIST_CHEF, KEY_OFTEN_COOK_CHEF,
                        KEY_COUNTRY_CHEF, KEY_CITY_CHEF, KEY_MIN_GUEST_CHEF, KEY_MAX_GUEST_CHEF, KEY_SERVICE_LIST_CHEF,
                        KEY_IS_CHEF_CHEF, KEY_IS_CHEF_VERIFY_CHEF, KEY_COUNTRY_CODE, KEY_KITCHEN_TITLE_CHEF, KEY_KITCHEN_DESC_CHEF,
                        KEY_LATITUDE, KEY_LONGITUDE, KEY_LUNCH, KEY_BRUNCH, KEY_DINNER, KEY_CURRENCY_CHEF, KEY_CUISIN_CHEF,
                        KEY_FAV_CHEF, KEY_IS_VERIFY_USER_CHEF, KEY_IS_USER_CHEF, KEY_REFERELCODE_CHEF, KEY_ACCESSTOKEN_CHEF, KEY_DISTANCE_CHEF,
                        KEY_FAVCOUNT_CHEF, KEY_PRICE_CHEF, KEY_ALLIMAGES_CHEF, KEY_OK_CHEF, KEY_RECOMMENDED_CHEF, KEY_NOT_RECOMMENDED_CHEF,
                        KEY_AVG_RATING_CHEF, KEY_TOTAL_REVIEW_CHEF, KEY_MAX_PRICE},
                KEY_ID_CHEF + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            Log.e("Id", "" + cursor);
            UserDetail contact = new UserDetail();
            contact.setUser_id((cursor.getString(0)));
            contact.setFirstName(cursor.getString(1));
            contact.setLastName(cursor.getString(2));
            contact.setEmailId(cursor.getString(3));
            contact.setPhoneNumber(cursor.getString(4));
            contact.setPassword(cursor.getString(5));
            contact.setAuthToken(cursor.getString(6));
            contact.setProfilePic(cursor.getString(7));
            contact.setLanguage(cursor.getString(8));
            contact.setLanguageList(cursor.getString(9));
            contact.setOftenCook(cursor.getString(10));
            contact.setCountry(cursor.getString(11));
            contact.setCity(cursor.getString(12));
            contact.setMin_guest(cursor.getString(13));
            contact.setMax_guest(cursor.getString(14));
            contact.setServiceType(cursor.getString(15));
            contact.setIsChef(cursor.getString(16));
            contact.setIsChefVerify(cursor.getString(17));
            contact.setCountryCode(cursor.getString(18));
            contact.setKitchenTitle(cursor.getString(19));
            contact.setKitchenDescription(cursor.getString(20));
            contact.setLatitude(cursor.getString(21));
            contact.setLongitude(cursor.getString(22));
            contact.setLunch(cursor.getString(23));
            contact.setBrunch(cursor.getString(24));
            contact.setDinner(cursor.getString(25));
            contact.setCurrency(cursor.getString(26));
            contact.setCuisine(cursor.getString(27));
            contact.setFav(cursor.getString(28));
            contact.setIsVerifyUser(cursor.getString(29));
            contact.setIsUser(cursor.getString(30));
            contact.setReferralCode(cursor.getString(31));
            contact.setAccessToken(cursor.getString(32));
            contact.setDistance(cursor.getString(33));
            contact.setFavCount(cursor.getString(34));
            contact.setPrice(cursor.getString(35));
            contact.setAllImages(cursor.getString(36));
            contact.setOk(cursor.getString(37));
            contact.setRecommended(cursor.getString(38));
            contact.setNotRecommended(cursor.getString(39));
            contact.setAvgRating(cursor.getString(40));
            contact.setTotalReview(cursor.getString(41));
            contact.setMaxPrice(cursor.getString(42));

        }


        // return contact
        Log.e("Db", "Got chef");
        return cursor;
    }

    /*------------------------------------FETCHING BOOKING DETAILS-------------------------------------------------------*/
    public UserDetail getBookingDetails(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_BOOKING, new String[]{
                        KEY_USER_ID, KEY_DATE, KEY_TIME, KEY_SEATS, KEY_MENU_TITLE,
                        KEY_MENU_ID, KEY_MENU_PRICE, KEY_MENU_CURRENCY, KEY_SERVICE_TYPE,
                        KEY_FOOD_DELIVERY, KEY_KITCHEN_TOOL, KEY_F_NAME, KEY_L_NAME, KEY_EMAIL, KEY_CONTACT_NO,
                        KEY_DELIVERY_ADD, KEY_USER_MSG, KEY_BOOKING_ID, KEY_BOOK_NOW_ID}, KEY_CHEF_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        UserDetail contact = new UserDetail((cursor.getString(0)),
                cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5),
                cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11),
                cursor.getString(12), cursor.getString(13), cursor.getString(14), cursor.getString(15), cursor.getString(16), cursor.getString(17),
                cursor.getString(18), cursor.getString(19));
        Log.e("Db", "Got user");
        // return contact
        return contact;
    }


    /*-------------------------------------GET ALL DETAILS USER------------------------------------------------------------------*/
    public List<UserDetail> getAllBookingDetails() {
        List<UserDetail> contactList = new ArrayList<UserDetail>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_BOOKING;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UserDetail contact = new UserDetail();
                contact.setUser_id((cursor.getString(0)));
                contact.setChef_id(cursor.getString(1));
                contact.setDate(cursor.getString(2));
                contact.setTime(cursor.getString(3));
                contact.setNoOfSeats(cursor.getString(4));
                contact.setMenuTitle(cursor.getString(5));
                contact.setMenuId(cursor.getString(6));
                contact.setMenuPrice(cursor.getString(7));
                contact.setCurrency(cursor.getString(8));
                contact.setServiceType(cursor.getString(9));
                contact.setFoodDelivery(cursor.getString(10));
                contact.setKitchenTool(cursor.getString(11));
                contact.setFirstName(cursor.getString(12));
                contact.setLastName(cursor.getString(13));
                contact.setEmailId(cursor.getString(14));
                contact.setPhoneNumber(cursor.getString(15));
                contact.setDeliveryAddress(cursor.getString(16));
                contact.setUserMsg(cursor.getString(17));
                contact.setBookingId(cursor.getString(18));
                contact.setBooNowId(cursor.getString(19));

                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }
        Log.e("Db", "Got All");
        // return contact list
        return contactList;
    }


    /*----------------------------------------------GET ALL DETAILS CHEF-------------------------------------------------------------*/
    public List<UserDetail> getAllChef() {
        List<UserDetail> contactList = new ArrayList<UserDetail>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CHEF;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UserDetail contact = new UserDetail();
                contact.setUser_id((cursor.getString(0)));
                contact.setFirstName(cursor.getString(1));
                contact.setLastName(cursor.getString(2));
                contact.setEmailId(cursor.getString(3));
                contact.setPhoneNumber(cursor.getString(4));
                contact.setPassword(cursor.getString(5));
                contact.setAuthToken(cursor.getString(6));
                contact.setProfilePic(cursor.getString(7));
                contact.setLanguage(cursor.getString(8));
                contact.setLanguageList(cursor.getString(9));
                contact.setOftenCook(cursor.getString(10));
                contact.setCountry(cursor.getString(11));
                contact.setCity(cursor.getString(12));
                contact.setMin_guest(cursor.getString(13));
                contact.setMax_guest(cursor.getString(14));
                contact.setServiceType(cursor.getString(15));
                contact.setIsChef(cursor.getString(16));
                contact.setIsChefVerify(cursor.getString(17));
                contact.setCountryCode(cursor.getString(18));
                contact.setKitchenTitle(cursor.getString(19));
                contact.setKitchenDescription(cursor.getString(20));
                contact.setLatitude(cursor.getString(21));
                contact.setLongitude(cursor.getString(22));
                contact.setLunch(cursor.getString(23));
                contact.setBrunch(cursor.getString(24));
                contact.setDinner(cursor.getString(25));
                contact.setCurrency(cursor.getString(26));
                contact.setCuisine(cursor.getString(27));
                contact.setFav(cursor.getString(28));
                contact.setIsVerifyUser(cursor.getString(29));
                contact.setIsUser(cursor.getString(30));
                contact.setReferralCode(cursor.getString(31));
                contact.setAccessToken(cursor.getString(32));
                contact.setDistance(cursor.getString(33));
                contact.setFavCount(cursor.getString(34));
                contact.setPrice(cursor.getString(35));
                contact.setAllImages(cursor.getString(36));
                contact.setOk(cursor.getString(37));
                contact.setRecommended(cursor.getString(38));
                contact.setNotRecommended(cursor.getString(39));
                contact.setAvgRating(cursor.getString(40));
                contact.setTotalReview(cursor.getString(41));
                contact.setMaxPrice(cursor.getString(42));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        Log.e("Db", "Got All");
        /* return contact list */
        return contactList;
    }

    /*-----------------------------------GET ALL BOOKING DETAILS---------------------------------------------------------*/
    public List<UserDetail> getAllDetails() {
        List<UserDetail> contactList = new ArrayList<UserDetail>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UserDetail contact = new UserDetail();
                contact.setUser_id((cursor.getString(0)));
                contact.setFirstName(cursor.getString(1));
                contact.setLastName(cursor.getString(2));
                contact.setEmailId(cursor.getString(3));
                contact.setPhoneNumber(cursor.getString(4));
                contact.setAuthToken(cursor.getString(6));
                contact.setLanguage(cursor.getString(7));
                contact.setIsChef(cursor.getString(8));
                contact.setProfilePic(cursor.getString(9));
                contact.setFbId(cursor.getString(10));
                contact.setIsVerifyUser(cursor.getString(11));
                contact.setIsUser(cursor.getString(12));
                contact.setIsChefVerify(cursor.getString(13));
                contact.setCurrency(cursor.getString(14));
                contact.setAccessToken(cursor.getString(15));
                contact.setChefName(cursor.getString(16));
                contact.setChefLastName(cursor.getString(17));
                contact.setLanguageList(cursor.getString(18));
                contact.setServiceType(cursor.getString(19));
                contact.setOftenCook(cursor.getString(20));
                contact.setChefCountry(cursor.getString(21));
                contact.setChefCity(cursor.getString(22));
                contact.setMin_guest(cursor.getString(23));
                contact.setMax_guest(cursor.getString(24));
                contact.setKitchenTitle(cursor.getString(25));
                contact.setKitchenDescription(cursor.getString(26));
                contact.setCheflat(cursor.getString(27));
                contact.setFav(cursor.getString(28));
                contact.setCuisine(cursor.getString(29));
                contact.setLunch(cursor.getString(31));
                contact.setBrunch(cursor.getString(32));
                contact.setDinner(cursor.getString(33));
                contact.setChefLong(cursor.getString(30));
                contact.setReferralCode(cursor.getString(34));

                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }
        Log.e("Db", "Got All");
        // return contact list
        return contactList;
    }


    /*--------------------------------------------GETTING USER COUNT--------------------------------------------------*/
    public int getUserCount() {
        String countQuery = "SELECT  * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


    /*------------------------------------------GETTING USER COUNT----------------------------------------------------*/
    public int getChefCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CHEF;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    /*------------------------------------------GETTING Booking COUNT----------------------------------------------------*/
    public int getBookingCount() {
        String countQuery = "SELECT  * FROM " + TABLE_BOOKING;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


    /*-------------------------------------------UPDATE USER------------------------------------------------------------*/
    public int updateUser(UserDetail userDetail) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_USER, userDetail.getUser_id());
        values.put(KEY_FIRST_NAME_USER, userDetail.getFirstName());
        values.put(KEY_LAST_NAME_USER, userDetail.getLastName());
        values.put(KEY_EMAIL_USER, userDetail.getEmailId());
        values.put(KEY_CONTACT_USER, userDetail.getPhoneNumber());
        values.put(KEY_PASSWORD_USER, userDetail.getPassword());
        values.put(KEY_AUTH_TOKEN_USER, userDetail.getAuthToken());
        values.put(KEY_PROFILE_USER, userDetail.getProfilePic());
        values.put(KEY_LANGUAGE_USER, userDetail.getLanguage());
        values.put(KEY_IS_CHEF_USER, userDetail.getIsChef());
        values.put(KEY_FB_AUTH_ID_USER, userDetail.getFbId());
        values.put(KEY_IS_VERIFY_USER, userDetail.getIsVerifyUser());
        values.put(KEY_IS_USER, userDetail.getIsUser());
        values.put(KEY_VERIFY_CHEF_USER, userDetail.getIsChefVerify());
        values.put(KEY_CURRENCY_USER, userDetail.getCurrency());
        values.put(KEY_ACCESS_TOKEN_USER, userDetail.getAccessToken());
        values.put(KEY_CHEF_NAME_USER, userDetail.getChefName());
        values.put(KEY_CHEF_LAST_NAME_USER, userDetail.getChefLastName());
        values.put(KEY_LANGUAGE_SPEAK_USER, userDetail.getLanguageList());
        values.put(KEY_SERVICE_TYPE_USER, userDetail.getServiceType());
        values.put(KEY_OFTEN_COOK_USER, userDetail.getOftenCook());
        values.put(KEY_CHEF_COUNTRY_USER, userDetail.getChefCountry());
        values.put(KEY_CHEF_CITY_USER, userDetail.getChefCity());
        values.put(KEY_FROMGUEST_USER, userDetail.getMin_guest());
        values.put(KEY_UPTOGUEST_USER, userDetail.getMax_guest());
        values.put(KEY_KITCHEN_TITLE_USER, userDetail.getKitchenTitle());
        values.put(KEY_KITCHEN_DESC_USER, userDetail.getKitchenDescription());
        values.put(KEY_CHEF_LAT_USER, userDetail.getCheflat());
        values.put(KEY_CHEF_LONG_USER, userDetail.getChefLong());
        values.put(KEY_LUNCH_USER, userDetail.getLunch());
        values.put(KEY_BRUNCH_USER, userDetail.getBrunch());
        values.put(KEY_DINNER_USER, userDetail.getDinner());
        values.put(KEY_CUISINE_USER, userDetail.getCuisine());
        values.put(KEY_FAV_USER, userDetail.getFav());
        values.put(KEY_REFERRAL_CODE, userDetail.getReferralCode());

        Log.e("Db", "User Updated");
        // updating row
        return db.update(TABLE_USER, values, KEY_ID_USER + " = ?",
                new String[]{String.valueOf(userDetail.getUser_id())});
    }

    /*-------------------------------------------UPDATE Booking------------------------------------------------------------*/
    public int updateBooking(UserDetail userDetail) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userDetail.getUser_id());
        values.put(KEY_CHEF_ID, userDetail.getChef_id());
        values.put(KEY_DATE, userDetail.getDate());
        values.put(KEY_TIME, userDetail.getTime());
        values.put(KEY_SEATS, userDetail.getNoOfSeats());
        values.put(KEY_MENU_TITLE, userDetail.getMenuTitle());
        values.put(KEY_MENU_ID, userDetail.getMenuId());
        values.put(KEY_MENU_CURRENCY, userDetail.getCurrency());
        values.put(KEY_MENU_PRICE, userDetail.getMenuPrice());
        values.put(KEY_SERVICE_TYPE, userDetail.getServiceType());
        values.put(KEY_FOOD_DELIVERY, userDetail.getFoodDelivery());
        values.put(KEY_KITCHEN_TOOL, userDetail.getKitchenTool());
        values.put(KEY_F_NAME, userDetail.getFirstName());
        values.put(KEY_L_NAME, userDetail.getLastName());
        values.put(KEY_EMAIL, userDetail.getEmailId());
        values.put(KEY_CONTACT_NO, userDetail.getPhoneNumber());
        values.put(KEY_DELIVERY_ADD, userDetail.getDeliveryAddress());
        values.put(KEY_USER_MSG, userDetail.getUserMsg());
        values.put(KEY_BOOKING_ID, userDetail.getBookingId());
        values.put(KEY_BOOK_NOW_ID, userDetail.getBooNowId());

        Log.e("Db", "User Updated");
        // updating row
        return db.update(TABLE_BOOKING, values, KEY_CHEF_ID + " = ?",
                new String[]{String.valueOf(userDetail.getChef_id())});
    }


    /*------------------------------------UPDATE CHEF DETAILS-------------------------------------------------------*/
    public long updateChefDetails(UserDetail userDetail) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_ID_CHEF, userDetail.getUser_id());
        contentValues.put(KEY_FIRST_NAME_CHEF, userDetail.getFirstName());
        contentValues.put(KEY_LAST_NAME_CHEF, userDetail.getLastName());
        contentValues.put(KEY_EMAIL_CHEF, userDetail.getEmailId());
        contentValues.put(KEY_CONTACT_CHEF, userDetail.getPhoneNumber());
        contentValues.put(KEY_PASSWORD_CHEF, userDetail.getPassword());
        contentValues.put(KEY_AUTH_TOKEN_CHEF, userDetail.getAuthToken());
        contentValues.put(KEY_PROFILE_CHEF, userDetail.getProfilePic());
        contentValues.put(KEY_LANGUAGE_CHEF, userDetail.getLanguage());
        contentValues.put(KEY_LANGUAGE_LIST_CHEF, userDetail.getLanguageList());
        contentValues.put(KEY_OFTEN_COOK_CHEF, userDetail.getOftenCook());
        contentValues.put(KEY_COUNTRY_CHEF, userDetail.getCountry());
        contentValues.put(KEY_CITY_CHEF, userDetail.getCity());
        contentValues.put(KEY_MIN_GUEST_CHEF, userDetail.getMin_guest());
        contentValues.put(KEY_MAX_GUEST_CHEF, userDetail.getMax_guest());
        contentValues.put(KEY_KITCHEN_TITLE_CHEF, userDetail.getKitchenTitle());
        contentValues.put(KEY_KITCHEN_DESC_CHEF, userDetail.getKitchenDescription());
        contentValues.put(KEY_SERVICE_LIST_CHEF, userDetail.getServiceType());
        contentValues.put(KEY_IS_CHEF_CHEF, userDetail.getIsChef());
        contentValues.put(KEY_IS_CHEF_VERIFY_CHEF, userDetail.getIsChefVerify());
        contentValues.put(KEY_COUNTRY_CODE, userDetail.getCountryCode());
        contentValues.put(KEY_LATITUDE, userDetail.getLatitude());
        contentValues.put(KEY_LONGITUDE, userDetail.getLongitude());
        contentValues.put(KEY_LUNCH, userDetail.getLunch());
        contentValues.put(KEY_BRUNCH, userDetail.getBrunch());
        contentValues.put(KEY_DINNER, userDetail.getDinner());
        contentValues.put(KEY_CURRENCY_CHEF, userDetail.getCurrency());
        contentValues.put(KEY_CUISIN_CHEF, userDetail.getCuisine());
        contentValues.put(KEY_FAV_CHEF, userDetail.getFav());
        contentValues.put(KEY_IS_VERIFY_USER_CHEF, userDetail.getIsVerifyUser());
        contentValues.put(KEY_IS_USER_CHEF, userDetail.getIsUser());
        contentValues.put(KEY_REFERELCODE_CHEF, userDetail.getReferralCode());
        contentValues.put(KEY_ACCESSTOKEN_CHEF, userDetail.getAccessToken());
        contentValues.put(KEY_DISTANCE_CHEF, userDetail.getDistance());
        contentValues.put(KEY_FAVCOUNT_CHEF, userDetail.getFavCount());
        contentValues.put(KEY_PRICE_CHEF, userDetail.getPrice());
        contentValues.put(KEY_ALLIMAGES_CHEF, userDetail.getAllImages());
        contentValues.put(KEY_OK_CHEF, userDetail.getOk());
        contentValues.put(KEY_RECOMMENDED_CHEF, userDetail.getRecommended());
        contentValues.put(KEY_NOT_RECOMMENDED_CHEF, userDetail.getNotRecommended());
        contentValues.put(KEY_TOTAL_REVIEW_CHEF, userDetail.getTotalReview());
        contentValues.put(KEY_AVG_RATING_CHEF, userDetail.getAvgRating());
        contentValues.put(KEY_MAX_PRICE, userDetail.getMaxPrice());

        Log.e("19", "Chef Updated");
        // updating row
        return db.insert(TABLE_CHEF, null,contentValues );
    }


    /*--------------------------------------------------DELETE SINGLE USER------------------------------------------------*/
    public void deleteUser(UserDetail userDetail) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USER, KEY_ID_USER + " = ?",
                new String[]{String.valueOf(userDetail.getUser_id())});
        db.close();
    }

    /*--------------------------------------------------DELETE SINGLE USER------------------------------------------------*/
    public void deleteBookingUser(UserDetail userDetail) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BOOKING, KEY_CHEF_ID + " = ?",
                new String[]{String.valueOf(userDetail.getUser_id())});
        db.close();
    }


    /*-------------------------------------------------DELETE-------------------------------------------------------------------*/
    public void deleteChef(UserDetail userDetail) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CHEF, KEY_ID_USER + " = ?",
                new String[]{String.valueOf(userDetail.getUser_id())});
        db.close();
    }

    public void deleteUser() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_USER);
    }

    public void deleteChef() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + TABLE_CHEF);
    }

    public void deleteBooking() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + TABLE_BOOKING);
        Log.e("Data", "Deleted");
    }

}
