package com.vervelogic.chefdiner.logger;

import android.content.Context;

import com.vervelogic.chefdiner.db.DBHelper;


/**
 * Created by Vishnu Saini 05-01-2016.
 */
public class Env {
    public static Context appContext;
    //    public static Activity currentActivity;
    public static DBHelper dbHelper;
    public static String logFilePath;
    public static boolean isDebugMode;

    public static void init(Context appContext, DBHelper dbHelper, String logFilePath, boolean isDebugMode) {
        Env.appContext = appContext;
        Env.dbHelper = dbHelper;
        Env.logFilePath = logFilePath;
        Env.isDebugMode = isDebugMode;
    }
}
