package com.vervelogic.chefdiner.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.activity.MyHistoryDetails;
import com.vervelogic.chefdiner.model.HistoryMeals;

import java.util.List;

/**
 * Created by Verve on 21-12-2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    List<HistoryMeals.GethistoryBean> historyMeals;
    Context context;
    String kitchenImage, menuTitle, code, date, menuPrice, kitchenName, statusDinner, houseRules;

    public HistoryAdapter(Context context, List<HistoryMeals.GethistoryBean> historyMeals) {
        this.historyMeals = historyMeals;
        this.context = context;
    }

    @Override
    public HistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_meal, viewGroup, false);
        return new HistoryAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.MyViewHolder holder, int position) {

         /*HorizontalScrollModel horizontalScrollModel=horizontalScrollModels.get(position);*/
        holder.date_text.setText(historyMeals.get(position).getDate());
        holder.no_of_guest_history.setText(historyMeals.get(position).getTotal_seats());
        holder.menu_history.setText(historyMeals.get(position).getMenu_title());
        kitchenImage = historyMeals.get(position).getKitchenimage();
        menuTitle = historyMeals.get(position).getMenu_title();
        code = historyMeals.get(position).getBooking_id();
        date = historyMeals.get(position).getDate();
        menuPrice =historyMeals.get(position).getMenu_currency()+" "+historyMeals.get(position).getMenu_price();
        kitchenName = historyMeals.get(position).getKitchen_title();
        statusDinner = historyMeals.get(position).getBooking_status();
        houseRules = historyMeals.get(position).getKitchen_descrition();
        holder.more_details_ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MyHistoryDetails.class);
                intent.putExtra("kitchenImage", kitchenImage);
                intent.putExtra("menuTitle", menuTitle);
                intent.putExtra("code", code);
                intent.putExtra("date", date);
                intent.putExtra("menuPrice", menuPrice);
                intent.putExtra("kitchenName", kitchenName);
                intent.putExtra("statusDinner", statusDinner);
                intent.putExtra("houseRules", houseRules);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return historyMeals.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView date_text, no_of_guest_history, menu_history;
        LinearLayout more_details_ly;

        MyViewHolder(View itemView) {
            super(itemView);
            date_text = itemView.findViewById(R.id.date_text);
            no_of_guest_history = itemView.findViewById(R.id.no_of_guest_history);
            menu_history = itemView.findViewById(R.id.menu_history);
            more_details_ly = itemView.findViewById(R.id.more_details_ly);

        }
    }
}
