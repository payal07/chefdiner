package com.vervelogic.chefdiner.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.model.CuisineSubCuisineModel;

import java.util.List;

public class FilterValRecyclerAdapter extends RecyclerView.Adapter<FilterValRecyclerAdapter.ValueViewHolder> {

    private final FragmentActivity context;
    private final List<CuisineSubCuisineModel.CuisineBean.SubCuisneBean> filterModels;
    ItemResponse itemResponse;
    String pos;
    OnItemClickListener mItemClickListener;

    public FilterValRecyclerAdapter(FragmentActivity context, List<CuisineSubCuisineModel.CuisineBean.SubCuisneBean> filterModels, ItemResponse itemResponse, String pos) {
        this.context = context;
        this.filterModels = filterModels;
        this.itemResponse = itemResponse;
        this.pos = pos;
    }


    @Override
    public ValueViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(this.context)
                .inflate(R.layout.sub_cuisine_ly, viewGroup, false);
        return new ValueViewHolder(v, i);
    }

    @Override
    public void onBindViewHolder(ValueViewHolder personViewHolder, int i) {
        personViewHolder.subCategoryName.setText(filterModels.get(i).getSubcuisine_name());
        personViewHolder.cbSelected.setChecked(filterModels.get(i).isChecked());

    }

    @Override
    public int getItemCount() {
        return filterModels.size();
    }

    public void setItemSelected(int position) {
        if (position != -1) {
            filterModels.get(position).setChecked(!filterModels.get(position).isChecked());
            notifyDataSetChanged();
        }
        itemResponse.itemClickResponse(true, filterModels, pos);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;

    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ValueViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        TextView subCategoryName;
        CheckBox cbSelected;

        ValueViewHolder(View itemView, int type) {
            super(itemView);
            itemView.setOnClickListener(this);
            subCategoryName = itemView.findViewById(R.id.txt_item_list_title);
            cbSelected = itemView.findViewById(R.id.cbSelected);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}
