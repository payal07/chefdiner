package com.vervelogic.chefdiner.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.model.ChooseSubMenuModel;

import java.util.List;

/**
 * Created by Verve on 06-10-2017.
 */

public class HorizontalChooseMenuAdapter extends RecyclerView.Adapter<HorizontalChooseMenuAdapter.MyViewHolder> {

    List<List<ChooseSubMenuModel.MenusSubmenusBean>> MenusSubmenus;
    private int selectedPosition = -1;// no selection by default
    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    private Context context;

    public HorizontalChooseMenuAdapter(Context context, List<List<ChooseSubMenuModel.MenusSubmenusBean>> MenusSubmenus) {
        this.MenusSubmenus = MenusSubmenus;
        this.context = context;
    }

    @Override
    public HorizontalChooseMenuAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.choose_menu_content, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final HorizontalChooseMenuAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        //HorizontalScrollModel horizontalScrollModel=horizontalScrollModels.get(position);

        holder.menu_title.setText(MenusSubmenus.get(position).get(0).getMenu_title());
        holder.hkd.setText(MenusSubmenus.get(position).get(0).getCurrency()+" "+MenusSubmenus.get(position).get(0).getMenu_price());


        ChooseMenuAdapter itemListDataAdapter = new ChooseMenuAdapter(context, MenusSubmenus.get(position));

        holder.choose_menu.setHasFixedSize(true);
        holder.choose_menu.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        holder.choose_menu.setAdapter(itemListDataAdapter);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                notifyDataSetChanged();
                notifyItemChanged(selectedPosition);

                // Save the selected positions to the SparseBooleanArray

            }
        });
        if (selectedPosition == position) {
            selectedItems.put(position, true);
            holder.cardView.setSelected(true);
            holder.cardView.setBackgroundResource(R.drawable.button_click);


        } else {
            if (selectedItems.get(position, false)) {

                selectedItems.delete(position);
                holder.cardView.setSelected(false);
                holder.cardView.setBackgroundResource(R.color.white);

            }
        }

       /* if(horizontalScrollModel.isSelected()){
            holder.cardView.setBackground(R.color.colorPrimary);
        }else{
            holder.cardView.setBackground(Color.GRAY);
        }*/

    }

    @Override
    public int getItemCount() {
        return MenusSubmenus.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView menu_title, dish_name, dish_desc, food_type;
        TextView hkd;
        ImageView selectedMenu;
        CardView cardView;
        RecyclerView choose_menu;

        MyViewHolder(View itemView) {
            super(itemView);
            menu_title = itemView.findViewById(R.id.menu_title);

            hkd = itemView.findViewById(R.id.hkd_menu);
            selectedMenu = itemView.findViewById(R.id.select_menu);
            cardView = itemView.findViewById(R.id.chef_card);
            choose_menu = itemView.findViewById(R.id.choose_menu_recyclerview);
        }
    }
}

