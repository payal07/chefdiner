package com.vervelogic.chefdiner.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.model.ChooseSubMenuModel;

import java.util.List;

/**
 * Created by Verve on 01-12-2017.
 */

public class ChooseMenuAdapter extends RecyclerView.Adapter<ChooseMenuAdapter.MyViewHolder> {

    Context context;
    private List<ChooseSubMenuModel.MenusSubmenusBean> horizontalScrollModels;
    private int selectedPosition = 0;// no selection by default
    private SparseBooleanArray selectedItems = new SparseBooleanArray();

    public ChooseMenuAdapter(Context context, List<ChooseSubMenuModel.MenusSubmenusBean> horizontalScrollModels) {
        this.horizontalScrollModels = horizontalScrollModels;
        this.context = context;
    }

    @Override
    public ChooseMenuAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.choose_menu_ly, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ChooseMenuAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        /*HorizontalScrollModel horizontalScrollModel=horizontalScrollModels.get(position);*/
        holder.dish_name.setText(horizontalScrollModels.get(position).getDish_name());
        // holder.dish_desc.setText(horizontalScrollModels.get(position).getDish_name());
        holder.food_type.setText(horizontalScrollModels.get(position).getDish_category());
        if (!TextUtils.isEmpty(horizontalScrollModels.get(position).getDish_image())) {
            Picasso.with(context)
                    .load(horizontalScrollModels.get(position).getDish_image())
                    .error(R.drawable.briyani).fit()
                    .into(holder.dish_image_custom);
        }

    }

    @Override
    public int getItemCount() {
        return horizontalScrollModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView dish_name, dish_desc, food_type;
        ImageView dish_image_custom;

        MyViewHolder(View itemView) {
            super(itemView);
            dish_name = itemView.findViewById(R.id.dish_name);
            // dish_desc = itemView.findViewById(R.id.dish_desc);
            food_type = itemView.findViewById(R.id.food_type);
            dish_image_custom = itemView.findViewById(R.id.dish_image_custom);

        }
    }
}

