package com.vervelogic.chefdiner.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.model.ChooseSubMenuModel;
import com.vervelogic.chefdiner.model.ProfileMenuModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Verve on 07-10-2017.
 */

public class ProfileMenuAdapter extends RecyclerView.Adapter<ProfileMenuAdapter.MyViewHolder> {
    Context context;
  /*  private List<ProfileMenuModel> profileMenuModels = new ArrayList<>();*/


    List<List<ChooseSubMenuModel.MenusSubmenusBean>> MenusSubmenus;
    private int selectedPosition = 0;// no selection by default
    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    String whatsHotKey;

    public ProfileMenuAdapter(Context context,List<List<ChooseSubMenuModel.MenusSubmenusBean>> MenusSubmenus,String whatsHotKey) {
        this.MenusSubmenus = MenusSubmenus;
        this.context = context;
        this.whatsHotKey = whatsHotKey;
    }

    @Override
    public ProfileMenuAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_profile_menu, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProfileMenuAdapter.MyViewHolder holder, final int position) {
        //final ProfileMenuModel profileMenuModel = MenusSubmenus.get(position);

        holder.dish_name.setText(MenusSubmenus.get(position).get(0).getMenu_title());
        holder.perGuest.setText(MenusSubmenus.get(position).get(0).getCurrency()+" "+MenusSubmenus.get(position).get(0).getMenu_price());
        Log.e("Adapter", "" + whatsHotKey);
        if (!TextUtils.isEmpty(whatsHotKey)) {
            if (whatsHotKey.equalsIgnoreCase("1")) {
                if (!TextUtils.isEmpty(MenusSubmenus.get(position).get(0).getWhatshot())) {
                    if (MenusSubmenus.get(position).get(0).getWhatshot().equalsIgnoreCase("yes")) {
                        holder.hot_img.setVisibility(View.VISIBLE);
                        holder.hot_img.setImageResource(R.drawable.ic_fire);
                    } else {
                        holder.hot_img.setVisibility(View.GONE);
                    }
                } else {
                    holder.hot_img.setVisibility(View.GONE);
                }
            } else {
                holder.hot_img.setVisibility(View.GONE);
            }
        } else {
            holder.hot_img.setVisibility(View.GONE);
        }
        if (!MenusSubmenus.get(position).get(0).getMenu_image().isEmpty()) {
            Picasso.with(context).
                    load(MenusSubmenus.get(position).get(0).getMenu_image()).
                    fit().
                    error(R.drawable.briyani).
                    into(holder.dish_img);
        }

        // holder.dish_img.setImageResource(R.drawable.briyani);
        holder.single_menu_ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(view.getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.choose_menu_content);
                dialog.getWindow().setLayout(700, ViewGroup.LayoutParams.WRAP_CONTENT);
                TextView menu_title = dialog.findViewById(R.id.menu_title);
                TextView hkd_menu = dialog.findViewById(R.id.hkd_menu);
                ImageView select_menu = dialog.findViewById(R.id.select_menu);
                select_menu.setVisibility(View.GONE);
                RecyclerView choose_menu_recyclerview = dialog.findViewById(R.id.choose_menu_recyclerview);

                //ChooseMenuAdapter itemListDataAdapter = new ChooseMenuAdapter(context, MenusSubmenus.get(position));


                choose_menu_recyclerview.setHasFixedSize(true);
                choose_menu_recyclerview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                //   choose_menu_recyclerview.setAdapter(itemListDataAdapter);

                menu_title.setText(MenusSubmenus.get(position).get(0).getMenu_title());
                hkd_menu.setText(MenusSubmenus.get(position).get(0).getCurrency()+" "+MenusSubmenus.get(position).get(0).getMenu_price());

                ChooseMenuAdapter itemListDataAdapter = new ChooseMenuAdapter(context, MenusSubmenus.get(position));
                choose_menu_recyclerview.setAdapter(itemListDataAdapter);

                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return MenusSubmenus.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView dish_name;
        ImageView dish_img;
        TextView perGuest;
        ImageView nextScreen, hot_img;
        RelativeLayout single_menu_ly;

        MyViewHolder(View itemView) {
            super(itemView);
            dish_img = itemView.findViewById(R.id.single_image);
            dish_name = itemView.findViewById(R.id.dish_name_single);
            perGuest = itemView.findViewById(R.id.guest_detail_single);
            nextScreen = itemView.findViewById(R.id.sign_to_right);
            hot_img = itemView.findViewById(R.id.hot_img);
            single_menu_ly = itemView.findViewById(R.id.single_menu_ly);
        }
    }
}
