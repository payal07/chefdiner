package com.vervelogic.chefdiner.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.model.CuisineSubCuisineModel;

import java.util.List;


public class FilterRecyclerAdapter extends RecyclerView.Adapter<FilterRecyclerAdapter.PersonViewHolder> {


    private final FragmentActivity context;
    List<CuisineSubCuisineModel.CuisineBean> filterModels;
    OnItemClickListener mItemClickListener;
    private int selectedPosition = 0;// no selection by default
    private SparseBooleanArray selectedItems = new SparseBooleanArray();

    public FilterRecyclerAdapter(FragmentActivity context, List<CuisineSubCuisineModel.CuisineBean> filterModels) {
        this.context = context;
        this.filterModels = filterModels;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(this.context)
                .inflate(R.layout.cuisine_ly, viewGroup, false);
        return new PersonViewHolder(v);
    }


    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, final int i) {

        personViewHolder.personName.setText(filterModels.get(i).getCuisine_name());
        personViewHolder.parentView.setSelected(filterModels.get(i).isSelectedCuisine());
        personViewHolder.counter.setText("" + filterModels.get(i).getC());


        if (selectedPosition == i) {

           /* personViewHolder.personName.setTextColor(context.getResources().getColor(R.color.white));
            personViewHolder.counter.setTextColor(context.getResources().getColor(R.color.white));*/
            personViewHolder.ly.setBackgroundResource(R.drawable.select_filter_bg);
        } else {

           /* personViewHolder.personName.setTextColor(context.getResources().getColor(R.color.black));
            personViewHolder.counter.setTextColor(context.getResources().getColor(R.color.black));*/
            personViewHolder.ly.setBackgroundResource(R.color.filter_color);

        }
    }

    @Override
    public int getItemCount() {
        return filterModels.size();
    }

    public void setItemSelected(int position) {
        for (CuisineSubCuisineModel.CuisineBean filterModel : filterModels) {
            filterModel.setSelectedCuisine(false);
        }
        if (position != -1) {
            filterModels.get(position).setSelectedCuisine(true);
            notifyDataSetChanged();
        }

    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public View parentView;
        TextView personName, counter;
        // CardView filter_root_layout;
        LinearLayout ly;

        PersonViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            personName = itemView.findViewById(R.id.txt_item_list_title);
            counter = itemView.findViewById(R.id.counter);
            ly = itemView.findViewById(R.id.ly);
            //   filter_root_layout = itemView.findViewById(R.id.filter_root_layout);
            parentView = itemView;

        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                selectedPosition = getPosition();
                notifyDataSetChanged();
                notifyItemChanged(selectedPosition);
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }


}

