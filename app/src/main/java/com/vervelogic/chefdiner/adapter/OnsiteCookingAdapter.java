package com.vervelogic.chefdiner.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.model.OnsiteCookingModel;

import java.util.ArrayList;

/**
 * Created by Verve on 04-12-2017.
 */


public class OnsiteCookingAdapter extends RecyclerView.Adapter<OnsiteCookingAdapter.ViewHolder> {
    private ArrayList<OnsiteCookingModel> android;
    private Context context;

    public OnsiteCookingAdapter(Context context, ArrayList<OnsiteCookingModel> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public OnsiteCookingAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_onsite_radio_button, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final OnsiteCookingAdapter.ViewHolder viewHolder, int i) {
        //in some cases, it will prevent unwanted situations
        viewHolder.checkBox.setOnCheckedChangeListener(null);

        //if true, your checkbox will be selected, else unselected
        viewHolder.checkBox.setChecked(android.get(i).isSelected());
        viewHolder.checkBox.setText(android.get(i).getCheckBox());

        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                android.get(viewHolder.getAdapterPosition()).setSelected(isChecked);
              /*  Toast.makeText(
                        buttonView.getContext(),
                        "Clicked on Checkbox: " +  viewHolder.checkBox.getText() + " is "
                                +  viewHolder.checkBox.isChecked(), Toast.LENGTH_LONG).show();*/
            }
        });


    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox checkBox;

        public ViewHolder(View view) {
            super(view);

            checkBox = view.findViewById(R.id.checkbox_onsite_cooking);

        }
    }

}
