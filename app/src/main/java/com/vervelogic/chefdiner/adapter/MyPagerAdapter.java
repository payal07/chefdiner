package com.vervelogic.chefdiner.adapter;

/**
 * Created by Verve on 17-11-2017.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.activity.GridImagesChef;
import com.vervelogic.chefdiner.model.GridItem;

import java.util.ArrayList;


public class MyPagerAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<GridItem> listItems;
    private int adapterType;

    public MyPagerAdapter(Context context, ArrayList<GridItem> listItems, int adapterType) {
        this.context = context;
        this.listItems = listItems;
        this.adapterType = adapterType;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(context).inflate(R.layout.single_image, null);
        try {

            GridItem gridItem = listItems.get(position);
            LinearLayout linMain = view.findViewById(R.id.linMain);
            ImageView imageCover = view.findViewById(R.id.imageCover);
            linMain.setTag(position);

            switch (adapterType) {
                case GridImagesChef.ADAPTER_TYPE_TOP:
                    linMain.setBackgroundResource(R.drawable.shadow);
                    break;
                /*case MainActivity.ADAPTER_TYPE_BOTTOM:
                    linMain.setBackgroundResource(0);
                    break;*/
            }

            Glide.with(context)
                    .load(gridItem.getImage())
                    .into(imageCover);

            container.addView(view);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

}