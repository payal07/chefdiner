package com.vervelogic.chefdiner.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.model.InstaModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Verve on 07-10-2017.
 */

public class InstagramAdapter extends RecyclerView.Adapter<InstagramAdapter.MyViewHolder> {
    private List<InstaModel> profileMenuModels = new ArrayList<>();
    private Context context;

    public InstagramAdapter(Context context, List<InstaModel> profileMenuModels) {
        this.profileMenuModels = profileMenuModels;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.insta_ly, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final InstaModel profileMenuModel = profileMenuModels.get(position);
        if (!TextUtils.isEmpty(profileMenuModel.getUri())) {
            Picasso.with(context)
                    .load(profileMenuModel.getUri())
                    .error(R.drawable.profile).fit()
                    .into(holder.dish_img);
        } else {
            holder.dish_img.setImageResource(R.drawable.briyani);
        }

        holder.dish_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(view.getRootView().getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.choose_menu_content);
                dialog.getWindow().setLayout(700, 900);

                LinearLayout card_view_layout_menu = dialog.findViewById(R.id.card_view_layout_menu);
                ImageView imageView = dialog.findViewById(R.id.imageView);


                card_view_layout_menu.setVisibility(View.GONE);

                Picasso.with(context)
                        .load(profileMenuModel.getUri())
                        .fit()
                        .into(imageView);

                dialog.show();
            }
        });

       /* holder.dish_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Next Screen Yet To Come", Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return profileMenuModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView dish_img;

        MyViewHolder(View itemView) {
            super(itemView);
            dish_img = itemView.findViewById(R.id.insta_img);

        }
    }
}
