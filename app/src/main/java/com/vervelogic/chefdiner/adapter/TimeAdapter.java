package com.vervelogic.chefdiner.adapter;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.model.TimeModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Verve on 06-10-2017.
 */

public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.MyViewHolder> {

    private static int row_index = -1;
    private List<TimeModel> timeModels = new ArrayList<>();

    public TimeAdapter(List<TimeModel> timeModels) {
        this.timeModels = timeModels;
    }

    @Override
    public TimeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_time_slot, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TimeAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        TimeModel timeModel = timeModels.get(position);
        holder.time.setText(timeModel.getTime());
        holder.text_ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                row_index = position;

                notifyDataSetChanged();
                notifyItemChanged(row_index);

            }
        });
        if (row_index == position) {
            holder.text_ly.setBackgroundResource(R.drawable.custom_button);
        } else {
          //  Log.e("TAGIndex", "" + row_index);
            holder.text_ly.setBackgroundResource(R.color.bg);
        }
    }

    @Override
    public int getItemCount() {
        return timeModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView time;
        LinearLayout text_ly;
        private SparseBooleanArray selectedItems = new SparseBooleanArray();

        MyViewHolder(View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time);
            text_ly = itemView.findViewById(R.id.text_ly);
           /* time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (selectedItems.get(getAdapterPosition(), false)) {
                        selectedItems.delete(getAdapterPosition());
                        time.setSelected(false);
                    }
                    else {
                        selectedItems.put(getAdapterPosition(), true);
                        time.setSelected(true);
                        time.setTextColor(R.drawable.text_select);
                        text_ly.setBackgroundResource(R.drawable.custom_button);

                    }
                }
            });*/
        }
    }
}
