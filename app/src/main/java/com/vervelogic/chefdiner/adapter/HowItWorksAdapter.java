package com.vervelogic.chefdiner.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.model.HowItWorksModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Verve on 05-12-2017.
 */

public class HowItWorksAdapter extends RecyclerView.Adapter {

    Context context;
    private List<HowItWorksModel> howItWorksModels = new ArrayList<>();
    private int selectedPosition = 0;// no selection by default
    private SparseBooleanArray selectedItems = new SparseBooleanArray();

    public HowItWorksAdapter(Context context, List<HowItWorksModel> howItWorksModels) {
        this.howItWorksModels = howItWorksModels;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View view;
        switch (i) {
            case HowItWorksModel.EVEN:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.how_it_works_flip, parent, false);
                return new MyViewHolderFlip(view);
            case HowItWorksModel.ODD:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.how_it_works_ly, parent, false);
                return new MyViewHolder(view);

        }
        return null;

       /* View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.how_it_works_ly,parent,false);
        return new HowItWorksAdapter.MyViewHolder(view);*/
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HowItWorksModel howItWorksModel = howItWorksModels.get(position);
        if (position % 2 == 0) {
            ((MyViewHolderFlip) holder).titleFlip.setText(howItWorksModel.getTitle());
            ((MyViewHolderFlip) holder).titlesDescFlip.setText(howItWorksModel.getTitleDesc());
            if (!howItWorksModel.getUrl().equals("")) {
                Picasso.with(context)
                        .load(howItWorksModel.getUrl())
                        .error(R.drawable.profile)
                        .into(((MyViewHolderFlip) holder).urlFlip);
            }

        } else {
            ((MyViewHolder) holder).title.setText(howItWorksModel.getTitle());
            ((MyViewHolder) holder).titlesDesc.setText(howItWorksModel.getTitleDesc());
            if (!howItWorksModel.getUrl().equals("")) {
                Picasso.with(context)
                        .load(howItWorksModel.getUrl())
                        .error(R.drawable.briyani)
                        .into(((MyViewHolder) holder).url);
            }
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return HowItWorksModel.EVEN;
        } else {
            return HowItWorksModel.ODD;
        }

    }

    @Override
    public int getItemCount() {
        return howItWorksModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, titlesDesc;
        ImageView url;

        MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title_how_it_works);
            titlesDesc = itemView.findViewById(R.id.titleDesc_how_it_works);
            url = itemView.findViewById(R.id.image_how_it_works);
        }
    }

    class MyViewHolderFlip extends RecyclerView.ViewHolder {
        TextView titleFlip, titlesDescFlip;
        ImageView urlFlip;

        MyViewHolderFlip(View itemView) {
            super(itemView);
            titleFlip = itemView.findViewById(R.id.title_how_it_works_flip);
            titlesDescFlip = itemView.findViewById(R.id.titleDesc_how_it_works_flip);
            urlFlip = itemView.findViewById(R.id.image_how_it_works_flip);
        }
    }
}
