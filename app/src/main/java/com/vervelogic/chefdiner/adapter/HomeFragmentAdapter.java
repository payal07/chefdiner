package com.vervelogic.chefdiner.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.activity.ChefProfile;
import com.vervelogic.chefdiner.model.HomeFragmentModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Verve on 05-10-2017.
 */

public class HomeFragmentAdapter extends RecyclerView.Adapter<HomeFragmentAdapter.MyViewHolder> {

    private List<HomeFragmentModel> homeFragmentModels = new ArrayList<>();
    private Context context;

    public HomeFragmentAdapter(Context context, List<HomeFragmentModel> homeFragmentModels) {
        this.homeFragmentModels = homeFragmentModels;
        this.context = context;
    }

    @Override
    public HomeFragmentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_home_fragment, parent, false);
        view.setMinimumWidth(parent.getMeasuredWidth());
        return new MyViewHolder(view);
    }
   /* public HomeFragmentAdapter(Context context,List<HomeFragmentModel> homeFragmentModels) {
        this.context = context;
        this.homeFragmentModels=homeFragmentModels;
    }*/

    @Override
    public void onBindViewHolder(final HomeFragmentAdapter.MyViewHolder holder, int position) {
        final HomeFragmentModel homeFragmentModel = homeFragmentModels.get(position);
        holder.HKD.setText(homeFragmentModel.getHKD());

        String str = homeFragmentModel.getCuisine();
        if (!TextUtils.isEmpty(str)) {
            List<String> items = Arrays.asList(str.split("\\s*,\\s*"));
            ArrayList<String> listView = new ArrayList<>();
            for (int j = 0; j < items.size(); j++) {
                if (!items.get(j).equals("")) {
                    listView.add(items.get(j));
                }
            }
            if (!listView.isEmpty()) {
                homeFragmentModel.setFoodType1(listView.get(0));
                if (listView.size()>1)
                {
                    homeFragmentModel.setFoodType2(listView.get(1));
                }

            }
        }

        holder.foodStyle1.setText(homeFragmentModel.getFoodType1());
        holder.foodStyle2.setText(homeFragmentModel.getFoodType2());
        holder.chef_bookmarked.setText(homeFragmentModel.getBookmarkedCount());
        holder.rateCount.setText(homeFragmentModel.getRateCount());
        holder.reviewCount.setText(homeFragmentModel.getReviewCount());
        holder.chef_name.setText(homeFragmentModel.getKitchen_title());
        holder.chef_address.setText(homeFragmentModel.getChefAddress() + "," + homeFragmentModel.getChefCountry());
        holder.sadCount.setText(homeFragmentModel.getSadCount());
        holder.happyCount.setText(homeFragmentModel.getHappyCount());
        if (!TextUtils.isEmpty(homeFragmentModel.getRateCount())) {
            holder.avgRatingHome.setRating(Float.parseFloat(homeFragmentModel.getRateCount()));
        }
        if (!TextUtils.isEmpty(homeFragmentModel.getProfileUrl())) {
            Picasso.with(context)
                    .load(homeFragmentModel.getProfileUrl())
                    .error(R.drawable.profile).fit()
                    .into(holder.profile_image);
        }
        if (!TextUtils.isEmpty(homeFragmentModel.getImageUrl())) {
            Picasso.with(context)
                    .load(homeFragmentModel.getImageUrl())
                    .error(R.drawable.people).fit()
                    .into(holder.image1);
        }
        if (!TextUtils.isEmpty(homeFragmentModel.getImage2Url())) {
            Picasso.with(context)
                    .load(homeFragmentModel.getImage2Url())
                    .error(R.drawable.food).resize(400, 0)
                    .into(holder.image2);
        }
        if (!TextUtils.isEmpty(homeFragmentModel.getImage3Url())) {
            Picasso.with(context)
                    .load(homeFragmentModel.getImage3Url())
                    .error(R.drawable.dessert).resize(400, 0)
                    .into(holder.image3);
        }

        holder.chef_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ChefProfile.class);
                intent.putExtra("Chef_id", homeFragmentModel.getChef_id());
                intent.putExtra("maxprice", homeFragmentModel.getMaxprice());
                intent.putExtra("whatsHotKey", homeFragmentModel.getWhatsHotKey());
                intent.putExtra("keyChef", "home");
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return homeFragmentModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView image1, image2, image3;
        CircleImageView profile_image;
        ImageView bookmarked_img;
        TextView HKD, chef_bookmarked, chef_name, chef_address, sadCount, happyCount, reviewCount, rateCount, foodStyle1, foodStyle2;
        SimpleRatingBar avgRatingHome;
        CardView chef_card;

        //   SimpleRatingBar ratingBar;
        MyViewHolder(View itemView) {
            super(itemView);
            image1 = itemView.findViewById(R.id.image1);
            image2 = itemView.findViewById(R.id.image2);
            image3 = itemView.findViewById(R.id.image3);
            profile_image = itemView.findViewById(R.id.profile_image);
            bookmarked_img = itemView.findViewById(R.id.bookmarked_image);
            HKD = itemView.findViewById(R.id.guest_detail);
            chef_bookmarked = itemView.findViewById(R.id.bookmarked);
            chef_name = itemView.findViewById(R.id.chef_name);
            chef_address = itemView.findViewById(R.id.chef_address);
            sadCount = itemView.findViewById(R.id.sad_count);
            happyCount = itemView.findViewById(R.id.happy_count);
            rateCount = itemView.findViewById(R.id.rate_count);
            reviewCount = itemView.findViewById(R.id.review_text);
            foodStyle1 = itemView.findViewById(R.id.food_style1);
            foodStyle2 = itemView.findViewById(R.id.food_style2);
            avgRatingHome = itemView.findViewById(R.id.avgRatingHome);
            chef_card = itemView.findViewById(R.id.chef_card);
            // ratingBar=(SimpleRatingBar) itemView.findViewById(R.id.rating_bar);
        }
    }
}
