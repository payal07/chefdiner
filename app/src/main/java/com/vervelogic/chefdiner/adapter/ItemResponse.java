package com.vervelogic.chefdiner.adapter;

/**
 * Created by Verve on 13-12-2017.
 */

public interface ItemResponse {

    void itemClickResponse(Object status, Object response, String pos);

}
