package com.vervelogic.chefdiner.db;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by RAMNIVAS SINGH 05-01-2016.
 */
public interface DBHelper {
    int getDBVersion();

    String getDBName();

    void onCreate(SQLiteDatabase db);

    void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);
}
