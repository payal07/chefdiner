package com.vervelogic.chefdiner.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.vervelogic.chefdiner.logger.Env;


/**
 * Created by RAMNIVAS SINGH 05-01-2016.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    public SQLiteHelper() {
        super(Env.appContext, Env.dbHelper.getDBName(), null, Env.dbHelper.getDBVersion());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Env.dbHelper.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Env.dbHelper.onUpgrade(db, oldVersion, newVersion);
    }
}
