package com.vervelogic.chefdiner.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.activity.EditBookingDetails;
import com.vervelogic.chefdiner.activity.MessageOpenActivity;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.ChatDetailsModel;
import com.vervelogic.chefdiner.model.UpcomingData;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Upcoming extends Fragment {

    private static final String TAG = "Upcoming";
    /*--------------------------Declarations----------------------------------------*/
    EditText promoCode;
    Button applyCode, cancelDinner, changeDetails, openChat,invite_earn;
    ImageView kitchenImage;
    LinearLayout main_ly;
    TextView dinner_time_meals, meal_status, kitchen_name_meals, code_meals, date_meals, guest_meals, total_price_meals, house_rules_meals,
            menu_meals;
    RecyclerView recycler_view_meals;
    View view;
    String user_id, kitchen_image, promoCodeStr, bookingcode, dateText, timeText, guestText, chefId, oldDate;
    Activity context = getActivity();
    SharedPreference sharedPreference;
    UpcomingAdapter historyAdapter;
    String referralCode = "";
    DbHelper dbHelper;

    ChatDetailsModel chatDetailsModel;
    public Upcoming()
    {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_upcoming, container, false);
            sharedPreference = new SharedPreference();
            if (!TextUtils.isEmpty(sharedPreference.getUserId(getActivity().getApplicationContext()))) {
                user_id = sharedPreference.getUserId(getActivity().getApplicationContext());
            } else {
                user_id = "";
            }

            //user_id = sharedPreference.getUserId(getActivity());
            chatDetailsModel=new ChatDetailsModel();

            promoCode = view.findViewById(R.id.promo_code_meals);
            applyCode = view.findViewById(R.id.apply_code);
            cancelDinner = view.findViewById(R.id.cancel_dinner);
            changeDetails = view.findViewById(R.id.change_details);
            openChat = view.findViewById(R.id.open_chat);
            kitchenImage = view.findViewById(R.id.kitchen_image_meal);
            dinner_time_meals = view.findViewById(R.id.dinner_time_meals);
            meal_status = view.findViewById(R.id.meal_status);
            kitchen_name_meals = view.findViewById(R.id.kitchen_name_meals);
            code_meals = view.findViewById(R.id.code_meals);
            date_meals = view.findViewById(R.id.date_meals);
            guest_meals = view.findViewById(R.id.guest_meals);
            total_price_meals = view.findViewById(R.id.total_price_meals);
            house_rules_meals = view.findViewById(R.id.house_rules_meals);
            menu_meals = view.findViewById(R.id.menu_meals);
            recycler_view_meals = view.findViewById(R.id.recycler_view_meals);
            main_ly = view.findViewById(R.id.main_ly);
            invite_earn = view.findViewById(R.id.invite_earn);


            dbHelper = new DbHelper(getActivity().getApplicationContext());
            List<UserDetail> contacts = dbHelper.getAllDetails();
            for (UserDetail cn : contacts) {
                if (!TextUtils.isEmpty(cn.getReferralCode())) {
                    referralCode = cn.getReferralCode();
                    Log.e("referal_code", "" + referralCode);
                } else {
                    referralCode = "";
                }
            }

            ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            if (netInfo == null) {
                new android.support.v7.app.AlertDialog.Builder(getActivity())
                        .setTitle(getResources().getString(R.string.app_name))
                        .setMessage(getResources().getString(R.string.internet_error))
                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
            } else {
                upcomingMealAPI();
            }
            main_ly.setVisibility(View.GONE);
            applyCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    promoCodeStr = promoCode.getText().toString();
                    if (!TextUtils.isEmpty(promoCodeStr)) {
                        ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                        if (netInfo == null) {
                            new android.support.v7.app.AlertDialog.Builder(getActivity())
                                    .setTitle(getResources().getString(R.string.app_name))
                                    .setMessage(getResources().getString(R.string.internet_error))
                                    .setPositiveButton(getResources().getString(R.string.ok), null).show();
                        } else {
                            sharedBookingAPI();
                        }

                    } else {
                        promoCode.setError(getString(R.string.please_enter_promo_code));
                    }
                }
            });
            cancelDinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                    if (netInfo == null) {
                        new android.support.v7.app.AlertDialog.Builder(getActivity())
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.internet_error))
                                .setPositiveButton(getResources().getString(R.string.ok), null).show();
                    } else {
                        cancelDinnerAPI();
                    }

                }
            });
            changeDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), EditBookingDetails.class);
                    intent.putExtra("bookingcode", bookingcode);
                    intent.putExtra("dateText", dateText);
                    intent.putExtra("timeText", timeText);
                    intent.putExtra("guestText", guestText);
                    intent.putExtra("oldDate", oldDate);
                    intent.putExtra("chefId", chefId);
                    startActivity(intent);
                }
            });

            openChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    chatDetailsModel.setUserId(user_id);
                    chatDetailsModel.setChefId(chefId);
                   /* chatDetailsModel.setChefName();*/
                    Intent cal = new Intent(getActivity(), MessageOpenActivity.class);
                    cal.putExtra("Model",chatDetailsModel);
                    startActivity(cal);
                }
            });

            invite_earn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                        String sAux = "\nLet me recommend you this application\n\n";
                        sAux = sAux + AppConfig.REFERRAL_CODE_URL + referralCode + "\n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        return view;
    }

    public void upcomingMealAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.UPCOMING_MEAL_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            UpcomingData upcomingData = new Gson().fromJson(s, UpcomingData.class);
                            if (status == 1) {
                                Log.e(TAG, "" + s);
                                main_ly.setVisibility(View.VISIBLE);
                                for (int i = 0; i < upcomingData.getGetupcoming().size(); i++) {
                                    meal_status.setText(upcomingData.getGetupcoming().get(0).getBooking_status());
                                    date_meals.setText(upcomingData.getGetupcoming().get(0).getDate() + "," + upcomingData.getGetupcoming().get(0).getTime());
                                    total_price_meals.setText(upcomingData.getGetupcoming().get(0).getMenu_currency() + " " + upcomingData.getGetupcoming().get(0).getMenu_price());
                                    guest_meals.setText(upcomingData.getGetupcoming().get(0).getTotal_seats() + " guests");
                                    code_meals.setText(upcomingData.getGetupcoming().get(0).getBooking_id());
                                    house_rules_meals.setText(upcomingData.getGetupcoming().get(0).getKitchen_descrition());
                                    menu_meals.setText(upcomingData.getGetupcoming().get(0).getMenu_title());
                                    kitchen_name_meals.setText(upcomingData.getGetupcoming().get(0).getKitchen_title());
                                    kitchen_image = upcomingData.getGetupcoming().get(0).getKitchenimage();
                                    bookingcode = upcomingData.getGetupcoming().get(0).getBooknow_id();
                                    dateText = upcomingData.getGetupcoming().get(0).getDate();
                                    timeText = upcomingData.getGetupcoming().get(0).getTime();
                                    guestText = upcomingData.getGetupcoming().get(0).getTotal_seats();
                                    chefId = upcomingData.getGetupcoming().get(0).getChef_id();
                                    oldDate = upcomingData.getGetupcoming().get(0).getOlddate();
                                    if (!TextUtils.isEmpty(kitchen_image)) {
                                        Picasso.with(context)
                                                .load(kitchen_image)
                                                .error(R.drawable.people).fit()
                                                .into(kitchenImage);
                                    }

                                    if (upcomingData.getGetupcoming().get(0).getDays().equalsIgnoreCase("0 days")) {
                                        dinner_time_meals.setText("Dinner in next " + upcomingData.getGetupcoming().get(0).getHours());
                                    } else {
                                        dinner_time_meals.setText("Dinner in next " + upcomingData.getGetupcoming().get(0).getDays());
                                    }
                                    if (i > 0) {
                                        recycler_view_meals.setVisibility(View.VISIBLE);
                                        LinearLayoutManager horizontalLayoutManager
                                                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                        recycler_view_meals.setLayoutManager(horizontalLayoutManager);
                                        //   Toast.makeText(getActivity(), ""+upcomingData.getGetupcoming().size(), Toast.LENGTH_SHORT).show();
                                        recycler_view_meals.setNestedScrollingEnabled(true);
                                        historyAdapter = new UpcomingAdapter(getActivity(), upcomingData.getGetupcoming());
                                        recycler_view_meals.setAdapter(historyAdapter);
                                    } else {
                                        recycler_view_meals.setVisibility(View.GONE);
                                    }

                                }

                            }
                            if (status == 0) {
                                progressDialog.dismiss();
                                String message = upcomingData.getMessage();
                                Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                                main_ly.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if(getActivity() != null) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if(getActivity() != null) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                Log.e(TAG, "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("user_id", user_id);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void sharedBookingAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Sharing Code");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.SHARED_BOOKING_MEAL_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        try {
                            JSONObject object = new JSONObject(s);
                            int status = object.getInt("status");
                            if (status == 1) {
                                String message = object.getString("message");
                                Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                                ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                                            .setTitle(getResources().getString(R.string.app_name))
                                            .setMessage(getResources().getString(R.string.internet_error))
                                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                } else {
                                    upcomingMealAPI();
                                }
                            }
                            if (status == 0) {
                                String message = object.getString("message");
                                Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if(getActivity() != null) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if(getActivity() != null) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("user_id", user_id);
                map.put("bookingcode", bookingcode);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void cancelDinnerAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cancelling");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.CANCEL_BOOKING_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressDialog.dismiss();
                        Log.e("Res", "" + s);
                        try {
                            JSONObject object = new JSONObject(s);
                            int status = object.getInt("status");
                            if (status == 1) {
                                String message = object.getString("message");
                                Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                                ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                                if (netInfo == null) {
                                    new android.support.v7.app.AlertDialog.Builder(getActivity())
                                            .setTitle(getResources().getString(R.string.app_name))
                                            .setMessage(getResources().getString(R.string.internet_error))
                                            .setPositiveButton(getResources().getString(R.string.ok), null).show();
                                } else {
                                    upcomingMealAPI();
                                }
                            }
                            if (status == 0) {
                                String message = object.getString("message");
                                Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if(getActivity() != null) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            if(getActivity() != null) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (error instanceof AuthFailureError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (error instanceof NoConnectionError) {
                            message = "Please check your internet connection.";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onErrorResponse: " + message);
                        Log.e("Error: ", error.toString());

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("user_id", user_id);
                map.put("bookingcode", bookingcode);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public class UpcomingAdapter extends RecyclerView.Adapter<UpcomingAdapter.MyViewHolder> {

        List<UpcomingData.GetupcomingBean> historyMeals;
        Context context;
        String kitchenImage, menuTitle, code, date, menuPrice, kitchenName, statusDinner, houseRules;

        public UpcomingAdapter(Context context, List<UpcomingData.GetupcomingBean> historyMeals) {
            this.historyMeals = historyMeals;
            this.context = context;
        }

        @Override
        public UpcomingAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.upcoming_meal, viewGroup, false);
            return new UpcomingAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(UpcomingAdapter.MyViewHolder holder, int position) {

         /*HorizontalScrollModel horizontalScrollModel=horizontalScrollModels.get(position);*/
         /*   holder.date_text.setText(historyMeals.get(position).getDate());*/
            holder.no_of_guest_history.setText(historyMeals.get(position).getTotal_seats() + " Guests");
            holder.menu_history.setText(historyMeals.get(position).getMenu_title());
            holder.menu_code.setText(historyMeals.get(position).getBooking_id());
            kitchenImage = historyMeals.get(position).getKitchenimage();
            menuTitle = historyMeals.get(position).getMenu_title();
            code = historyMeals.get(position).getBooking_id();
            date = historyMeals.get(position).getDate();
            menuPrice = historyMeals.get(position).getMenu_price();
            kitchenName = historyMeals.get(position).getKitchen_title();
            statusDinner = historyMeals.get(position).getBooking_status();
            houseRules = historyMeals.get(position).getKitchen_descrition();
            /*holder.more_details_ly.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, MyHistoryDetails.class);
                    intent.putExtra("kitchenImage", kitchenImage);
                    intent.putExtra("menuTitle", menuTitle);
                    intent.putExtra("code", code);
                    intent.putExtra("date", date);
                    intent.putExtra("menuPrice", menuPrice);
                    intent.putExtra("kitchenName", kitchenName);
                    intent.putExtra("statusDinner", statusDinner);
                    intent.putExtra("houseRules", houseRules);
                    context.startActivity(intent);
                }
            });*/

        }

        @Override
        public int getItemCount() {
            return historyMeals.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView date_text, no_of_guest_history, menu_history, menu_code;
            LinearLayout more_details_ly;

            MyViewHolder(View itemView) {
                super(itemView);
                // date_text = itemView.findViewById(R.id.date_text);
                no_of_guest_history = itemView.findViewById(R.id.no_of_guest_history);
                menu_history = itemView.findViewById(R.id.menu_history);
                menu_code = itemView.findViewById(R.id.menu_code);
                /*more_details_ly = itemView.findViewById(R.id.more_details_ly);*/

            }
        }
    }

}
