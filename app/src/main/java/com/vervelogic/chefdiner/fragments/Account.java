package com.vervelogic.chefdiner.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.activity.FavoriteChefList;
import com.vervelogic.chefdiner.activity.NotificationChef;
import com.vervelogic.chefdiner.adapter.ItemResponse;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SessionManager;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.StringTokenizer;


@SuppressLint("ValidFragment")
public class Account extends Fragment implements View.OnClickListener {

    LinearLayout logout_ly, payment_method_ly, currency_ly, sms_notification_ly, favorites_ly;
    TextView cancel;
    Spinner currency_account;
    Button save;
    String selectedCurrency;
    ItemResponse itemResponse;

    // Session Manager Class
    SessionManager session;
    String user_id;
    DbHelper dbHelper;
    ArrayAdapter<Currency> stringArrayAdapter;
    private SharedPreference sharedPreference;

    public Account(ItemResponse profile) {
        this.itemResponse = profile;
    }
    public Account()
    {

    }

    public static List<Currency> getAllCurrencies() {
        List<Currency> toret = new ArrayList<>();
        Locale[] locs = Locale.getAvailableLocales();
        List<String> currencySysmbol = new ArrayList<>();

        for (Locale loc : locs) {

            try {
                Currency currency = Currency.getInstance(loc);
                if (currency != null) {
                    toret.add(currency);
                    currencySysmbol.add(currency.getSymbol());

                }
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }

        return toret;
    }

    public static List<String> getAllCurrencySymbol() {
        List<Currency> toret = new ArrayList<>();
        Locale[] locs = Locale.getAvailableLocales();
        List<String> currencySysmbol = new ArrayList<>();

        for (Locale loc : locs) {

            try {
                Currency currency = Currency.getInstance(loc);
                if (currency != null) {
                    toret.add(currency);
                    currencySysmbol.add(currency.getSymbol());
                }
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }

        return currencySysmbol;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // ------------------------Inflate the layout for this fragment---------------------------

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        View view = inflater.inflate(R.layout.fragment_account, container, false);

        //---------------------------- IDs--------------------------------------------
        favorites_ly = view.findViewById(R.id.favorites_ly);
        sms_notification_ly = view.findViewById(R.id.sms_notification_ly);
        currency_ly = view.findViewById(R.id.currency_ly);
        payment_method_ly = view.findViewById(R.id.payment_method_ly);
        logout_ly = view.findViewById(R.id.logout_ly);


        //------------------------------- Listeners----------------------------------
        favorites_ly.setOnClickListener(this);
        sms_notification_ly.setOnClickListener(this);
        currency_ly.setOnClickListener(this);
        payment_method_ly.setOnClickListener(this);
        logout_ly.setOnClickListener(this);

        //------------------------- Session class instance----------------------------
        session = new SessionManager(getContext());

        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(getActivity()))) {
            //language = sharedPreference.getLanguage(context);
            user_id = sharedPreference.getUserId(getActivity());
        } else {
            user_id = "";
        }

        // user_id = sharedPreference.getUserId(getActivity());


        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.favorites_ly:
                Intent intent = new Intent(getActivity().getApplicationContext(), FavoriteChefList.class);
                intent.putExtra("cuisine", "0");
                startActivity(intent);
                break;
            case R.id.sms_notification_ly:
                Intent intentSMS = new Intent(getActivity(), NotificationChef.class);
                startActivity(intentSMS);
                break;
            case R.id.currency_ly:

                // ---------------------------custom dialog---------------------------------
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                Window window = dialog.getWindow();
                window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.currency_list);
                List<Currency> strings = getAllCurrencies();

                // add elements to al, including duplicates
                Set<Currency> hs = new HashSet<>();
                hs.addAll(strings);
                strings.clear();
                strings.addAll(hs);

                Collections.sort(strings, new Comparator<Currency>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public int compare(Currency lhs, Currency rhs) {
                        String stringName1 = lhs.getCurrencyCode();
                        String stringName2 = rhs.getCurrencyCode();

                        return stringName1.compareToIgnoreCase(stringName2);
                      //  return lhs.getDisplayName().compareTo(rhs.getDisplayName());
                    }
                });

                currency_account = dialog.findViewById(R.id.currency_account);
                save = dialog.findViewById(R.id.save_currency);
                stringArrayAdapter = new ArrayAdapter<Currency>(getActivity(), R.layout.currency_spinner_text, strings);
                currency_account.setAdapter(stringArrayAdapter);
                // --------------------------------LISTENER ON SPINNER----------------------------
                currency_account.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        String first = currency_account.getSelectedItem().toString();
                        StringTokenizer tokens = new StringTokenizer(first, "(");
                        selectedCurrency = tokens.nextToken();// this will contain "Fruit"
                        //  String second = tokens.nextToken();// this will contain " they taste good"

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                dbHelper = new DbHelper(getActivity().getApplicationContext());
                List<UserDetail> contacts = dbHelper.getAllDetails();
                for (UserDetail cn : contacts) {
                    String c = cn.getCurrency();
                    if (!TextUtils.isEmpty(c)) {
                        Log.e("Data",""+stringArrayAdapter.getPosition(Currency.getInstance(c)));
                        currency_account.setSelection(stringArrayAdapter.getPosition(Currency.getInstance(c)));
                    }

                }
                cancel = dialog.findViewById(R.id.cancel);

                //---------------------- if button is clicked, close the custom dialog-----------------
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        itemResponse.itemClickResponse(true, selectedCurrency, "1");
                        dialog.dismiss();
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

              /*  List<String> currencyList = getAllCurrencySymbol();*/

              /*  List<String> strings1 = new ArrayList<>();
                for (int i = 0; i < strings.size(); i++) {
                    strings1.add(strings.get(i) + "(" + currencyList.get(i) + ")");
                }
*/

                dialog.show();
                break;
            case R.id.payment_method_ly:
                /*Intent intent1 = new Intent(getActivity(), InviteAndEarn.class);
                startActivity(intent1);*/
                break;
            case R.id.logout_ly:
                session.logoutUser();
                LoginManager.getInstance().logOut();
                break;
        }
    }


}
