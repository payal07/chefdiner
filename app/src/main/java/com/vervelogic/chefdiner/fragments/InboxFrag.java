package com.vervelogic.chefdiner.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.activity.MessageOpenActivity;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.appdatabase.DatabaseMgrApp;
import com.vervelogic.chefdiner.chatModel.DeviceTokenModel;
import com.vervelogic.chefdiner.chatModel.RecentChatModel;
import com.vervelogic.chefdiner.firebase.ChatingContactAdapter;
import com.vervelogic.chefdiner.firebase.FirebaseConstant;
import com.vervelogic.chefdiner.firebase.RecentActiveAdapter;
import com.vervelogic.chefdiner.model.ChatDetailsModel;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;


public class InboxFrag extends Fragment implements View.OnClickListener, ChildEventListener {

    View view;
    Button messageClass;

    private ChatingContactAdapter chatingContactAdapter;
    private RecentActiveAdapter recentActiveAdapter;
    private RecyclerView recyclerView_ChattingContactList;
    private RecyclerView recyclerView_ChattingRecentList;

    private static final String TAG = "Chat";
    ArrayList<String> chatlist;
    ArrayList<RecentChatModel> listRecentChatModel;
    ArrayList<RecentChatModel> recentActivityList;
    private String senderId = "38";
    private String user_id = "";
    SharedPreference sharedPreference;
    Activity context;
    TextView no_data;

    private LinearLayout lin_recentChat;
    private LinearLayout lin_allContacts;
    public InboxFrag()
    {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_inbox, container, false);
            messageClass = view.findViewById(R.id.messageClass);
            no_data = view.findViewById(R.id.no_data);
            context = getActivity();
            inIt();
            messageClass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), MessageOpenActivity.class);
                    startActivity(intent);
                }
            });
        }


        return view;
    }

    private void inIt() {
        sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getUserId(context))) {
            user_id = sharedPreference.getUserId(context);
        } else {
            user_id = "";
        }

        //setViewDinamicaly();

        chatlist = new ArrayList<>();
        listRecentChatModel = new ArrayList<>();
        recentActivityList = new ArrayList<>();

        AppController.ref.child(FirebaseConstant.TABLE_RECENT).child(user_id).addChildEventListener(this);

        recyclerView_ChattingContactList = view.findViewById(R.id.recyclerView_ChattingContactList);
        recyclerView_ChattingRecentList = view.findViewById(R.id.recyclerView_ChattingRecentList);

        lin_recentChat= view.findViewById(R.id.lin_recentChat);
        lin_allContacts= view.findViewById(R.id.lin_allContacts);

        chatingContactAdapter = new ChatingContactAdapter(getActivity(), this, listRecentChatModel);
        recyclerView_ChattingContactList.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        recyclerView_ChattingContactList.setItemAnimator(new DefaultItemAnimator());
        recyclerView_ChattingContactList.setAdapter(chatingContactAdapter);

        lin_recentChat.setVisibility(View.GONE);
        //  no_data.setVisibility(View.VISIBLE);
        lin_allContacts.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.img_wallpaperItem:
                int possion = (int) view.getTag();
                ChatDetailsModel chatDetailsModel = new ChatDetailsModel();
                chatDetailsModel.setChefId(listRecentChatModel.get(possion).getObjectId());
                DeviceTokenModel deviceTokenModel = DatabaseMgrApp.getDeviceTokenData(null, listRecentChatModel.get(possion).getObjectId());
                chatDetailsModel.setUserId(user_id);
                chatDetailsModel.setDefaultMessage("");
                chatDetailsModel.setIsArchive("0");
                chatDetailsModel.setChefName(deviceTokenModel.getUserName());
                Intent zoom = new Intent(getActivity(), MessageOpenActivity.class);
                zoom.putExtra("Model", chatDetailsModel);
                startActivity(zoom);
                break;

            default:
                break;
        }

    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        chatlist.add(dataSnapshot.getKey().toString());

        try {
            // HashMap<String,Chat> hashMap=dataSnapshot.getValue(HashMap.class);
            Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
            RecentChatModel recentChatModel = new RecentChatModel();

            recentChatModel.setObjectId(dataSnapshot.getKey());
            recentChatModel.setCounter((String) map.get("counter"));
            recentChatModel.setCreatedAt((String) map.get("createdAt"));
            recentChatModel.setDescription((String) map.get("description"));
            recentChatModel.setGroupId((String) map.get("groupId"));
            recentChatModel.setIsDeleted((String) map.get("isDeleted"));
            recentChatModel.setLastMessage((String) map.get("lastMessage"));
            recentChatModel.setPicture((String) map.get("picture"));
            recentChatModel.setType((String) map.get("type"));
            recentChatModel.setUpdatedAt((String) map.get("updatedAt"));
            recentChatModel.setSenderId((String) map.get("senderId"));
            recentChatModel.setReciverId((String) map.get("reciverId"));
            recentChatModel.setGroupName((String) map.get("groupName"));
            recentChatModel.setData((String) map.get("data"));
            recentChatModel.setIsArchive((String) map.get("isArchive"));

            // Utils.dbHelper.updateRemoteJid(recentChatModel.getObjectId(), recentChatModel.getGroupId());
            if (((String) map.get("isArchive")).equalsIgnoreCase("0"))
                listRecentChatModel.add(recentChatModel);

            notifyTOAdapter();

            Log.d(TAG, "Value is: " + map.toString());
        } catch (Exception e) {
            Log.d(TAG, "Value is: " + e.toString());
        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        if (listRecentChatModel.size() > 0) {
            boolean isAvailable=false;
            for (int i = 0; i < listRecentChatModel.size(); i++) {
                if (listRecentChatModel.get(i).getObjectId().contains(dataSnapshot.getKey().toString())) {
                    isAvailable=true;
                    try {
                        Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();

                        RecentChatModel recentChatModel = new RecentChatModel();
                        recentChatModel.setObjectId(dataSnapshot.getKey());
                        recentChatModel.setCounter((String) map.get("counter"));
                        recentChatModel.setCreatedAt((String) map.get("createdAt"));
                        recentChatModel.setDescription((String) map.get("description"));
                        recentChatModel.setGroupId((String) map.get("groupId"));
                        recentChatModel.setIsDeleted((String) map.get("isDeleted"));
                        recentChatModel.setLastMessage((String) map.get("lastMessage"));
                        recentChatModel.setPicture((String) map.get("picture"));
                        recentChatModel.setType((String) map.get("type"));
                        recentChatModel.setUpdatedAt((String) map.get("updatedAt"));
                        recentChatModel.setSenderId((String) map.get("senderId"));
                        recentChatModel.setReciverId((String) map.get("reciverId"));
                        recentChatModel.setGroupName((String) map.get("groupName"));
                        recentChatModel.setData((String) map.get("data"));
                        recentChatModel.setIsArchive((String) map.get("isArchive"));

                        if (recentChatModel.getType().equals(FirebaseConstant.CHAT_TYPE_PRIVTE)) {
                            //Utils.dbHelper.updateRemoteJid(recentChatModel.getReciverId(), recentChatModel.getGroupId());
                        }
                        if (((String) map.get("isArchive")).equalsIgnoreCase("1")) {
                            listRecentChatModel.remove(i);
                        }
                        else
                        {
                            listRecentChatModel.remove(i);
                            listRecentChatModel.add(0, recentChatModel);
                        }
                        notifyTOAdapter();

                        Log.d(TAG, "Value is: " + map.toString());
                    } catch (Exception e) {
                        Log.d(TAG, "Value is: " + e.toString());
                    }
                    break;

                }
            }
            if (!isAvailable)
            {
                try {
                    // HashMap<String,Chat> hashMap=dataSnapshot.getValue(HashMap.class);
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    RecentChatModel recentChatModel = new RecentChatModel();

                    recentChatModel.setObjectId(dataSnapshot.getKey());
                    recentChatModel.setCounter((String) map.get("counter"));
                    recentChatModel.setCreatedAt((String) map.get("createdAt"));
                    recentChatModel.setDescription((String) map.get("description"));
                    recentChatModel.setGroupId((String) map.get("groupId"));
                    recentChatModel.setIsDeleted((String) map.get("isDeleted"));
                    recentChatModel.setLastMessage((String) map.get("lastMessage"));
                    recentChatModel.setPicture((String) map.get("picture"));
                    recentChatModel.setType((String) map.get("type"));
                    recentChatModel.setUpdatedAt((String) map.get("updatedAt"));
                    recentChatModel.setSenderId((String) map.get("senderId"));
                    recentChatModel.setReciverId((String) map.get("reciverId"));
                    recentChatModel.setGroupName((String) map.get("groupName"));
                    recentChatModel.setData((String) map.get("data"));
                    recentChatModel.setIsArchive((String) map.get("isArchive"));

                    // Utils.dbHelper.updateRemoteJid(recentChatModel.getObjectId(), recentChatModel.getGroupId());
                    if (((String) map.get("isArchive")).equalsIgnoreCase("0"))
                        listRecentChatModel.add(recentChatModel);

                    notifyTOAdapter();

                    Log.d(TAG, "Value is: " + map.toString());
                } catch (Exception e) {
                    Log.d(TAG, "Value is: " + e.toString());
                }
            }
        } else {
            try {
                // HashMap<String,Chat> hashMap=dataSnapshot.getValue(HashMap.class);
                Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                RecentChatModel recentChatModel = new RecentChatModel();

                recentChatModel.setObjectId(dataSnapshot.getKey());
                recentChatModel.setCounter((String) map.get("counter"));
                recentChatModel.setCreatedAt((String) map.get("createdAt"));
                recentChatModel.setDescription((String) map.get("description"));
                recentChatModel.setGroupId((String) map.get("groupId"));
                recentChatModel.setIsDeleted((String) map.get("isDeleted"));
                recentChatModel.setLastMessage((String) map.get("lastMessage"));
                recentChatModel.setPicture((String) map.get("picture"));
                recentChatModel.setType((String) map.get("type"));
                recentChatModel.setUpdatedAt((String) map.get("updatedAt"));
                recentChatModel.setSenderId((String) map.get("senderId"));
                recentChatModel.setReciverId((String) map.get("reciverId"));
                recentChatModel.setGroupName((String) map.get("groupName"));
                recentChatModel.setData((String) map.get("data"));
                recentChatModel.setIsArchive((String) map.get("isArchive"));

                // Utils.dbHelper.updateRemoteJid(recentChatModel.getObjectId(), recentChatModel.getGroupId());
                if (((String) map.get("isArchive")).equalsIgnoreCase("0"))
                    listRecentChatModel.add(recentChatModel);

                notifyTOAdapter();

                Log.d(TAG, "Value is: " + map.toString());
            } catch (Exception e) {
                Log.d(TAG, "Value is: " + e.toString());
            }
        }

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        chatlist.add(dataSnapshot.getKey().toString());
        notifyTOAdapter();

        try {
            // HashMap<String,Chat> hashMap=dataSnapshot.getValue(HashMap.class);
            Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
            String value = dataSnapshot.getValue(String.class);

            Log.d(TAG, "Value is: " + map.toString());
        } catch (Exception e) {
            Log.d(TAG, "Value is: " + e.toString());
        }
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        notifyTOAdapter();

        try {
            // HashMap<String,Chat> hashMap=dataSnapshot.getValue(HashMap.class);
            Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
            String value = dataSnapshot.getValue(String.class);

            Log.d(TAG, "Value is: " + map.toString());
        } catch (Exception e) {
            Log.d(TAG, "Value is: " + e.toString());
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    public void notifyTOAdapter() {
        sort();
        recentActivityList = new ArrayList<>();
        if (listRecentChatModel.size() > 4) {
            for (int i = 0; i < 4; i++) {
                recentActivityList.add(listRecentChatModel.get(i));
            }
        } else {
            for (int i = 0; i < listRecentChatModel.size(); i++) {
                recentActivityList.add(listRecentChatModel.get(i));
            }
        }
        if (recentActivityList.size() > 0) {
            lin_recentChat.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);
        } else {
            no_data.setVisibility(View.VISIBLE);
            lin_recentChat.setVisibility(View.GONE);
        }
        if (listRecentChatModel.size() > 0) {
            lin_allContacts.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);
        } else {
            no_data.setVisibility(View.VISIBLE);
            lin_allContacts.setVisibility(View.GONE);
        }
        chatingContactAdapter.notifyDataSetChanged();

        recentActiveAdapter = new RecentActiveAdapter(getActivity(), this, recentActivityList);
        recyclerView_ChattingRecentList.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        recyclerView_ChattingRecentList.setItemAnimator(new DefaultItemAnimator());
        recyclerView_ChattingRecentList.setAdapter(recentActiveAdapter);
    }

    public void sort() {
        Collections.sort(listRecentChatModel, new Comparator<RecentChatModel>() {
            @Override
            public int compare(RecentChatModel item1, RecentChatModel item2) {
                return item2.getUpdatedAt().compareTo(item1.getUpdatedAt());
            }
        });
    }
}
