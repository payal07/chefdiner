package com.vervelogic.chefdiner.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.activity.AddAMenu;
import com.vervelogic.chefdiner.activity.BecomeAChefSelectLanguage;
import com.vervelogic.chefdiner.activity.BookingListCalendar;
import com.vervelogic.chefdiner.activity.CalendarActivity;
import com.vervelogic.chefdiner.activity.CalendarMealTime;
import com.vervelogic.chefdiner.activity.UploadPictures;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.util.List;


public class ChefProfileFragment extends Fragment implements View.OnClickListener {

    CardView become_a_chef_ly, edit_menu_ly, pictures_ly, guestList_ly, lunchBrunch_ly, availCalendar;
    DbHelper dbHelper;
    String is_verify = "", Chef_id;
    String language,isChefVerify;

    public ChefProfileFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chef_profile, container, false);

        /*-----------------------------Ids-----------------------*/
        become_a_chef_ly = view.findViewById(R.id.become_a_chef_ly);
        edit_menu_ly = view.findViewById(R.id.edit_menu_ly);
        pictures_ly = view.findViewById(R.id.pictures_ly);
        guestList_ly = view.findViewById(R.id.guestList_ly);
        lunchBrunch_ly = view.findViewById(R.id.lunchBrunch_ly);
        availCalendar = view.findViewById(R.id.availCalendar);

        /*--------------------------DB access-------------------------*/
        dbHelper = new DbHelper(getActivity().getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            is_verify = cn.getIsChef();
            Chef_id = cn.getUser_id();
            isChefVerify = cn.getIsChefVerify();
        }

        SharedPreference sharedPreference = new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(getActivity().getApplicationContext()))) {
            language = sharedPreference.getLanguage(getActivity().getApplicationContext());
        } else {
            Intent intent = getActivity().getIntent();
            language = intent.getStringExtra("language");
        }

        Log.e("ischef", "" + is_verify);
        if (!TextUtils.isEmpty(is_verify)) {
            if (is_verify.equals("1")) {
                edit_menu_ly.setEnabled(true);
                pictures_ly.setEnabled(true);
                edit_menu_ly.setVisibility(View.VISIBLE);
                pictures_ly.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(isChefVerify))
                {
                    if (isChefVerify.equalsIgnoreCase("yes"))
                    {
                        availCalendar.setVisibility(View.VISIBLE);
                        lunchBrunch_ly.setVisibility(View.VISIBLE);
                        guestList_ly.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        availCalendar.setVisibility(View.GONE);
                        lunchBrunch_ly.setVisibility(View.GONE);
                        guestList_ly.setVisibility(View.GONE);
                    }
                }

            } else {
               /* edit_menu_ly.setEnabled(false);
                pictures_ly.setEnabled(false);
                availCalendar.setEnabled(false);
                lunchBrunch_ly.setEnabled(false);
                guestList_ly.setEnabled(false);*/
                become_a_chef_ly.setVisibility(View.GONE);
                edit_menu_ly.setVisibility(View.GONE);
                pictures_ly.setVisibility(View.GONE);
                availCalendar.setVisibility(View.GONE);
                lunchBrunch_ly.setVisibility(View.GONE);
                guestList_ly.setVisibility(View.GONE);
                //   Toast.makeText(getActivity().getApplicationContext(), "Chef is not verified", Toast.LENGTH_SHORT).show();
            }
        } else {
            become_a_chef_ly.setVisibility(View.GONE);
            edit_menu_ly.setVisibility(View.GONE);
            pictures_ly.setVisibility(View.GONE);
            availCalendar.setVisibility(View.GONE);
            lunchBrunch_ly.setVisibility(View.GONE);
            guestList_ly.setVisibility(View.GONE);
            // Toast.makeText(getActivity().getApplicationContext(), "Chef is not verified", Toast.LENGTH_SHORT).show();
        }
        /*-----------------------Listeners--------------*/
        become_a_chef_ly.setOnClickListener(this);
        edit_menu_ly.setOnClickListener(this);
        pictures_ly.setOnClickListener(this);
        availCalendar.setOnClickListener(this);
        lunchBrunch_ly.setOnClickListener(this);
        guestList_ly.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.become_a_chef_ly:
                Intent intent = new Intent(getContext(), BecomeAChefSelectLanguage.class);
                intent.putExtra("language", language);
                startActivity(intent);
                break;
            case R.id.edit_menu_ly:
                Intent editIntent = new Intent(getContext(), AddAMenu.class);
                editIntent.putExtra("language", language);
                startActivity(editIntent);
                break;
            case R.id.pictures_ly:
                Intent pictureIntent = new Intent(getContext(), UploadPictures.class);
                pictureIntent.putExtra("language", language);
                startActivity(pictureIntent);
                break;
            case R.id.guestList_ly:
                Intent guestList = new Intent(getContext(), BookingListCalendar.class);
                guestList.putExtra("Chef_id", Chef_id);
                startActivity(guestList);
                break;
            case R.id.lunchBrunch_ly:
                Intent lunchBrunch = new Intent(getContext(), CalendarMealTime.class);
                lunchBrunch.putExtra("Chef_id", Chef_id);
                startActivity(lunchBrunch);
                break;
            case R.id.availCalendar:
                Intent cal = new Intent(getContext(), CalendarActivity.class);
                cal.putExtra("Chef_id", Chef_id);
                startActivity(cal);
                break;
        }
    }
}
