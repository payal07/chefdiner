package com.vervelogic.chefdiner.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.activity.HowItWorks;
import com.vervelogic.chefdiner.activity.InviteAndEarn;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import java.util.List;

public class About extends Fragment implements View.OnClickListener {

    LinearLayout how_it_works_ly, tell_a_friend_ly, free_dinners_ly;
    String language;
    DbHelper dbHelper;
    String referralCode="";
    public About()
    {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        SharedPreference sharedPreference=new SharedPreference();
        if (!TextUtils.isEmpty(sharedPreference.getLanguage(getActivity().getApplicationContext())))
        {
            language=sharedPreference.getLanguage(getActivity().getApplicationContext());
        }
        else
        {
            Intent intent = getActivity().getIntent();

            language = intent.getStringExtra("language");
        }

        /*----------------------IDs--------------------------------------*/

        how_it_works_ly = view.findViewById(R.id.how_it_works_ly);
        tell_a_friend_ly = view.findViewById(R.id.tell_a_friend_ly);
        free_dinners_ly = view.findViewById(R.id.free_dinners_ly);

        /*---------------------Listeners----------------------------------*/
        how_it_works_ly.setOnClickListener(this);
        tell_a_friend_ly.setOnClickListener(this);
        free_dinners_ly.setOnClickListener(this);

        dbHelper = new DbHelper(getActivity().getApplicationContext());
        List<UserDetail> contacts = dbHelper.getAllDetails();
        for (UserDetail cn : contacts) {
            if (!TextUtils.isEmpty(cn.getReferralCode())) {
                referralCode = cn.getReferralCode();
                Log.e("referal_code", "" + referralCode);
            } else {
                referralCode = "";
            }
        }


        return view;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.how_it_works_ly:
                Intent intent = new Intent(getActivity().getApplicationContext(), HowItWorks.class);
                intent.putExtra("language", language);
                startActivity(intent);
                break;
            case R.id.tell_a_friend_ly:
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                    String sAux = "\nLet me recommend you this application\n\n";
                    sAux = sAux + AppConfig.REFERRAL_CODE_URL + referralCode + "\n\n";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.free_dinners_ly:
                Intent intent1 = new Intent(getActivity(), InviteAndEarn.class);
                startActivity(intent1);
                break;
        }

    }
}
