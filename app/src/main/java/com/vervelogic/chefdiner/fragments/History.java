package com.vervelogic.chefdiner.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.vervelogic.chefdiner.R;
import com.vervelogic.chefdiner.adapter.HistoryAdapter;
import com.vervelogic.chefdiner.app.AppConfig;
import com.vervelogic.chefdiner.app.AppController;
import com.vervelogic.chefdiner.dbHelper.DbHelper;
import com.vervelogic.chefdiner.model.HistoryMeals;
import com.vervelogic.chefdiner.model.UserDetail;
import com.vervelogic.chefdiner.utils.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class History extends Fragment {

    private static final String TAG = "History";
    /*--------------------------Declarations----------------------------------------*/
    RecyclerView history_recyclerView;
    View view;
    String user_id;
    Button invite_earn_history;
    HistoryAdapter historyAdapter;
    DbHelper dbHelper;
    String referralCode="";

    public History()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_history, container, false);
            SharedPreference sharedPreference = new SharedPreference();
            if (!TextUtils.isEmpty(sharedPreference.getUserId(getActivity()))) {
                //language = sharedPreference.getLanguage(context);
                user_id = sharedPreference.getUserId(getActivity());
            } else {
                user_id = "";
            }
            //  user_id = sharedPreference.getUserId(getActivity());

            history_recyclerView = view.findViewById(R.id.history_recycler_view);
            invite_earn_history = view.findViewById(R.id.invite_earn_history);

            dbHelper = new DbHelper(getActivity().getApplicationContext());
            List<UserDetail> contacts = dbHelper.getAllDetails();
            for (UserDetail cn : contacts) {
                if (!TextUtils.isEmpty(cn.getReferralCode())) {
                    referralCode = cn.getReferralCode();
                    Log.e("referal_code", "" + referralCode);
                } else {
                    referralCode = "";
                }
            }
            ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            if (netInfo == null) {
                new android.support.v7.app.AlertDialog.Builder(getActivity())
                        .setTitle(getResources().getString(R.string.app_name))
                        .setMessage(getResources().getString(R.string.internet_error))
                        .setPositiveButton(getResources().getString(R.string.ok), null).show();
            } else {
                historyAPI();
            }

            invite_earn_history.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                        String sAux = "\nLet me recommend you this application\n\n";
                        sAux = sAux + AppConfig.REFERRAL_CODE_URL + referralCode + "\n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        }
        return view;
    }

    public void historyAPI() {
       /* final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Collecting Data...");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.HISTORY_MEAL_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                       /* progressDialog.dismiss();*/
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            HistoryMeals historyMeals = new Gson().fromJson(s, HistoryMeals.class);
                            if (status == 1) {
                                Log.e(TAG, "" + s);
                                history_recyclerView.setVisibility(View.VISIBLE);
                                LinearLayoutManager horizontalLayoutManager
                                        = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                history_recyclerView.setLayoutManager(horizontalLayoutManager);
                                history_recyclerView.setNestedScrollingEnabled(false);
                                historyAdapter = new HistoryAdapter(getActivity(), historyMeals.getGethistory());
                                history_recyclerView.setAdapter(historyAdapter);
                                Log.e("Size", "" + historyMeals.getGethistory().size());
                                Log.e("String", "" + historyMeals.getGethistory().toString());

                            }
                            if (status == 0) {
                              /*  progressDialog.dismiss();*/
                                String message = historyMeals.getMessage();
                                Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                                history_recyclerView.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                           /* progressDialog.dismiss();*/
                            if(getActivity() != null) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                          /*  progressDialog.dismiss();*/
                            if(getActivity() != null) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Please check your internet connection.";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                Log.e(TAG, "onErrorResponse: " + message);
                Log.e("Error: ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("user_id", user_id);
                Log.e("Map", "" + map);
                return map;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
